<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>{!! config('app.name') !!} - Dashboard</title>

        @include('includes.external-resources.general-css')
        @include('includes.external-resources.tags')

    </head>

    <body id="page-top">
        <div id="wrapper">

            <!-- Sidebar Navigation -->
            @include('includes.admin.sidebar')
            <!-- End of Sidebar Navigation -->

            <!-- Content Wrapper -->
            <div id="content-wrapper" 
                class="d-flex flex-column">

                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar Navigation -->
                    @include('includes.admin.top-nav')
                    <!-- End of Topbar Navigation -->

                    <!-- Page Content -->
                    <div class="container-fluid">
                        @yield('content')
                    </div>
                    <!-- /.container-fluid -->
                    <!-- End of Page Content -->

                </div>
                <!-- End of Main Content -->

                <!-- Footer -->
                @include('includes.footer')
                <!-- End of Footer -->

            </div>
            <!-- End of Content Wrapper -->

        </div>

        @include('includes.external-resources.general-js')
        @yield('scripts')

    </body>
</html>
