<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>{!! config('app.name') !!}</title>

        @include('includes.external-resources.general-css')
        @include('includes.external-resources.posts')
        @include('includes.external-resources.tags')

    </head>

    <body id="page-top" 
        style="display: flex; flex-direction: column; min-height: 100vh;">
        <div id="wrapper">

            <!-- Content Wrapper -->
            <div id="content-wrapper" 
                class="d-flex flex-column">
 
                <!-- Main Content -->
                <div id="content">

                    <!-- Topbar Navigation -->
                    @include('includes.home.top-nav')
                    <!-- End of Topbar Navigation -->

                </div>
                <!-- End of Main Content -->

            </div>
            <!-- End of Content Wrapper -->

        </div>

        <!-- Page Content -->
        <div class="container" 
            style="flex: 1 0 auto; margin-top:40px;">
            <div class="row">

                <!-- Main Content Column -->
                <div class="col-md-8">
                    @yield('content')
                </div>
                <!-- End of Main Content Column -->

                <!-- Sidebar Widgets Column -->
                <div class="col-md-4">

                    <!-- New Post Widget -->
                    @include('includes.home.posts.create_widget')

                    <!-- Search Widget -->
                    @include('includes.home.search_widget')

                    <!-- Categories Widget -->
                    @yield('categories')

                    <!-- Side Widget -->
                    @yield('relatedposts')

                </div>
                <!-- End of Sidebar Widgets Column -->

            </div>
        </div>
        <!-- End of Page Content -->

        <!-- Footer -->
        @include('includes.footer')
        <!-- End of Footer -->

        @include('includes.external-resources.general-js')
        @yield('scripts')

    </body>
</html>