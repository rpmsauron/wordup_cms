@if(session('category-updated-message'))
    <div class="alert alert-success">
        {{ session('category-updated-message') }}
    </div>
@elseif(session('category-deleted-message'))
    <div class="alert alert-danger">
        {{ session('category-deleted-message') }}
    </div>
@elseif(session('category-created-message'))
    <div class="alert alert-success">
        {{ session('category-created-message') }}
    </div>
@elseif(session('category-toggled-message'))
    <div class="alert alert-success">
        {{ session('category-toggled-message') }}
    </div>
@elseif(session('category-merged-message'))
    <div class="alert alert-success">
        {{ session('category-merged-message') }}
    </div>

@elseif(session('permission-updated-message'))
    <div class="alert alert-success">
        {{ session('permission-updated-message') }}
    </div>
@elseif(session('permission-deleted-message'))
    <div class="alert alert-danger">
        {{ session('permission-deleted-message') }}
    </div>
@elseif(session('permission-created-message'))
    <div class="alert alert-success">
        {{ session('permission-created-message') }}
    </div>
@elseif(session('permission-enabled-message'))
    <div class="alert alert-success">
        {{ session('permission-enabled-message') }}
    </div>
@elseif(session('permission-disabled-message'))
    <div class="alert alert-danger">
        {{ session('permission-disabled-message') }}
    </div>

@elseif(session('role-updated-message'))
    <div class="alert alert-success">
        {{ session('role-updated-message') }}
    </div>
@elseif(session('role-deleted-message'))
    <div class="alert alert-danger">
        {{ session('role-deleted-message') }}
    </div>
@elseif(session('role-created-message'))
    <div class="alert alert-success">
        {{ session('role-created-message') }}
    </div>
@elseif(session('role-attached-message'))
    <div class="alert alert-success">
        {{ session('role-attached-message') }}
    </div>
@elseif(session('role-detached-message'))
    <div class="alert alert-danger">
        {{ session('role-detached-message') }}
    </div>

@elseif(session('post-updated-message'))
    <div class="alert alert-success">
        {{ session('post-updated-message') }}
    </div>
@elseif(session('post-deleted-message'))
    <div class="alert alert-danger">
        {{ session('post-deleted-message') }}
    </div>
@elseif(session('post-created-message'))
    <div class="alert alert-success">
        {{ session('post-created-message') }}
    </div>
@elseif(session('post-restored-message'))
    <div class="alert alert-success">
        {{ session('post-restored-message') }}
    </div>
@elseif(session('post-tags-limit-message'))
    <div class="alert alert-danger">
        {{ session('post-tags-limit-message') }}
    </div>
@elseif(session('post-bookmarked-message'))
    <div class="alert alert-success">
        {{ session('post-bookmarked-message') }}
    </div>
@elseif(session('post-unbookmarked-message'))
    <div class="alert alert-danger">
        {{ session('post-unbookmarked-message') }}
    </div>
@elseif(session('post-assigned-message'))
    <div class="alert alert-success">
        {{ session('post-assigned-message') }}
    </div>
@elseif(session('post-accepted-message'))
    <div class="alert alert-success">
        {{ session('post-accepted-message') }}
    </div>
@elseif(session('post-rejected-message'))
    <div class="alert alert-danger">
        {{ session('post-rejected-message') }}
    </div>

@elseif(session('user-updated-message'))
    <div class="alert alert-success">
        {{ session('user-updated-message') }}
    </div>
@elseif(session('user-deleted-message'))
    <div class="alert alert-danger">
        {{ session('user-deleted-message') }}
    </div>
@elseif(session('user-attached-message'))
    <div class="alert alert-success">
        {{ session('user-attached-message') }}
    </div>
@elseif(session('user-detached-message'))
    <div class="alert alert-danger">
        {{ session('user-detached-message') }}
    </div>
@elseif(session('user-restored-message'))
    <div class="alert alert-success">
        {{ session('user-restored-message') }}
    </div>
@elseif(request()->input('user-deleted-message'))
    <div class="alert alert-danger">
        {!! ('user-deleted-message') !!}
    </div>

@elseif(session('comment-updated-message'))
    <div class="alert alert-success">
        {{ session('comment-updated-message') }}
    </div>
@elseif(session('comment-deleted-message'))
    <div class="alert alert-danger">
        {{ session('comment-deleted-message') }}
    </div>
@elseif(session('comment-created-message'))
    <div class="alert alert-success">
        {{ session('comment-created-message') }}
    </div>
@elseif(session('comment-reported-message'))
    <div class="alert alert-warning">
        {{ session('comment-reported-message') }}
    </div>
@elseif(session('comment-accepted-message'))
    <div class="alert alert-success">
        {{ session('comment-accepted-message') }}
    </div>
@elseif(session('comment-rejected-message'))
    <div class="alert alert-danger">
        {{ session('comment-rejected-message') }}
    </div>

@elseif(session('cleanup-cleaned-message'))
    <div class="alert alert-success">
        {{ session('cleanup-cleaned-message') }}
    </div>

@elseif(session('suggestion-created-message'))
    <div class="alert alert-success">
        {{ session('suggestion-created-message') }}
    </div>
@elseif(session('suggestion-rejected-message'))
    <div class="alert alert-danger">
        {{ session('suggestion-rejected-message') }}
    </div>
@elseif(session('suggestion-accepted-message'))
    <div class="alert alert-success">
        {{ session('suggestion-accepted-message') }}
    </div>
@elseif(session('suggestion-count-exceeded-message'))
    <div class="alert alert-danger">
        {{ session('suggestion-count-exceeded-message') }}
    </div>
@elseif(session('suggestion-remove-message'))
    <div class="alert alert-danger">
        {{ session('suggestion-remove-message') }}
    </div>

@elseif(session('message-sent-message'))
    <div class="alert alert-success">
        {{ session('message-sent-message') }}
    </div>
@elseif(session('message-archived-message'))
    <div class="alert alert-success">
        {{ session('message-archived-message') }}
    </div>

@elseif(session('contact-sent-message'))
    <div class="alert alert-success">
        {{ session('contact-sent-message') }}
    </div>
@elseif(session('contact-replied-message'))
    <div class="alert alert-success">
        {{ session('contact-replied-message') }}
    </div>

@endif