<div class="modal fade" 
    id="userDetachRoleModal_{{ $user->id }}_{{$role->id}}" 
    tabindex="-1" 
    role="dialog" 
    aria-labelledby="exampleModalLabel" 
    aria-hidden="true">
    <div class="modal-dialog" 
        role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" 
                    id="exampleModalLabel">Detach Role from User?</h5>
                <button class="close" 
                    type="button" 
                    data-dismiss="modal" 
                    aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-footer">
                <button style="position: absolute; left: 0px; margin-left: 20px;" 
                    class="btn btn-secondary" 
                    type="button" 
                    data-dismiss="modal" 
                    title="Cancel">
                    <i class="fa fa-chevron-circle-left fa-lg"></i>
                </button>
                <form method="post" 
                    action="{{ route('user.role.detach', $user) }}">
                    @csrf
                    @method('PUT')
                    <input type="hidden" 
                        name="role" 
                        value="{{ $role->id }}">
                    <button type="submit" 
                        class="btn btn-danger" 
                        title="Detach">
                        <i class="fa fa-check fa-lg"></i>
                    </button>
                    @if($role->slug == Config::get('constants.roles.role.admin.slug'))
                        @include('includes.admin.users.user_roles_edit_dangersign')
                    @endif
                </form>
            </div>
        </div>
    </div>
</div>