<div class="modal fade" 
    id="postDeleteModal_{{ $id }}" 
    tabindex="-1" 
    role="dialog" 
    aria-labelledby="exampleModalLabel" 
    aria-hidden="true">
    <div class="modal-dialog" 
        role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" 
                    id="exampleModalLabel">Delete Post?</h5>
                <button class="close" 
                    type="button" 
                    data-dismiss="modal" 
                    aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-footer">
                <button style="position: absolute; left: 0px; margin-left: 20px;" 
                    class="btn btn-secondary" 
                    type="button" 
                    data-dismiss="modal" 
                    title="Cancel">
                    <i class="fa fa-chevron-circle-left fa-lg"></i>
                </button>
                <form method="post" 
                    action="{{ route('post.destroy', $post->id) }}">
                    @csrf
                    @method('DELETE')
                    <button type="submit" 
                        class="btn btn-danger" 
                        title="Delete">
                        <i class="fa fa-trash fa-lg"></i>
                    </button>
                </form>
            </div>
        </div>
    </div>
</div>