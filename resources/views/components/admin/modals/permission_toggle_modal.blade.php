<div class="modal fade" 
    id="permissionToggleModal_{{ $id }}" 
    tabindex="-1" 
    role="dialog" 
    aria-labelledby="exampleModalLabel" 
    aria-hidden="true">
    <div class="modal-dialog" 
        role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" 
                    id="exampleModalLabel">
                    @if($permission->is_active == 1)
                        Disable Permission?
                    @else
                        Enable Permission?
                    @endif
                </h5>
                <button class="close" 
                    type="button" 
                    data-dismiss="modal" 
                    aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-footer">
                <button style="position: absolute; left: 0px; margin-left: 20px;" 
                    class="btn btn-secondary" 
                    type="button" 
                    data-dismiss="modal" 
                    title="Cancel">
                    <i class="fa fa-chevron-circle-left fa-lg"></i>
                </button>
                <form action="{{ route('permissions.toggle', $permission) }}" 
                    method="post">
                    @csrf
                    @method("PATCH")
                    @if($permission->is_active == 1)
                        <button type="submit" 
                            class="btn btn-danger" 
                            title="Disable">
                            <i class="fa fa-check fa-lg"></i>
                        </button>
                    @else
                        <button type="submit" 
                            class="btn btn-primary" 
                            title="Enable">
                            <i class="fa fa-check fa-lg"></i>
                        </button>
                    @endif
                </form>
            </div>
        </div>
    </div>
</div>