<div class="modal fade" 
    id="{{ $modalId }}" 
    name="{{ $scopeIdBase }}"
    tabindex="-1" 
    role="dialog" 
    aria-labelledby="exampleModalLabel" 
    aria-hidden="true">
    <div class="modal-dialog scrollable-modal" 
        role="document">
        <div class="modal-content" 
            id="icons-modal-content-{{ $scopeIdBase }}">
            <div class="modal-header">
                <h5 class="modal-title" 
                    id="exampleModalLabel-{{ $scopeIdBase }}">Delete orphaned {{ $headerContent }}?</h5>
                <button class="close" 
                    type="button" 
                    data-dismiss="modal" 
                    aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>
            <div class="modal-footer">
                <button style="position: absolute; left: 0px; margin-left: 20px;" 
                    class="btn btn-secondary" 
                    type="button" 
                    data-dismiss="modal" 
                    title="Cancel">
                    <i class="fa fa-chevron-circle-left fa-lg"></i>
                </button>
                <form action="{{ $routeAll }}" 
                    method="post">
                    @csrf
                    @method("DELETE")
                    <button type="submit" 
                        class="btn btn-danger" 
                        title="Delete all">
                        <i class="fa fa-trash fa-lg"></i>
                    </button>
                </form>
            </div>
            <div class="modal-header">
                <h6>Orphan files found:</h5>
            </div>
            <div class="modal-body">
                <form action="{{ $route }}" 
                    method="post">
                    @csrf
                    @method("DELETE")
                    @foreach($orphans as $orphan)
                        <div id="icon-wrapper-{{ $scopeIdBase . $loop->index }}" 
                            name="{{ $scopeIdBase . $loop->index }}">
                            <div class="form-group">
                                @if($loop->index > 0 && $loop->index % 5 == 0)
                                    <hr>
                                @endif
                                <img src="{{ asset($orphan) }}" 
                                    name="{{ $scopeIdBase . $loop->index }}" 
                                    alt="" 
                                    width="45px" 
                                    class="mr-2" 
                                    id="icon-preview-id-{{ $scopeIdBase . $loop->index }}">
                                <label for="{{ $inputName }}"><small>{{ $orphan }}</small></label>
                                <input type="checkbox" 
                                    name="{{ $inputName }}" 
                                    class="float-right" 
                                    value="{{ $orphan }}">
                            </div>
                            <div id="icon-preview-{{ $scopeIdBase . $loop->index }}" 
                                style="display:none;" 
                                class="mb-4">
                                <img src="" 
                                    alt="" 
                                    class="img-fluid">
                            </div>
                        </div>
                    @endforeach
                    <div class="form-group">
                        <button type="submit" 
                            class="btn btn-danger float-right" 
                            title="Delete selected">
                            <i class="fa fa-trash fa-lg"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
