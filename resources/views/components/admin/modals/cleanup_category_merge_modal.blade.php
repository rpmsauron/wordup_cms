<div class="modal fade" 
    id="cleanupCategoryMergeModal" 
    tabindex="-1" 
    role="dialog" 
    aria-labelledby="exampleModalLabel" 
    aria-hidden="true">
    <div class="modal-dialog" 
        role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" 
                    id="category-merge-modal-title"></h5>
                <button class="close" 
                    type="button" 
                    data-dismiss="modal" 
                    aria-label="Close">
                    <span aria-hidden="true">×</span>
                </button>
            </div>

            <div class="modal-body">
                <form action="{{ route('categories.merge') }}" 
                    method="post" 
                    id="merge-form">
                    @csrf

                    <div class="form-group">
                        <input type="hidden" 
                           name="categories[]" 
                           value="">
                        <input type="hidden" 
                           name="label" 
                           value="">
                    </div>

                    <div class="form-group mt-4" 
                        id="icon-selection-preview" 
                        style="display:none;">
                        <h6 class="modal-title mb-2">Choose an icon for the merged Category</h6>
                        <ul class="list-unstyled list-inline" 
                            id="icon-selection-preview-list"></ul>
                    </div>
                    
                    <div class="form-group">
                        <button type="submit" 
                            id="merge-submit"
                            class="btn btn-primary" 
                            title="Merge Categories">
                            <i class="fa fa-object-group fa-lg"></i>
                        </button>
                    </div>
                 </form>
            </div>

            <div class="modal-body">
                <hr>
                <div class="form-group">
                    <button 
                        class="btn btn-secondary" 
                        type="button" 
                        data-dismiss="modal" 
                        title="Cancel">
                        <i class="fa fa-chevron-circle-left fa-lg"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
</div>
