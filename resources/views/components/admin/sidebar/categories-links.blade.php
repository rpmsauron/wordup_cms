<li class="nav-item">
    <a class="nav-link collapsed" 
        href="#" 
        data-toggle="collapse" 
        data-target="#collapseCategories" 
        aria-expanded="true" 
        aria-controls="collapseCategories">
        <i class="fas fa-fw fa-layer-group"></i>
        <span>Categories</span>
    </a>
    <div id="collapseCategories" 
        class="collapse" 
        aria-labelledby="headingCategories" 
        data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Categories:</h6>
            <a class="collapse-item" href="{{ route('categories.index') }}">Categories</a>
        </div>
    </div>
</li>
