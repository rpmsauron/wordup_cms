<li class="nav-item">
    <a class="nav-link collapsed" 
        href="#" 
        data-toggle="collapse" 
        data-target="#collapseContacts" 
        aria-expanded="true" 
        aria-controls="collapseContacts">
        <i class="fas fa-fw fa-envelope"></i>
        <span>Contact Messages</span>
    </a>
    <div id="collapseContacts" 
        class="collapse" 
        aria-labelledby="headingContacts" 
        data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Contact Messages:</h6>
            <a class="collapse-item" href="{{ route('contact.index.pending') }}">Pending</a>
            <a class="collapse-item" href="{{ route('contact.index.archived') }}">Archived</a>
        </div>
    </div>
</li>
