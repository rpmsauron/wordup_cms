<li class="nav-item">
    <a class="nav-link collapsed" 
        href="#" 
        data-toggle="collapse" 
        data-target="#collapseAuthorization" 
        aria-expanded="true" 
        aria-controls="collapseAuthorization">
        <i class="fas fa-fw fa-user-tag"></i>
        <span>Authorizations</span>
    </a>
    <div id="collapseAuthorization" 
        class="collapse" 
        aria-labelledby="headingAuthorization" 
        data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Authorizations:</h6>
            @if(iAmAdmin() || iHavePermissionFor(App\Role::class))
                <a class="collapse-item" 
                    href="{{ route('roles.index') }}">Roles</a>
            @endif
            @if(iAmAdmin() || iHavePermissionFor(App\Permission::class))
                <a class="collapse-item" 
                    href="{{ route('permissions.index') }}">Permissions</a>
            @endif
        </div>
    </div>
</li>
