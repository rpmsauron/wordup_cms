<li class="nav-item">
    <a class="nav-link collapsed" 
        href="#" 
        data-toggle="collapse" 
        data-target="#collapseSuggestions" 
        aria-expanded="true" 
        aria-controls="collapseSuggestions">
        <i class="fas fa-fw fa-hand-point-up"></i>
        <span>Suggestions</span>
    </a>
    <div id="collapseSuggestions" 
        class="collapse" 
        aria-labelledby="headingSuggestions" 
        data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Suggested Categories:</h6>
            <a class="collapse-item" href="{{ route('suggestions.given') }}">Given</a>
            <a class="collapse-item" href="{{ route('suggestions.received') }}">Received</a>
        </div>
    </div>
</li>
