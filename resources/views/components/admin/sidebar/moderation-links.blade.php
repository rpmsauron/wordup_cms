<li class="nav-item">
    <a class="nav-link collapsed" 
        href="#" 
        data-toggle="collapse" 
        data-target="#collapseModeration" 
        aria-expanded="true" 
        aria-controls="collapseModeration">
        <i class="fas fa-fw fa-user-tag"></i>
        <span>Moderation Reviews</span>
    </a>
    <div id="collapseModeration" 
        class="collapse" 
        aria-labelledby="headingModeration" 
        data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Moderation reviews:</h6>
            @if(iAmAdmin() || iAmModerator() || iHavePermissionFor(App\Post::class, "review"))
                <a class="collapse-item" 
                    href="{{ route('posts.listreview') }}">Posts to review</a>
            @endif
            @if(iAmAdmin() || iAmModerator() || iHavePermissionFor(App\Comment::class, "review"))
                <a class="collapse-item" 
                    href="{{ route('comments.listreview') }}">Comment Reports</a>
            @endif
        </div>
    </div>
</li>
