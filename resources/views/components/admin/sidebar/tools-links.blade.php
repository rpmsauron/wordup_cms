<li class="nav-item">
    <a class="nav-link collapsed" 
        href="#" 
        data-toggle="collapse" 
        data-target="#collapseTools" 
        aria-expanded="true" 
        aria-controls="collapseTools">
        <i class="fas fa-fw fa-tools"></i>
        <span>Tools</span>
    </a>
    <div id="collapseTools" 
        class="collapse" 
        aria-labelledby="headingTools" 
        data-parent="#accordionSidebar">
        <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Tools:</h6>
            <a class="collapse-item" href="{{ route('admin.cleanup') }}">Cleanup</a>
        </div>
    </div>
</li>
