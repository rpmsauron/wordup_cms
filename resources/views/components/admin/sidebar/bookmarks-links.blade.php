<li class="nav-item">
    <a class="nav-link" 
        href="{{ route('admin.bookmarks.index') }}">
        <i class="fas fa-fw fa-chart-area"></i>
        <span>Bookmarks</span>
    </a>
</li>