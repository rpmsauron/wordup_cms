<li class="nav-item dropdown no-arrow mx-1">
    <a class="nav-link dropdown-toggle" 
        href="#" 
        id="notificationsDropdown" 
        role="button" 
        data-toggle="dropdown" 
        aria-haspopup="true" 
        aria-expanded="false">
        <i class="fas fa-bell fa-fw"></i>
        <!-- Counter - Notifications -->
        <span class="badge badge-danger badge-counter">{{ me()->getCountNewNotifications() }}</span>
    </a>

    <!-- Dropdown - Notifications -->
    <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" 
        aria-labelledby="notificationsDropdown"
        id="notifications-modal">
        <div id="notifications-modal-header">
            <h6 class="dropdown-header">
                Notifications
            </h6>
        </div>

        @foreach(me()->getNotificationsShownDefault() as $notification)
            @if($notification->new == 1 && $notification->action == "delete")
                <a class="dropdown-item d-flex align-items-center bg-warning" 
                    href="#">
            @elseif($notification->new == 1)
                <a class="dropdown-item d-flex align-items-center bg-success" 
                    href="#">
            @else
                <a class="dropdown-item d-flex align-items-center" 
                    href="#">
            @endif
                <div class="mr-3">
                    @if(!$notification->hideNotifier())
                        <div class="icon-circle bg-white">
                            <img class="rounded-circle" 
                                src="{{ $notification->notifier->avatar }}" 
                                width="{{ config('constants.image.user.avatar_notification_size') }}px" 
                                alt="">
                    @else
                        <div class="icon-circle">
                    @endif
                    </div>
                </div>

                <div class="mr-0" 
                    style="width: 100%;">
                    @if($notification->new == 1)
                        <div class="small text-white">{{ $notification->created_at->diffForHumans() . " (" . $notification->created_at . ")" }}</div>
                    @else
                        <div class="small text-gray-500">{{ $notification->created_at->diffForHumans() . " (" . $notification->created_at . ")" }}</div>
                    @endif

                    <div class="font-weight-bold container-fluid pr-0 pl-0">
                        <table style="width: 100%;">
                            <tr class="clickable-row"
                                data-href="{{ $notification->getLinkRoute() }}">
                                <td style="width: 80%;">
                                    {{ $notification->toText() }}
                                </td>
                                <td style="width: 20%;">
                                    @if($notification->new == 1)
                                        <form action="{{ route('notifications.clear', $notification) }}" 
                                            method="post">
                                            @csrf
                                            @method("PATCH")
                                            <button class="btn btn-white" 
                                                type="submit" 
                                                title="Clear" 
                                                name="clear">
                                                <i class="fas fa-circle fa-xs text-white"></i>
                                            </button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </a>
        @endforeach

        <form action="{{ route('notifications.clearall') }}"
            method="post">
            @csrf
            @method("PATCH")
            <button class="dropdown-item text-center small text-gray-500" 
                type="submit" 
                title="Clear All" 
                name="clearall">
                Clear all notifications
            </button>
        </form>

        <a class="dropdown-item text-center small text-gray-500" 
            href="{{ route('notifications.index') }}">Show all notifications</a>
    </div>
</li>