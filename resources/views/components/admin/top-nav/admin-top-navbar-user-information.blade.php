<li class="nav-item dropdown no-arrow">
    <a class="nav-link dropdown-toggle" 
        href="#" 
        id="userDropdown" 
        role="button" 
        data-toggle="dropdown" 
        aria-haspopup="true" 
        aria-expanded="false">
        <span class="mr-2 d-none d-lg-inline text-gray-600 small">
            @auth
                {{ me()->name }}
            @endauth
        </span>
        @auth
            <img class="img-profile rounded-circle" 
                 src="{{ me()->avatar }}"
                @if(me()->hasAvatar() && getimagesize(me()->avatar)[0] > getimagesize(me()->avatar)[1])
                    width="{{ Config::get('constants.image.user.avatar_size') }}px"/>
                @else
                    height="{{ Config::get('constants.image.user.avatar_size') }}px"/>
                @endif

                @if(iAmAdmin())
                    <div style="position:relative; bottom:10px; right:10px; ">
                        <i class="fa fa-shield-alt fa-medium text-warning"></i>
                    </div>
                @endif
        @endauth
    </a>
    <!-- Dropdown - User Information -->
    <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" 
        aria-labelledby="userDropdown">
        <a class="dropdown-item" 
            href="{{ route('user.profile.show', me()->id) }}">
            <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>Profile
        </a>
        <div class="dropdown-divider"></div>
        <a class="dropdown-item" 
            href="#" 
            data-toggle="modal" 
            data-target="#logoutModal">
            <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>Logout
        </a>
    </div>
</li>

<!-- Logout Modal-->
@include('components.admin.modals.logout_modal')