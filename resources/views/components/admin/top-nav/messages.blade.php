<li class="nav-item dropdown no-arrow mx-1">
    <a class="nav-link dropdown-toggle" 
        href="#" 
        id="messagesDropdown" 
        role="button" 
        data-toggle="dropdown" 
        aria-haspopup="true" 
        aria-expanded="false">
        <i class="fas fa-envelope fa-fw"></i>
        <!-- Counter - Messages -->
        <span class="badge badge-danger badge-counter">{{ me()->getCountNewMessages() }}</span>
    </a>

    <!-- Dropdown - Messages -->
    <div class="dropdown-list dropdown-menu dropdown-menu-right shadow animated--grow-in" 
        aria-labelledby="messagesDropdown" 
        id="messages-modal">
        <div id="messages-modal-header">
            <h6 class="dropdown-header">
                Messages
            </h6>
        </div>

        @foreach(me()->getMessagesShownDefault() as $message)
            @if($message->new == 1)
                <a class="dropdown-item d-flex align-items-center bg-success" 
                    href="#">
            @else
                <a class="dropdown-item d-flex align-items-center" 
                    href="#">
            @endif
                <div class="mr-3">
                    <div class="icon-circle bg-white">
                            <img class="rounded-circle" 
                                src="{{ $message->from->avatar }}" 
                                width="{{ config('constants.image.user.avatar_notification_size') }}px" 
                                alt="">
                    </div>
                </div>
                
                <div class="mr-0" 
                    style="width: 100%;">
                    @if($message->new == 1)
                        <div class="small text-white">{{ $message->created_at->diffForHumans() . " (" . $message->created_at . ")" }}</div>
                    @else
                        <div class="small text-gray-500">{{ $message->created_at->diffForHumans() . " (" . $message->created_at . ")" }}</div>
                    @endif

                    <div class="font-weight-bold container-fluid pr-0 pl-0">
                        <table style="width: 100%;">
                            <tr class="clickable-row"
                                data-href="{{ route('message.show', $message) }}">
                                <td style="width: 80%;">
                                    {{ $message->subject }}
                                </td>
                                <td style="width: 20%;">
                                    @if($message->archived_for_to == 0)
                                        <form action="{{ route('message.archive', $message) }}" 
                                            method="post">
                                            @csrf
                                            @method("PATCH")
                                            <button class="btn btn-white" 
                                                type="submit" 
                                                title="Archive" 
                                                name="clear">
                                                <i class="fas fa-circle fa-xs text-white"></i>
                                            </button>
                                        </form>
                                    @endif
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </a>
        @endforeach

        <a class="dropdown-item text-center small text-gray-500" 
            href="{{ route('message.index') }}">Read all messages</a>
    </div>
</li>