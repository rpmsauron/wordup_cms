<x-home-master>
    @section('content')

        <x-flash-messages>
        </x-flash-messages>

        @if(!$posts)
            <h3 class="my-4 pt-4"><small>NO HEADLINES:</small></h3>
        @else
            @if(Route::current()->getName() == 'post.filter')
                <h3 class="my-4 pt-4"><small>POSTS IN '{{ $category_filter->label }}':</small></h3>
            @elseif(Route::current()->getName() == 'post.filter.tag')
                <h3 class="my-4 pt-4"><small>POSTS TAGGED WITH '{{ $tag_filter->name }}':</small></h3>
            @elseif(Route::current()->getName() == 'post.search')
                <h3 class="my-4 pt-4"><small>SEARCH RESULTS FOR '{{ $search }}':</small></h3>
            @else
                <h3 class="my-4 pt-4"><small>WORD UP HEADLINES:</small></h3>
            @endif

            @foreach($posts as $post)
                <!-- Blog Post -->
                <a class="post-anchor" 
                   id="post_{{ $post->id }}"></a>
                <div class="card mb-4">
                    @if($post->hasPostImage())
                        <img class="card-img-top" 
                            src="{{ $post->post_image }}" 
                            alt="">
                    @endif
                    <div class="card-body">

                        @include('includes.post.post_title_field', ['showBookmarkFlag' => true])

                        <p class="card-text">
                        @if(!$post->hasHtmlbody())
                            {!! Str::limit($post->body, 100, '(…)') !!}
                        @endif
                        </p>
                        <a href="{{ route('post.show', $post->slug) }}" 
                            class="btn btn-primary">
                            <i class="fa fa-hand-point-right fa-lg"></i>
                        </a>
                    </div>
                    <div class="card-footer text-muted">
                        @include('includes.home.posts.user_and_created_at_fields')

                        @include('includes.home.posts.category_field')
                    </div>
                </div>
            @endforeach

            <div class="d-flex">
                <div class="mx-auto">
                    {{-- instead of using links() to output pagination links, this retains url get parameters to retain filter parameter when switching page --}}
                    {{ $posts->appends(request()->input())->links() }}
                </div>
            </div>
        @endif
    
    @endsection
    
    @section('categories')
        @include('includes.home.categories_widget')
    @endsection
</x-home-master>