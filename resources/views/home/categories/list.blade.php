<x-home-master>
    @section('content')
        
        <div class="row">
            <div class="col-sm-9">
                @if(!$categories)
                    <h6>There are no categories to suggest.</h6>
                @else
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Pick a Category for Post '{{ $post->title }}'</h6>
                        </div>

                        <div class="card-body">
                            
                            @can("assign", $post)
                                <form method="post" 
                                    action="{{ route('post.assign', $post) }}">
                                    @method("PATCH")
                            @else
                                <form method="post" 
                                    action="{{ route('suggestions.store', $post) }}">
                            @endcan
                                @csrf

                                <div class="table-responsive">
                                    <table class="table table-bordered" id="categoriesTable" width="100%" cellspacing="0">
                                        <tbody>
                                            @foreach($categories as $category)
                                                @if($category->is_active == 1 && !$post->isSuggestedAs($category))
                                                    <tr class="text-center">
                                                        <td class="border-right-0">
                                                            {{ $category->label }}
                                                        </td>
                                                        <td class="border-left-0 border-right-0">
                                                            @if($category->hasIcon())
                                                                <img src="{{ $category->icon }}"
                                                                    width="{{ Config::get('constants.image.category.icon_size') }}px"/>
                                                            @endif
                                                        </td>
                                                        <td class="border-left-0">
                                                            <input type="radio" name="category[]" value="{{ $category->id }}">
                                                        </td>
                                                    </tr>
                                                @endif
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>

                                <button class="btn btn-secondary" 
                                    type="submit"
                                    disabled
                                    id="confirm-category-button"
                                    @can("assign", $post)
                                        title="Assign">
                                    @else
                                        title="Suggest">
                                    @endcan
                                    <i class="fa fa-check-circle fa-lg"></i>
                                </button>
                            </form>
                        </div>
                    </div>

                    <div class="d-flex">
                        <div class="mx-auto">
                            {{ $categories->links() }}
                        </div>
                    </div>

                @endif
            </div>
        </div>
    
    @endsection
</x-home-master>