<x-home-master>
    @section('content')

        <h1 class="h3 mb-4 text-gray-800">
            @if(isset($post))
                Stats for Post '{{ $post->title }}'
            @endif
        </h1>

        <canvas id="statsChart" 
            width="400">
        </canvas>
        <hr>

    @endsection

    @section('scripts')
        @include("includes.charts.chart")
    @endsection
</x-home-master>