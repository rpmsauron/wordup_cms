<x-admin-master>
    @section('content')

        <x-flash-messages>
        </x-flash-messages>

        @can("merge", App\Category::class)
            <div class="row mb-4">
                <div class="col-sm-9">
                    <a class="btn btn-primary" 
                        title="Merge Categories" 
                        href="{{ route('categories.show.mergeable') }}">
                        <i class="fa fa-object-group fa-lg"></i>
                    </a>
                    <label for="label" 
                        style="margin-left:10px;">Merge Categories</label>
                </div>
            </div>
        @endcan

        @if(iAmAdmin())
            <div class="row mb-4">
                <div class="col-sm-9">
                    @if($numberOrphanTags > 0)
                        <form method="post" 
                            action="{{ route('tag.cleanup') }}">
                            @csrf
                            @method('DELETE')
                            <div class="form-group">
                                <button class="btn btn-primary" 
                                    type="submit" 
                                    title="Remove all orphaned Tags">
                                    <i class="fa fa-broom fa-lg"></i>
                                </button>
                                <label for="label" 
                                    style="margin-left:10px;">Remove orphaned Tags</label>
                            </div>
                        </form>
                    @else
                        <button class="btn btn-secondary" 
                            disabled 
                            title="Remove all orphaned Tags">
                            <i class="fa fa-broom fa-lg"></i>
                        </button>
                        <label for="label" 
                            style="margin-left:10px;">There are no Tags to cleanup</label>
                    @endif
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-sm-9">
                    @if($numberOrphanUserAvatars > 0)
                        <a class="btn btn-primary" 
                            title="Delete" 
                            href="#" 
                            data-toggle="modal" 
                            data-target="#cleanupUserAvatarModal">
                            <i class="fa fa-trash fa-lg"></i>
                        </a>
                        <label for="label" 
                            style="margin-left:10px;">Remove orphaned User Avatars</label>
                    @else
                        <button class="btn btn-secondary" 
                            disabled 
                            title="Remove all orphaned User avatars">
                            <i class="fa fa-broom fa-lg"></i>
                        </button>
                        <label for="label" 
                            style="margin-left:10px;">There are no User Avatars to cleanup</label>
                    @endif
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-sm-9">
                    @if($numberOrphanPostImages > 0)
                        <a class="btn btn-primary" 
                            title="Delete" 
                            href="#" 
                            data-toggle="modal" 
                            data-target="#cleanupPostImageModal">
                            <i class="fa fa-trash fa-lg"></i>
                        </a>
                        <label for="label" 
                            style="margin-left:10px;">Remove orphaned Post Images</label>
                    @else
                        <button class="btn btn-secondary" 
                            disabled 
                            title="Remove all orphaned Post Images">
                            <i class="fa fa-broom fa-lg"></i>
                        </button>
                        <label for="label" 
                            style="margin-left:10px;">There are no Post images to cleanup</label>
                    @endif
                </div>
            </div>

            <div class="row mb-4">
                <div class="col-sm-9">
                    @if($numberOrphanCategoryIcons > 0)
                        <a class="btn btn-primary" 
                            title="Delete" 
                            href="#" 
                            data-toggle="modal" 
                            data-target="#cleanupCategoryIconModal">
                            <i class="fa fa-trash fa-lg"></i>
                        </a>
                        <label for="label" 
                            style="margin-left:10px;">Remove orphaned Category Icons</label>
                    @else
                        <button class="btn btn-secondary" 
                            disabled 
                            title="Remove all orphaned Category icons">
                            <i class="fa fa-broom fa-lg"></i>
                        </button>
                        <label for="label" 
                            style="margin-left:10px;">There are no Category icons to cleanup</label>
                    @endif
                </div>
            </div>

            <div class="row mb-4 ml-0">
                <ul class="list-unstyled">
                    @foreach($jobruns as $jobrun)
                        <li>
                            {{ $jobrun->created_at }} <b>&middot;</b> <a href="{{ route('user.profile.show', $jobrun->user) }}">{{ $jobrun->user->name }}</a> {{ " " . $jobrun->job->display_text . " " . $jobrun->created_at->diffForHumans() }}
                        </li>
                    @endforeach
                </ul>
            </div>

            <div class="d-flex">
                <div class="mx-auto">
                    {{ $jobruns->links() }}
                </div>
            </div>

            <!-- Category Icons Delete Modal-->
            @include('components.admin.modals.cleanup_images_modal', [ 'orphans' => $orphanCategoryIcons, 'modalId' => 'cleanupCategoryIconModal', 'scopeIdBase' => 'category_icons', 'headerContent' => 'Category icons', 'routeAll' => route('category.icon.cleanup.all'), 'route' => route('category.icon.cleanup'), 'inputName' => 'category_icons[]' ])
            <!-- Post Images Delete Modal-->
            @include('components.admin.modals.cleanup_images_modal', [ 'orphans' => $orphanPostImages,    'modalId' => 'cleanupPostImageModal',    'scopeIdBase' => 'post_images',    'headerContent' => 'Post images',    'routeAll' => route('post.image.cleanup.all'),    'route' => route('post.image.cleanup'),    'inputName' => 'post_images[]'    ])
            <!-- User Avatars Delete Modal-->
            @include('components.admin.modals.cleanup_images_modal', [ 'orphans' => $orphanUserAvatars,   'modalId' => 'cleanupUserAvatarModal',   'scopeIdBase' => 'user_avatars',   'headerContent' => 'User avatars',   'routeAll' => route('user.avatar.cleanup.all'),   'route' => route('user.avatar.cleanup'),   'inputName' => 'avatars[]'        ])

        @endif
        
   @endsection 
</x-admin-master>