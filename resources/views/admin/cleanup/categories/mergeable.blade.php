<x-admin-master>
    @section('content')

        <x-flash-messages>
        </x-flash-messages>

        <div class="row">
            <div class="col-sm-9">
                @if(!$categories)
                    <h6>There are no categories to show.</h6>
                @else
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Merge Categories</h6>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">

                                <table class="table table-bordered" id="categoriesTable" width="100%" cellspacing="0">
                                    <thead>
                                        <tr class="text-center">
                                            <th>Id</th>
                                            <th>Created by</th>
                                            <th colspan="2">Label</th>
                                            <th class='border-0'>
                                                <input type="checkbox" 
                                                    id="select_all_categories"
                                                    name="select_all_categories">
                                            </th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($categories as $category)
                                            @if(iAm($category->user))
                                                <tr id="category-row-{{ $category->id }}" class="table-success text-center">
                                            @else 
                                                <tr id="category-row-{{ $category->id }}" class="text-center">
                                            @endif

                                                <td>{{ $category->id }}</td>

                                                <td>
                                                    <a href="{{ route('user.profile.show', $category->user->id) }}">
                                                        {{ $category->user->name }}
                                                    </a>
                                                </td>

                                                <td class="border-right-0">
                                                    @if($category->posts()->count() > 0)
                                                        <label for="categories[]">
                                                            <a href="{{ route('post.filter', $category->slug) }}">
                                                                {{ $category->label }}
                                                            </a>
                                                            ({{ $category->posts()->count() }} posts)
                                                        </label>
                                                    @else
                                                        <label for="categories[]">{{ $category->label }}</label>
                                                    @endif
                                                </td>

                                                <td class="border-left-0">
                                                    @if($category->hasIcon())
                                                        <img src="{{ $category->icon }}"
                                                            id="category-icon-{{ $category->id }}"
                                                            width="{{ Config::get('constants.image.category.icon_size') }}px"/>
                                                    @endif
                                                </td>

                                                <td class="border-0" style="background-color:white !important;">
                                                    <input type="checkbox"
                                                        id="category-select-{{ $category->id }}"
                                                        name="categories[]" 
                                                        onclick="addPreviewIconToPrompt({{ $category->id }})"
                                                        value="{{ $category->label }}">
                                                </td>
                                            </tr>
                                        @endforeach

                                        <tr>
                                            <td class="border-0">
                                                <a class="btn btn-secondary" 
                                                    id="category_merge_button"
                                                    title="Merge Categories" 
                                                    href="#" 
                                                    data-toggle="modal" 
                                                    disabled
                                                    onClick="readCategoryMergeValues()">
                                                    <i class="fa fa-object-group fa-lg"></i>
                                                </a>
                                            </td>
                                            <td class="border-0" style="background-color:white !important;">
                                                <input type="text" 
                                                    name="label" 
                                                    value="{{ old('label') }}"
                                                    id="merged-label-reference"
                                                    placeholder="Rename to"
                                                    class="form-control
                                                    @error('label') is-invalid @enderror
                                                ">
                                                <div>
                                                    @error('label')
                                                        <span><strong>{{ $message }}</strong></span>
                                                    @enderror
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                    <!-- Category Merge Modal-->
                                    @include('components.admin.modals.cleanup_category_merge_modal')
                                </table>

                            </div>
                        </div>
                    </div>

                @endif
            </div>
        </div>

    @endsection
</x-admin-master>