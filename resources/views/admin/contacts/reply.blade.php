<x-home-master>
    @section('content')

        <div class="row">
            <div class="col-sm-12 pt-4 post_wrapper">

                <div class="card mt-4">
                    <div class="card-header">
                        @if($contact->from != null && $contact->with_login == 1)
                            {{ $contact->created_at->diffForHumans() }}, '<a href="{{ route('user.profile.show', $contact->from) }}">{{ $contact->from->name }}</a>' sent the following message:
                        @elseif($contact->from != null && $contact->with_login == 0)
                            {{ $contact->created_at->diffForHumans() }}, '{{ $contact->email }}' (not logged in, but it is User <a href="{{ route('user.profile.show', $contact->from) }}">{{ $contact->from->name }}</a>'s email address) sent the following message:
                        @else
                            {{ $contact->created_at->diffForHumans() }}, '{{ $contact->email }}' sent the following message:
                        @endif
                    </div>
                    <div class="card-body">
                        <p>{{ $contact->body }}</p>
                    </div>
                </div>

                @foreach($contact->replies as $reply)
                    <div class="card mt-4">
                        <div class="card-header">
                            {{ $reply->created_at->diffForHumans() }}, '<a href="{{ route('user.profile.show', $reply->from) }}">{{ $reply->from->name }}</a>' replied the following:
                        </div>
                        <div class="card-body">
                            <p>{{ $reply->body }}</p>
                        </div>
                    </div>
                @endforeach
                
                <form action="{{ route('contact.reply', $contact) }}"
                    method="post">
                    @csrf
                    @method("PATCH")

                    <div class="card mt-4">
                        <div class="card-header">
                            Reply:
                        </div>
                        <div class="card-body px-0 py-0">
                            <div class="form-group">
                                <textarea name="body" 
                                    value="{{ old('body') }}"
                                    class="form-control" 
                                    style="border: none !important;"
                                    id="body" 
                                    cols="30"
                                    rows="10"
                                    class="form-control
                                    @error('body') is-invalid @enderror
                                    "></textarea>
                                <div>
                                    @error('body')
                                        <span><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            </div>
                        </div>
                    </div>
                    
                    <button type="submit" 
                        class="btn btn-primary" 
                        title="Reply">
                        <i class="fa fa-reply fa-lg"></i>
                    </button>
                </form>

            </div>
        </div>

    @endsection

</x-home-master>
