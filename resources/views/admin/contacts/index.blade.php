<x-admin-master>
    @section('content')

        <x-flash-messages>
        </x-flash-messages>

        <div class="row">
            <div class="col-sm-9">
                @if(!$contacts)
                    <h6>There are no pending contacts.</h6>
                @else
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Contacts received</h6>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" 
                                    id="contactsTable" 
                                    width="100%" 
                                    cellspacing="0">
                                    <thead>
                                        <tr class="text-center">
                                            <th>From</th>
                                            <th>When</th>
                                            <th>Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($contacts as $contact)

                                            <tr class="text-center">
                                                <td>
                                                    @if($contact->from && $contact->with_login == 1)
                                                        <a href="{{ route('user.profile.show', $contact->from) }}">{{ $contact->from->name }}</a>
                                                    @elseif($contact->from && $contact->with_login == 0)
                                                        {{ $contact->email }} (not logged in, but it is User 
                                                        <a href="{{ route('user.profile.show', $contact->from) }}">
                                                            {{ $contact->from->name }}</a>'s 
                                                            email address)
                                                    @else
                                                        {{ $contact->email }}
                                                    @endif
                                                </td>
                                                <td>{{ $contact->created_at->diffForHumans() . " (" . $contact->created_at . ")" }}</td>

                                                <td> 
                                                    @include('includes.admin.buttons.contact_reply')
                                                </td>
                                            </tr>

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="d-flex">
                        <div class="mx-auto">
                            {{ $contacts->links() }}
                        </div>
                    </div>

                @endif
            </div>
        </div>

    @endsection
</x-admin-master>