<x-admin-master>
    @section('content')

        <x-flash-messages>
        </x-flash-messages>

        @include('includes.tinyeditor')

        @if(isset($message))
            <h1>Reply to '{{ $user->name }}'</h1>
        @else
            <h1>Send message to '{{ $user->name }}'</h1>
        @endif

        <div class="row">
            <div class="col-sm-9">
                @if(isset($message))
                    <form method="post" 
                        action="{{ route('message.store', $message) }}"
                        id="create-message-form">
                @else
                    <form method="post" 
                        action="{{ route('message.store') }}"
                        id="create-message-form">
                @endif
                    @csrf

                    <div class="form-group">
                        <label for="subject">Subject</label>
                        <input type="text" 
                            name="subject" 
                            id="title" 
                            placeholder="Enter subject" 
                            @if(isset($message))
                                value="{{ config('constants.messages.reply_to_prefix') . $message->subject }}"
                            @else
                                value="{{ old('subject') }}"
                            @endif
                            aria-describedby=""
                            class="form-control
                            @error('subject') is-invalid @enderror
                            ">
                        <div>
                            @error('subject')
                                <span><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="body">Body</label>
                        <textarea name="body" 
                            class="form-control mceNoEditor" 
                            id="body" 
                            cols="30"
                            rows="10"
                            class="form-control
                            @error('body') is-invalid @enderror
                            ">{{ old('body') }}</textarea>
                            @if(isset($message))
                                <p class="mt-4">Replying to:</p>
                                <p class="form-control bg-light text-secondary">{{ $message->body }}</p>
                            @endif
                        <div>
                            @error('body')
                                <span><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>
            
                    @if(!isset($message))
                        <div>
                            <input type="hidden" 
                               value="{{ $user->id }}" 
                               name="to">
                        </div>
                    @endif

                    <div class="form-group">
                        <button type="submit" 
                            class="btn btn-primary" 
                            title="Send">
                            <i class="fa fa-paper-plane fa-lg"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <div class="row pb-4">
             <div class="col-sm-9">
                <form action="{{ route('user.profile.show', $user) }}" 
                    method="get">
                    @csrf
                    <button type="submit" 
                        class="btn btn-secondary" 
                        title="Cancel">
                        <i class="fa fa-chevron-circle-left fa-lg"></i>
                    </button>
                </form>
            </div>
        </div>

    @endsection
</x-admin-master>