<x-admin-master>
    @section('content')

        <h1>
            Your messages
        </h1>

        <x-flash-messages>
        </x-flash-messages>

        @if(me()->getCountNewMessages() == 0)
            <h6>You have no new messages.</h6>
        @else

            @include('includes.admin.messages.messages_filter_form')

            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" 
                            id="messagesTable" 
                            width="100%" 
                            cellspacing="0">
                            <thead>
                                <tr class="text-center">
                                    @if(request('filter') == 'received' || !request('filter'))
                                        <th>From</th>
                                    @elseif(request('filter') == 'sent')
                                        <th>To</th>
                                    @elseif(request('filter') == 'archived')
                                        <th>Correspondent</th>
                                    @endif
                                    <th>Subject</th>
                                    <th>Received at</th>
                                    <th colspan="2">Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($messages as $message)
                                    @if(request('filter') == 'received' || !request('filter'))
                                        @if($message->new == 1)
                                            <tr class="text-center" 
                                                style="background-color:rgb(191,240,222);">
                                        @else
                                            <tr class="text-center">
                                        @endif
                                    @elseif(request('filter') == 'sent')
                                        <tr class="text-center">
                                    @elseif(request('filter') == 'archived')
                                        @if(iAm($message->to_id))
                                            <tr class="text-center">
                                        @elseif(iAm($message->from_id))
                                            <tr class="text-center" 
                                                style="background-color:rgb(220,220,220);">
                                        @endif
                                    @else
                                        <tr class="text-center">
                                    @endif

                                        @if(request('filter') == 'received' || !request('filter'))
                                            <td>
                                            @if($message->from && !$message->from->deleted_at)
                                                <a href="{{ route('user.profile.show', $message->from) }}">
                                                    {{ $message->from->name }}
                                                </a>
                                            @else
                                                {{ $message->from->name }}
                                            @endif
                                            </td>
                                        @elseif(request('filter') == 'sent')
                                            <td>
                                            @if($message->to && !$message->to->deleted_at)
                                                <a href="{{ route('user.profile.show', $message->to) }}">
                                                    {{ $message->to->name }}
                                                </a>
                                            @else
                                                {{ $message->to->name }}
                                            @endif
                                            </td>
                                        @elseif(request('filter') == 'archived')
                                            <td>
                                                @if(iAm($message->to_id))
                                                    @if($message->from && !$message->from->deleted_at)
                                                        <a href="{{ route('user.profile.show', $message->from) }}">
                                                            {{ $message->from->name }}
                                                        </a>
                                                    @endif
                                                @elseif(iAm($message->from_id))
                                                    @if($message->to && !$message->to->deleted_at)
                                                        <a href="{{ route('user.profile.show', $message->to) }}">
                                                            {{ $message->to->name }}
                                                        </a>
                                                    @endif
                                                @endif
                                            </td>
                                        @endif

                                        <td>{{ $message->subject }}</td>

                                        <td>{{ $message->created_at->diffForHumans() }}</td>

                                        <td class="border-right-0">
                                            @include('includes.admin.buttons.message_show_archive')
                                        </td>

                                        <td class="border-left-0 border-right-0">
                                            @include('includes.admin.buttons.message_show')
                                        </td>
                                    </tr>
                                    
                                    <!-- Message Archive Modal-->
                                    @include('components.admin.modals.message_archive_modal', ['id' => $message->id])

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="d-flex">
                <div class="mx-auto">
                    {{-- instead of using links() to output pagination links, this retains url get parameters to retain filter parameter when switching page --}}
                    {{ $messages->appends(request()->input())->links() }}
                </div>
            </div>

            @include('includes.admin.messages.messages_legend')

        @endif

    @endsection

    @section('scripts')

        <!-- Page level plugins -->
        <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <!-- Page level custom scripts -->
        <!--<script src="{{ asset('js/demo/datatables-demo.js') }}"></script>-->

    @endsection
</x-admin-master>