<x-home-master>
    @section('content')

        <div class="row">
            <div class="col-sm-12 pt-4 post_wrapper">
                <h6>Messages</h6>

                <div class="card mt-4">
                    <div class="card-header">
                        '<a href="{{ route('user.profile.show', $message->from) }}">{{ $message->from->name }}</a>' sent you a message:
                    </div>
                    <div class="card-body">
                        <p>{{ $message->body }}</p>

                        <table class="table table-borderless" style="margin-bottom:0;" 
                            id="commentsTable" 
                            width="100%" 
                            cellspacing="0">
                            <tr class="text-center">
                                <td class="border-right-0">
                                    @include('includes.admin.buttons.message_archive')
                                </td>

                                <td class="border-left-0 border-right-0">
                                    @include('includes.admin.buttons.message_reply')
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                <!-- Message Archive Modal-->
                @include('components.admin.modals.message_archive_modal', ['id' => $message->id])
            </div>
        </div>

    @endsection

</x-home-master>
