<x-admin-master>
    @section('content')

        <h1>{{ $user->name }}'s user profile 
            @if(!iAm($user))
                <a href="{{ route('message.create', $user) }}" 
                    title="Message">
                    <i class="fa fa-envelope fa-lg"></i>
                <a>
            @endif
            <a href="{{ route('admin.user.charts', $user) }}" 
                title="View stats">
                <i class="fa fa-chart-area fa-lg"></i>
            <a>
        </h1>

        <x-flash-messages>
        </x-flash-messages>

        <div class="row">
            <div class="col-sm-6">
                <div style="height:300px;">
                    <img class="img-profile rounded-circle" 
                         src="{{ $user->avatar }}" 
                        @if($user->hasAvatar() && getimagesize($user->avatar)[0] > getimagesize($user->avatar)[1])
                            width="{{ Config::get('constants.image.user.image_size') }}px">
                        @else
                            height="{{ Config::get('constants.image.user.image_size') }}px">
                        @endif
                </div>
                <hr>
                <div>
                    <h6>{{ $user->name }} <i>({{ $user->username }})</i></h6>
                    @if($user->is_public_email == 1)
                        <h6>{{ $user->email }}
                    @elseif(iAm($user) || iAmAdmin())
                        <h6>{{ $user->email }}&nbsp;&nbsp;&nbsp;&nbsp;<span class="fas fa-eye-slash"/></h6>
                    @endif
                </div>

                @include('includes.admin.buttons.user_edit', ['customMessage' => "User deleted. Cannot be edited. Restore first."])

            </div>
        </div>

        @include('includes.admin.users.user_posts')
        
        @if(iAm($user))
            <div class="row mb-4">
                <div class="col-sm-6">
                    <form method="get" 
                        action="{{ route('user.destroy.self.prompt') }}">
                        @csrf
                        <button type="submit" 
                            class="btn btn-danger" 
                            title="Delete">
                            <i class="fa fa-trash fa-lg"></i>
                        </button>
                    </form>
                </div>
            </div>
        @endif
        
        <!-- User Delete Modal-->
        @include('components.admin.modals.user_delete_modal', ['id' => $user->id])

    @endsection
</x-admin-master>