<x-admin-master>
    @section('content')

        @if(!iAmAdmin() && me()->hasUsersGranted(Config::get('constants.roles.role.admin.slug')))
            <h1>Users you granted 'Admin' Role</h1>
        @else
            <h1>Users</h1>
        @endif

        <x-flash-messages>
        </x-flash-messages>

        @if(!$users)
            <h6>There are no users to show.</h6>
        @else

            @if(iAmAdmin() || iHavePermissionFor(App\User::class, ['delete', 'restore']))
                @include('includes.admin.users.users_filter_form')
            @endif

            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" 
                            id="usersTable" 
                            width="100%" 
                            cellspacing="0">
                            <thead>
                                <tr class="text-center">
                                    <th>Id</th>
                                    <th>Username</th>
                                    <th>Name</th>
                                    <th>Email</th>
                                    <th>Avatar</th>

                                    @if(!iAmAdmin() && !iHavePermissionFor(App\User::class) && me()->hasUsersGranted(Config::get('constants.roles.role.admin.slug')))
                                        <th colspan="2">Actions</th>
                                    @else
                                        <th colspan="3">Actions</th>
                                    @endif

                                </tr>
                            </thead>
                            <tbody>
                                @foreach($users as $user)
                                    @if(iAm($user))
                                        <tr class="text-center table-success">
                                    @else
                                        <tr class="text-center">
                                    @endif
                                        <td>{{ $user->id }}</td>
                                        <td>
                                            <a href="{{ route('user.profile.show', $user->id) }}">
                                                {{ $user->username }}
                                            </a>
                                        </td>
                                        <td>{{ $user->name }}</td>

                                        @if($user->is_public_email == 1)
                                            <td>{{ $user->email }}</td>
                                        @else
                                            <td>{{ $user->email }}&nbsp;&nbsp;&nbsp;&nbsp;<i class="fas fa-eye-slash"></td>
                                        @endif

                                        <td>
                                            <img class="img-profile rounded-circle" 
                                                src="{{ $user->avatar }}" 
                                                @if($user->hasAvatar() && getimagesize($user->avatar)[0] > getimagesize($user->avatar)[1])
                                                    width="{{ Config::get('constants.image.user.avatar_size') }}px">
                                                @else
                                                    height="{{ Config::get('constants.image.user.avatar_size') }}px">
                                                @endif
                                        </td>

                                        <td class="border-right-0">
                                            @include('includes.admin.buttons.user_edit')
                                        </td>

                                        <td class="border-left-0 border-right-0">
                                            @include('includes.admin.buttons.user_delete_restore')
                                        </td>

                                        <td class="border-left-0">
                                            @include('includes.admin.buttons.user_message')
                                        </td>
                                    </tr>

                                    <!-- User Delete Modal-->
                                    @include('components.admin.modals.user_delete_modal', ['id' => $user->id])
                                    <!-- User Restore Modal-->
                                    @include('components.admin.modals.user_restore_modal', ['id' => $user->id])

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="d-flex">
                <div class="mx-auto">
                    {{-- instead of using links() to output pagination links, this retains url get parameters to retain filter parameter when switching page --}}
                    {{ $users->appends(request()->input())->links() }}
                </div>
            </div>
        @endif

    @endsection

    @section('scripts')

        <!-- Page level plugins -->
        <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <!-- Page level custom scripts -->
        <script src="{{ asset('js/demo/datatables-demo.js') }}"></script>

    @endsection
</x-admin-master>