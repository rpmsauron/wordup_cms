<x-admin-master>
    @section('content')

        <x-flash-messages>
        </x-flash-messages>

        @if(iAm($user))
            <h1>Edit your user profile</h1>
        @elseif(!iAmAdmin() && iGaveAdminTo($user))
            <h1>Role tracking for {{ $user->name }}</h1>
        @else
            <h1>Edit {{ $user->name }}'s user profile</h1>
        @endif

        @can('update', $user)
            <div class="row">
                <div class="col-sm-6">
                    <form method="post" 
                        action="{{ route('user.profile.update', $user) }}" 
                        enctype="multipart/form-data"
                        id="update">
                        @csrf
                        @method('PATCH')

                        <div class="form-group">
                            <div class="mb-2">
                                @if($user->hasAvatar())
                                    <button type="button" 
                                        class="btn btn-secondary" 
                                        id="clear-image-icon"
                                        title="Clear" 
                                        onClick="clearImageField();">
                                        <i class="fa fa-times fa-lg"></i>
                                    </button>
                                    <img src="{{ $user->avatar }}"
                                        id="image-preview"
                                        alt=""
                                        class="img-fluid img-profile rounded-circle">
                                @else
                                    <button type="button" 
                                        class="btn btn-secondary" 
                                        style="display:none;"
                                        id="clear-image-icon"
                                        title="Clear" 
                                        onClick="clearImageField();">
                                        <i class="fa fa-times fa-lg"></i>
                                    </button>
                                    <img src="#"
                                        id="image-preview"
                                        alt=""
                                        class="img-fluid img-profile rounded-circle">
                                @endif
                            </div>

                            <label class="btn btn-primary" 
                                for="user_image" 
                                onclick="$('#user_image').click();">
                                <input type="file" 
                                    name="avatar" 
                                    class="d-none"
                                    id="user_image" 
                                    value="{{ $user->avatar }}">
                                Upload Image
                            </label>
                        </div>

                        <div class="form-group">
                            <label for="username">Username</label>
                            <input type="text" 
                                name="username" 
                                class="form-control @error('username') is-invalid @enderror" 
                                id="username" 
                                value="{{ $user->username }}">
                            @error('username')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" 
                                name="name" 
                                class="form-control @error('name') is-invalid @enderror" 
                                id="name" 
                                value="{{ $user->name }}">
                            @error('name')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="name">Email</label>
                            <input type="text" 
                                name="email" 
                                class="form-control @error('email') is-invalid @enderror" 
                                id="email" 
                                value="{{ $user->email }}">
                            @error('email')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="name">Email shown public</label>
                            <input type="checkbox" 
                                name="is_public_email" 
                                id="is_public" 
                                @if($user->is_public_email == 1)
                                    checked
                                @endif
                             >
                        </div>

                        <div class="form-group">
                            <label for="password">Password</label>
                            <input type="password" 
                                name="password" 
                                class="form-control @error('password') is-invalid @enderror" 
                                id="password">
                            @error('password')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <label for="password-confirmation">Confirm password</label>
                            <input type="password" 
                                name="password-confirmation" 
                                class="form-control @error('password-confirmation') is-invalid @enderror" 
                                id="password-confirmation">
                            @error('password-confirmation')
                                <div class="invalid-feedback">{{ $message }}</div>
                            @enderror
                        </div>

                        <div class="form-group">
                            <button type="submit" 
                                class="btn btn-primary" 
                                title="Save">
                                <i class="fa fa-chevron-circle-left fa-lg"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>

            <div class="row pb-4">
                 <div class="col-sm-9">
                    <form action="{{ route('admin.index') }}" 
                        method="get">
                        @csrf
                        <button type="submit" 
                            class="btn btn-secondary" 
                            title="Cancel">
                            <i class="fa fa-chevron-circle-left fa-lg"></i>
                        </button>
                    </form>
                </div>
            </div>
        @else
            <h6>You can edit this profile because you have previously granted 'Admin' role to it as an Admin.</h6>
        @endcan

        @canany(['attachRole', 'detachRole'], $user)
            @include('includes.admin.users.user_roles_edit')
        @else
            @include('includes.admin.users.user_roles_view')
        @endcanany
        
        @canany(['attachPermission', 'detachPermission'], $user)
            @include('includes.admin.users.user_permissions_edit')
        @else
            @include('includes.admin.users.user_permissions_view')
        @endcanany

    @endsection
</x-admin-master>