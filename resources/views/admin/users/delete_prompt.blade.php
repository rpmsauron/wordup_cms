<x-admin-master>
    @section('content')

        <x-flash-messages>
        </x-flash-messages>

        <div class="row">
            <div class="col-sm-6">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" 
                            id="exampleModalLabel">
                            Delete your Account?
                        </h5>
                    </div>

                    <div class="modal-footer">
                        <form method="get"
                              action="{{ route('user.profile.show', me()) }}">
                            @csrf
                            <button type="submit" 
                                class="btn btn-secondary" 
                                title="Cancel">
                                <i class="fa fa-chevron-circle-left fa-lg"></i>
                            </button>
                        </form>
                        
                        <form method="post" 
                              action="{{ route('user.destroy', me()) }}">
                            @csrf
                            @method('DELETE')
                            <button type="submit" 
                                class="btn btn-danger" 
                                title="Delete">
                                <i class="fa fa-trash fa-lg"></i>
                            </button>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    @endsection
</x-admin-master>