<x-admin-master>
    @section('content')

        <h1 class="h3 mb-4 text-gray-800">
            @if(isset($user))
                @if(iAm($user))
                    Your stats
                @else
                    Stats for User '{{ $user->name }}'
                @endif
            @else
                General system stats
            @endif
        </h1>

        <canvas id="statsChart" 
            width="400">
        </canvas>
        <hr>

    @endsection

    @section('scripts')
        @include("includes.charts.chart")
    @endsection
</x-admin-master>