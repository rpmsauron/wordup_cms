<x-admin-master>
    @section('content')

        <x-flash-messages>
        </x-flash-messages>

        <div class="row">
            <div class="col-sm-9 col-md-9 col-lg-6">
                <form method="post" 
                    action="{{ route('categories.store') }}"
                    enctype="multipart/form-data">
                    @csrf

                    <div class="form-group">
                        <label for="label">Label</label>
                        <input type="text" 
                            value="{{ old('label') }}"
                            name="label" 
                            id="label" 
                            class="form-control 
                            @error('label') is-invalid @enderror
                        ">
                        <div>
                            @error('label')
                            <span><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="mb-2">
                            <button type="button" 
                                class="btn btn-secondary" 
                                style="display:none;" 
                                id="clear-image-icon"
                                title="Clear" 
                                onClick="clearImageField();">
                                <i class="fa fa-times fa-lg"></i>
                            </button>
                            <img src="#"
                                id="image-preview"
                                alt=""
                                style="position: relative; left: {{ Config::get('constants.image.category.icon_size') + 10 }}px"
                                width="{{ Config::get('constants.image.category.icon_size') }}px"/>
                        </div>

                        <label class="btn btn-primary" 
                            for="category_image" 
                            onclick="$('#category_image').click();">
                            <input type="file" 
                                name="icon" 
                                class="d-none"
                                id="category_image">
                            Upload Image
                        </label>
                    </div>

                    @include('includes.captcha_form_field')

                    <div class="form-group">
                        <button class="btn btn-primary" 
                            type="submit" 
                            title="Create">
                            <i class="fa fa-check-circle fa-lg"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <div class="row">
            <div class="col-sm-9">
                @if(!$categories)
                    <h6>There are no categories to show.</h6>
                @else
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Categories</h6>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" 
                                    id="categoriesTable" 
                                    width="100%" 
                                    cellspacing="0">
                                    <thead>
                                        <tr class="text-center">
                                            @if(iAmAdmin())
                                                <th>Id</th>
                                                <th>Created by</th>
                                            @endif

                                            <th colspan="2">Label</th>

                                            @if(me()->hasManageableCategories())
                                                <th colspan="3">Actions</th>
                                            @endif
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($categories as $category)

                                            @if(iAm($category->user))
                                                <tr class="table-success text-center">
                                            @else 
                                                <tr class="text-center">
                                            @endif

                                            @if(iAmAdmin())
                                                <td>{{ $category->id }}</td>
                                                <td>
                                                    <a href="{{ route('user.profile.show', $category->user->id) }}">
                                                        {{ $category->user->name }}
                                                    </a>
                                                </td>
                                            @endif

                                            <td class="border-right-0">
                                                @if($category->is_active == 1 && $category->posts()->count() > 0)
                                                    <a href="{{ route('post.filter', $category->slug) }}">
                                                        {{ $category->label }}
                                                    </a>
                                                @else
                                                    {{ $category->label }}
                                                @endif
                                            </td>
                                            
                                            <td class="border-left-0">
                                                @if($category->hasIcon())
                                                    <img src="{{ $category->icon }}"
                                                        width="{{ Config::get('constants.image.category.icon_size') }}px"/>
                                                @endif
                                            </td>

                                            @if(me()->hasManageableCategories())
                                                <td class="border-right-0">
                                                    @include('includes.admin.buttons.category_toggle')
                                                </td>

                                                <td class="border-left-0 border-right-0">
                                                    @include('includes.admin.buttons.category_edit')
                                                </td>

                                                <td class="border-left-0">
                                                    @include('includes.admin.buttons.category_delete')
                                                </td>
                                            @endif

                                            </tr>

                                            <!-- Category Delete Modal-->
                                            @include('components.admin.modals.category_delete_modal', ['id' => $category->id])
                                            <!-- Category Toggle Modal-->
                                            @include('components.admin.modals.category_toggle_modal', ['id' => $category->id, 'is_active' => $category->is_active])

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="d-flex">
                        <div class="mx-auto">
                            {{ $categories->links() }}
                        </div>
                    </div>

                    @include('includes.admin.categories.categories_legend')

                @endif
            </div>
        </div>

    @endsection
</x-admin-master>