<x-admin-master>
    @section('content')

        <x-flash-messages>
        </x-flash-messages>

        <h1>Edit category '{{ $category->label }}'</h1>

        <div class="row">
            <div class="col-sm-6">
                <form method="post" 
                      action="{{ route('categories.update', $category) }}"
                      enctype="multipart/form-data"
                      id="update">
                    @csrf
                    @method('PATCH')

                    <div class="form-group">
                        <label for="label">Label</label>
                        <input type="text" 
                               name="label" 
                               value="{{ $category->label }}"
                               id="label" 
                               class="form-control" 
                               @error('label') is-invalid @enderror
                        ">
                    </div>

                    <div class="form-group">
                        <label for="is_active">Is active</label>
                        <input type="checkbox" name="is_active"
                            @if($category->is_active == 1)
                               checked
                            @endif
                        ">
                        <div>
                            @error('label')
                            <span><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <div class="mb-2">
                            @if($category->hasIcon())
                                <button type="button" 
                                    class="btn btn-secondary" 
                                    id="clear-image-icon"
                                    title="Clear" 
                                    onClick="clearImageField();">
                                    <i class="fa fa-times fa-lg"></i>
                                </button>
                                <img src="{{ $category->icon }}"
                                    id="image-preview"
                                    alt=""
                                    style="position: relative; left: {{ Config::get('constants.image.category.icon_size') + 10 }}px"
                                    width="{{ Config::get('constants.image.category.icon_size') }}px"/>
                            @else
                                <button type="button" 
                                    class="btn btn-secondary" 
                                    style="display:none;" 
                                    id="clear-image-icon"
                                    title="Clear" 
                                    onClick="clearImageField();">
                                    <i class="fa fa-times fa-lg"></i>
                                </button>
                                <img src="#"
                                    id="image-preview"
                                    alt=""
                                    style="position: relative; left: {{ Config::get('constants.image.category.icon_size') + 10 }}px"
                                    width="{{ Config::get('constants.image.category.icon_size') }}px"/>
                            @endif
                        </div>

                        <label class="btn btn-primary" 
                            for="category_image" 
                            onclick="$('#category_image').click();">
                            <input type="file" 
                                name="icon" 
                                class="d-none"
                                id="category_image">
                            Upload Image
                        </label>
                    </div>
                    
                    <div class="form-group">
                        <button class="btn btn-primary" type="submit" title="Save"><i class="fa fa-check-circle fa-lg"></i></button>
                    </div>
                </form>
            </div>
        </div>

        <div class="row pb-4">
             <div class="col-sm-6">
                <form action="{{ route('categories.index') }}" method="get">
                    @csrf
                    <button type="submit" class="btn btn-secondary" title="Cancel"><i class="fa fa-chevron-circle-left fa-lg"></i></button>
                </form>
            </div>
        </div>

    @endsection
</x-admin-master>