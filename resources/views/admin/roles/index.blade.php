<x-admin-master>
    @section('content')

        <x-flash-messages>
        </x-flash-messages>

        @can("store", App\Role::class)
            <div class="row">
                <div class="col-sm-3">
                    <form method="post" 
                        action="{{ route('roles.store') }}">
                        @csrf
                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" 
                                name="name" 
                                value="{{ old('name') }}"
                                id="name" 
                                class="form-control @error('name') is-invalid @enderror">
                            <div>
                                @error('name')
                                    <span><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="text" 
                                name="description" 
                                value="{{ old('description') }}"
                                id="description" 
                                class="form-control @error('description') is-invalid @enderror">
                            <div>
                                @error('description')
                                    <span><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                             <button class="btn btn-primary" 
                                type="submit" 
                                title="Create">
                                 <i class="fa fa-check-circle fa-lg"></i>
                             </button>
                        </div>
                    </form>
                </div>
            </div>
        @endcan

        <div class="row">
            <div class="col-sm-12">
                @if(!$roles)
                    <h6>There are no roles available.</h6>
                @else
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Roles</h6>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" 
                                    id="rolesTable" 
                                    width="100%" 
                                    cellspacing="0">
                                    <thead>
                                        <tr class="text-center">
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Slug</th>
                                            <th colspan="2">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($roles as $role)
                                            <tr class="text-center">
                                                <td>{{ $role->id }}</td>
                                                <td>{{ $role->name }}</td>
                                                <td>{{ $role->description }}</td>
                                                <td>{{ $role->slug }}</td>

                                                <td class="border-right-0">
                                                    @include('includes.admin.buttons.role_edit')
                                                </td>

                                                <td class="border-left-0">
                                                    @include('includes.admin.buttons.role_delete')
                                                </td>
                                            </tr>

                                            <!-- Role Delete Modal-->
                                            @include('components.admin.modals.role_delete_modal', ['id' => $role->id])

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="d-flex">
                        <div class="mx-auto">
                            {{ $roles->links() }}
                        </div>
                    </div>
                @endif
            </div>
        </div>

    @endsection
</x-admin-master>