<x-admin-master>
    @section('content')

        <x-flash-messages>
        </x-flash-messages>

        <h1>Edit role '{{ $role->name }}'</h1>

        @can("update", App\Role::class)
            <div class="row">
                <div class="col-sm-6">
                    <form method="post" action="{{ route('roles.update', $role) }}">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="name">Name</label>

                            @if($role->is_undeletable != 1)
                                <input type="text" 
                                       name="name" 
                                       value="{{ $role->name }}"
                                       id="name" 
                                       class="form-control
                                    @error('name') is-invalid @enderror
                                ">
                                <div>
                                    @error('name')
                                        <span><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            @else   
                                <p class="form-control" style="background-color:#f8f9fc;">{{ $role->name }}</p>
                            @endif
                        </div>
                        <div class="form-group">
                            <div class="form-group">
                                <label for="title">Description</label>
                                <textarea name="description" 
                                       class="form-control" 
                                       id="description" 
                                       cols="30"
                                       rows="1">{{ $role->description }}</textarea>
                            </div>
                            <div>
                                @error('description')
                                    <span><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" 
                                type="submit" 
                                title="Save">
                                <i class="fa fa-check-circle fa-lg"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        @endcan

        <div class="row pb-4">
             <div class="col-sm-9">
                <form action="{{ route('roles.index') }}" 
                    method="get">
                    @csrf
                    <button type="submit" 
                        class="btn btn-secondary" 
                        title="Cancel">
                        <i class="fa fa-chevron-circle-left fa-lg"></i>
                    </button>
                </form>
            </div>
        </div>

        @include('includes.admin.roles.role_permissions')

    @endsection
</x-admin-master>