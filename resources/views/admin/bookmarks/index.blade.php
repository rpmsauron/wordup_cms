<x-admin-master>
    @section('content')

        <h1>My bookmarks</h1>

        <x-flash-messages>
        </x-flash-messages>

        @if(count($bookmarks) == 0)
            <h6>There are no bookmarks to show.</h6>
        @else

            <div class="card shadow mb-4 col-sm-8">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="bookmarksTable" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center">
                                    <th>Owner</th>
                                    <th>Title</th>
                                    <th>Image</th>
                                    <th>Category</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($bookmarks as $bookmark)
                                    <tr class="text-center">
                                        <td>
                                            @if(isset($bookmark->post->user))
                                                {{ $bookmark->post->user->name }}
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('post.show', $bookmark->post->slug) }}">
                                                {{ $bookmark->post->title }}
                                            </a>
                                        </td>
                                        <td>
                                            @if($bookmark->post->hasPostImage())
                                            <img src="{{ $bookmark->post->post_image }}"
                                                alt=""
                                                height="{!! getPostDisplayDimensions($bookmark->post->post_image)[1] !!}px"
                                                width="{!! getPostDisplayDimensions($bookmark->post->post_image)[0] !!}px"/>
                                            @endif
                                        </td>
                                        @if($bookmark->post->category)
                                            @if($bookmark->post->category->is_active)
                                                <td>{{ $bookmark->post->category->label }}</td>
                                            @else
                                                <td class="table-dark">{{ $bookmark->post->category->label }} <br><small>INACTIVE</small></td>
                                            @endif
                                        @else
                                            <td></td>
                                        @endif

                                        <td>
                                            @include('includes.admin.buttons.bookmark_remove')
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="d-flex">
                <div class="mx-auto">
                    {{-- instead of using links() to output pagination links, this retains url get parameters to retain filter parameter when switching page --}}
                    {{ $bookmarks->appends(request()->input())->links() }}
                </div>
            </div>

        @endif

    @endsection
</x-admin-master>