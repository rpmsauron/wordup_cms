<x-admin-master>
    @section('content')

        <h1>
            Search Results for '{{ $search }}'
        </h1>

        <x-flash-messages>
        </x-flash-messages>

        <div class="card shadow mb-4">
            <div class="card-body">
                <div class="table-responsive">
                    <table class="table table-bordered" 
                        id="postsTable" 
                        width="100%" 
                        cellspacing="0">
                        <thead>
                            <tr class="text-center">
                                <th>Content</th>
                                <th>By</th>
                                <th>Type</th>
                                <th colspan="3">Actions</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($searchables as $searchable)
                                @if(isset($searchable['reviewed']) && $searchable['reviewed'] == 0)
                                    <tr class="text-center table-success">
                                @elseif(isset($searchable['deleted_at']) && $searchable['deleted_at'])
                                    <tr class="text-center table-dark table-borderless">
                                @else
                                    <tr class="text-center">
                                @endif

                                @if($searchable instanceof App\Post)
                                    @include('includes.admin.search.posts_results')
                                @elseif($searchable instanceof App\User)
                                    @include('includes.admin.search.users_results')
                                @elseif($searchable instanceof App\Category)
                                    @include('includes.admin.search.categories_results')
                                @elseif($searchable instanceof App\Comment)
                                    @include('includes.admin.search.comments_results')
                                @endif
                                </tr>

                                @if(isset($searchable->id))
                                    <!-- Search Post Restore Modal-->
                                    @include('components.admin.modals.search_post_restore_modal', ['id' => $searchable->id])
                                    <!-- Search User Restore Modal-->
                                    @include('components.admin.modals.search_user_restore_modal', ['id' => $searchable->id])
                                    <!-- Search Post Delete Modal-->
                                    @include('components.admin.modals.search_post_delete_modal', ['id' => $searchable->id])
                                @endif

                            @endforeach

                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        <div class="d-flex">
            <div class="mx-auto">
                {{-- instead of using links() to output pagination links, this retains url get parameters to retain filter parameter when switching page --}}
                {{ $searchables->appends(request()->input())->links() }}
            </div>
        </div>

    @endsection

    @section('scripts')

        <!-- Page level plugins -->
        <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>

    @endsection
</x-admin-master>