<x-admin-master>
    @section('content')

        <h1>
            @if(isset($customHeader))
                {{ $customHeader }}
            @else
                @can('manageEveryones', App\Comment::class)
                    Moderation for Comments
                @endcan
            @endif
        </h1>

        <x-flash-messages>
        </x-flash-messages>

        @if(count($comments) == 0)
            <h6>There are no comments needing moderation.</h6>
        @else
            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" 
                            id="commentsTable" 
                            width="100%" 
                            cellspacing="0">
                            <thead>
                                <tr class="text-center">
                                    @can('manageEveryones', App\Comment::class)
                                        <th>Id</th>
                                    @endcan
                                    <th>Reported by</th>
                                    <th>Author</th>
                                    <th>On post</th>
                                    <th>Commented at</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($comments as $comment)
                                    <tr class="text-center">

                                        @can('manageEveryones', App\Comment::class)
                                            <td>{{ $comment->id }}</td>
                                        @endcan

                                        <td>
                                            <a href="{{ route('user.profile.show', $comment->reporter) }}">{{ $comment->reporter->name }}</a>
                                        </td>

                                        <td>
                                            <a href="{{ route('user.profile.show', $comment->user) }}">{{ $comment->user->name }}</a>
                                        </td>

                                        <td>
                                            <a href="{{ route('post.show', $comment->post->slug) }}">{{ $comment->post->title }}</a>
                                        </td>

                                        <td>
                                            {{ $comment->created_at->diffForHumans() . " (" . $comment->created_at . ")" }}
                                        </td>

                                        <td>
                                            @include('includes.admin.buttons.comment_review')
                                        </td>
                                    </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="d-flex">
                <div class="mx-auto">
                    {{-- instead of using links() to output pagination links, this retains url get parameters to retain filter parameter when switching page --}}
                    {{ $comments->appends(request()->input())->links() }}
                </div>
            </div>

        @endif

    @endsection

    @section('scripts')

        <!-- Page level plugins -->
        <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <!-- Page level custom scripts -->
        <!--<script src="{{ asset('js/demo/datatables-demo.js') }}"></script>-->

    @endsection
</x-admin-master>