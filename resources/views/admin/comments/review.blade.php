<x-home-master>
    @section('content')

        <div class="row">
            <div class="col-sm-12 pt-4 post_wrapper">
                <h6>
                    @if(same(me(), $comment->reporter))
                        You
                    @else
                        '<a href="{{ route('user.profile.show', $comment->reporter->id) }}">{{ $comment->reporter->name }}</a>' 
                    @endif
                    reported the following Comment on Post '<a href="{{ route('post.show', $comment->post->slug) }}">{{ $comment->post->title }}</a>'</h6>

                <div class="card mt-4">
                    <div class="card-header">
                        '<a href="{{ route('user.profile.show', $comment->user) }}">{{ $comment->user->name }}</a>' commented:
                    </div>
                    <div class="card-body">
                        <p>{{ $comment->body }}</p>

                        <table class="table table-borderless" 
                            style="margin-bottom:0;" 
                            id="commentsTable" 
                            width="100%" 
                            cellspacing="0">
                            <tr class="text-center">
                                <td>
                                    @include('includes.admin.buttons.comment_accept')
                                </td>
                                <td>
                                    @include('includes.admin.buttons.comment_reject')
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>

                @if($comment->hasParent())
                    <div class="card ml-4 mt-4">
                        <div class="card-header">
                            in reply to '<a href="{{ route('user.profile.show', $comment->parent->user) }}">{{ $comment->parent->user->name }}</a>':
                        </div>
                        <div class="card-body">
                            <p>{{ $comment->parent->body }}</p>
                        </div>
                    </div>
                @endif
            </div>
        </div>

        <!-- Comment Accept and Reject Modal-->
        @include('components.admin.modals.comment_accept_modal', ['id' => $comment->id])
        <!-- Comment Restore Modal-->
        @include('components.admin.modals.comment_reject_modal', ['id' => $comment->id])

    @endsection

</x-home-master>
