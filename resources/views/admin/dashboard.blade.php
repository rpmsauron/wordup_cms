<x-admin-master>
    @section('content')

        <h1>My Dashboard</h1>

        <x-flash-messages>
        </x-flash-messages>

        @if(count($suggestions) > 0)
            <div class="row col-sm-12 mt-4">
                <div class="py-3">
                    <h6 class="m-0 font-weight-bold text-primary">You have received suggestions</h6>
                </div>
            </div>

            <div class="row col-sm-12">
                <div>
                    <div class="table-responsive">
                        <table class="table table-borderless" id="suggestionsReceivedTable" width="100%" cellspacing="0">
                            <tbody>
                                @foreach($suggestions as $suggestion)

                                    <tr class="text-center">
                                        <td>
                                            <div style='vertical-align:middle; display:inline;'>
                                                <a href="{{ route('user.profile.show', $suggestion->user) }}">{{ $suggestion->user->name }}</a> 
                                                suggested '<a href="{{ route('post.show', $suggestion->post->slug) }}">{{ $suggestion->post->title }}</a>' 
                                                to '<a href="{{ route('post.filter', $suggestion->category->slug) }}">{{ $suggestion->category->label }}</a>'
                                            </div>
                                            @if($suggestion->category->hasIcon())
                                                <img src="{{ $suggestion->category->icon }}"
                                                    alt=""
                                                    style="vertical-align: middle;"
                                                    width="{{ Config::get('constants.image.category.icon_size') }}px"/>
                                            @endif
                                        </td>

                                        <td class="border-right-0"> 
                                            <form action="{{ route('suggestions.accept', $suggestion) }}" 
                                                method="post">
                                                @csrf
                                                @method("PATCH")
                                                <button type="submit" 
                                                    class="btn btn-primary"
                                                    title="Accept">
                                                    <i class="fa fa-check-circle fa-lg"></i>
                                                </button>
                                            </form>
                                        </td>
                                        <td class="border-left-0"> 
                                            <form action="{{ route('suggestions.reject', $suggestion) }}" 
                                                method="post">
                                                @csrf
                                                @method("PATCH")
                                                <button type="submit" 
                                                    class="btn btn-danger" 
                                                    title="Reject">
                                                    <i class="fa fa-times fa-lg"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>

                <div class="d-flex">
                    <div class="mx-auto">
                        {{ $suggestions->links() }}
                    </div>
                </div>

            </div>
        @endif

        <div class="row col-sm-6 mt-4">
                @if(!$notifications)
                    <h6>No notifications. You're all caught up!</h6>
                @else
                    <div>
                        <div class="table-responsive">
                            <table class="table" 
                                id="notificationsTable" 
                                width="100%" 
                                cellspacing="0">
                                <tbody>
                                    @foreach($notifications as $notification)
                                        @if($notification->new == 1 && $notification->action == "delete")
                                            <tr class="bg-warning clickable-row" 
                                                data-href="{{ $notification->getLinkRoute() }}">
                                        @elseif($notification->new == 1)
                                            <tr class="bg-success clickable-row" 
                                                data-href="{{ $notification->getLinkRoute() }}">
                                        @else
                                            <tr class="borderless clickable-row" 
                                                data-href="{{ $notification->getLinkRoute() }}">
                                        @endif
                                            <td>
                                                @if(!$notification->hideNotifier())
                                                    <img class="rounded-circle" 
                                                        src="{{ $notification->notifier->avatar }}" 
                                                        width="{{ config('constants.image.user.avatar_notification_size') }}px" 
                                                        alt="">
                                                @endif
                                            </td>
                                            <td>
                                                @if($notification->new == 1)
                                                <p class="text-white mb-0"><small>{{ $notification->created_at->diffForHumans() . " (" . $notification->created_at . ")" }}</small><br>
                                                @else
                                                <p class="text-gray-500 ,b-0"><small>{{ $notification->created_at->diffForHumans() . " (" . $notification->created_at . ")" }}</small><br>
                                                @endif
                                                {{ $notification->toText() }}</p>
                                            </td>
                                            <td>
                                                @if($notification->new == 1)
                                                    <form action="{{ route('notifications.clear', $notification) }}" 
                                                        method="post">
                                                        @csrf
                                                        @method("PATCH")
                                                        <button class="btn btn-white" 
                                                            type="submit" 
                                                            title="Clear" 
                                                            name="clear">
                                                            <i class="fas fa-circle fa-xs text-white"></i>
                                                        </button>
                                                    </form>
                                                @endif
                                            </td>
                                        </tr>

                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                @endif
        </div>

        @if(count($bookmarks) > 0)
            <div class="row col-sm-6 mt-4">
                <div class="py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Your bookmarks</h6>
                </div>
            </div>

            <div class="row col-sm-6">
                <div>
                    <div class="table-responsive">
                        <table class="table table-borderless" 
                            id="bookmarksTable" 
                            width="100%" 
                            cellspacing="0">
                            <thead>
                                <tr class="text-center">
                                    <th>Owner</th>
                                    <th>Title</th>
                                    <th>Image</th>
                                    <th>Category</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($bookmarks as $bookmark)
                                    <tr class="text-center">
                                        <td>
                                            @if(isset($bookmark->post->user))
                                                {{ $bookmark->post->user->name }}
                                            @endif
                                        </td>
                                        <td>
                                            <a href="{{ route('post.show', $bookmark->post->slug) }}">
                                                {{ $bookmark->post->title }}
                                            </a>
                                        </td>
                                        <td>
                                            @if($bookmark->post->hasPostImage())
                                            <img src="{{ $bookmark->post->post_image }}"
                                                alt=""
                                                height="{!! getPostDisplayDimensions($bookmark->post->post_image)[1] !!}px"
                                                width="{!! getPostDisplayDimensions($bookmark->post->post_image)[0] !!}px"/>
                                            @endif
                                        </td>
                                        @if($bookmark->post->category)
                                            @if($bookmark->post->category->is_active)
                                                <td>{{ $bookmark->post->category->label }}</td>
                                            @else
                                                <td class="table-dark">{{ $bookmark->post->category->label }} <br><small>INACTIVE</small></td>
                                            @endif
                                        @else
                                            <td></td>
                                        @endif
                                        <td>
                                            <form action="{{ route('post.bookmark', $bookmark->post) }}" 
                                                method="post">
                                                @csrf
                                                @method('PATCH')
                                                <button class="btn btn-danger" 
                                                    type="submit" 
                                                    title="Remove bookmark" 
                                                    name="toggle">
                                                    <i class="fas fa-times fa-lg"></i>
                                                </button>
                                            </form>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="d-flex">
                <div class="mx-auto">
                    {{-- instead of using links() to output pagination links, this retains url get parameters to retain filter parameter when switching page --}}
                    {{ $bookmarks->appends(request()->input())->links() }}
                </div>
            </div>

        @endif

    @endsection
</x-admin-master>