<x-admin-master>
    @section('content')

        <h1>
            @if(isset($customHeader))
                {{ $customHeader }}
            @else
                @can('manageEveryones', App\Post::class)
                    Posts by all users
                @else
                    All your posts
                @endcan
            @endif
        </h1>

        <x-flash-messages>
        </x-flash-messages>

        @if(count($posts) == 0)
            <h6>There are no posts to show.</h6>
        @else
            @include('includes.admin.posts.posts_filter_form')

            <div class="card shadow mb-4">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" 
                            id="postsTable" 
                            width="100%" 
                            cellspacing="0">
                            <thead>
                                <tr class="text-center">
                                    @can('manageEveryones', App\Post::class)
                                        <th>Id</th>
                                        <th>Owner</th>
                                    @endcan
                                    <th>Title</th>
                                    <th>Image</th>
                                    <th colspan="2">Category</th>
                                    <th>Created at</th>
                                    @can('manageEveryones', App\Post::class)
                                        <th colspan="3">Actions</th>
                                    @else
                                        <th colspan="2">Actions</th>
                                    @endcan
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($posts as $post)
                                    @if((request('filter') == 'active' || !request('filter')) && iAm($post->user))
                                        <tr class="text-center table-success" 
                                            style="background-color:rgb(191,240,222);">
                                    @elseif(request('filter') == 'all' && $post->deleted_at)
                                        <tr class="text-center" 
                                            style="background-color:rgb(220,220,220);">
                                    @elseif(request('filter') == 'all' && iAm($post->user))
                                        <tr class="text-center table-success" 
                                            style="background-color:rgb(191,240,222);">
                                    @elseif(request('filter') == 'deleted' && iAm($post->user))
                                        <tr class="text-center table-success" 
                                            style="background-color:rgb(191,240,222);">
                                    @elseif(request('filter') == 'own' && $post->deleted_at)
                                        <tr class="text-center" 
                                            style="background-color:rgb(220,220,220);">
                                    @else
                                        <tr class="text-center">
                                    @endif

                                        @can('manageEveryones', App\Post::class)
                                            <td>{{ $post->id }}</td>
                                            <td>
                                                @if($post->user)
                                                    {{ $post->user->name }}
                                                @else
                                                    {{ Config::get('constants.fallback_messages.post_without_author') }}
                                                @endif
                                            </td>
                                        @endcan

                                        <td>
                                            @if($post->deleted_at || !$post->isAccepted())
                                                {{ $post->title }}
                                            @else
                                                <a href="{{ route('post.show', $post->slug) }}">
                                                    {{ $post->title }}
                                                </a>
                                            @endif
                                        </td>

                                        <td>
                                            @if($post->hasPostImage())
                                                <img src="{{ $post->post_image }}"
                                                    alt=""
                                                    height="{!! getPostDisplayDimensions($post->post_image)[1] !!}px"
                                                    width="{!! getPostDisplayDimensions($post->post_image)[0] !!}px"/>
                                            @endif
                                        </td>

                                        @if(iAmAdmin() && $post->category)
                                            @if($post->category->is_active)
                                                <td class="border-right-0">{{ $post->category->label }}</td>
                                                <td class="border-left-0">
                                                    @if($post->category->hasIcon())
                                                        <img src="{{ $post->category->icon }}"
                                                            alt=""
                                                            width="{{ Config::get('constants.image.category.icon_size') }}px"/>
                                                    @endif
                                                </td>
                                            @else
                                                <td colspan="2" 
                                                    class="table-dark">
                                                    {{ $post->category->label }} <br><small>INACTIVE</small>
                                                </td>
                                            @endif
                                        @elseif(!iAmAdmin() && $post->category && $post->category->is_active)
                                            <td class="border-right-0">{{ $post->category->label }}</td>
                                            <td class="border-left-0">
                                                @if($post->category->hasIcon())
                                                    <img src="{{ $post->category->icon }}"
                                                        alt=""
                                                        width="{{ Config::get('constants.image.category.icon_size') }}px"/>
                                                @endif
                                            </td>
                                        @else
                                            <td colspan="2"></td>
                                        @endif

                                        <td>{{ $post->created_at->diffForHumans() }}</td>

                                        <td class="border-right-0">
                                            @include('includes.admin.buttons.post_edit')
                                        </td>

                                        <td class="border-left-0 border-right-0">
                                            @include('includes.admin.buttons.post_delete_restore')
                                        </td>

                                        <td class="border-left-0">
                                            @include('includes.admin.buttons.post_review')
                                        </td>
                                    </tr>
                                    
                                    <!-- Post Delete Modal-->
                                    @include('components.admin.modals.post_delete_modal', ['id' => $post->id])
                                    <!-- Post Restore Modal-->
                                    @include('components.admin.modals.post_restore_modal', ['id' => $post->id])

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="d-flex">
                <div class="mx-auto">
                    {{-- instead of using links() to output pagination links, this retains url get parameters to retain filter parameter when switching page --}}
                    {{ $posts->appends(request()->input())->links() }}
                </div>
            </div>

            @include('includes.admin.posts.posts_legend')

        @endif

    @endsection

    @section('scripts')

        <!-- Page level plugins -->
        <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
        <script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script>
        <!-- Page level custom scripts -->
        <!--<script src="{{ asset('js/demo/datatables-demo.js') }}"></script>-->

    @endsection
</x-admin-master>