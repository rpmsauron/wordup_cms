<x-home-master>
    @section('content')

        <div class="row">
            <div class="col-sm-12 pt-4 post_wrapper">
                <h6>Moderation for Post '{{ $post->title }}'</h6>
                @include('includes.post.post_title_field', ['showBookmarkFlag' => false])

                <span>
                    @foreach($post->tags as $tag)
                        <a href="{{ route('post.filter.tag', $tag->slug) }}">
                            <label class="post-tag"><strong>{{ strtoupper($tag->name) }}</strong></label>
                        </a>
                    @endforeach
                </span><br>

                @include('includes.home.posts.user_and_created_at_fields')
                <br>
                @include('includes.home.posts.category_field')
                <hr>

                @if($post->hasPostImage())
                    <img class="img-fluid rounded" src="{{ $post->post_image }}" alt=""
                        @if(getPostDisplayDimensions($post->post_image)[0] > Config::get('constants.image.post.image_max_width'))
                            width="{{ Config::get('constants.image.post.image_max_width') }}px">
                        @else
                            width="{!! getPostDisplayDimensions($post->post_image)[0] !!}px">
                        @endif
                    <hr>
                @endif

                <p>{!! $post->body !!}</p>
                <hr>
            </div>
        </div>
    
        @can('manageEveryones', App\Post::class)
            <div class="row">
                <table class="table table-borderless" 
                    id="postsTable" 
                    width="100%" 
                    cellspacing="0">
                    <tr class="text-center">
                        <td>
                            @include('includes.admin.buttons.post_accept')
                        </td>

                        <td>
                            @include('includes.admin.buttons.post_reject')
                        </td>
                    </tr>
                </table>
            </div>
        @endcan

        <!-- Post Accept and Reject Modal-->
        @include('components.admin.modals.post_accept_modal', ['id' => $post->id])
        <!-- Post Restore Modal-->
        @include('components.admin.modals.post_reject_modal', ['id' => $post->id])

    @endsection

</x-home-master>
