<x-admin-master>
    @section('content')

        <x-flash-messages>
        </x-flash-messages>

        @include('includes.tinyeditor')

        <h1>Edit post '{{ $post->title }}'</h1>

        <div class="row">
            <div class="col-sm-9">
                <form method="post" 
                    action="{{ route('post.update', $post->id) }}?page={{ $page }}&filter={{ $filter }}" 
                    enctype="multipart/form-data"
                    id="update">
                    @csrf
                    @method('PATCH')

                    <div class="form-group">
                        <label for="title">Title</label>
                        <input type="text" 
                            name="title" 
                            class="form-control" 
                            id="title" 
                            placeholder="Enter title" 
                            aria-describedby=""
                            value="{{ $post->title }}">
                    </div>

                    <div class="form-group">
                        <label for="tags">Tags</label><br>
                        <input data-role="tagsinput" 
                            type="text" 
                            name="tags"
                            class="input-tags form-control" 
                            id="tags"
                            value="{{ implode(',', $post->tagNames()) }}"
                            onchange="validateTagsAmount()">
                        <div id="warning-tags-limit">{{ Config::get('constants.messages.error.post_tags_limit') }}</div>
                        @error('tags')
                            <div class="invalid-feedback">{{ $message }}</div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <div class="mb-2">
                            @if($post->hasPostImage())
                                <button type="button" 
                                    class="btn btn-secondary" 
                                    id="clear-image-icon"
                                    title="Clear" 
                                    onClick="clearImageField();">
                                    <i class="fa fa-times fa-lg"></i>
                                </button>
                                <img src="{{ $post->post_image }}"
                                    id="image-preview"
                                    alt=""
                                    class="img-fluid"/>
                            @else
                                <button type="button" 
                                    class="btn btn-secondary" 
                                    style="display:none;" 
                                    id="clear-image-icon"
                                    title="Clear" 
                                    onClick="clearImageField();">
                                    <i class="fa fa-times fa-lg"></i>
                                </button>
                                <img src="#"
                                    id="image-preview"
                                    alt=""
                                    class="img-fluid"/>
                            @endif
                        </div>

                        <label class="btn btn-primary" 
                            for="post_image" 
                            onclick="$('#post_image').click();">
                            <input type="file" 
                                name="post_image" 
                                class="d-none"
                                id="post_image" 
                                value="{{ $post->post_image }}">
                            Upload Image
                        </label>
                    </div>

                    <div class="form-group">
                        <label for="title">Body</label>
                        <textarea name="body" 
                            class="form-control" 
                            id="body" 
                            cols="30"
                            rows="10">{{ $post->body }}</textarea>
                    </div>

                    <div class="form-group">
                        <label for="category_id">Category</label>
                        <select name="category_id"
                            class="form-control m-bot15"
                            id="category_id">
                            @if($categories->count())
                                <option selected disabled>Select a category</option>
                                <option value=""></option>
                                @foreach($categories as $category)
                                    <option value="{{ $category->id }}" 
                                        @if($post->category && $post->category->id == $category->id)
                                            selected
                                        @endif
                                    >{{ $category->label }}</option>    
                                @endforeach
                            @endif
                        </select>
                    </div>

                    <div class="form-group">
                        <button type="submit" 
                            class="btn btn-primary" 
                            title="Update">
                            <i class="fa fa-check-circle fa-lg"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>

        <div class="row pb-4">
             <div class="col-sm-9">
                <form action="{{ session('referer') }}" 
                    method="get">
                    @csrf
                    <button type="submit" 
                        class="btn btn-secondary" 
                        title="Cancel">
                        <i class="fa fa-chevron-circle-left fa-lg"></i>
                    </button>
                </form>
            </div>
        </div>

    @endsection
</x-admin-master>