<x-admin-master>
    @section('content')

        <x-flash-messages>
        </x-flash-messages>

        <div class="row">
            <div class="col-sm-9">
                @if(!$notifications)
                    <h6>No new notifications.</h6>
                @else
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Notifications</h6>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table" 
                                    id="notificationsTable" 
                                    width="100%" 
                                    cellspacing="0">
                                    <tbody>
                                        @foreach($notifications as $notification)
                                            @if($notification->new == 1 && $notification->action == "delete")
                                                <tr class="bg-warning clickable-row" 
                                                    data-href="{{ $notification->getLinkRoute() }}">
                                            @elseif($notification->new == 1)
                                                <tr class="bg-success clickable-row" 
                                                    data-href="{{ $notification->getLinkRoute() }}">
                                            @else
                                                <tr class="borderless clickable-row" 
                                                    data-href="{{ $notification->getLinkRoute() }}">
                                            @endif
                                                <td>
                                                    @if(!$notification->hideNotifier())
                                                        <img class="rounded-circle" 
                                                            src="{{ $notification->notifier->avatar }}" 
                                                            width="{{ config('constants.image.user.avatar_notification_size')*2 }}px" 
                                                            alt="">
                                                    @endif
                                                </td>

                                                <td>
                                                    <p>{{ $notification->toText() }}</p>
                                                    @if($notification->new == 1)
                                                        <p class="small text-white">{{ $notification->created_at->diffForHumans() . " (" . $notification->created_at . ")" }}</p>
                                                    @else
                                                        <p class="small text-gray-500">{{ $notification->created_at->diffForHumans() . " (" . $notification->created_at . ")" }}</p>
                                                    @endif
                                                </td>

                                                <td>
                                                    @include('includes.admin.buttons.notification_clear')
                                                </td>
                                            </tr>

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="d-flex">
                        <div class="mx-auto">
                            {{ $notifications->links() }}
                        </div>
                    </div>

                @endif
            </div>
        </div>

    @endsection
</x-admin-master>