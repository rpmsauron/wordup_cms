<x-admin-master>
    @section('content')

        <x-flash-messages>
        </x-flash-messages>

        <div class="row">
            <div class="col-sm-9">
                @if(!$suggestions)
                    <h6>You have not received Category suggestions for any Post.</h6>
                @else
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Suggestions Received</h6>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" 
                                    id="suggestionsReceivedTable" 
                                    width="100%" 
                                    cellspacing="0">
                                    <thead>
                                        <tr class="text-center">
                                            <th>Post</th>
                                            <th colspan="2">Category</th>
                                            <th>By whom</th>
                                            <th colspan="2">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($suggestions as $suggestion)

                                            <tr class="text-center">
                                                <td>{{ $suggestion->post->title }}</td>
                                                <td class="border-right-0">{{ $suggestion->category->label }}</td>
                                                <td class="border-left-0">
                                                    @if($suggestion->category->hasIcon())
                                                        <img src="{{ $suggestion->category->icon }}"
                                                            alt=""
                                                            width="{{ Config::get('constants.image.category.icon_size') }}px"/>
                                                    @endif
                                                </td>

                                                <td>
                                                    <a href="{{ route('user.profile.show', $suggestion->user) }}">{{ $suggestion->user->name }}</a>
                                                </td>

                                                <td class="border-right-0"> 
                                                    @include('includes.admin.buttons.suggestion_received_accept')
                                                </td>

                                                <td class="border-left-0"> 
                                                    @include('includes.admin.buttons.suggestion_received_reject')
                                                </td>
                                            </tr>

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="d-flex">
                        <div class="mx-auto">
                            {{ $suggestions->links() }}
                        </div>
                    </div>

                @endif
            </div>
        </div>

    @endsection
</x-admin-master>