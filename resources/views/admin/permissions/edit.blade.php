<x-admin-master>
    @section('content')

        <x-flash-messages>
        </x-flash-messages>

        <h1>Edit permission '{{ $permission->name }}'</h1>

        @can("update", App\Permission::class)
            <div class="row">
                <div class="col-sm-6">
                    <form method="post" 
                        action="{{ route('permissions.update', $permission) }}">
                        @csrf
                        @method('PUT')

                        <div class="form-group">
                            <label for="name">Name</label>

                            @if($permission->is_undeletable != 1)
                                <input type="text" 
                                    name="name" 
                                    value="{{ $permission->name }}"
                                    id="name" 
                                    class="form-control
                                    @error('name') is-invalid @enderror
                                ">
                                <div>
                                    @error('name')
                                        <span><strong>{{ $message }}</strong></span>
                                    @enderror
                                </div>
                            @else   
                                <p class="form-control" 
                                    style="background-color:#f8f9fc;">{{ $permission->name }}</p>
                            @endif
                        </div>

                        <div class="form-group">
                            <div class="form-group">
                                <label for="title">Description</label>
                                <textarea name="description" 
                                    class="form-control" 
                                    id="description" 
                                    cols="30"
                                    rows="1">{{ $permission->description }}</textarea>
                            </div>
                            <div>
                                @error('description')
                                    <span><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="model">Model</label>
                            <select name="model"
                                id="model"
                                class="form-control m-bot15
                                @error('model') is-invalid @enderror
                                @error('values') is-invalid @enderror
                            ">
                                <option disabled>Select a domain entity</option>
                                @foreach(config("constants.permissions.models") as $key => $value)
                                    @if(isset($value['classname']))
                                        <option value="{{ $value['classname'] }}"
                                        @if($permission->model == $value['classname'])
                                            selected
                                        @endif
                                        >{{ $key }}</option>  
                                    @endif
                                @endforeach
                            </select>
                            <div>
                                @error('model')
                                    <span><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="action">Action</label>
                            <select name="action"
                                id="action"
                                class="form-control m-bot15
                                @error('action') is-invalid @enderror
                                @error('values') is-invalid @enderror
                            ">
                                <option disabled 
                                    @if($permission->action == null)
                                        selected
                                    @endif
                                >Select an action</option>
                                @foreach(getActionsAvailableForPermissionModel($permission->model) as $action)
                                    <option 
                                        @if($permission->action == $action)
                                            selected
                                        @endif
                                    >{{ $action }}</option>
                                @endforeach
                            </select>
                            <div>
                                @error('action')
                                    <span><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>

                        <div>
                            @error('values')
                                <span><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary" 
                                type="submit" 
                                title="Save">
                                <i class="fa fa-check-circle fa-lg"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        @endcan

        <div class="row pb-4">
             <div class="col-sm-9">
                <form action="{{ route('permissions.index') }}" 
                    method="get">
                    @csrf
                    <button type="submit" 
                        class="btn btn-secondary" 
                        title="Cancel">
                        <i class="fa fa-chevron-circle-left fa-lg"></i>
                    </button>
                </form>
            </div>
        </div>

        @include('includes.admin.permissions.permission_roles')

    @endsection
</x-admin-master>