<x-admin-master>
    @section('content')

        <x-flash-messages>
        </x-flash-messages>

        @can("store", App\Permission::class)
            <div class="row">
                <div class="col-sm-3">
                    <form method="post" 
                        action="{{ route('permissions.store') }}">
                        @csrf

                        <div class="form-group">
                            <label for="name">Name</label>
                            <input type="text" 
                                name="name" 
                                value="{{ old('name') }}"
                                id="name" 
                                class="form-control 
                                @error('name') is-invalid @enderror
                            ">
                            <div>
                                @error('name')
                                    <span><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="description">Description</label>
                            <input type="text" 
                                name="description" 
                                value="{{ old('description') }}"
                                id="description" 
                                class="form-control 
                                @error('description') is-invalid @enderror
                            ">
                            <div>
                                @error('description')
                                    <span><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="model">Model</label>
                            <select name="model"
                                id="model"
                                class="form-control m-bot15
                                @error('model') is-invalid @enderror
                                @error('values') is-invalid @enderror
                            ">
                                <option selected disabled>Select a domain entity</option>
                                @foreach(config("constants.permissions.models") as $key => $value)
                                    @if(isset($value['classname']))
                                        <option value="{{ $value['classname'] }}">{{ $key }}</option>  
                                    @endif
                                @endforeach
                            </select>
                            <div>
                                @error('model')
                                    <span><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="action">Action</label>
                            <select name="action"
                                id="action"
                                class="form-control m-bot15
                                @error('action') is-invalid @enderror
                                @error('values') is-invalid @enderror
                            ">
                                <option selected disabled>Select an action</option>
                            </select>
                            <div>
                                @error('action')
                                    <span><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>

                        <div>
                            @error('values')
                                <span><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>

                        <div class="form-group">
                            <button class="btn btn-primary" 
                                type="submit" 
                                title="Create">
                                <i class="fa fa-check-circle fa-lg"></i>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        @endcan

        <div class="row">
            <div class="col-sm-12">
                @if(!$permissions)
                    <h6>There are no permissions available.</h6>
                @else
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Permissions</h6>
                        </div>

                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" 
                                    id="permissionsTable" 
                                    width="100%" 
                                    cellspacing="0">
                                    <thead>
                                        <tr class="text-center">
                                            <th>Id</th>
                                            <th>Name</th>
                                            <th>Description</th>
                                            <th>Slug</th>
                                            <th>Action</th>
                                            <th>Model</th>
                                            <th colspan="2">Actions</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($permissions as $permission)
                                            <tr class="text-center">
                                                <td>{{ $permission->id }}</td>
                                                <td>
                                                    @can("update", App\Permission::class) 
                                                        <a href="{{ route('permissions.edit', $permission) }}">
                                                            {{ $permission->name }}
                                                        </a>
                                                    @else
                                                         {{ $permission->name }}
                                                    @endcan
                                                </td>
                                                <td>{{ $permission->description }}</td>
                                                <td>{{ $permission->slug }}</td>

                                                @if($permission->action)
                                                    <td>{{ Str::ucfirst($permission->action) }}</td>
                                                    <td>{{ $permission->modelHumanReadable() }}</td>
                                                @else
                                                    <td colspan="2">{{ $permission->modelHumanReadable() }}</td>
                                                @endif

                                                @if($permission->is_undeletable != 1)
                                                    <td class="border-right-0">
                                                        @include('includes.admin.buttons.permission_toggle')
                                                    </td>

                                                    <td class="border-left-0">
                                                        @include('includes.admin.buttons.permission_delete')
                                                    </td>
                                                @else
                                                    @include('includes.admin.buttons.lock', ['colspan' => 2])
                                                @endif
                                            </tr>

                                            <!-- Permission Delete Modal-->
                                            @include('components.admin.modals.permission_delete_modal', ['id' => $permission->id])
                                            <!-- Permission Toggle Modal-->
                                            @include('components.admin.modals.permission_toggle_modal', ['id' => $permission->id, 'is_active' => $permission->is_active])

                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                    <div class="d-flex">
                        <div class="mx-auto">
                            {{ $permissions->links() }}
                        </div>
                    </div>
                @endif
            </div>
        </div>

    @endsection
</x-admin-master>