<!-- Bootstrap core JavaScript -->
<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<!-- Core plugin JavaScript -->
<script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

<!-- Custom scripts for all pages -->
<script src="{{ asset('js/demo/utils.js') }}"></script>
<script src="{{ asset('js/sb-admin-2.js') }}"></script>

<script>

/**
 * Load affected 'actions' for selected 'model' option of the Form select field, for Permissions creation.
 */
$("select[id^=model]").change(
    function()
    {
        $.ajaxSetup(
            {
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            }
        );

        $.ajax({
            type:    'GET',
            url:     '/admin/permissions/' + $(this).find("option:selected").html() + '/actions',
            success: function (data)
            {
                buildDropdown(
                    data,
                    $('#action'),
                    'Select an action'
                );
            }
        });
    }
);

/**
 * Builds the options for a dropdown, for 'action' field of Permission's form, from a list of entries.
 *
 * @param array                result       The array of results to list as options.
 * @param {JQuery|HTMLElement} dropdown     The selected Element to append dropdown options to.
 * @param string               emptyMessage The message to show as placeholder for the dropdown.
 */
function buildDropdown(result, dropdown, emptyMessage)
{
    // Remove current options
    dropdown.html('');
    // Add the empty option with the empty message
    dropdown.append('<option value="">' + emptyMessage + '</option>');
    // Check result isnt empty
    if(result != '')
    {
        // Loop through each of the results and append the option to the dropdown
        $.each(result, 
            function(k, v)
            {
                dropdown.append('<option value="' + v + '">' + v + '</option>');
            }
        );
    }
}

</script>