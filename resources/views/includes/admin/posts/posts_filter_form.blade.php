@if(Route::currentRouteName() == 'post.index')
    @can('manageEveryones', App\Post::class)
        <table>
            <tr>
                <form action="{{ route('post.index') }}" 
                    method="get" 
                    name="postsFilter">
                    @csrf
                    <div class="input-group">
                        <td class="pr-5">
                            <input type="radio" 
                                id="active" 
                                name="filter" 
                                value="active" 
                                onChange="autoSubmitPosts();"
                                @if(request('filter') == 'active' || !request('filter'))
                                   checked='checked' 
                                @endif
                            >
                            <label for="active" 
                                   class="control-label">
                                Live posts <small>(both published and to review)</small>
                            </label><br>
                        </td>

                        @canany(['restore','delete'], App\Post::class)
                            <td class="pr-5">
                                <input type="radio" 
                                    id="all" 
                                    name="filter" 
                                    value="all" 
                                    onChange="autoSubmitPosts();"
                                    @if(request('filter') == 'all')
                                       checked='checked' 
                                    @endif
                                >
                                <label for="all" 
                                    class="control-label">
                                    All posts
                                </label><br>
                            </td>
                            <td class="pr-5">
                                <input type="radio" 
                                    id="deleted" 
                                    name="filter" 
                                    value="deleted" 
                                    onChange="autoSubmitPosts();"
                                    @if(request('filter') == 'deleted')
                                       checked='checked' 
                                    @endif
                                >
                                <label for="deleted" 
                                    class="control-label">
                                    Deleted posts
                                </label>
                            </td>
                        @endcanany

                        <td class="pr-5">
                            <input type="radio" 
                                id="own" 
                                name="filter" 
                                value="own" 
                                onChange="autoSubmitPosts();"
                                @if(request('filter') == 'own')
                                   checked='checked' 
                                @endif
                            >
                            <label for="own" 
                                class="control-label">
                                Own posts
                            </label>
                        </td>
                    </div>
                </form>
            </tr>
        </table>
    @endcan
@endif
