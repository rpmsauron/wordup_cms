<div id="legend" class="mb-4">
    <table>
        <tr>
            @if(Route::currentRouteName() == 'post.index')
                @if(request('filter') == 'all')
                    <td style="background-color:rgb(220,220,220); border: 0px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td class="pl-2 pr-4">Any deleted posts</td>
                    <td style="background-color:rgb(191,240,222); border: 0px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td class="pl-2 pr-4">Your live posts, both published and to review</small></td>
                @elseif(request('filter') == 'active' || !request('filter'))
                    <td style="background-color:rgb(191,240,222); border: 0px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td class="pl-2 pr-4">Your posts</td>
                @elseif(request('filter') == 'deleted')
                    <td style="background-color:rgb(191,240,222); border: 0px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td class="pl-2 pr-4">Your posts</td>
                @elseif(request('filter') == 'own')
                    <td style="background-color:rgb(220,220,220); border: 0px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td class="pl-2 pr-4">Deleted posts</td>
                @endif
            @elseif(Route::currentRouteName() == 'posts.listreview')
                <td style="background-color:rgb(191,240,222); border: 0px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                <td class="pl-2 pr-4">Your posts</td>
            @endif
        </tr>
    </table>
</div>