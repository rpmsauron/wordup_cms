<div class="row pb-4">
    <div class="col-sm-12">
        @if($permission->roles->isEmpty())
            <h6>There are no roles using this permission.</h6>
        @else
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Roles using this Permission</h6>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" id="rolesPerPermissionsTable" width="100%" cellspacing="0">
                            <thead>
                                <tr class="text-center">
                                    <th>Id</th>
                                    <th>Name</th>
                                    <th>Slug</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($permission->roles as $role)
                                    <tr class="text-center">
                                        <td>{{ $permission->id }}</td>
                                        <td>
                                            <a href="{{ route('roles.edit', $role) }}">
                                                {{ $role->name }}
                                            </a>
                                        </td>
                                        <td>{{ $role->slug }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>