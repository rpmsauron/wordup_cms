<div id="legend" class="mb-4">
    <table>
        <tr>
            @if(Route::currentRouteName() == 'message.index')
                @if(request('filter') == 'received' || !request('filter'))
                    <td style="background-color:rgb(191,240,222); border: 0px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td class="pl-2 pr-4">New messages</small></td>
                @elseif(request('filter') == 'archived')
                    <td style="background-color:rgb(220,220,220); border: 0px solid black;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td class="pl-2 pr-4">Sent messages </td>
                @endif
            @endif
        </tr>
    </table>
</div>