<table>
    <tr>
        <form action="{{ route('message.index') }}" 
            method="get" 
            name="messagesFilter">
            @csrf
            <div class="input-group">
                <td class="pr-5">
                    <input type="radio" 
                        id="received" 
                        name="filter" 
                        value="received" 
                        onChange="autoSubmitMessages();"
                        @if(request('filter') == 'received' || !request('filter'))
                           checked='checked' 
                        @endif
                    >
                    <label for="received" 
                        class="control-label">
                        Received messages
                    </label><br>
                </td>
                <td class="pr-5">
                    <input type="radio" 
                        id="sent" 
                        name="filter" 
                        value="sent" 
                        onChange="autoSubmitMessages();"
                        @if(request('filter') == 'sent')
                           checked='checked' 
                        @endif
                    >
                    <label for="sent" 
                        class="control-label">
                        Sent messages
                    </label><br>
                </td>
                <td class="pr-5">
                    <input type="radio" 
                        id="archived" 
                        name="filter" 
                        value="archived" 
                        onChange="autoSubmitMessages();"
                        @if(request('filter') == 'archived')
                           checked='checked' 
                        @endif
                    >
                    <label for="archived" 
                        class="control-label">
                        Archived messages
                    </label>
                </td>
            </div>
        </form>
    </tr>
</table>