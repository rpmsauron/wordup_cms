<nav class="navbar navbar-expand topbar static-top shadow navbar-light bg-white mb-4">

    <!-- Topbar Search -->
    <form method="get" 
        action="{{ route('admin.search') }}" 
        class="d-none d-sm-inline-block form-inline mr-auto ml-md-3 my-2 my-md-0 mw-100 navbar-search">
        <div class="input-group">
            <input type="text" 
                name="keywords"
                class="form-control bg-light border-0 small" 
                placeholder="Search for..." 
                aria-label="Search" 
                aria-describedby="basic-addon2">
            <div class="input-group-append">
                <button class="btn btn-primary" 
                    type="submit">
                    <i class="fas fa-search fa-sm"></i>
                </button>
            </div>
        </div>
    </form>

    @if(Auth::check())
        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">
            <!-- Nav Item - Notifications -->
            @include('components.admin.top-nav.notifications')

            <!-- Nav Item - Messages -->
            @include('components.admin.top-nav.messages')

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <x-admin.top-nav.admin-top-navbar-user-information>
            </x-admin.top-nav.admin-top-navbar-user-information>
        </ul>
    @endif
</nav>