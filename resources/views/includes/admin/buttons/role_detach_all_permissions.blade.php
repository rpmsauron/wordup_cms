@can('detachPermission', $role)
    @if($role->permissions->count() > 0)
        <a class="btn btn-danger" 
            title="Detach All" 
            href="#" 
            data-toggle="modal" 
            data-target="#roleDetachAllPermissionsModal_{{ $role->id }}">
            <i class="fa fa-unlink fa-lg"></i>
        </a>
    @else
        <a class="btn btn-danger disabled">
            <i class="fa fa-unlink fa-lg"></i>
        </a>
    @endif
@endcan