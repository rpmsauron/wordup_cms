<form action="{{ route('post.bookmark', $bookmark->post) }}" 
    method="post">
    @csrf
    @method('PATCH')
    <button class="btn btn-danger" 
        type="submit" 
        title="Remove bookmark" 
        name="toggle">
        <i class="fas fa-times fa-lg"></i>
    </button>
</form>
