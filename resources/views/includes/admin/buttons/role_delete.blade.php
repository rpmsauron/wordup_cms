@if($role->is_undeletable == 1)
    <i class="fa fa-lock fa-lg" 
        title="System Role: cannot be deleted."></i>
@else
    @can("delete", $role)
        <a class="btn btn-danger" 
            title="Delete" 
            href="#" 
            data-toggle="modal" 
            data-target="#roleDeleteModal_{{ $role->id }}">
            <i class="fa fa-trash fa-lg"></i>
        </a>
    @endcan
@endif

