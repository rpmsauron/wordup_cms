<a class="btn btn-primary" 
    title="Accept" 
    href="#" 
    data-toggle="modal" 
    data-target="#postAcceptModal_{{ $post->id }}">
    <i class="fa fa-thumbs-up fa-lg"></i>
</a>