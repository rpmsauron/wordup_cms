@can('update', $post)
    @if($post->deleted_at == null)
        <form method="get" 
            action="{{ route('post.edit', $post->id) }}?page={{ $page }}">
            @csrf
            <input type="hidden" 
                value="{{ $page }}" 
                name="page">
            @if(isset($filter))
                <input type="hidden" 
                    value="{{ $filter }}" 
                    name="filter">
            @endif
            <button type="submit" 
                class="btn btn-primary" 
                title="Edit">
                <i class="fa fa-edit fa-lg"></i>
            </button>
        </form>
    @endif
@else
    <i class="fa fa-lock fa-lg"></i>
@endcan