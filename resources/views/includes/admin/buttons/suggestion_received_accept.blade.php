<form action="{{ route('suggestions.accept', $suggestion) }}" 
    method="post">
    @csrf
    @method("PATCH")

    <button type="submit" 
        class="btn btn-primary" 
        title="Accept">
        <i class="fa fa-check-circle fa-lg"></i>
    </button>
</form>