@if($user->deleted_at)
    @can('restore', $user)
        <a class="btn btn-primary" 
            title="Restore" 
            href="#" 
            data-toggle="modal" 
            data-target="#userRestoreModal_{{ $user->id }}">
            <i class="fa fa-undo fa-lg"></i>
        </a>
    @endcan
@else
    @can('delete', $user)
        <a class="btn btn-danger" 
            title="Delete" 
            @if(iAm($user))
                href="{{ route('user.destroy.self.prompt') }}"
            @else
                href="#" 
                data-toggle="modal" 
                data-target="#userDeleteModal_{{ $user->id }}"
            @endif
            ><i class="fa fa-trash fa-lg"></i>
        </a>
    @endcan
@endif