<form action="{{ route('suggestions.reject', $suggestion) }}" 
    method="post">
    @csrf
    @method("PATCH")

    <button type="submit" 
        class="btn btn-danger" 
        title="Reject">
        <i class="fa fa-times fa-lg"></i>
    </button>
</form>