@if($colspan)
    <td colspan="{{ $colspan }}">
@else
    <td>
@endif
        <i class="fa fa-lock fa-lg"></i>
    </td>