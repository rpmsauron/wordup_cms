<a class="btn btn-primary"
    href="{{ route('roles.edit', $role) }}"
    title="Edit">
    <i class="fa fa-edit fa-lg"></i>
</a>