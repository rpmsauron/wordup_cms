@if($user->deleted_at)
    <i class="fa fa-lock fa-lg"></i>
    @if(isset($customMessage))
        &nbsp;{{ $customMessage }}
    @endif
@else    
    @can('edit', $user)
        <form method="get" 
            action="{{ route('user.profile.edit', $user->id) }}">
            @csrf

            <button type="submit" 
                class="btn btn-primary" 
                title="Edit">
                <i class="fa fa-edit fa-lg"></i>
            </button>
        </form>
    @endcan
@endif