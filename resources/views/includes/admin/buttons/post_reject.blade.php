<a class="btn btn-danger" 
    title="Reject" 
    href="#" 
    data-toggle="modal" 
    data-target="#postRejectModal_{{ $post->id }}">
    <i class="fa fa-thumbs-down fa-lg"></i>
</a>