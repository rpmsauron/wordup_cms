@if($user->permissions->contains($permission))
    @can('detachPermission', $user)
        <a class="btn btn-danger" 
            title="Detach" 
            href="#" 
            data-toggle="modal" 
            data-target="#userDetachPermissionModal_{{ $user->id }}_{{ $permission->id }}">
            <i class="fa fa-unlink fa-lg"></i>
        </a>
    @endcan
@else
    @can('attachPermission', $user)
        <a class="btn btn-primary" 
            title="Attach" 
            href="#" 
            data-toggle="modal" 
            data-target="#userAttachPermissionModal_{{ $user->id }}_{{ $permission->id }}">
            <i class="fa fa-link fa-lg"></i>
        </a>
    @endcan
@endif