<form action="{{ route('contact.showreply', $contact) }}" 
    method="get">
    @csrf
    <button type="submit" 
        class="btn btn-primary" 
        title="Reply">
        <i class="fa fa-reply fa-lg"></i>
    </button>
</form