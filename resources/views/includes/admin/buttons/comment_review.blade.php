@can('review', $comment)
    <a class="btn btn-primary" 
        title="Review" 
        href="{{ route('comment.review', $comment) }}">
        <i class="fa fa-vote-yea fa-lg"></i>
    </a>
@endcan