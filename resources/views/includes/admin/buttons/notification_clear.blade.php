@if($notification->new == 1)
    <form action="{{ route('notifications.clear', $notification) }}" 
        method="post">
        @csrf
        @method("PATCH")
        <button class="btn btn-white" 
            type="submit" 
            title="Clear" 
            name="clear">
            <i class="fas fa-circle fa-xs text-white"></i>
        </button>
    </form>
@endif