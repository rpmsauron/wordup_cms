@can('review', $post)
    @if($post->reviewed != 1)
        <a class="btn btn-primary" 
            title="Review" 
            href="{{ route('post.review', $post) }}">
            <i class="fa fa-vote-yea fa-lg"></i>
        </a>
    @endif
@endcan
