<a class="btn btn-primary" 
    title="Reply" 
    href="{{ route('message.reply', $message) }}">
    <i class="fa fa-reply fa-lg"></i>
<a>