@can("toggle", $category)
    @if($category->is_active == 1)
        <a class="btn btn-primary" 
            title="Disable" 
            href="#" 
            data-toggle="modal" 
            data-target="#categoryToggleModal_{{ $category->id }}">
            <i class="fa fa-toggle-on fa-lg"></i>
        </a>
    @else
        <a class="btn btn-danger" 
            title="Enable" 
            href="#" 
            data-toggle="modal" 
            data-target="#categoryToggleModal_{{ $category->id }}">
            <i class="fa fa-toggle-off fa-lg"></i>
        </a>
    @endif
@endcan