<form method="get" 
    action="{{ route('message.show', $message) }}?page={{ $page }}">
    @csrf
    <input type="hidden" 
        value="{{ $page }}" 
        name="page">
    <button type="submit" 
        class="btn btn-primary" 
        title="Show">
        <i class="fa fa-eye fa-lg"></i>
    </button>
</form>