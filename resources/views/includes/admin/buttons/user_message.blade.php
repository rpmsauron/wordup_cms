@can('message', [App\Message::class, $user])
    <a href="{{ route('message.create', $user) }}"
       class="btn btn-primary"
       title="Message {{ $user->name }}">
        <i class="fa fa-envelope fa-lg"></i>
    <a>
@endcan