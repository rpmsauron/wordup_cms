@if($post->deleted_at)
    @can('restore', $post)
        <a class="btn btn-primary" 
            title="Restore" 
            href="#" 
            data-toggle="modal" 
            data-target="#postRestoreModal_{{ $post->id }}">
            <i class="fa fa-undo fa-lg"></i>
        </a>
    @endcan
@else
    @can('delete', $post)
        <a class="btn btn-danger" 
            title="Delete" 
            href="#" 
            data-toggle="modal" 
            data-target="#postDeleteModal_{{ $post->id }}">
            <i class="fa fa-trash fa-lg"></i>
        </a>
    @endcan
@endif