@can("update", $category)
    <form action="{{ route('categories.edit', $category) }}" 
        method="get">
        @csrf
        <button type="submit" 
            class="btn btn-primary" 
            title="Edit">
            <i class="fa fa-edit fa-lg"></i>
        </button>
    </form>
@endcan