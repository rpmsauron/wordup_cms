<form action="{{ route('suggestions.remove', $suggestion) }}" 
    method="post">
    @csrf
    @method("PATCH")

    <button type="submit" 
        class="btn btn-danger" 
        title="Remove">
        <i class="fa fa-trash fa-lg"></i>
    </button>
</form>
