@if($user->roles->contains($role))
    @can('detachRole', [$user, $role])
        <a class="btn btn-danger" 
            title="Detach" 
            href="#" 
            data-toggle="modal" 
            data-target="#userDetachRoleModal_{{ $user->id }}_{{ $role->id }}">
            <i class="fa fa-unlink fa-lg"></i>
        </a>
    @endcan
@else
    @can('attachRole', $user)
        <a class="btn btn-primary" 
            title="Attach" 
            href="#" 
            data-toggle="modal" 
            data-target="#userAttachRoleModal_{{ $user->id }}_{{ $role->id }}">
            <i class="fa fa-link fa-lg"></i>
        </a>
    @endcan
@endif