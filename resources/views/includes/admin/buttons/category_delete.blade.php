@can("delete", $category)
    <a class="btn btn-danger" 
        title="Delete" 
        href="#" 
        data-toggle="modal" 
        data-target="#categoryDeleteModal_{{ $category->id }}">
        <i class="fa fa-trash fa-lg"></i>
    </a>
@endif