@if(request('filter') != 'archived')
    @can('archive', $message)
        <a class="btn btn-primary" 
            title="Archive" 
            href="#" 
            data-toggle="modal" 
            data-target="#messageArchiveModal_{{ $message->id }}">
            <i class="fa fa-archive fa-lg"></i>
        </a>
    @endcan
@endif