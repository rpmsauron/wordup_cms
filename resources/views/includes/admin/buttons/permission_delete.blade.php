@can("delete", $permission)
    <a class="btn btn-danger" 
        title="Delete" 
        href="#" 
        data-toggle="modal" 
        data-target="#permissionDeleteModal_{{ $permission->id }}">
        <i class="fa fa-trash fa-lg"></i>
    </a>
@endcan