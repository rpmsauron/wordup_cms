@if($permission->is_active == 0)
    <form action="{{ route('permissions.toggle', $permission) }}" 
        method="post">
        @csrf
        @method("PATCH")
        <label for="enable">Permission disabled.&nbsp;&nbsp;</label>
        <button type="submit" 
            name="enable" 
            class="btn btn-danger" 
            title="Enable">
            <i class="fa fa-toggle-off fa-lg"></i>
        </button>
    </form>
@elseif($role->permissions->contains($permission))
    <a class="btn btn-danger" 
        title="Detach" 
        href="#" 
        data-toggle="modal" 
        data-target="#roleDetachPermissionModal_{{ $role->id }}_{{$permission->id}}">
        <i class="fa fa-unlink fa-lg"></i>
    </a>
@else
    <a class="btn btn-primary" 
        title="Attach" 
        href="#" 
        data-toggle="modal" 
        data-target="#roleAttachPermissionModal_{{ $role->id }}_{{$permission->id}}">
        <i class="fa fa-link fa-lg"></i>
    </a>
@endif