<td>
    @if(!$searchable->reviewed || !$searchable->allowed || $searchable->reviewed_by == null)
        {{ $searchable->title }}
    @else
        <a href="{{ route('post.show', $searchable['slug']) }}">
            {{ $searchable->title }}
        </a>
    @endif
</td>

<td>
    @include('includes.user.user_username_field')
</td>

<td>Post</td>

<td class="border-right-0">
    @include('includes.admin.buttons.post_edit', ['post' => $searchable])
</td>

<td class="border-left-0 border-right-0">
    @include('includes.admin.buttons.post_delete_restore', ['post' => $searchable])
</td>

<td class="border-left-0">
    @include('includes.admin.buttons.post_review', ['post' => $searchable])
</td>