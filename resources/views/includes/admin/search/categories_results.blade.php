<td>
    <a href="{{ route('categories.edit', $searchable->id) }}">{{ $searchable->label }}</a>
</td>

<td>
    @include('includes.user.user_username_field')
</td>

<td>Category</td>

<td class="border-right-0">
    @include('includes.admin.buttons.category_edit', ['category' => $searchable])
</td>

<td class="border-left-0 border-right-0">
    @include('includes.admin.buttons.category_delete', ['category' => $searchable])
</td>

<td class="border-left-0">
    @include('includes.admin.buttons.category_toggle', ['category' => $searchable])
</td>