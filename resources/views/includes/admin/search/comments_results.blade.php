<td>
    @if($searchable->allowed)
        <a href="{{ route('post.show', $searchable->slug) }}">
        {{ substr($searchable->body, 0, 50) }}
        @if(strlen($searchable->body) > 50)
            (&hellip;)
        @endif
        </a>
    @else
        {{ substr($searchable->body, 0, 50) }}
        @if(strlen($searchable->body) > 50)
            (&hellip;)
        @endif
    @endif
</td>

<td>
    @include('includes.user.user_username_field')
</td>

<td>Comment</td>

<td colspan="3">
    @include('includes.admin.buttons.comment_review', ['comment' => $searchable])
</td>