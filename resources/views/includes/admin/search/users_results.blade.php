<td>
    <a href="{{ route('user.profile.show', $searchable->id) }}">{{ $searchable->username }}</a>
</td>

<td></td>

<td>User</td>

<td class="border-right-0">
    @include('includes.admin.buttons.user_edit', ['user' => $searchable])
</td>

<td class="border-left-0 border-right-0">
    @include('includes.admin.buttons.user_delete_restore', ['user' => $searchable])
</td>

<td class="border-left-0">
    @include('includes.admin.buttons.user_message', ['user' => $searchable])
</td>