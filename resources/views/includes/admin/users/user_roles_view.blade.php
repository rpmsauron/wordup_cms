<div class="row">
    <div class="col-sm-6">
        @if(count($user->roles) == 0)
            @if(iAm($user))
                <h6>You have no roles.</h6>
            @else
                <h6>This user has no roles.</h6>
            @endif
        @else
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Assigned Roles</h6>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered"
                            id="rolesPerUserTable" 
                            width="100%" 
                            cellspacing="0">
                            <thead>
                                <tr class="text-center">
                                    <th>Name</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($user->roles as $role)
                                    <tr class="text-center">
                                        <td>{{ $role->name }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>