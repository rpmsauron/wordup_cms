<div class="row">
    <div class="col-sm-12">
        @if($permissions->isEmpty())
            <h6>There are no Permissions available.</h6>
        @else
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Attach/detach Permissions</h6>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" 
                            id="permissionsPerUserTable" 
                            width="100%" 
                            cellspacing="0">
                            <thead>
                                <tr class="text-center">
                                    <th></th>
                                    @if(iAmAdmin() || iHavePermissionFor(App\Permission::class))
                                        <th>Id</th>
                                    @endif
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Action</th>
                                    <th>Model</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr class="text-center">
                                    <td colspan="6" 
                                        class="border-right-0">All</td>
                                    <td class="border-left-0">
                                        @include('includes.admin.buttons.role_detach_all_permissions')
                                    </td>
                                </tr>
                                @foreach($permissions as $permission)
                                    <tr class="text-center">
                                        <td>
                                            @foreach($user->permissions as $userPermission)
                                                @if($userPermission->slug == $permission->slug)
                                                   <i class="fas fa-check"></i>
                                                @endif
                                            @endforeach
                                        </td>

                                        @if(iAmAdmin() || iHavePermissionFor(App\Permission::class))
                                            <td>{{ $permission->id }}</td>
                                        @endif

                                        <td>{{ $permission->name }}</td>
                                        <td>{{ $permission->slug }}</td>

                                        @if($permission->action)
                                            <td>{{ Str::ucfirst($permission->action) }}</td>
                                            <td>{{ $permission->modelHumanReadable() }}</td>
                                        @else
                                            <td colspan="2">{{ $permission->modelHumanReadable() }}</td>
                                        @endif

                                        <td>
                                            @include('includes.admin.buttons.user_attach_detach_permission')
                                        </td>
                                    </tr>

                                    <!-- Attach Permission to User Modal-->
                                    @include('components.admin.modals.user_attach_permission_modal', ['user' => $user, 'permission' => $permission])
                                    <!-- Detach Permission from User Modal-->
                                    @include('components.admin.modals.user_detach_permission_modal', ['user' => $user, 'permission' => $permission])
                                    <!-- Detach all Permissions from User Modal-->
                                    @include('components.admin.modals.user_detach_all_permissions_modal', ['user' => $user])

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>