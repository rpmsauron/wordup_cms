@if(!iAmAdmin() && me()->gaveRoleTo($user, Config::get('constants.roles.role.admin.slug')))
    <button class="btn" 
            style="color:yellow; text-shadow: 0 0 3px #000; position:relative; left: -30px; top: 15px;" 
            title="Cannot undo unless you are an admin!">
        <i class="fa fa-exclamation-triangle fa-lg"></i>
    </button>
@elseif(iAm($user) && iAmAdmin())
    <button class="btn" 
            style="color:yellow; text-shadow: 0 0 3px #000; position:relative; left: -30px; top: 15px;" 
            title="If you relinquish your status of 'Admin', only another 'Admin' can give it back to you. You won't be able to assign yourself any more Roles either.">
        <i class="fa fa-exclamation-triangle fa-lg"></i>
    </button>
@endif