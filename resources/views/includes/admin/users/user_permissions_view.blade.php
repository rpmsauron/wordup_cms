<div class="row">
    <div class="col-sm-6">
        @if(count($user->permissions) == 0)
            @if(iAm($user))
                <h6>You have no permissions.</h6>
            @else
                <h6>This user has no permissions.</h6>
            @endif
        @else
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Assigned Permissions</h6>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" 
                            id="permissionsPerUserTable" 
                            width="100%" 
                            cellspacing="0">
                            <thead>
                                <tr class="text-center">
                                    @if(iAmAdmin() || iHavePermissionFor(App\Permission::class))
                                        <th>Id</th>
                                    @endif
                                    <th>Name</th>
                                    <th>Description</th>
                                    @if(iAmAdmin() || iHavePermissionFor(App\Permission::class))
                                        <th>Action</th>
                                        <th>Model</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($user->permissions as $permission)
                                    <tr class="text-center">
                                        @if(iAmAdmin() || iHavePermissionFor(App\Permission::class))
                                            <td>{{ $permission->id }}</td>
                                        @endif
                                        <td>{{ $permission->name }}</td>
                                        <td>{{ $permission->description }}</th>
                                        @if(iAmAdmin() || iHavePermissionFor(App\Permission::class))
                                            @if($permission->action)
                                                <td>{{ Str::ucfirst($permission->action) }}</td>
                                                <td>{{ $permission->modelHumanReadable() }}</td>
                                            @else
                                                <td colspan="2">{{ $permission->modelHumanReadable() }}</td>
                                            @endif
                                        @endif
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>