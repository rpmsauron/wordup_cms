<div class="row">
    <div class="col-sm-12">
        @if($roles->isEmpty())
            <h6>There are no Roles available.</h6>
        @else
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Attach/detach Roles</h6>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" 
                            id="rolesPerUserTable" 
                            width="100%" 
                            cellspacing="0">
                            <thead>
                                <tr class="text-center">
                                    <th></th>
                                    @if(iAmAdmin() || iHavePermissionFor(App\Permission::class))
                                        <th>Id</th>
                                    @endif
                                    <th>Name</th>
                                    <th>Slug</th>
                                    <th>Actions</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($roles as $role)
                                    <tr class="text-center">
                                        <td>
                                            @foreach($user->roles as $userRole)
                                                @if($userRole->slug == $role->slug)
                                                   <i class="fas fa-check"></i>
                                                @endif
                                            @endforeach
                                        </td>

                                        @if(iAmAdmin() || iHavePermissionFor(App\Permission::class))
                                            <td>{{ $role->id }}</td>
                                        @endif

                                        <td>{{ $role->name }}</td>
                                        <td>{{ $role->slug }}</td>

                                        <td>
                                            @include('includes.admin.buttons.user_attach_detach_role')
                                        </td>

                                    </tr>

                                    <!-- Attach Role to User Modal-->
                                    @include('components.admin.modals.user_attach_role_modal', ['user' => $user, 'role' => $role])
                                    <!-- Detach Role from User Modal-->
                                    @include('components.admin.modals.user_detach_role_modal', ['user' => $user, 'role' => $role])

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        @endif
    </div>
</div>