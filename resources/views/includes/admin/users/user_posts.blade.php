<div class="row">
    <div class="col-sm-12 pt-4">        
        @if($user->posts->isEmpty())
            <h6>This user doesn't have any posts.</h6>
        @else
            <div class="card shadow mb-4">
                <div class="card-header py-3">
                    <h6 class="m-0 font-weight-bold text-primary">Posts by {{ $user->name }}</h6>
                </div>

                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-bordered" 
                            id="postsPerUserTable" 
                            width="100%" 
                            cellspacing="0">
                            <thead>
                                <tr class="text-center">
                                    <th>Title</th>
                                    <th>Image</th>
                                    <th>Category</th>
                                    <th>Created at</th>
                                    @if(iAmAdmin())
                                        <th>Actions</th>
                                    @endif
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($userPosts as $post)
                                    <tr class="text-center">
                                        <td>
                                            <a href="{{ route('post.show', $post->slug) }}">{{ $post->title }}</a>
                                        </td>
                                        <td>
                                            <img src="{{ $post->post_image }}" 
                                                alt="" 
                                                width="100px"/>
                                        </td>
                                        
                                        @if(iAmAdmin() && $post->category)
                                            @if($post->category->is_active)
                                                <td>{{ $post->category->label }}</td>
                                            @else
                                            <td class="table-dark">{{ $post->category->label }} <br><small>INACTIVE</small></td>
                                            @endif
                                        @elseif(!iAmAdmin() && $post->category && $post->category->is_active)
                                            <td>{{ $post->category->label }}</td>
                                        @else
                                            <td></td>
                                        @endif
                                        
                                        <td>{{ $post->created_at->diffForHumans() }}</td>

                                        <td>
                                            @include('includes.admin.buttons.post_delete_restore')
                                        </td>
                                    </tr>
                                    
                                    <!-- Post Delete Modal-->
                                    @include('components.admin.modals.post_delete_modal', ['id' => $post->id])

                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="d-flex">
                <div class="mx-auto">
                    {{ $userPosts->links() }}
                </div>
            </div>
        @endif
    </div>
</div>