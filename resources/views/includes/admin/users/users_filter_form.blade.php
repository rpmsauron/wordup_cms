<table>
    <tr>
        <form action="{{ route('users.index') }}" 
              method="get" 
              name="usersFilter">
            @csrf
            <div class="input-group">
                <td class="pr-5">
                    <input type="radio" 
                           id="active" 
                           name="filter" 
                           value="active" 
                           onChange="autoSubmitUsers();"
                        @if(request('filter') == 'active' || !request('filter'))
                           checked='checked' 
                        @endif
                    >
                    <label for="active" 
                           class="control-label">
                        Active users
                    </label><br>
                </td>
                <td class="pr-5">
                    <input type="radio" 
                           id="all" 
                           name="filter" 
                           value="all" 
                           onChange="autoSubmitUsers();"
                        @if(request('filter') == 'all')
                           checked='checked' 
                        @endif
                    >
                    <label for="all" 
                           class="control-label">
                        All users
                    </label><br>
                </td>
                <td class="pr-5">
                    <input type="radio" 
                           id="deleted" 
                           name="filter" 
                           value="deleted" 
                           onChange="autoSubmitUsers();"
                        @if(request('filter') == 'deleted')
                           checked='checked' 
                        @endif
                    >
                    <label for="deleted" 
                           class="control-label">
                        Deleted users
                    </label>
                </td>
            </div>
        </form>
    </tr>
</table>