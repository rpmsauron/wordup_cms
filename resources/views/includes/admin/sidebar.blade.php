<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('home') }}">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-home"></i>
            <div class="sidebar-brand-text mx-3">{!! env('APP_NAME') !!}</div>
        </div>
    </a>

    <hr class="sidebar-divider my-0">
    <!-- Nav Item - Dashboard -->
    <li class="nav-item">
        <a class="nav-link" href="{{ route('admin.index') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>

    <hr class="sidebar-divider">
    <!-- Nav Item - Posts -->
    <x-admin.sidebar.posts-links>
    </x-admin.sidebar.posts-links>

    <!-- Nav Item - Categories -->
    <x-admin.sidebar.categories-links>
    </x-admin.sidebar.categories-links>

    <!-- Nav Item - Bookmarks -->
    <x-admin.sidebar.bookmarks-links>
    </x-admin.sidebar.bookmarks-links>
    
    <!-- Nav Item - Suggestions -->
    <x-admin.sidebar.suggestions-links>
    </x-admin.sidebar.suggestions-links>

    @if(iAmAdmin() || me()->hasUsersGranted(Config::get('constants.roles.role.admin.slug')) || iHavePermissionFor(App\User::class))
        <!-- Nav Item - Users -->
        <x-admin.sidebar.users-links>
        </x-admin.sidebar.users-links>
    @endif

    @if(iAmAdmin())
        <!-- Nav Item - Charts -->
        <x-admin.sidebar.charts-links>
        </x-admin.sidebar.charts-links>
    @endif

    @if(iAmAdmin() || iHavePermissionFor([App\Role::class, App\Permission::class]))
        <!-- Nav Item - Permissions and Roles -->
        <x-admin.sidebar.authorization-links>
        </x-admin.sidebar.authorization-links>
    @endif

    @can("merge",App\Category::class)
        <!-- Nav Item - Charts -->
        <x-admin.sidebar.tools-links>
        </x-admin.sidebar.tools-links>
    @endcan

    @if(iAmAdmin() || iHavePermissionFor(App\Contact::class))
        <!-- Nav Item - Contacts -->
        <x-admin.sidebar.contacts-links>
        </x-admin.sidebar.contacts-links>
    @endif

    @if(iAmAdmin() || iAmModerator() || iHavePermissionFor([App\Comment::class, App\Post::class], "review"))
        <!-- Nav Item - Moderator's reviews -->
        <x-admin.sidebar.moderation-links>
        </x-admin.sidebar.moderation-links>
    @endif

    <hr class="sidebar-divider d-none d-md-block">
    <!-- Sidebar Toggler (Sidebar) -->
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>

  </ul>