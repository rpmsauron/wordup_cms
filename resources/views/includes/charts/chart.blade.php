<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.9.3/Chart.min.js" 
    integrity="sha256-R4pqcOYV8lt7snxMQO/HSbVCFRPMdrhAFMH+vr9giYI=" 
    crossorigin="anonymous">
</script>

<script>
    var ctx = document.getElementById('statsChart');
    var statsChart = new Chart(ctx, {
        type: 'bar',
        data: {
            @if(isset($user))
                labels: ['Posts', 'Comments'],
            @elseif(isset($post))
                labels: ['Comments', 'Views'],
            @else
                labels: ['Posts', 'Users', 'Categories', 'Comments', 'Tags'],
            @endif
            datasets: [{
                label: 'Total amount',
                @if(isset($user))
                    data: [{{ $postsCount }}, {{ $commentsCount }}],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(75, 192, 192, 1)',
                    ],
                @elseif(isset($post))
                    data: [{{ $commentsCount }}, {{ $views }}],
                    backgroundColor: [
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(166, 100, 232, 0.2)',
                    ],
                    borderColor: [
                        'rgba(75, 192, 192, 1)',
                        'rgba(166, 100, 232, 1)',
                    ],
                @else
                    data: [{{ $postsCount }}, {{ $usersCount }}, {{ $categoriesCount }}, {{ $commentsCount }}, {{ $tagsCount }}],
                    backgroundColor: [
                        'rgba(255, 99, 132, 0.2)',
                        'rgba(54, 162, 235, 0.2)',
                        'rgba(255, 206, 86, 0.2)',
                        'rgba(75, 192, 192, 0.2)',
                        'rgba(75, 192, 50, 0.2)'
                    ],
                    borderColor: [
                        'rgba(255, 99, 132, 1)',
                        'rgba(54, 162, 235, 1)',
                        'rgba(255, 206, 86, 1)',
                        'rgba(75, 192, 192, 1)',
                        'rgba(75, 192, 50, 1)'
                    ],
                @endif
                borderWidth: 0
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true,
                        stepSize: 1
                    }
                }]
            }
        }
    });
</script>