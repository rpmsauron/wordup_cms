<div class="card my-4">
    <h5 class="card-header">Search Posts</h5>
    <div class="card-body">
        <div class="input-group">
            <form method="get" 
                action="{{ route('post.search') }}" 
                class="input-group">
                @csrf
                <input type="text" 
                    class="form-control" 
                    placeholder="Search for..." 
                    name="keywords">
                <span class="input-group-btn">
                    <button class="btn btn-secondary" 
                        type="submit" 
                        title="Search">
                        <i class="fa fa-search fa-lg"></i>
                    </button>
                </span>
            </form>
        </div>
    </div>
</div>