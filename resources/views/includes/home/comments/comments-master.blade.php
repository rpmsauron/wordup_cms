@include('includes.home.comments.comment-form', ['post' => $post])

<x-flash-messages>
</x-flash-messages>

@include('includes.home.comments.comments-list', ['comments' => $rootCommentsPaginated])

<div class="d-flex mt-4">
    <div class="mx-auto">
        {{ $rootCommentsPaginated->links() }}
    </div>
</div>