@foreach($comments as $comment)
    <!-- Root Comment -->
    @include('includes.home.comments.comment-show', ['comment' => $comment])
    
    <!-- Comment Delete Modal-->
    @include('components.home.modals.comment_delete_modal', ['id' => $comment->id])

    <!-- Comment Report Modal-->
    @include('components.home.modals.comment_report_modal', ['id' => $comment->id])

    @if($comment->hasChildren())
        {{-- recursively include this view, passing in the new collection of comments to iterate --}}
        @include('includes.home.comments.comments-list', ['comments' => $comment->childrenAllowed])
    @endif
@endforeach