<div class="card my-4">
    @auth
        <div class="card-header pull-left">
            <img class="icon-circle" 
                style="float:left;" 
                src="{{ me()->avatar }}" 
                alt="" 
                width="30px">
            <h5 style="margin-top:10px;">&nbsp;{{ me()->name }} says:</h5>
        </div>
        <div class="card-body">
            <form method="post" action="{{ route('comment.store') }}">
                @csrf
                <div class="form-group">
                    <textarea class="form-control" name="body" rows="3"></textarea>
                </div>
                <div>
                    <input name="post_id" type="hidden" value="{{ $post->id }}">
                </div>
                <button type="submit" class="btn btn-primary" title="Comment">
                    <i class="fa fa-reply fa-lg"></i>
                </button>
            </form>
        </div>
    @else
        <div class="card-header pull-left mb-0 pb-2 pt-2 pl-2">
            <form method="post" action="{{ route('comment.store') }}">
                @csrf
                <button class="btn btn-link pr-0" 
                    style="text-decoration: none;" 
                    type="submit">
                    <big>Login to comment.</big>
                </button>
            </form>
        </div>
    @endauth
</div>