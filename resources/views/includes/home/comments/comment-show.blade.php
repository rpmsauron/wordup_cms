@if($comment->hasParent())
    <div class="media ml-5 my-0 mx-0 px-3 py-3" 
         style="background-color:rgb(230, 230, 230);">
        <img class="d-flex mr-3 rounded-circle" 
            src="{{ $comment->user->avatar }}" 
            width="{{ Config::get('constants.image.user.avatar_reply_size') }}px" 
            height="{{ Config::get('constants.image.user.avatar_reply_size') }}px"
            alt="">
@else
    <hr>
    <div class="media mt-3 mb-0 mx-0 px-3 py-3">
        <img class="d-flex mr-3 rounded-circle" 
            src="{{ $comment->user->avatar }}" 
            width="{{ Config::get('constants.image.user.avatar_comment_size') }}px" 
            height="{{ Config::get('constants.image.user.avatar_comment_size') }}px"
            alt="">
@endif

    <div class="media-body container">
        <div class="row">
            <h5 class="mt-0" 
                style="float: right;">
                @if(!$comment->hasParent())
                    @if(iAm($comment->user))
                        <a href="{{ route('user.profile.show', me()->id) }}">You</a> said {{ $comment->created_at->diffForHumans() }}:
                    @else
                        <a href="{{ route('user.profile.show', $comment->user->id) }}">{{ $comment->user->name }}</a> said {{ $comment->created_at->diffForHumans() }}:
                    @endif
                @else
                    @if(iAm($comment->user))
                        <a href="{{ route('user.profile.show', me()->id) }}">You</a> replied to {{ $comment->parent->user->name }} {{ $comment->created_at->diffForHumans() }}:
                    @else
                        @if(iAm($comment->parent->user))
                            <a href="{{ route('user.profile.show', $comment->user->id) }}">{{ $comment->user->name }}</a> replied to you, {{ $comment->created_at->diffForHumans() }}:
                        @else
                            <a href="{{ route('user.profile.show', $comment->user->id) }}">{{ $comment->user->name }}</a> replied to {{ $comment->parent->user->name }}, {{ $comment->created_at->diffForHumans() }}:
                        @endif
                    @endif
                @endif
            </h5>
            @include('includes.home.comments.comments-action-buttons')
        </div>
        <div class="row">
            @if(isset($commentToEdit) && $commentToEdit->id == $comment->id)
                <form method="post" 
                    action="{{ route('comment.update', $comment) }}" 
                    class="flex-fill">
                    @csrf
                    @method('PATCH')

                    <div class="input-group">
                        <textarea class="form-control" 
                            name="body" 
                            rows="1">
                            {{ $commentToEdit->body }}
                        </textarea>
                        <button type="submit" 
                            class="btn btn-primary" 
                            title="Save">
                            <i class="fa fa-check-circle fa-lg"></i>
                        </button>
                    </div>
                </form>
            @else
                <p>{!! $comment->body !!}</p>
            @endif
        </div>
        <div class="row">
            <button type="submit" 
                class="btn d-none" 
                title="Close reply form"
                id="reply_form_hide_button_{{ $comment->id }}"
                onclick="toggleReply({{ $comment->id }})">
                <i class="fa fa-times fa-sm" 
                   id="reply_form_hide_icon_{{ $comment->id }}">
                </i>
            </button>
            @include('includes.home.comments.reply-form', ['comment' => $comment, 'post' => $post])
        </div>
    </div>
</div>