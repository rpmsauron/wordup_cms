@if(Auth::check())
    <div class="ml-auto">    
        <table>
            <tr>
                <td>
                    <button type="submit" 
                        class="btn" 
                        title="Reply"
                        id="reply_form_show_button_{{ $comment->id }}"
                        onclick="toggleReply({{ $comment->id }})">
                        <i class="fa fa-reply fa-sm" 
                           id="reply_form_show_icon_{{ $comment->id }}"></i>
                    </button>
                </td>

                @can("update", $comment)
                    <td>
                        <form action="{{ route('comment.edit', $comment) }}" 
                            method="get">
                            @csrf
                            <button type="submit" 
                                class="btn" 
                                title="Edit">
                                <i class="fa fa-edit fa-sm"></i>
                            </button>
                        </form>
                    </td>
                @endcan

                @can("delete", $comment)
                    <td>
                        <a class="btn" 
                            title="Delete" 
                            href="#" 
                            data-toggle="modal" 
                            data-target="#commentDeleteModal_{{ $comment->id }}">
                            <i class="fa fa-trash fa-sm"></i>
                        </a>
                    </td>
                @endif

                @if(!iAm($comment->user))
                    @if($comment->isReportedBy(me()))
                        <td>
                            <i class="fa fa-exclamation fa-sm" 
                                style="color:gray" 
                                title="You already reported this Comment"></i>
                        </td>
                    @else
                        <td>
                            <a class="btn" 
                                title="Report" 
                                href="#" 
                                data-toggle="modal" 
                                data-target="#commentReportModal_{{ $comment->id }}">
                                <i class="fa fa-exclamation fa-sm"></i>
                            </a>
                        </td>
                    @endif
                @endif
            </tr>
        </table>
    </div>
@endif