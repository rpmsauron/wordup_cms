<div class="flex-fill mt-2 d-none" id="reply_form_{{ $comment->id }}">
    @auth
        <form method="post" action="{{ route('comment.store', $comment) }}" class="flex-fill">
            @csrf
            <div class="input-group ml-4">
                @include('includes.home.comments.reply-form-text-toolbar')
            </div>
            <div class="input-group">
                <img class="rounded-circle mr-2" 
                    style="float:left;" 
                    src="{{ me()->avatar }}" 
                    alt="" 
                    width="{{ Config::get('constants.image.user.avatar_reply_size') }}px"
                    height="{{ Config::get('constants.image.user.avatar_reply_size') }}px">    
                <textarea class="form-control" name="body" rows="1" id="reply_form_textarea_{{ $comment->id }}"></textarea>
                <input name="post_id" type="hidden" value="{{ $post->id }}">
                <button type="submit" class="btn btn-primary" title="Reply">
                    <i class="fa fa-reply fa-lg"></i>
                </button>
            </div>
        </form>
    @else
        <h5 style="margin-top:10px;"><a href="/login">Login</a> to reply.</h5>  
    @endauth
</div>