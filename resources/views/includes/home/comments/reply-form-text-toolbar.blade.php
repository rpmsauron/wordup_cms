<table>
    <tr>
        <td>
            <button class="btn" title="Bold" type="button" onclick="applyTextFormattingTags('{{ $comment->id }}', 'bold');">
                <i class="fas fa-bold"></i>
            </button>
        </td>
        <td>
            <button class="btn" title="Italic" type="button" onclick="applyTextFormattingTags('{{ $comment->id }}', 'italic');">
                <i class="fas fa-italic"></i>
            </button>
        </td>
        <td>
            <button class="btn" title="Small" type="button" onclick="applyTextFormattingTags('{{ $comment->id }}', 'small');">
                <i class="fas fa-search-minus"></i>
            </button>
        </td>
        <td>
            <button class="btn" title="Strikethrough" type="button" onclick="applyTextFormattingTags('{{ $comment->id }}', 'deleted');">
                <i class="fas fa-strikethrough"></i>
            </button>
        </td>
        <td>
            <button class="btn" title="Underlined" type="button" onclick="applyTextFormattingTags('{{ $comment->id }}', 'inserted');">
                <i class="fas fa-underline"></i>
            </button>
        </td>
        <td>
            <button class="btn" title="Subscript" type="button" onclick="applyTextFormattingTags('{{ $comment->id }}', 'subscript');">
                <i class="fas fa-subscript"></i>
            </button>
        </td>
        <td>
            <button class="btn" title="Superscript" type="button" onclick="applyTextFormattingTags('{{ $comment->id }}', 'superscript');">
                <i class="fas fa-superscript"></i>
            </button>
        </td>
    </tr>
</table>