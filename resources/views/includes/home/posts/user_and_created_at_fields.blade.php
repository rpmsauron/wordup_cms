<span>
    <i class="fas fa-clock" title="{{ $post->created_at }}"></i>
    @if($post->created_at)
        Posted {{ $post->created_at->diffForHumans() }} by
    @else
        Posted by
    @endif

    @if(iAm($post->user) && $post->user)
        <a href="{{ route('user.profile.show', $post->user->id) }}">you</a><br>
    @elseif(!iAm($post->user) && $post->user)
        <a href="{{ route('user.profile.show', $post->user->id) }}">{{ $post->user->name }}</a><br>
    @else
        {{ Config::get('constants.fallback_messages.post_without_author') }}<br>
    @endif
</span>