<span>
    @if($post->category && $post->category->is_active == 1)
        @if($post->category->hasIcon())
            This is a '<a href="{{ route('post.filter', $post->category->slug) }}">{{ $post->category->label }}</a>' 
            <img src="{{ $post->category->icon }}" 
                width="{{ Config::get('constants.image.category.icon_size') / 2 }}px"> topic.
        @else
            This is a '<a href="{{ route('post.filter', $post->category->slug) }}">{{ $post->category->label }}</a>' topic.
        @endif
    @elseif($post->category && $post->category->is_active == 0)
        No Category
    @else
        @if($post->suggestedTimesMaxedOut() && !iAmAdmin())
            You have already suggested this Post {{ config("constants.suggestions.post_user_max") }} times. No longer possible.
        @else
            <form method="get" 
                action="{{ route('suggestions.create', $post) }}">
                @csrf

                <div class="form-group">
                    This post does not have a Category.
                    @can("assign", $post)
                        <button type="submit" 
                            class="btn btn-link" 
                            title="Assign">
                            Assign one?
                        </button>
                    @else
                        <button type="submit" 
                            class="btn btn-link" 
                            title="Suggest">
                            Suggest one to author?
                        </button>
                    @endcan
                </div>
            </form>
        @endif
    @endif
 </span>