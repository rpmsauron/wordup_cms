
        <form action="{{ route('post.create') }}" method="get">
            <div class="input-group">
                @csrf
                <input type="hidden" name="from" value="home" class="form-control">
                <label for="new-post" class="form-control border-0" style="background-color:#f8fafc;">
                    <a href="{{ route('post.create') }}" class="text-decoration-none"><h3>Create a new Post!</h3></a>
                </label>
                <span class="input-group-btn">
                    <button type="submit" class="btn btn-primary" name="new-post" title="New Post"><i class="fa fa-plus-circle fa-lg"></i></button>
                </span>
            </div>
        </form>

