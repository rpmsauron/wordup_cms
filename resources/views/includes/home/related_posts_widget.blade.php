@if($relatedPosts->count() > 0)

    <div class="card my-4">
        <h5 class="card-header">You might be interested too in:</h5>
        <div class="card-body">
            <div class="row">
                <div class="col-lg-12">
                    <ul class="list-unstyled mb-0">
                        @foreach($relatedPosts as $relatedPost)
                            <li>
                                <a href="{{ route('post.show', $relatedPost->slug) }}">{{ $relatedPost['title'] }}</a>
                            </li>
                        @endforeach
                     </ul>
                </div>
            </div>
        </div>
    </div>
@endif