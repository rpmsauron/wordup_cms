<nav class="navbar navbar-expand topbar fixed-top navbar-dark bg-dark">

    <div class="container" 
        style='min-height:65px;'>

        <a class="sidebar-brand d-flex align-items-center justify-content-center" 
            style="text-decoration: none;" 
            href="{{ route('home') }}">
            <div class="sidebar-brand-icon rotate-n-15">
                <div class="sidebar-brand-text mx-6 text-white"><h2>{!! env('APP_NAME') !!}</h2></div>
            </div>
        </a>

        <button class="navbar-toggler" 
            type="button" 
            data-toggle="collapse" 
            data-target="#navbarResponsive" 
            aria-controls="navbarResponsive" 
            aria-expanded="false" 
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" 
            id="navbarResponsive">
            <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a class="nav-link" 
                        href="{{ route('home') }}">Home
                        <span class="sr-only">(current)</span>
                    </a>
                </li>

                @if(Auth::check())
                    <li class="nav-item">
                        <a class="nav-link" 
                            href="{{ route('admin.index') }}">Admin</a>
                    </li>
                @else
                    <li class="nav-item">
                        <a class="nav-link" 
                            href="/login">Login</a>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" 
                            href="/register">Register</a>
                    </li>        
                @endif

                <li class="nav-item">
                    <a class="nav-link" 
                        href="{{ route('contact.create') }}">Contact</a>
                </li>
            </ul>
        </div>
    </div>

    @if(Auth::check())
        <!-- Topbar Navbar -->
        <ul class="navbar-nav ml-auto">

            <!-- Nav Item - Notifications -->
            @include('components.admin.top-nav.notifications')

            <!-- Nav Item - Messages -->
            @include('components.admin.top-nav.messages')

            <div class="topbar-divider d-none d-sm-block"></div>

            <!-- Nav Item - User Information -->
            <x-admin.top-nav.admin-top-navbar-user-information>
            </x-admin.top-nav.admin-top-navbar-user-information>
        </ul>
    @endif
</nav>