<div class="card my-4">
    <h5 class="card-header">Categories</h5>
    <div class="card-body">
        <div class="row">
            @if(!$categories)
                <h6>There are no categories yet.</h6>
            @else
                <div class="col-lg-6">
                    <ul class="list-unstyled mb-0">
                        @foreach($categories as $category)
                            @if(count($category->posts))
                                <li>
                                    <a href="{{ route('post.filter', $category->slug) }}">
                                    @if($category->hasIcon())
                                        <img src="{{ $category->icon }}" width="{{ Config::get('constants.image.category.icon_size') / 2 }}px">
                                    @endif
                                    {{ $category['label'] }}</a>
                                </li>

                                @if($loop->index == ceil($loop->count / 2) - 1)
                                        </ul>
                                    </div>
                                    <div class="col-lg-6">
                                        <ul class="list-unstyled mb-0">
                                @endif
                            @endif
                        @endforeach
                     </ul>
                </div>
            @endif
        </div>
    </div>
</div>