<footer class="sticky-footer py-5 bg-dark" style="flex-shrink: 0;">
    <div class="container">
        <p class="m-0 text-center text-white">
            <small>Copyright &copy; </small>
            {!! config('app.name') !!}
            <small> {{ \Carbon\Carbon::now()->year }}</small>
        </p>
    </div>
</footer>