<div class="input-group mt-4 mb-4 captcha">
    <span>{!! captcha_img() !!}</span>
    <button type="button" 
        class="btn btn-primary reload" 
        title="Reload captcha" 
        id="reload">
        <i class="fa fa-redo-alt fa-lg"></i>
    </button>

    <input id="captcha" 
        type="text" 
        class="form-control ml-4" 
        placeholder="{{ config('constants.captcha.label') }}" 
        name="captcha">
</div>
