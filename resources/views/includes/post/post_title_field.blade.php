<h1 class="card-title mt-4 d-flex align-items-center">
    @if(iAm($post->user) || !$showBookmarkFlag)
        <label for="toggle">{{ $post->title }}</label>
    @else
        <form action="{{ route('post.bookmark', $post) }}" 
            method="post">
            @csrf
            @method('PATCH')
            @if($post->isBookmarkedByMe())
                <button class="btn ml-auto" 
                    type="submit" 
                    title="Remove bookmark" 
                    name="toggle">
                    <i class="fas fa-bookmark fa-2x" 
                        style="color:gold;"></i>
            @else
                <button class="btn ml-auto" 
                    type="submit" 
                    title="Bookmark me" 
                    name="toggle">
                    <i class="fas fa-bookmark fa-2x" 
                        style="color:gray;"></i>
            @endif
            </button>
            <label for="toggle">{{ $post->title }}</label>
        </form>
    @endif
</h1>