@section('content')

    <x-flash-messages>
    </x-flash-messages>

    @include('includes.tinyeditor')

    <h1>Create post</h1>

    <div class="row">
        <div class="col-sm-9">
            <form method="post" 
                action="{{ route('post.store') }}" 
                enctype="multipart/form-data">
                @csrf

                <div class="form-group">
                    <label for="title">Title</label>
                    <input type="text" 
                        name="title" 
                        value="{{ old('title') }}"
                        id="title" 
                        placeholder="Enter title" 
                        aria-describedby=""
                        class="form-control
                        @error('title') is-invalid @enderror
                        ">
                    <div>
                        @error('title')
                            <span><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>

                <div class="form-group">
                    <label for="tags">Tags</label><br>
                    <input data-role="tagsinput" 
                        type="text" 
                        name="tags"
                        value="{{ old('tags') }}"
                        id="tags"
                        onchange="validateTagsAmount()"
                        class="input-tags form-control">
                    <div id="warning-tags-limit">{{ Config::get('constants.messages.error.post_tags_limit') }}</div>
                    <div>
                        @error('tags')
                            <span><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>

                <div class="form-group">
                    <div class="mb-2">
                        <button type="button" 
                            class="btn btn-secondary" 
                            style="display:none;" 
                            id="clear-image-icon"
                            title="Clear" 
                            onClick="clearImageField();">
                            <i class="fa fa-times fa-lg"></i>
                        </button>
                        <img src="#"
                            id="image-preview"
                            alt=""
                            class="img-fluid"/>
                    </div>

                    <label class="btn btn-primary" 
                        for="post_image" 
                        onclick="$('#post_image').click();">
                        <input type="file" 
                            name="post_image" 
                            class="d-none"
                            id="post_image">
                        Upload Image
                    </label>
                </div>

                <div class="form-group">
                    <label for="body">Body</label>
                    <textarea name="body" 
                        value="{{ old('body') }}"
                        class="form-control" 
                        id="body" 
                        cols="30"
                        rows="10"
                        class="form-control
                        @error('body') is-invalid @enderror
                        ">
                        </textarea>
                    <div>
                        @error('body')
                            <span><strong>{{ $message }}</strong></span>
                        @enderror
                    </div>
                </div>

                <div class="form-group">
                    <label for="category_id">Category</label>
                    <select name="category_id"
                        class="form-control m-bot15"
                        id="category_id">
                        @if($categories->count())
                            <option selected disabled>Select a category</option>
                            @foreach($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->label }}</option>    
                            @endforeach
                        @endif
                    </select>
                </div>

                @include('includes.captcha_form_field')

                <div class="form-group">
                    <button type="submit" 
                        class="btn btn-primary" 
                        title="Create">
                        <i class="fa fa-check-circle fa-lg"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>

    <div class="row pb-4">
         <div class="col-sm-9">
            <form action="{{ route('admin.index') }}" 
                method="get">
                @csrf
                <button type="submit" 
                    class="btn btn-secondary" 
                    title="Cancel">
                    <i class="fa fa-chevron-circle-left fa-lg"></i>
                </button>
            </form>
        </div>
    </div>

@endsection