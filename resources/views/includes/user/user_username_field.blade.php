@if($searchable->creator->deleted_at)
    {{ $searchable->creator->name }}
@else
    <a href="{{ route('user.profile.show', $searchable->creator) }}">{{ $searchable->creator->name }}</a>
@endif