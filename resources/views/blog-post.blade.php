<x-home-master>
    @section('content')

        <div class="row">
            <div class="col-sm-12 pt-4 post_wrapper">

                @include('includes.post.post_title_field', ['showBookmarkFlag' => true])

                <span class="mr-4">
                    <a href="{{ route('post.charts', $post) }}" 
                        title="View stats">
                        <i class="fa fa-chart-area fa-lg"></i>
                    <a>
                </span>

                <span>
                    @foreach($post->tags as $tag)
                        <a href="{{ route('post.filter.tag', $tag->slug) }}">
                            <label class="post-tag"><strong>{{ strtoupper($tag->name) }}</strong></label>
                        </a>
                    @endforeach
                </span><br>

                @include('includes.home.posts.user_and_created_at_fields')
                <br>
                @include('includes.home.posts.category_field')
                <hr>

                @if($post->hasPostImage())
                    <img class="img-fluid rounded" 
                        src="{{ $post->post_image }}" 
                        alt=""
                        @if(getPostDisplayDimensions($post->post_image)[0] > Config::get('constants.image.post.image_max_width'))
                            width="{{ Config::get('constants.image.post.image_max_width') }}px">
                        @else
                            width="{!! getPostDisplayDimensions($post->post_image)[0] !!}px">
                        @endif
                    <hr>
                @endif

                <p>{!! $post->body !!}</p>
                <hr>

                @if(isset($commentToEdit))
                    @include('includes.home.comments.comments-master', ['commentToEdit' => $commentToEdit])
                @else
                    @include('includes.home.comments.comments-master')
                @endif
            </div>
        </div>

    @endsection

    @section('categories')
        @include('includes.home.categories_widget')
    @endsection

    @section('relatedposts')
        @include('includes.home.related_posts_widget')
    @endsection
</x-home-master>
