<div>

    <h4>Message id #{{ $contact->id }}</h4>
    
    <h3>Subject:</h3>
    <p>{{ $contact->subject }}</p>

    <h3>Original message:</h3>
    <p>{{ $contact->body }}</p>

    <h3>Reply</h3>
    <p>{{ $reply }}</p>
</div>