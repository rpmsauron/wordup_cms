<x-home-master>
    @section('content')

        <x-flash-messages>
        </x-flash-messages>

        <h1>Send us a message here</h1>

        <div class="row">
            <div class="col-sm-9">
                <form method="post" 
                    action="{{ route('contact.store') }}" 
                    id="create-message-form">
                    @csrf

                    @auth
                        <div class="form-group">
                            <label for="email">Sending message as {{ me()->name }}:</label>
                        </div>
                    @endauth
                    
                    @guest
                        <div class="form-group">
                            <label for="email">Your e-mail address</label>
                            <input type="text" 
                                value="{{ old('email') }}"
                                name="email" 
                                id="email" 
                                placeholder="Enter an e-mail" 
                                aria-describedby=""
                                class="form-control
                                @error('email') is-invalid @enderror
                                ">
                            <div>
                                @error('email')
                                    <span><strong>{{ $message }}</strong></span>
                                @enderror
                            </div>
                        </div>
                    @endguest

                    <div class="form-group">
                        <label for="subject">Subject</label>
                        <input type="text" 
                            value="{{ old('subject') }}"
                            name="subject" 
                            id="title" 
                            placeholder="Enter subject" 
                            aria-describedby=""
                            class="form-control
                            @error('subject') is-invalid @enderror
                            ">
                        <div>
                            @error('subject')
                                <span><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="body">Body</label>
                        <textarea name="body" 
                            class="form-control mceNoEditor" 
                            id="body" 
                            cols="30"
                            rows="10"
                            class="form-control
                            @error('body') is-invalid @enderror
                            ">{{ old('body') }}</textarea>
                        <div>
                            @error('body')
                                <span><strong>{{ $message }}</strong></span>
                            @enderror
                        </div>
                    </div>

                    @include("includes.captcha_form_field")

                    <div class="form-group">
                        <button type="submit" 
                            class="btn btn-primary" 
                            title="Send">
                            <i class="fa fa-paper-plane fa-lg"></i>
                        </button>
                    </div>
                </form>
            </div>
        </div>

    @endsection
    
    @section('categories')
        @include('includes.home.categories_widget')
    @endsection
</x-home-master>