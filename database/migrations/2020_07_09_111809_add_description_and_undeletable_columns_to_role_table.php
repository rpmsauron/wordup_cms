<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddDescriptionAndUndeletableColumnsToRoleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->text('description')->nullable();
            $table->boolean('is_undeletable')->default(0);
        });

        DB::table('roles')->whereIn('slug', 
                [
                    config('constants.roles.role.admin.slug'),
                    config('constants.roles.role.moderator.slug'),
                    config('constants.roles.role.author.slug')
                ]
        )->update(
                [
                    'is_undeletable' => 1,
                    'description'    => config('constants.roles.default.description')
                ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('roles', function (Blueprint $table) {
            $table->dropColumn('description');
            $table->dropColumn('is_undeletable');
        });
    }
}
