<?php

use Carbon\Carbon;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJobsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobs', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->string('display_text');
            $table->timestamps();
        });

        DB::table('jobs')->insert(
            [
                [
                    'name'         => config('constants.jobs.clear_tags.name'),
                    'created_at'   => Carbon::now(),
                    'display_text' => config('constants.jobs.clear_tags.display_text')
                ],
                [
                    'name'         => config('constants.jobs.clear_user_avatars.name'),
                    'created_at'   => Carbon::now(),
                    'display_text' => config('constants.jobs.clear_user_avatars.display_text')
                ],
                [
                    'name'         => config('constants.jobs.clear_all_user_avatars.name'),
                    'created_at'   => Carbon::now(),
                    'display_text' => config('constants.jobs.clear_all_user_avatars.display_text')
                ],
                [
                    'name'         => config('constants.jobs.clear_post_images.name'),
                    'created_at'   => Carbon::now(),
                    'display_text' => config('constants.jobs.clear_post_images.display_text')
                ],
                [
                    'name'         => config('constants.jobs.clear_all_post_images.name'),
                    'created_at'   => Carbon::now(),
                    'display_text' => config('constants.jobs.clear_all_post_images.display_text')
                ],
                [
                    'name'         => config('constants.jobs.clear_category_icons.name'),
                    'created_at'   => Carbon::now(),
                    'display_text' => config('constants.jobs.clear_category_icons.display_text')
                ],
                [
                    'name'         => config('constants.jobs.clear_all_category_icons.name'),
                    'created_at'   => Carbon::now(),
                    'display_text' => config('constants.jobs.clear_all_category_icons.display_text')
                ],
                [
                    'name'         => config('constants.jobs.merge_categories.name'),
                    'created_at'   => Carbon::now(),
                    'display_text' => config('constants.jobs.merge_categories.display_text')
                ]
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jobs');
    }
}
