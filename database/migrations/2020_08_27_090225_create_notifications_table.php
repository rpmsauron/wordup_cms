<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNotificationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notifications', function (Blueprint $table) {
            $table->id();
            $table->string('action');
            $table->string('type');
            $table->foreignId('notified_id')->references('id')->on('users')->onDelete('cascade');
            $table->integer('notifiable_id');
            $table->foreignId('notifier_id')->references('id')->on('users')->onDelete('cascade');
            $table->boolean('new')->default(1);
            $table->integer('target_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('notifications');
    }
}
