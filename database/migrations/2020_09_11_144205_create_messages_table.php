<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMessagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('messages', function (Blueprint $table) {
            
            Schema::disableForeignKeyConstraints();
            $table->id();
            $table->integer('reply_to')->unsigned()->nullable()->default(null);
            $table->foreignId('from_id')->references('id')->on('users')->onDelete('cascade');
            $table->foreignId('to_id')->references('id')->on('users')->onDelete('cascade');
            $table->boolean("archived_for_from")->default(0);
            $table->boolean("archived_for_to")->default(0);
            $table->string('subject');
            $table->text('body');
            $table->boolean("new")->default(1);
            $table->timestamps();
            $table->softDeletes();
            $table->foreignId('reply_to', 'id', 'messages')->onDelete('cascade')->change();
            Schema::enableForeignKeyConstraints();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('messages');
    }
}
