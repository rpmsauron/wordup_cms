<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContactsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contacts', function (Blueprint $table) {
            $table->id();
            $table->string('email');
            $table->string('subject');
            $table->text('body');
            $table->boolean("new")->default(1);
            $table->boolean("replied")->default(0);
            $table->timestamps();
            $table->foreignId('replied_by')->nullable()->references('id')->on('users')->onDelete('cascade')->default(null);
            $table->foreignId('from_id')->nullable()->references('id')->on('users')->nullable();
            $table->boolean('with_login');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contacts');
    }
}
