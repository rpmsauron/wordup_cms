<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddModerationColumnsToCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->boolean('reported')->default(0);
            $table->boolean('reviewed')->default(0);
            $table->boolean('allowed')->default(1);
            $table->foreignId('reviewed_by', 'id', 'users')->nullable();
            $table->foreignId('reported_by', 'id', 'users')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('comments', function (Blueprint $table) {
            $table->dropColumn('reported');
            $table->dropColumn('reviewed');
            $table->dropColumn('allowed');
            $table->dropColumn('reviewed_by');
            $table->dropColumn('reported_by');
        });
    }
}
