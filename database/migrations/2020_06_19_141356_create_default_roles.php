<?php

use Illuminate\Database\Migrations\Migration;

class CreateDefaultRoles extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        DB::table('roles')->insert(
            [
                [
                    'name' => config('constants.roles.role.admin.name'),
                    'slug' => config('constants.roles.role.admin.slug'),
                ],
                [
                    'name' => config('constants.roles.role.moderator.name'),
                    'slug' => config('constants.roles.role.moderator.slug'),
                ],
                [
                    'name' => config('constants.roles.role.author.name'),
                    'slug' => config('constants.roles.role.author.slug')
                ]
            ]
        );
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::table('roles')->whereIn('slug', [
            config('constants.roles.role.admin.slug'),
            config('constants.roles.role.moderator.slug'),
            config('constants.roles.role.author.slug')
        ])->delete();
    }
}
