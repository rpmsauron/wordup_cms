<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database:
     *
     * Creates five users;
     *
     * Creates three posts per user (total of fifteen);
     *
     * Creates randomly between 1 and 10 tags per post (total of fifteen to one hundred and fifty);
     *
     * Creates twelve root comments per post (total of one hundred and eighty);
     *
     * Creates two replies per each root comment (total of three hundred and sixty replies, 
     * with a total of five hundred and forty); 
     *
     * Assigns created Comments randomly to created Users by user_id foreign_key field reference;
     *
     * Creates ten categories;
     *
     * @return void
     */
    public function run()
    {
        $userIdRefs  = []; // stores User id references for later assignment to Comments by 'user_id' field.
        $commentRefs = []; // stores Comment references for later assignment of 'user_id' field.
        
        // Creates users.
        factory('App\User', 5)->create()->each(function($user) use(&$userIdRefs, &$commentRefs) {

            // save for reference for further shuffling
            array_push($userIdRefs, $user->id);

            // Creates posts.
            $posts = factory('App\Post', 3)->create()->each(function($post) use (&$commentRefs) {
                $post = &$post;

                // Creates tags.
                $faker = Faker::create();
                $tags = [];
                for($i = 0; $i < mt_rand(1, 10); $i++)
                {
                    array_push($tags, ucfirst($faker->unique()->word(3)));
                }

                // Creates root comments.
                $rootComments = factory('App\Comment', 12)->create()->each(function($rootComment) use(&$post, &$commentRefs) {

                    // save for reference for further shuffling
                    array_push($commentRefs, $rootComment);

                    // Creates replies.
                    $replies = factory('App\Comment', 2)->create()->each(function($reply) use (&$commentRefs) {
                        // save for reference for further shuffling
                        array_push($commentRefs, $reply);
                    });
                    $rootComment->children()->saveMany($replies);
                    $post->comments()->saveMany($replies);
                });
                $post->comments()->saveMany($rootComments);
                $post->tag($tags);
            });
            $user->posts()->saveMany($posts);
        });

        // Assigns authorship of Comments randomly among created Users
        foreach($commentRefs as $commentRef)
        {
            $commentRef->user_id = $userIdRefs[array_rand($userIdRefs)];
            $commentRef->save();
        }

        // Creates categories.
        factory('App\Category', 10)->create();
    }
}
