<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Comment;
use Faker\Generator as Faker;

$factory->define(Comment::class, function (Faker $faker) {
    return [
        'parent_id' => null,
        'user_id'  => 1,
        'post_id'  => 1,
        'body'     => $faker->paragraph()
    ];
});

