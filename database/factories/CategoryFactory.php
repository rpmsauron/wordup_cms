<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Category;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

$factory->define(Category::class, function (Faker $faker) {
    // Randomly generates capitalised and non-capitalised Category names.
    $randomiser = mt_rand(-1, 1);
    if($randomiser < 0)
    {
        $label = Str::ucfirst($faker->word());
    } else
    {
        $label = $faker->word();
    }
    
    return [
        'label'      => $label,
        'created_by' => 1,
        'is_active'  => 1
    ];
});

