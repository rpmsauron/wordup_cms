/**
 * Toggles between showing or hiding the reply button.
 *
 * @param  int id The suffix id of html element.
 *
 * @return void
 */
function toggleReply(id)
{
    $('#reply_form_' + id).toggleClass('d-none d-flex');
    $('#reply_form_hide_button_' + id).toggleClass('d-none');
    $('#reply_form_show_button_' + id).toggleClass('d-none');
}

/**
 * Automatically submits form of the filtering radio button on listing Users on Dashboard, 
 * that decides what type of Users to show.
 *
 * @return void
 */
function autoSubmitUsers()
{
    var formObject = document.forms['usersFilter'];
    formObject.submit();
}

/**
 * Automatically submits form of the filtering radio button on listing Posts on Dashboard, 
 * that decides what type of Posts to show.
 *
 * @return void
 */
function autoSubmitPosts()
{
    var formObject = document.forms['postsFilter'];
    formObject.submit();
}

/**
 * Automatically submits form of the filtering radio button on listing Messages on Dashboard, 
 * that decides what type of Messages to show.
 *
 * @return void
 */
function autoSubmitMessages()
{
    var formObject = document.forms['messagesFilter'];
    formObject.submit();
}

/**
 * Counts the Tags added into form input field.
 *
 * @return int length The amount of tags in the form input field.
 */
function countTags()
{
    return $('div.bootstrap-tagsinput > span').length;
}

/**
 * Validates if number of Tags added into form input field does not exceed maximum so far.
 * If it does, hides the input field to not write anything else into it.
 *
 * @return void
 */
function validateTagsAmount()
{
    if (countTags() < 10)
    {
        $('div.bootstrap-tagsinput > input').last().show();
    } else
    {
        $('div.bootstrap-tagsinput > input').last().hide();
    }
}

/**
 * Inserts or removes a given text-formatting HTML tag to selected text on a specific HTML textarea.
 *
 * @param  int    id     The suffix id of html element.
 * @param  string format The textual key string representing the formatting.
 *
 * @return void
 */
function applyTextFormattingTags(id, format)
{
    var elem = $("#reply_form_textarea_" + id);

    if (format === "bold")
    {
        var tag = "b";
    } else if (format === "italic")
    {
        var tag = "i";
    } else if (format === "small")
    {
        var tag = "sm";
    } else if (format === "deleted")
    {
        var tag = "del";
    } else if (format === "inserted")
    {
        var tag = "ins";
    } else if (format === "subscript")
    {
        var tag = "sub";
    } else if (format === "superscript")
    {
        var tag = "sup";
    }

    var tagOpen = "<" + tag + ">";
    var tagClose = "</" + tag + ">";
    var tagLength = tagOpen.length - 2;

    // Remove tag.
    if (getSelectedText(id).startsWith(tagOpen) && getSelectedText(id).endsWith(tagClose))
    {
        var newVal = elem.val().substring(0, getSelectionStart(id))
                + elem.val().substring(getSelectionStart(id) + tagLength + 2, getSelectionEnd(id) - (tagLength + 3))
                + elem.val().substring(getSelectionEnd(id), elem.val().length);
        elem.val(newVal);
    }
    // Add tag.
    else
    {
        var newVal = elem.val().substring(0, getSelectionStart(id)) + tagOpen
                + elem.val().substring(getSelectionStart(id), getSelectionEnd(id)) + tagClose
                + elem.val().substring(getSelectionEnd(id), elem.val().length);
        elem.val(newVal);
    }
}


/**
 * Commment text basic formatting for detecting selected text.
 * ----------------------------------------------------------
 */

/**
 * Gets the selected text on a specific HTML textarea.
 *
 * @param  int id The suffix id of html element.
 *
 * @return string The substring text starting where the selection begins and ending 
 *                where the selection ends.
 */
function getSelectedText(id)
{
    var elem = $("#reply_form_textarea_" + id);
    return (elem.val().substring(elem.prop('selectionStart'), elem.prop('selectionEnd')));
}

/**
 * Gets the starting index for the selected text on a specific HTML textarea.
 *
 * @param  int id The suffix id of html element.
 *
 * @return int The index where the selected text starts.
 */
function getSelectionStart(id)
{
    var elem = $("#reply_form_textarea_" + id);
    return elem.prop('selectionStart');
}

/**
 * Gets the ending index for the selected text on a specific HTML textarea.
 *
 * @param  int id The suffix id of html element.
 *
 * @return int The index where the text selection ends.
 */
function getSelectionEnd(id)
{
    var elem = $("#reply_form_textarea_" + id);
    return elem.prop('selectionEnd');
}


/**
 * Form 'file' input fields placeholder functions for Posts' and Users' images.
 * ----------------------------------------------------------------------------
 */

/**
 * Clears image file for Post's image, User's avatar, or Category's icon, 
 * from form's 'file' input field; hides the preview and the button that clears it along.
 * Used both on create and edit forms for specified models.
 *
 * @return void
 */
function clearImageField()
{
    $("#post_image").val('');
    $("#user_image").val('');
    $("#category_image").val('');
    $("#image-preview").hide();
    $("#clear-image-icon").hide();

    // Add info to clear the image field, only for 'edit' form action (not for 'create'):
    var form = $("form#update");
    if (form.length)
    {
        var formAction = form.attr('action');
        // Append to already existing query params
        if (formAction.indexOf("?") !== -1)
        {
            form.attr('action', formAction + "&image=clear");
        }
        // Append as first query param
        else
        {
            form.attr('action', formAction + "?image=clear");
        }
        // Added parameter does not need to be removed for newly inserted file on form field, 
        // since the param is only taken into consideration when the form field has no file in it.
    }
}


/**
 * Reads resource location for temporary file location sent through form's 'file' input field.
 *
 * @param {type} input
 *
 * @return void
 */
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#image-preview').attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]); // convert to base64 string
    }
}

$("#post_image").change(function () {
    readURL(this);
    $("#image-preview").show();
    $("#clear-image-icon").show();
});

$("#user_image").change(function () {
    readURL(this);
    $("#image-preview").show();
    $("#clear-image-icon").show();
});

$("#category_image").change(function () {
    readURL(this);
    $("#image-preview").show();
    $("#clear-image-icon").show();
});


/**
 * Form functionalities for Category merging.
 * ------------------------------------------
 */

/**
 * Handles button working or not working for Cleanup Category Merge functionality, 
 * depending on whether there are checkboxes selected and the new merged Category label
 * form field is filled correctly or not (minimum of three characters length string.
 *
 * @return void
 */
function handleMergeButton()
{
    var selectedCheckboxes = $('input:checked[type="checkbox"][name="categories[]"]');
    var button = $("#category_merge_button");

    /**
     * Change the button to become functional, if there are at least 2 selected ones and the label 
     * input field is filled with text with a min-length of 3 characters.
     */
    if (selectedCheckboxes.length >= 2 && $("#merged-label-reference").val().length >= 3)
    {
        button.attr("disabled", false);

        if (!button.is("[data-target]"))
        {
            button.attr("data-target", "#cleanupCategoryMergeModal");
        }

        if (!button.hasClass("btn-primary"))
        {
            button.addClass("btn-primary");
        }

        button.removeClass("btn-secondary");
    } else
    {
        /**
         * No checked checkboxes in the form (or just one) or not-correctly filled form input field 
         * for label makes the button become unfunctional again.
         */
        if (selectedCheckboxes.length < 2 || $("#merged-label-reference").val().length < 3)
        {
            button.attr("disabled", true);
            button.removeClass("btn-primary");
            button.removeAttr("data-target");
            button.addClass("btn-secondary");
        }
    }
}

// On writing / deleting from new merged Category label form field, either activates or deactivates the merge button.
$("input#merged-label-reference").on("input", function () {
    handleMergeButton();
});

// On checking / unchecking checkboxes for Categories, either activates or deactivates the merge button.
$('input[type="checkbox"][name="categories[]"]').click(function () {
    handleMergeButton();
});

/**
 * Checkbox to select / unselect all checkboxes for form for Cleanup Category merge.
 * Produces the actual click events that check / uncheck them, instead of just changing 
 * the "checked" attribute, in order to fire others events dependant on it.
 */
$("#select_all_categories").change(function () {
    var checkboxesChecked = $('input:checked[type="checkbox"][name="categories[]"]');
    var checkboxesUnchecked = $('input:not(:checked)[type="checkbox"][name="categories[]"]');

    if ($(this).is(":checked"))
    {
        checkboxesUnchecked.click();
    } else
    {
        checkboxesChecked.click();
    }

    handleMergeButton();
});

/**
 * Reads values of selected categories (by checkboxes) from outside the form.
 * To be used to actually transfer the values onto the form.
 *
 * @return array The labels of the selected Categories.
 */
function readSelectedCategories()
{
    return $('input:checked[type="checkbox"][name="categories[]"]').map(function () {
        return $(this).val();
    }).toArray();
}

/**
 * Reads value of new merged category label (from input field) from outside the form.
 * To be used to actually transfer the values onto the form.
 *
 * @return string The new merged Category label.
 */
function readMergedCategoryLabel()
{
    return $('input[type="text"][name="label"][id="merged-label-reference"]').val();
}

/**
 * Reads values of selected categories (by checkboxes), and of label for new merged category (from input field) 
 * from outside the form onto it, in hidden fields.
 * Needed to fetch previously filled values for form before triggering confirmation Modal, where the actual form is.
 *
 * @return void
 */
function readCategoryMergeValues()
{
    var categories = readSelectedCategories();
    var newLabel   = readMergedCategoryLabel();
    $('input[type="hidden"][name="categories[]"]').prop("value", categories);
    $('input[type="hidden"][name="label"]').prop("value", newLabel);
    // Change Modal header to refer the new merged Label.
    $('#category-merge-modal-title').text("Merge selected Categories into '" + newLabel + "'");
}

/**
 * Adds icon previews with radio button by side, to the confirmation modal on the DOM, 
 * when two or more categories with images have been selected from the category selection form.
 *
 * @param  integer id The id to select the html elements with.
 *
 * @return void
 */
function addPreviewIconToPrompt(id)
{
    var category = $("tr#category-row-" + id);
    if (category.length > 0)
    {
        var iconSelectionPreviewList = $("#icon-selection-preview-list");
        var input = $("input#category-select-" + id);

        var icon = $("img#category-icon-" + id);

        /**
         * If respective category is selected, it has an icon, and its preview icon is not yet added.
         */
        if (input.is(":checked") && icon.length > 0 && $("li#category-icon-preview-" + id).length === 0)
        {
            /**
             * Appending the image previews and radio buttons here are for visual purpose only, they will not work on the form for
             * unknown reasons (may be some DOM issue). It would work only by appending the input fields directly to the form tag itself, 
             * and not to elements inside it.
             * That, however, would not let the styling and presentation of the form to be under control.
             * The event below is called upon submitting the form to place the html inputs structurally inside the form directly, where
             * they will work, and since it is only changed on subbmitting the form, it will not present the changes visually.
             */
            iconSelectionPreviewList.append($(
                    "<li id='category-icon-preview-" + id + "' class='mb-2 list-inline-item'>" +
                    "<img src='" + icon.prop("src") + "' alt='' width='45px' class='mr-2'>" +
                    "<input type='radio' name='ids[]' value='" + id + "' id='id-" + id + "' class='mr-2' checked>" +
                    "</li>"
                    ));
        } else
        {
            $("li#category-icon-preview-" + id).detach();
            $("#icon-selection-preview").hide();
        }

        // Only show merge panel if at least two selected choices have icons.
        if ($("li[id^=category-icon-preview-]").length >= 2)
        {
            $("#icon-selection-preview").show();
        }
    }
}

/**
 * Change the place of the input radio button tags in the html structure, to be direct children 
 * to the form element. Otherwise, for unknown reason (may be some DOM issue: see function 
 * addPreviewIconToPrompt() above) these fields will not be submitted on the form POST request.
 * Read more at:
 * https://www.sitepoint.com/community/t/form-does-not-submit-jquery-added-input-fields/26485/6
 */
$("#merge-submit").click(function () {
    $('#icon-selection-preview-list').find("input[type='radio'][id^='id-']").each(function () {
        $('#merge-form').append($(this));
    });
    $('#icon-selection-preview-list').empty();
});



/**
 * Dashboard Category icon cleanup: icon previewing on hover on thumbnail.
 * -----------------------------------------------------------------------
 */

$("img[id^='icon-preview-id-']").mouseenter(function () {
    // Get the scope identifier, which resolves colliding html id elements (from modals present for different Model instances).
    var scope = $("div[id^='cleanup'][style*='display: block']").attr("name");
    var orphanPath = $(this).attr("src");
    // Index of file.
    var index = $(this).attr("name");

    $("#icon-preview-" + index + " > img").prop("src", orphanPath);
    $("#icon-preview-" + index).show();
    // Extend modal wrapping to fit the listing of files with the preview of the image along.
    $("#icons-modal-content-" + scope).attr('style', 'height: fit-content');
})

$("div[id^='icon-wrapper-']").mouseleave(function () {
    // Get the scope identifier, which resolves colliding html id elements (from modals present for different Model instances).
    var scope = $("div[id^='cleanup'][style*='display: block']").attr("name");
    // Index of file.
    var index = $(this).attr("name");

    $("#icon-preview-" + index + " > img").prop("src", "");
    $("#icon-preview-" + index).hide();
    // Collapse modal wrapping back to fit just the listing of files.
    $("#icons-modal-content-" + scope).attr('height', 'auto');
});



/**
 * Home Category assigning.
 * ------------------------
 */

/**
 * Handles button working or not working for Category assigning functionality on Home, 
 * depending on whether there is any radio button selected.
 *
 * @return {undefined}
 */
function handleCategoryConfirmButton()
{
    var selectedRadio = $('input:checked[type="radio"][name="category[]"]');
    var button = $("#confirm-category-button");

    if (selectedRadio.length === 1)
    {
        button.attr("disabled", false);

        if (!button.hasClass("btn-primary"))
        {
            button.addClass("btn-primary");
        }

        button.removeClass("btn-secondary");
    } else
    {
        button.attr("disabled", true);
        button.removeClass("btn-primary");
        button.addClass("btn-secondary");
    }
}

// On checking / unchecking radio buttons for Categories, either activates or deactivates the confirm button.
$('input[type="radio"][name="category[]"]').click(function () {
    handleCategoryConfirmButton();
});


/**
 * Clickable table rows for admin.
 * Used for Notifications listing.
 * ------------------------------
 */

jQuery(document).ready(
    function ($)
    {
        $(".clickable-row").click(
            function ()
            {
                window.location = $(this).data("href");
            }
        );
    }
);


/**
 * Draggable Notifications and Messages modals on Dashboard.
 */

// Make the DIV elements draggable:
if(document.getElementById("notifications-modal") !== null)
{
    dragElement(document.getElementById("notifications-modal"));
}
if(document.getElementById("messages-modal") !== null)
{
    dragElement(document.getElementById("messages-modal"));
}

function dragElement(elmnt)
{
  var pos1 = 0, pos2 = 0, pos3 = 0, pos4 = 0;
  if (document.getElementById(elmnt.id + "-header"))
  {
    // if present, the header is where you move the DIV from:
    document.getElementById(elmnt.id + "-header").onmousedown = dragMouseDown;
  } else
  {
    // otherwise, move the DIV from anywhere inside the DIV:
    elmnt.onmousedown = dragMouseDown;
  }

  function dragMouseDown(e)
  {
    e = e || window.event;
    e.preventDefault();
    // get the mouse cursor position at startup:
    pos3                 = e.clientX;
    pos4                 = e.clientY;
    document.onmouseup   = closeDragElement;
    // call a function whenever the cursor moves:
    document.onmousemove = elementDrag;
  }

  function elementDrag(e)
  {
    e = e || window.event;
    e.preventDefault();
    // calculate the new cursor position:
    pos1             = pos3 - e.clientX;
    pos2             = pos4 - e.clientY;
    pos3             = e.clientX;
    pos4             = e.clientY;
    // set the element's new position:
    elmnt.style.top  = (elmnt.offsetTop - pos2) + "px";
    elmnt.style.left = (elmnt.offsetLeft - pos1) + "px";
  }

  function closeDragElement()
  {
    // stop moving when mouse button is released:
    document.onmouseup   = null;
    document.onmousemove = null;
  }
}


/**
 * Captcha service AJAX calls.
 */
$('#reload').click(
    function()
    {
        $.ajax({
            type:    'GET',
            url:     '/reload-captcha',
            success: function (data)
            {
                $(".captcha span").html(data.captcha);
            }
        });
    }
);
