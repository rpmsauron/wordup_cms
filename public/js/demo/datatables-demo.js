// Call the dataTables jQuery plugin
$(document).ready(function() {
  $('#dataTable').DataTable();
  $('#usersTable').DataTable();
  $('#permissionsPerRoleTable').DataTable();
  $('#rolesTable').DataTable();
  $('#rolesPerPermissionsTable').DataTable();
  $('#postsPerUserTable').DataTable();
  $('#categoriesTable').DataTable();
  $('#postsPerCategoryTable').DataTable();
  $('#rolesPerUserTable').DataTable();
  $('#permissionsPerUserTable').DataTable();
  $('#postsTable').DataTable();
  $('#bookmarksTable').DataTable();
  $('#suggestionsGivenTable').DataTable();
  $('#suggestionsReceivedTable').DataTable();
  $('#notificationsTable').DataTable();
  $('#commentsTable').DataTable();
  $('#messagesTable').DataTable();
  $('#contactsTable').DataTable();
});
