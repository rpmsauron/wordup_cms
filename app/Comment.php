<?php

namespace App;

use App\SearchUtils;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Comment extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'parent_id', 
        'user_id', 
        'body', 
        'post_id',
        'reported',
        'reviewed',
        'allowed',
        'reviewer_id',
        'updated_by'
    ];

    /**
     * Returns the User which created this Comment.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Returns the User which created this Comment.
     * An alias function to user().
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->user();
    }

    /**
     * Returns the User which created this Comment.
     * An alias function to user().
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->user();
    }

    /**
     * Returns the User which last updated this Comment.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updater()
    {
        return $this->belongsTo(User::class, "updated_by");
    }

    /**
     * Returns the parent Comment of this Comment (being this one thus a Reply).
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Comment::class, "parent_id");
    }

    /**
     * Returns the Post which this Comment is for.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    /**
     * Returns the children Comments to this Comment (Replies).
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class, 'parent_id');
    }

    /**
     * Returns the children Comments to this Comment (Replies).
     * An alias function to comments().
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->comments();
    }

    /**
     * Returns the children Comments to this Comment (Replies).
     * An alias function to comments().
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function replies()
    {
        return $this->comments();
    }

    /**
     * Returns the children Comments to this Comment (Replies) that are allowed 
     * (regardless of their reported and reviewed status).
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commentsAllowed()
    {
        return $this->hasMany(Comment::class, 'parent_id')
                ->where("allowed", 1);
    }

    /**
     * Returns the children Comments to this Comment (Replies) that are allowed 
     * (regardless of their reported and reviewed status).
     * An alias function to commentsAllowed().
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function childrenAllowed()
    {
        return $this->commentsAllowed();
    }

    /**
     * Returns the children Comments to this Comment (Replies) that are allowed 
     * (regardless of their reported and reviewed status).
     * An alias function to commentsAllowed().
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function repliesAllowed()
    {
        return $this->commentsAllowed();
    }

    /**
     * Verifies if this Comment has children Comments (replies).
     *
     * @return bool Returns true if it has replies, or false otherwise.
     */
    public function hasChildren()
    {
        return $this->comments()->count() > 0;
    }

    /**
     * Verifies if this Comment has a parent Comment (if this is a root Comment).
     *
     * @return bool Returns true if it has a parent, or false otherwise.
     */
    public function hasParent()
    {
        return $this->attributes['parent_id'] != null;
    }

    /**
     * Returns the User which reviewed this Comment.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reviewer()
    {
        return $this->belongsTo(User::class, "reviewed_by");
    }

    /**
     * Returns the User which reviewed this Comment.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reporter()
    {
        return $this->belongsTo(User::class, "reported_by");
    }

    /**
     * Returns the User which last deleted this Comment.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deleter()
    {
        return $this->belongsTo(User::class, "deleted_by");
    }

    /**
     * Returns the User which last deleted this Comment.
     * An alias function to deleter()
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function remover()
    {
        return $this->deleter();
    }

    /**
     * Returns the User which last restored this Comment from deletion.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restorer()
    {
        return $this->belongsTo(User::class, "restored_by");
    }

    /**
     * Returns the User which last restored this Comment from deletion.
     * An alias function to restorer()
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recoverer()
    {
        return $this->restorer();
    }

    /**
     * Searches Comments matching the received keywords as parameters.
     *
     * @param  array $keywords The keywords to search based on.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function search(array $keywords)
    {
        $mandatoryKeywords = SearchUtils::getMandatoryKeywords($keywords);
        $optionalKeywords  = SearchUtils::getOptionalKeywords($keywords);
        $excludedKeywords  = SearchUtils::getExcludedKeywords($keywords);
        $fields            = ["body"];

        $queryBuilder = Comment::where(

        // Append the optional keywords (not using the 'include' operator).
            function($query) use ($optionalKeywords, $fields)
            {
                foreach($optionalKeywords as $keyword)
                {
                    SearchUtils::match($query, "comments", $keyword, $fields);
                }
            }
        )

        // Append the mandatory keywords from using the 'include' operator.
        ->where(
            function($query) use ($mandatoryKeywords, $fields)
            {
                foreach($mandatoryKeywords as $keyword)
                {
                    $query->where(
                        function($query) use ($keyword, $fields)
                        {
                            SearchUtils::match($query, "comments", $keyword, $fields);
                        }
                    );
                }
            }
        )

        // Append the excluded keywords from using the 'exclude' operator.
        ->where(
            function($query) use ($excludedKeywords, $fields)
            {
                foreach($excludedKeywords as $keyword)
                {
                    SearchUtils::notMatch($query, "comments", $keyword, $fields);
                }
            }
        )

        // Filter on Comment ownership or permissive Role.
        ->where(
            function($query)
            {
                // Only own and allowed Comments, otherwise, by default, 
                // everyone's Comments regardless of their acceptance status.
                if(!me()->can("manageEveryones", Comment::class))
                {
                    $query->where('comments.user_id', me()->id)
                        ->where('comments.allowed', 1);
                }
            }
        )

        /**
         * Only Comments on reviewed and accepted Posts.
         * Comments on not 'reviewed' Posts should never occur on the application 
         * flow, it is only a protection against just in case a manual edit on the 
         * database, by setting of existing Post with Comments already, to not 
         * 'reviewed'.
         */
        ->join("posts", "posts.id", "=", "comments.post_id")
            ->where("posts.reviewed", 1)
            ->where("posts.allowed", 1)
            ->whereNotNull("posts.reviewed_by");

        return $queryBuilder;
    }

    /**
     * Verifies if this Comment has been previously reported by a given User.
     *
     * @param  \App\User $user The User to verify if has reported this Comment.
     *
     * @return bool      Returns true if this Comment has been reported by the 
     *                   given User, false otherwise.
     */
    public function isReportedBy(User $user)
    {
        return Comment::where('reported', 1)
            ->where('reported_by', $user->id)
            ->where('id', $this->attributes['id'])
            ->count() > 0;
    }

    /**
     * Verifies if this Comment was last updated by someone else as moderation.
     *
     * @return bool Returns true is this Comment was last updated by some other 
     *              User other than the one that created it, false otherwise.
     */
    public function isChangedByModeration()
    {
        return $this->attributes['updated_by'] != null
                && $this->attributes['id'] != $this->attributes['updated_by'];
    }
}
