<?php

namespace App;

use App\SearchUtils;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use \Conner\Tagging\Taggable;
use Askedio\SoftCascade\Traits\SoftCascadeTrait;

class Post extends Model
{
    use Sluggable;
    use SoftDeletes;
    use Taggable;
    use SoftCascadeTrait;

    protected $softCascade = [
        'bookmarks'
    ];

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array The sluggable configuration for model 'Post'.
     */
    public function sluggable()
    {
        return [
            'slug'         => [
                'source'   => 'title',
                'onUpdate' => true
            ]
        ];
    }

    protected $dates    = [
        'deleted_at'
    ];

    protected $fillable = [
        'user_id', 
        'title', 
        'body', 
        'post_image', 
        'slug', 
        'category_id',
        'reviewed',
        'allowed',
        'reviewer_id',
        'updated_by',
        'assigned_category_by'
    ];

    /**
     * Returns the User which created this Post.
     * Relation needs to be kept to show name of Post creator even if the User 
     * account itself no longer exists.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class)
                ->withTrashed();
    }

    /**
     * Returns the User which created this Post.
     * An alias function to user().
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->user();
    }

    /**
     * Returns the User which created this Post.
     * An alias function to user().
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function author()
    {
        return $this->user();
    }

    /**
     * Returns the User which last updated this Post.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updater()
    {
        return $this->belongsTo(User::class, "updated_by");
    }

    /**
     * Returns the User which last assigned this Post to its current Category.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function assigner()
    {
        return $this->belongsTo(User::class, "assigned_category_by");
    }

    /**
     * Returns the User which last assigned this Post to its current Category.
     * An alias function to assigner().
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function categorizer()
    {
        return $this->assigner();
    }

    /**
     * Returns the Category of this Post.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Returns the Bookmarks to this Post.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookmarks()
    {
        return $this->hasMany(Bookmark::class);
    }

    /**
     * Returns the Suggestions for this Post.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function suggestions()
    {
        return $this->hasMany(Suggestion::class);
    }

    /**
     * Returns the User which reviewed this Post.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function reviewer()
    {
        return $this->belongsTo(User::class, "reviewed_by");
    }

    /**
     * Returns the User which last deleted this Post.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function deleter()
    {
        return $this->belongsTo(User::class, "deleted_by");
    }

    /**
     * Returns the User which last deleted this Post.
     * An alias function to deleter()
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function remover()
    {
        return $this->deleter();
    }

    /**
     * Returns the User which last restored this Post from deletion.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function restorer()
    {
        return $this->belongsTo(User::class, "restored_by");
    }

    /**
     * Returns the User which last restored this Post from deletion.
     * An alias function to restorer()
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function recoverer()
    {
        return $this->restorer();
    }

    /**
     * Returns the Views of this Post.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function views()
    {
        return $this->hasMany(View::class);
    }

    /**
     * Returns the Comments to this Post.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * Returns the Comments to this Post that are allowed (regardless of their 
     * reported and reviewed status).
     *
     * @return \Illuminate\Database\Eloquent\Builder The Query Builder to get the 
     *                                               allowed Comments.
     */
    public function commentsAllowed()
    {
        return $this->hasMany(Comment::class)
                ->where("allowed", 1);
    }

    /**
     * Returns the root Comments to this Post (the ones without a parent Comment).
     *
     * @return Illuminate\Database\Eloquent\Collection The Eloquent Collection of 
     *                                                 root Comments for this Post.
     */
    public function rootComments()
    {
        return $this->commentsAllowed()
                ->whereNull('parent_id')
                ->where('post_id', $this->attributes['id'])
                ->orderBy('created_at', 'desc')
                ->get();
    }

    /**
     * Returns the root Comments to this Post (the ones without a parent Comment) 
     * with pagination.
     *
     * @return Illuminate\Pagination\LengthAwarePaginator The Paginator of root 
     *                                                    Comments for this Post.
     */
    public function rootCommentsPaginated()
    {
        return $this->commentsAllowed()
                ->whereNull('parent_id')
                ->where('post_id', $this->attributes['id'])
                ->orderBy('created_at', 'desc')
                ->paginate(config('constants.pagination.comments_entries'));
    }

    /**
     * Accessor method that returns the 'post_image' attribute of this Post with 
     * relative path.
     *
     * @param  string $value The attribute value to access.
     *
     * @return string The modified 'post_image' attribute of this Post.
     */
    public function getPostImageAttribute($value)
    {
        return asset($value);
    }

    /**
     * Returns the 'post_image' filename from server, of this Post.
     *
     * @return string The server path of the 'post_image' filename.
     */
    public function getPostImageServerFile()
    {
        return public_path() . $this->attributes['post_image'];
    }

    /**
     * Returns a default placeholder image path for Posts that do not have a 
     * post_image, to default back to.
     *
     * @return string The url of the default placeholder image.
     */
    public function postImagePlaceholder()
    {
        return "https://via.placeholder.com/900x300";
    }

    /**
     * Verifies is this Post has a value for 'post_image' attribute.
     *
     * @return bool Returns true is it has a value, false otherwise.
     */
    public function hasPostImage()
    {
        return $this->attributes['post_image'] != null;
    }

    /**
     * @todo BETA: should be replace to full scale html parser.
     * Verifies that the 'body' field has html-format tags.
     * 
     * @return bool Returns true if it has html-format tags, or false otherwise.
     */
    public function hasHtmlbody()
    {
        return preg_match("/\/[a-z]*>/i", $this->attributes['body']) != 0;
    }

    /**
     * Gets all related Posts, where the related definition is the Posts sharing 
     * one or more Tags.
     *
     * @return Illuminate\Database\Eloquent\Collection The Eloquent Collection of 
     *                                                 related Posts (not RDBM-scope 
     *                                                 Relations, application domain-
     *                                                 scope Relations between Posts 
     *                                                 based on content.
     */
    public function getRelated()
    {
        $tags = $this->tagNames();

        return Post::where('id', '<>', $this->attributes['id'])
                ->where(
                    function($query) use ($tags)
                    {
                        $query->withAnyTag($tags)
                            ->orWhere('category_id', $this->attributes['category_id']);
                    }
                )
                ->inRandomOrder()
                ->take(config('constants.limits.home.related'))->get();
    }

    /**
     * Verifies if this Post has a Bookmark entry for the session logged-in User.
     * 
     * @return bool Returns true if there is a Bookmark entry, false if there 
     *              is not or if there is no logged in User either.
     */
    public function isBookmarkedByMe()
    {
        return loggedIn() ? 
                 Bookmark::where('user_id', me()->id)
                    ->where('post_id', $this->attributes['id'])
                    ->count() > 0 
                : false;
    }

    /**
     * Gets the Bookmark entry for this Post for the session logged-in User. 
     * If there is no logged-in User, returns null.
     *
     * @return null|App\Bookmark The Bookmark for this Post for logged-in User.
     */
    public function getBookmark()
    {
        return loggedIn() ? 
                Bookmark::where('user_id', me()->id)
                    ->where('post_id', $this->attributes['id'])
                    ->first() 
                : null;
    }

    /**
     * Gets the Bookmark entry for this Post for the session logged-in User, 
     * if it is in the Trash. 
     * If there is no logged-in User nor trashed Bookmark, returns null.
     *
     * @return null|App\Bookmark The trashed Bookmark for this Post for logged-in 
     *                           User.
     */
    public function getBookmarkTrashed()
    {
        return loggedIn() ? 
                Bookmark::onlyTrashed()->where('user_id', me()->id)
                    ->where('post_id', $this->attributes['id'])
                    ->first() 
                : null;
    }

    /**
     * Verifies if Post has been suggested for a specific Category, by a certain 
     * User (default is logged in User if not passed as argument) .
     *
     * @param \App\Category $category The Category to check is Post has been 
     *                                suggested for.
     * @param \App\User     $user     The User to check if has suggested the Post.
     *
     * @return bool         Returns true is User has suggested this Post for the 
     *                      given Category, false otherwise.
     */
    public function isSuggestedAs(Category $category, User $user = null)
    {
        if(loggedIn())
        {
            $user ??= me();

            return Suggestion::where("post_id", $this->attributes['id']) 
                ->where("category_id", $category->id)
                ->where("open", 1)
                ->where("user_id", $user->id)->count() > 0;
        }

        return false;
    }

    /**
     * Verifies if the number of times a Post has been suggested by a certain User 
     * (default is logged in User if not passed as argument) has reached allowed 
     * number of times (5: value from config).
     * 
     * @param  \App\User|null $user The User to verify if Post has maxed out 
     *                              number of suggestions.
     * 
     * @return bool           Returns true is Post has been suggested 5 times or 
     *                        more (value from config), false otherwise.
     */
    public function suggestedTimesMaxedOut(User $user = null)
    {
        if(loggedIn())
        {
            $user ??= me();

            $suggestion =  Suggestion::where("post_id", $this->attributes['id']) 
                ->where("open", 1)
                ->where("user_id", $user->id)->first();

            return 
                    $suggestion != null 
                    && $suggestion->count >= config("constants.suggestions.post_user_max");
        }

        return false;
    }

    /**
     * Searches Posts matching the received keywords as parameters.
     *
     * @param  array $keywords       The keywords to search based on.
     * @param  bool  $everyonesPosts Decides whether it searches based on logged 
     *                               in User's own Posts, or everyone's.
     *                               If omitted, the search is based on everyone's 
     *                               Posts.
     *                               Search based one one's own Posts still search 
     *                               on everyone's if logged in User is an Admin 
     *                               or Moderator User (check User Policy for 
     *                               'manageEveryones').
     *
     * @return \Illuminate\Database\Eloquent\Builder The Query Builder to execute 
     *                                               the search query.
     */
    public static function search(array $keywords, bool $everyonesPosts = true)
    {
        $mandatoryKeywords = SearchUtils::getMandatoryKeywords($keywords);
        $optionalKeywords  = SearchUtils::getOptionalKeywords($keywords);
        $excludedKeywords  = SearchUtils::getExcludedKeywords($keywords);
        $fields            = ["title", "body"];

        // Left join returns also the the Post entries that have no Tags related.
        $queryBuilder = Post::withTrashed()
                ->leftjoin('tagging_tagged', 'taggable_id', '=', 'posts.id')

                // Append the optional keywords (not using the 'include' operator).
                ->where(
                    function($query) use ($optionalKeywords, $fields)
                    {
                        foreach($optionalKeywords as $keyword)
                        {
                            SearchUtils::match($query, "posts", $keyword, $fields);
                            $query->orWhere(function($query) use ($keyword)
                            {
                                $query->withAnyTag([$keyword]);
                            });
                        }

                    }
                )

                // Append the mandatory keywords from using the 'include' operator.
                ->where(
                    function($query) use ($mandatoryKeywords, $fields)
                    {
                        foreach($mandatoryKeywords as $keyword)
                        {
                            $query->where(
                                function($query) use ($keyword, $fields)
                                {
                                    SearchUtils::match($query, "posts", $keyword, $fields);
                                }
                            );
                        }
                        self::orWithAnyTag($query, $mandatoryKeywords);
                    }
                )

                // Append the excluded keywords from using the 'exclude' operator.
                ->where(
                    function($query) use ($excludedKeywords, $fields)
                    {
                        foreach($excludedKeywords as $keyword)
                        {
                            SearchUtils::notMatch($query, "posts", $keyword, $fields);
                        }
                        $query->withoutTags($excludedKeywords);
                    }
                )

                /**
                 * Filter on Post ownership or permissive Role, and on live and 
                 * published Posts only for non-Admin Users, and on showing 
                 * moderated Posts or not.
                 */
                ->where(
                    function($query) use ($everyonesPosts)
                    {
                        if(!iAmModerator() && !iAmAdmin())
                        {
                            $query->where('reviewed', 1)
                                ->where('allowed', 1)
                                ->whereNotNull('reviewed_by');
                        }

                        if(
                            !$everyonesPosts 
                            && loggedIn() 
                            && !me()->can('manageEveryones', Post::class)
                        )
                        {
                            $query->where('user_id', me()->id);
                        }

                        if(
                            loggedIn() 
                            && !me()->can("manageDeleted", Post::class)
                        )
                        {
                            $query->whereNull('posts.deleted_at');
                        }
                    }
                )->distinct();

        return $queryBuilder;
    }

    /**
     * Attaches clause to Query Builder to include keywords verification as any 
     * related Tag.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query    The Query Builder to 
     *                                                        include clause into.
     * @param array                                 $keywords The keywords as values 
     *                                                        for clause to include.
     */
    private static function orWithAnyTag(&$query, array $keywords)
    {
        $query->orWhere(
            function($query) use ($keywords)
            {
                $query->withAllTags($keywords);
            }
        );
    }

    /**
     * Verifies if this Post is reviewed.
     *
     * @return bool Returns true if this Post is reviewed, false otherwise.
     */
    public function isReviewed()
    {
        return $this->attributes['reviewed'] == 1 && $this->attributes['reviewed_by'] != null;
    }
    
    /**
     * Verifies if this Post is accepted.
     *
     * @return bool Returns true if this Post is accepted, false otherwise.
     */
    public function isAccepted()
    {
        return $this->isReviewed() && $this->attributes['allowed'] == 1;
    }

    /**
     * Gets the Posts shown publicly to non-session Users in the application.
     *
     * @return Illuminate\Database\Eloquent\Builder The Query Builder to retrieve 
     *                                              the publicly viewable Posts.
     */
    public static function getPublicPosts()
    {
        return Post::orderBy('created_at', 'desc')
            ->where('reviewed', 1)
            ->where('allowed', 1)
            ->whereNotNull('reviewed_by');
    }
}
