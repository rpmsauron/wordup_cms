<?php

use App\Category;
use App\Contact;
use App\Exceptions\InvalidConfigurationException;
use App\Job;
use App\JobRun;
use App\Notification;
use App\Post;
use App\User;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Log;

/**
 * -------------------------------------------------------------------------------
 *
 * AUTHENTICATION AND SESSION ----------------------------------------------------
 *
 * -------------------------------------------------------------------------------
 */

/**
 * Returns logged in User.
 *
 * @return App\User The logged in User.
 */
function me()
{
    return auth()->user();
}

/**
 * Checks if there is a logged in User session.
 *
 * @return bool Returns true if there is a logged in User session, or false 
 *              otherwise.
 */
function loggedIn()
{
    return auth()->check();
}

/**
 * Checks if authenticated User is a same as another given User. 
 * User to compare authenticated User against, can be given as an object or as an 
 * integer id.
 *
 * @param  App\User|int $user The User or User id to check if authenticated User 
 *                            is the same.
 *
 * @return boolean      Returns true if the given User is the authenticated User, 
 *                      or false otherwise.
 */
function iAm($user)
{
    if(loggedIn())
    {
        return same(me(), $user);
    }

    return false;
}

/**
 * Checks if two Users are the same.
 * Users can be given as User instances or as integer ids.
 *
 * @param  App\User|int $source The first User or User id to verify for equality.
 * @param  App\User|int $target The second User or User id to verify for equality.
 *
 * @return boolean      Returns true if the Users are the same, or false otherwise.
 */
function same($source, $target)
{
    $sourceModel = is_int($source) ? User::find($source) : $source;
    $targetModel = is_int($target) ? User::find($target) : $target;

    if($sourceModel instanceof User && $targetModel instanceof User)
    {
        return $sourceModel->id == $targetModel->id;
    }

    return false;
}




/**
 * -------------------------------------------------------------------------------
 *
 * ROLES -------------------------------------------------------------------------
 *
 * -------------------------------------------------------------------------------
 */

/**
 * Verifies is logged in User has a specific Role given by the 'slug' attribute 
 * of the Role.
 *
 * @param  string  $slug The slug of the Role to verify.
 *
 * @return boolean Returns true if this User has the Role, or false otherwise.
 */
function iHaveRole(string $slug)
{
    return me()->hasRole($slug);
}

/**
 * Checks whether authenticated User has the Role 'Admin'.
 *
 * @return boolean Returns true if authenticated User has the Role 'Admin', or 
 *                 false otherwise.
 */
function iAmAdmin()
{
    return loggedIn() && me()->isAdmin();
}

/**
 * Checks whether authenticated User has the Role 'Moderator'.
 *
 * @return boolean Returns true if authenticated User has the Role 'Moderator', or 
 *                 false otherwise.
 */
function iAmModerator()
{
    return loggedIn() && me()->isModerator();
}

/**
 * Checks if authenticated User has granted another given User the Role 'Admin'.
 * User can be provided as an object or an an integer id.
 *
 * @param  App\User|int $user The User or User id to check if authenticated User 
 *                            has given the Role 'Admin' to.
 *
 * @return boolean      Returns true if authenticated User has granted the other 
 *                      User the Role 'Admin', or false otherwise.
 */
function iGaveAdminTo($user)
{
    $userModel = is_int($user) ? User::find($user) : $user;

    if($userModel instanceof User)
    {
        return me()->gaveAdminTo($userModel);
    }

    return false;
}




/**
 * -------------------------------------------------------------------------------
 *
 * PERMISSIONS -------------------------------------------------------------------
 *
 * -------------------------------------------------------------------------------
 */

/**
 * Checks if logged in User has any 'active' Permissions for one or more 'actions' 
 * over one or more 'models'.
 *
 * @param  mixed $models  The 'model' values which to verify there is any 
 *                        Permission for the logged in User.
 *                        Can be a string formatted 'model' value or as an array 
 *                        of values.
 * @param  mixed $actions The 'action' values which to verify there is any 
 *                        Permission for the logged in User. 
 *                        Can be a string formatted 'action' value or as an array 
 *                        of values.
 *
 * @return bool  Returns true if User has any Permission passed with 'action' 
 *               values and 'model' values, returns false otherwise.
 */
function iHavePermissionFor($models, $actions = null)
{
    return me()->hasPermissionFor($models, $actions);
}

/**
 * Checks based on config entries if a certain "model" entry supports empty 
 * "action" values, for Permissions.
 *
 * @param  string  $model The textual string-formatted class name of the 
 *                        Permission's "model".
 *
 * @return boolean Returns true if "model" entry supports empty "action" values, 
 *                 returns false otherwise.
 */
function modelAllowsEmptyAction(string $model)
{
    foreach(config("constants.permissions.models") as $configModel)
    {                
        // Invalid Config.
        try
        {
            checkInvalidPermissionConfig();
        } catch(InvalidConfigurationException $exception)
        {
            Log::error($exception);

            // Continue and return false.
            return false;
        }

        // Actual verification.
        if($configModel["classname"] == $model)
        {
            $result = !isset($configModel["actions"]) || empty($configModel["actions"]);

            return $result;
        }
    }

    return false;
}

/**
 * Gets an array of Permission 'actions' from config available for a given 
 * Permission 'model'.
 *
 * @param  string     $modelClassname The Permission 'model' value (a model 
 *                    classname) for which to get the 'actions'.
 *
 * @return array|null An array of 'actions' available for given 'model'. If model 
 *                    does not exist from config, returns null.
 */
function getActionsAvailableForPermissionModel(string $modelClassname)
{
    foreach(config("constants.permissions.models") as $key => $value)
    {
        if($modelClassname == $value["classname"])
        {
            return $value["actions"] ?? [];
        }
    }

    return null;
}




/**
 * -------------------------------------------------------------------------------
 *
 * IMAGE MANAGEMENT --------------------------------------------------------------
 *
 * -------------------------------------------------------------------------------
 */

/**
 * Gets the dimensions on a post image file when resized so to fit within a 
 * specified dimensions frame, without change to aspect ratio (deformation). 
 * The result is a two-dimensional array with the width value in index 0 and the 
 * height value in index 1, respectively.
 * If the resource file cannot be parsed or opened, an array with dimensions sets 
 * from config will be defaulted to, as fallback.
 *
 * @param  string $imageFile The URL of the image file.
 *
 * @return array  The two-dimensional array with width and height respectively.
 */
function getPostDisplayDimensions(string $imageFile)
{
    $widthConfig  = config('constants.image.post.thumb_max_width');
    $heightConfig = config('constants.image.post.thumb_max_height');
    $dimensions   = getDisplayDimensions($imageFile, $widthConfig, $heightConfig);

    /**
     * If there is any dimension set to zero (most likely the image resource could 
     * not be correctly read or parsed, to read its dimensions).
     */
    if(in_array(0, $dimensions))
    {
        return [$widthConfig, $heightConfig];
    }

    return $dimensions;
}

/**
 * Gets the dimensions of an image file when resized so to fit within a specified 
 * dimensions frame, without change to aspect ratio (deformation). 
 * The result is a two-dimensional array with the width value in index 0 and the 
 * height value in index 1, respectively.
 *
 * @param  string $imageFile The URL of the image file.
 * @param  int    $fitWidth  The width of the frame to fit the image into.
 * @param  int    $fitHeight The height of the frame to fit the image into.
 *
 * @return array  The two-dimensional array with width and height respectively.
 */
function getDisplayDimensions(string $imageFile, int $fitWidth, int $fitHeight)
{
    if(!stream_is_local($imageFile) && checkRemoteFile($imageFile) )
    {
        list($originalWidth, $originalHeight) = getimagesize($imageFile);
        if($originalWidth != 0 && $originalHeight != 0)
        {
            /**
             * Width resize ratio bigger than height resize ratio (enlarges to 
             * fit frame width).
             *    _______________________      _______________________
             *   |                       |    |                       |
             *   |                       |    |                       |
             *   | ___________           | -> | _____________________ |
             *   ||___________| ------>  |    ||                     ||
             *   |                       |    ||_____________________||
             *   `````````````````````````    `````````````````````````
             */
            if($originalWidth / $fitWidth > $originalHeight / $fitHeight)
            {
                $displayWidth  = $fitWidth;
                $displayHeight = $originalHeight / ($originalWidth / $fitWidth);
            } 

            /**
             * Height resize ratio bigger or equal than resize ratio (enlarges to 
             * fit frame height).
             *    ____________           ____________
             *   |   ^        |         ||``````|    |
             *   |   |        |         ||      |    |
             *   |   |        |         ||      |    |
             *   |            |         ||      |    |
             *   | |```|      |      -> ||      |    |
             *   | |   |      |         ||      |    |
             *   | |   |      |         ||      |    |
             *   | |___|      |         ||______|    |
             *   ``````````````         ``````````````
             */
            else
            {
                $displayHeight = $fitHeight;
                $displayWidth  = ($fitHeight / $originalHeight) * $originalWidth;
            }

            return [$displayWidth, $displayHeight];
        }
    } 

    // Timeout for remote file.
    return [0, 0];
}

/**
 * Checks if resource is a file that can be opened.
 *
 * @param  string $url The URL for the resource.
 *
 * @return bool   Returns true if resource can be opened, false otherwise.
 */
function checkRemoteFile(string $url)
{
    $handle = @fopen($url, 'r');

    return $handle;
}

/**
 * Checks if resource is a file that can be opened, with cURL.
 * 
 * UNUSED.
 *
 * @param  string $url The URL for the resource.
 *
 * @return bool   Returns true if resource can be opened, false otherwise.
 */
function checkRemoteFileWithCURL(string $url)
{
    // Timeout in seconds.
    $timeout = 1;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);

    // Don't download content.
    curl_setopt($ch, CURLOPT_NOBODY, 1);
    curl_setopt($ch, CURLOPT_FAILONERROR, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt ($ch, CURLOPT_TIMEOUT, $timeout);

    return curl_exec($ch) !== false;
}




/**
 * -------------------------------------------------------------------------------
 *
 * FILE MANAGEMENT ---------------------------------------------------------------
 *
 * -------------------------------------------------------------------------------
 */

/**
 * If given resource path is a valid file that has changed as a value on a model, 
 * it is deleted, and returns true if deletion is successful, or returns false 
 * otherwise.
 *
 * @param  string  $path  The path of the resource file.
 * @param  boolean $dirty If the model has been changed since last saving.
 * 
 * @return boolean Returns true is successfully returns the file, return false 
 *                 otherwise.
 */
function handleOldFile(string $path, bool $dirty = true)
{
    if($dirty && File::isFile($path))
    {
        return File::delete($path);
    }

    return false;
}

/**
 * Handles the processing of a specific file field for a specific model, from the 
 * request.
 *
 * @param  string    $fieldname The field name of the model that stores the file.
 * @param  undefined $model     The model to process the file for.
 *
 * @return int       Returns 1 if file has been replaced with a different one, 
 *                   returns 2 if file has been removed, 
 *                   returns 0 if file has not been changed at all.
 */
function processFileField(string $fieldname, &$model)
{
    $query = "image";
    $value = "clear";

    switch(get_class($model))
    {
        case Post::class:
            $storageFolder = config('constants.folder.post.image_post');
            break;

        case User::class:
            $storageFolder = config('constants.folder.user.avatar');
            break;

        case Category::class:
            $storageFolder = config('constants.folder.category.icon');
            break;
    }

    /**
     * Update Post image with new file.
     */
    if(request($fieldname))
    {
        $model->$fieldname = request($fieldname)->store($storageFolder);

        return 1;
    }

    /**
     * Update with removal of Post image. URL query parameter is required to 
     * differentiate from not altering the image field also, since that also 
     * rests on the criterion of having the request("post_image") empty, from 
     * the View form field.
     */
    elseif(request()->query($query) == $value)
    {
        $model->$fieldname = null;

        return 2;
    }

    /**
     * Fallback default: 
     * update not changing Post image, i.e., don't change $post->post_image.
     */
    return 0;
}

/**
 * Checks if a filename matches against a list of filenames to be ignored for 
 * further operations, read line by line from a default file 'cleanup_ignore' 
 * located at the root path.
 * 
 * @param  string  $filename The name of the file to check.
 * 
 * @return boolean Returns true if file matches any line entry from the default 
 *                 file, or returns false otherwise.
 */
function ignoreFile(string $filename)
{
    $file = fopen(base_path() . "/cleanup_ignore", "r");

    while(!feof($file))
    {
        $entry = fgets($file);

        if(basename($filename) == trim($entry))
        {
            fclose($file);

            return true;
        }
    }

    fclose($file);

    return false;
}




/**
 * -------------------------------------------------------------------------------
 *
 * NOTIFICATIONS -----------------------------------------------------------------
 *
 * -------------------------------------------------------------------------------
 */

/**
 * Gets a textual verb from config matching a Notification's textual action.
 * Falls back to a default textual string if none is found matching in the config.
 *
 * @param  string $action The Notification's textual action to get a verb from 
 *                        config for.
 *
 * @return string The verb to fetch from config based on a Notification's textual 
 *                action.
 */
function resolveVerb(string $action)
{
    foreach(config("constants.notifications.verbs") as $key => $value)
    {
        if($action == $key)
        {
            return $value;
        }
    }

    return config("constants.notifications.verbs.default");
}

/**
 * Gets a class name parsed without namespace for a given full class name.
 *
 * @param  string $classname The full class name.
 *
 * @return string The class name without the namespace.
 */
function resolveType(string $classname)
{
    $components = explode('\\', $classname);

    return array_pop($components);
}

/**
 * Creates a Notification. 
 * A User login session must be in place.
 * @todo to implement email notification as well.
 *
 * @param User   $notified     The notified User.
 * @param int    $notifiableId The id of the model whose handling triggers the 
 *                             Notification.
 * @param string $className    The domain class name of the model whose handling 
 *                             triggers the Notification.
 * @param string $action       The action that is the actual handling of the model.
 * @param int    $targetId     The id referenced as target model by the notifiable 
 *                             id object on relationship actions between two models.
 */
function notify(
        User $notified, 
        int $notifiableId, 
        string $className, 
        string $action, 
        int $targetId = null
)
{
    $notifier = loggedIn() ? me()->id : null;

    Notification::create([
        "notifier_id"   => $notifier,
        "notified_id"   => $notified->id,
        "notifiable_id" => $notifiableId,
        "type"          => $className,
        "action"        => $action,
        "target_id"     => $targetId
    ]);
}

/**
 * Creates similar Notifications for all Users that have the Role 'Admin', except
 * for the currently logged in User.
 * A User login session must be in place.
 *
 * @param int    $notifiableId The id of the model whose handling triggers the 
 *                             Notification.
 * @param string $className    The domain class name of the model whose handling 
 *                             triggers the Notification.
 * @param string $action       The action that is the actual handling of the model.
 * @param int    $targetId     The id referenced as target model by the notifiable 
 *                             id object on relationship actions between two models.
 */
function notifyAdmins(
        int $notifiableId, 
        string $className, 
        string $action, 
        int $targetId = null
)
{
    notifyAll(
            config("constants.roles.role.admin.name"), 
            false, 
            null, 
            $notifiableId, 
            $className, 
            $action, 
            $targetId
    );
}

/**
 * Creates similar Notifications for all Users that have the given Roles. 
 * An exception User can be given as parameter to exclude.
 * A User login session must be in place.
 *
 * @param type      roles         The role names which Users must have to create 
 *                                Notifications for. 
 *                                Can be passed as an array of various role names 
 *                                or a single role name as a string.
 * @param bool      $includeSelf  Defines whether Notifications created include 
 *                                logged in User or not.
 * @param User|null $excludeUser  A User to exclude from the Users to notify. 
 *                                If null there is none to exclude.
 * @param int       $notifiableId The id of the model whose handling triggers the 
 *                                Notification.
 * @param string    $className    The domain class name of the model whose handling 
 *                                triggers the Notification.
 * @param string    $action       The action that is the actual handling of the model.
 * @param int       $targetId     The id referenced as target model by the 
 *                                notifiable id object on relationship actions 
 *                                between two models.
 */
function notifyAll(
        $roles, 
        bool $includeSelf, 
        ?User $excludeUser, 
        int $notifiableId, 
        string $className, 
        string $action, 
        int $targetId = null
)
{
    $roles = is_array($roles) ? $roles : (array) $roles;

    $users = User::getAllUsersWithRoleNames($roles);

    foreach($users as $user)
    {
        /**
         * - if User is not the notifying User;
         * - or if User is the notifying User and is specifically referred to
         *   include self;
         * - or if User is not a User to exclude, referred from the notified 
         *   ones' list.
         */
        if(
            !same($user, $excludeUser) 
            && (
                ($includeSelf && iAm($user)) 
                || !iAm($user)
                || (
                    !loggedIn() 
                    && $className == Contact::class 
                    && $action = "create"
                )
            )
        )
        {
            notify($user, $notifiableId, $className, $action, $targetId);
        }
    }
}

/**
 * Clears out a specific Notification.
 *
 * @param User|null $notifier     The notifying User.
 *                                Can be omitted passing 'null' value, mean to 
 *                                not specify who the notifying User(s) is/are. 
 *                                Used, for instance, for User deletion, since 
 *                                retrieving the Notification for it does not 
 *                                require knowing the remover.
 * @param User|null $notified     The notified User.
 *                                Can be omitted passing 'null' value, mean to 
 *                                not specify who the notified User(s) is/are. 
 * @param int       $notifiableId The id of the model whose handling triggers 
 *                                the Notification.
 * @param string    $className    The domain class name of the model whose 
 *                                handling triggers the Notification.
 * @param type      $actions      The actions that are the actual handling of 
 *                                the model. Can be passed either as an array 
 *                                of several, or a single one as a string.
 * @param int       $targetId     The id referenced as target model by the 
 *                                notifiable id object on relationship actions 
 *                                between two models.
 */
function unnotify(
        ?User $notifier, 
        ?User $notified, 
        int $notifiableId, 
        string $className, 
        $actions, 
        int $targetId = null
)
{
    $actions = is_array($actions) ? : (array) $actions;

    Notification::where(
            function($query) use ($notifier)
            {
                if($notifier)
                {
                    $query->where("notifier_id", $notifier->id);
                }
            }
        )
        ->where(
            function($query) use ($notified)
            {
                if($notified)
                {
                    $query->where("notified_id", $notified->id);
                }
            }
        )             
        ->where("notifiable_id", $notifiableId)
        ->whereIn("action", $actions)
        ->where("type", $className)
        ->where(
            function($query) use ($targetId)
            {
                if($targetId)
                {
                    $query->where("target_id", $targetId);
                }
            }
        )
        ->where("new", 1)
        ->update(["new" => 0]);
}




/**
 * -------------------------------------------------------------------------------
 *
 * COMMENTS ----------------------------------------------------------------------
 *
 * -------------------------------------------------------------------------------
 */

/**
 * Removes NULL-byte characters, HTML and PHP tags from input text, letting only 
 * the basic HTML text-formatting tags remain.
 *
 * @param  string $text The input text to clean.
 *
 * @return string The text cleaned off the forbidden HTML and PHP tags, and off 
 *                the NULL-byte characters.
 */
function cleanText(string $text)
{
    return strip_tags($text, '<b><i><small><del><ins><sub><sup>');
}




/**
 * -------------------------------------------------------------------------------
 *
 * CONFIG ------------------------------------------------------------------------
 *
 * -------------------------------------------------------------------------------
 */

/**
 * Checks if config has duplicate operator entries and throws an exception, if so.
 *
 * @throws InvalidConfigurationException Thrown when there are duplicate operator 
 *                                       entries on the config.
 */
function checkInvalidConfigDuplicateOperators()
{
   $operators = config("constants.search.operators");

   if(count($operators) != count(array_unique($operators)))
   {
       throw new InvalidConfigurationException();
   }
}

/**
 * Checks if config has invalid format for Permissions' action and model values.
 *
 * @throws InvalidConfigurationException Thrown when format is invalid, such as a 
 *                                       missing 'classname' key-value entry for a 
 *                                       'model'.
 */
function checkInvalidPermissionConfig()
{
   foreach(config("constants.permissions.models") as $configModel)
   {
       if(!isset($configModel["classname"]))
       {
           throw new InvalidConfigurationException();
       }
   }
}




/**
 * -------------------------------------------------------------------------------
 *
 * JOBS --------------------------------------------------------------------------
 *
 * -------------------------------------------------------------------------------
 */

/**
 * Creates an instance of JobRun.
 *
 * @param string $jobName The name of the Job to create a JobRun instance for.
 */
function registerJob(string $jobName)
{
   $job = Job::where("name", $jobName)->first();

   JobRun::create([
       "job_id"  => $job->id,
       "user_id" => me()->id
   ]);
}