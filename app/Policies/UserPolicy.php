<?php

namespace App\Policies;

use App\Permission;
use App\Role;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    private $adminRole;

    public function __construct()
    {
        $this->adminRole = config('constants.roles.role.admin.slug');
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User $user The User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function viewAny(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User $user The User to verify the Policy for.
     * @param  \App\User $model The other User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function view(User $user, User $model)
    {
        return true;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\Use $user The User to verify the Policy for.
     *
     * @return bool     Returns true if Policy applies, false otherwise.
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can see the edit view for the model.
     *
     * @param  \App\User $user The User to verify the Policy for.
     * @param  \App\User $model The other User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function edit(User $user, User $model)
    {
        
        return 
                // Soft-deleted Users can not be shown view to edit.
                !$user->deleted_at

                && (
                    $user->isAdmin() 
                    || same($user, $model) 
                    || $user->hasPermissionFor(User::class, "update")

                    /**
                     * Requires verification of Role granting as well to be able to 
                     * access edit view to deal with the Roles.
                     */
                    || $user->gaveAdminTo($model)
                );
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User $user The User to verify the Policy for.
     * @param  \App\User $model The other User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function update(User $user, User $model)
    {
        return 
                // Soft-deleted Users can not be updated.
                !$user->deleted_at

                && (
                    $user->isAdmin() 
                    || same($user, $model)
                    || $user->hasPermissionFor(User::class, "update")
                );
    }

    /**
     * Determine whether the user can delete the model.
     * Also valid for self-deletion.
     *
     * @param  \App\User $user The User to verify the Policy for.
     * @param  \App\User $model The other User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function delete(User $user, User $model)
    {
        return 
                (
                // Delete someone else's User account.
                    !same($user, $model)
                    && (
                        $user->isAdmin() 
                        || $user->hasPermissionFor(User::class, "delete")
                    )
                )
                
                // Delete Self.
                || same($user, $model);
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User $user The User to verify the Policy for.
     * @param  \App\User $model The other User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function restore(User $user)
    {
        return 
                $user->isAdmin() 
                || $user->hasPermissionFor(User::class, "restore");
    }

    /**
     * Determine whether the user can attach Roles to the model.
     * Users cannot give themselves Roles (explicitly the "Attach Roles" 
     * Permission, to prevent escalation of unrestrained access.)
     * Roles can only be given to other Users.
     *
     * @param  \App\User $user  The User to verify the Policy for.
     * @param  \App\User $model The other User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function attachRole(User $user, User $model)
    {
        // Rules for limiting giving oneself a Role.
        $canGrantSelf = !same($user, $model);

        return 
                $user->isAdmin() 

                || (
                    // Simply has the Permission manually given to do so.
                    $user->hasPermissionFor(User::class, "attach role")

                    /**
                     * If not Admin, cannot give oneself any Role (explicitly none 
                     * allowing to attach Permissions; that would enable possibility 
                     * to give oneself full unrestrained access.)
                     */
                    && $canGrantSelf
                )

                /**
                 * User has given the Role 'Admin' to the User 'model' previously, 
                 * regardless of current Permissions and Roles.
                 */
                || $user->gaveAdminTo($model);
    }

    /**
     * Determine whether the user can detach Roles from the model.
     * User cannot detach Roles from self unless User is Admin. 
     * Having the Permission to detach Permission explicitly does not allow to do 
     * so either.
     *
     * @param  \App\User $user  The User to verify the Policy for.
     * @param  \App\User $model The other User to verify the Policy for.
     * @param  \App\Role $role  The Role to verify the Policy for. If given, then 
     *                          the Policy validates for a specific Role, if null, 
     *                          then it does not specify any Role in particular.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function detachRole(User $user, User $model, Role $role = null)
    {
        // Rules for limiting giving oneself a Role.
        $canRevokeFromSelf = !same($user, $model);

        /**
         * If $role is passed, then this var means the Role is "Admin" specifically, 
         * if it is not passed, then this var as true means there is not specific 
         * Role for verifying.
         */
        $roleIsAdminOrUnspecified = $role ? ($role->slug == $this->adminRole) : true;

        return 
                (
                    // As an Admin User.
                    $user->isAdmin()

                    && (
                        // Detach own Role (doesn't require a granted_by value).
                        same($user, $model) 

                        /**
                         * If as an Admin-Role-tracker, detach any Role from an Admin 
                         * User the Role was granted to.
                         */
                        || $user->gaveAdminTo($model) 

                        // Detach any Role from a non-Admin User.
                        || !$model->isAdmin()
                    )
                )

                || (
                    /**
                     * As a non-Admin User and without Permission to do so,
                     * if the User is an Admin-Role tracker, they can only detach 
                     * someone's Role for Role 'Admin', only. 
                     * All other Roles are managed by other Admin Users.
                     */
                    (
                        $user->gaveAdminTo($model) 
                        && $roleIsAdminOrUnspecified
                    )

                    || (
                        // Simply has the Permission manually given to do so.
                        $user->hasPermissionFor(User::class, "detach role")

                        // If not Admin, cannot revoke any Role from oneself.
                        && $canRevokeFromSelf
                    )
                );
    }

    /**
     * Determine whether the user can attach Permissions to the model.
     * Users cannot give themselves Permissions (explicitly the "Attach Permissions" 
     * Permission, to prevent escalation of unrestrained access.)
     * Permissions can only be given to other Users.
     *
     * @param  \App\User $user       The User to verify the Policy for.
     * @param  \App\User $model      The other User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function attachPermission(User $user, User $model)
    {
        // Rules for limiting giving oneself a Permission.
        $canGrantSelf = !same($user, $model);

        return 
                $user->isAdmin() 

                || (
                    // Simply has the Permission manually given to do so.
                    $user->hasPermissionFor(User::class, "attach permission")

                    /**
                     * If not Admin, cannot give oneself any Permission (explicitly 
                     * can never give self the Permission 'Attach Permissions'; that 
                     * would enable possibility to give oneself full unrestrained 
                     * access.)
                     */
                    && $canGrantSelf
                );
    }

    /**
     * Determine whether the user can detach Permissions from the model.
     * User cannot detach Permissions from self unless User is Admin. 
     * Having the Permission to detach Permission explicitly does not allow to do 
     * so either.
     *
     * @param  \App\User $user       The User to verify the Policy for.
     * @param  \App\User $model      The other User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function detachPermission(User $user, User $model)
    {
        // Rules for limiting revoking a Permission from oneself.
        $canRevokeFromSelf = !same($user, $model);

        return 
                $user->isAdmin() 

                || (
                        // Simply has the Permission manually given to do so.
                        $user->hasPermissionFor(User::class, "detach permission")

                        // If not Admin, cannot revoke any Permission from oneself.
                        && $canRevokeFromSelf
                );
    }
}
