<?php

namespace App\Policies;

use App\Contact;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ContactPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        return true;
    }

    /**
     * Determine whether the user can reply to the model.
     *
     * @param  \App\User  $user
     * @param  \App\Contact  $contact
     * @return mixed
     */
    public function reply(User $user, Contact $contact)
    {
        return 
            $contact->is_reply == 0
            && (
                $user->isAdmin() 
                || $user->hasPermissionFor(Contact::class, "reply")
            );
    }
}
