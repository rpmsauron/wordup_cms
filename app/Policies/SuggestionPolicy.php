<?php

namespace App\Policies;

use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class SuggestionPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User       $user       The User to verify the Policy for.
     * @param  \App\Suggestion $suggestion The Suggestion to verify the Policy for.
     *
     * @return bool            Returns true if Policy applies, false otherwise.
     */
    public function view(User $user, Suggestion $suggestion)
    {
        return true;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User $user The User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function create(User $user)
    {
        return $user->is($user);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User       $user       The User to verify the Policy for.
     * @param  \App\Suggestion $suggestion The Suggestion to verify the Policy for.
     *
     * @return bool            Returns true if Policy applies, false otherwise.
     */
    public function update(User $user, Suggestion $suggestion)
    {
        return true;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User       $user       The User to verify the Policy for.
     * @param  \App\Suggestion $suggestion The Suggestion to verify the Policy for.
     *
     * @return bool            Returns true if Policy applies, false otherwise.
     */
    public function delete(User $user, Suggestion $suggestion)
    {
        return 
                same($user, $suggestion->user) 
                || $user->isAdmin();
    }

    /**
     * Determine whether the user can accept the model.
     *
     * @param  \App\User       $user       The User to verify the Policy for.
     * @param  \App\Suggestion $suggestion The Suggestion to verify the Policy for.
     *
     * @return bool            Returns true if Policy applies, false otherwise.
     */
    public function accept(User $user, Suggestion $suggestion)
    {
        return 
                same($user, $suggestion->post->user) 
                && $suggestion->category->is_active == 1 
                && !same($user, $suggestion->user);
    }

    /**
     * Determine whether the user can reject the model.
     *
     * @param  \App\User       $user       The User to verify the Policy for.
     * @param  \App\Suggestion $suggestion The Suggestion to verify the Policy for.
     *
     * @return bool            Returns true if Policy applies, false otherwise.
     */
    public function reject(User $user, Suggestion $suggestion)
    {
        return 
                same($user, $suggestion->post->user)

                // Cannot accept own Suggestions given.
                && !same($user, $suggestion->user);
    }
}
