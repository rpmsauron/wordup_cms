<?php

namespace App\Policies;

use App\Permission;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PermissionPolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return 
                $user->isAdmin()
                || $user->hasPermissionFor(Permission::class, ["create", "update", "delete", "toggle"]);
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param  \App\User $user The User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function viewAny(User $user)
    {
        return $user->is($user);
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User       $user       The User to verify the Policy for.
     * @param  \App\Permission $permission The Permission to verify the Policy for.
     *
     * @return bool            Returns true if Policy applies, false otherwise.
     */
    public function view(User $user, Permission $permission)
    {
        return true;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User $user The User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function create(User $user)
    {
        return 
                $user->isAdmin()
                /**
                 * Needed since the Permission creation form shows within the default 
                 * view for all action for Permissions.
                 */
                || $user->hasPermissionFor(Permission::class);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User $user The User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function store(User $user)
    {
        return 
                $user->isAdmin()
                || $user->hasPermissionFor(Permission::class, "create");
    }

    /**
     * Determine whether the user can see the edit view for the model.
     *
     * @param  \App\User $user The User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function edit(User $user)
    {
        return 
                $user->isAdmin()
                || $user->hasPermissionFor(Permission::class, "update")
                
                /**
                 * Needed since the views for the Permission toggling 
                 * actions is on the edit view.
                 */
                || $user->hasPermissionFor(Permission::class, "toggle");
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User $user The User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function update(User $user)
    {
        return 
                $user->isAdmin()
                || $user->hasPermissionFor(Permission::class, "update");
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User       $user       The User to verify the Policy for.
     * @param  \App\Permission $permission The Permission to verify the Policy for.
     *
     * @return bool            Returns true if Policy applies, false otherwise.
     */
    public function delete(User $user, Permission $permission)
    {
        return 
                $permission->is_undeletable != 1 
                && (
                    $user->isAdmin()
                    || $user->hasPermissionFor(Permission::class, "delete")
                    || same($user, $permission->creator)
                );
    }

    /**
     * Determine whether the user can toggle the model.
     *
     * @param  \App\User $user The User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function toggle(User $user)
    {
        return 
                $user->isAdmin()
                || $user->hasPermissionFor(Permission::class, "toggle");
    }
}
