<?php

namespace App\Policies;

use App\Role;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class RolePolicy
{
    use HandlesAuthorization;

    public function index(User $user)
    {
        return 
                $user->isAdmin()
                || $user->hasPermissionFor(Role::class, 
                        [
                            "create", 
                            "update", 
                            "delete", 
                            "attach permission", 
                            "detach permission"
                        ]
                );
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User $user The User to verify the Policy for.
     * @param  \App\Role $role The Role to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function view(User $user, Role $role)
    {
        return true;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User $user The User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function create(User $user)
    {
        return 
                $user->isAdmin()

                /**
                 * Needed since the Role creation form shows within the default 
                 * view for all actions for Roles.
                 */
                || $user->hasPermissionFor(Role::class);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User $user The User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function store(User $user)
    {
        return 
                $user->isAdmin()
                || $user->hasPermissionFor(Role::class, "create");
    }

    /**
     * Determine whether the user can see the edit view for the model.
     *
     * @param  \App\User $user The User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function edit(User $user)
    {
        return 
                $user->isAdmin()
                || $user->hasPermissionFor(Role::class, "update")
                
                /**
                 * Needed since the views for the Role attaching / detaching
                 * actions for Permissions, is on the edit view.
                 */
                || $user->hasPermissionFor(Role::class, "attach permission")
                || $user->hasPermissionFor(Role::class, "detach permission");
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User $user The User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function update(User $user)
    {
        return 
                $user->isAdmin()
                || $user->hasPermissionFor(Role::class, "update");
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User $user The User to verify the Policy for.
     * @param  \App\Role $role The Role to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function delete(User $user, Role $role)
    {
        return 
                $role->is_undeletable != 1 
                && (
                    $user->isAdmin()
                    || $user->hasPermissionFor(Role::class, "delete")
                    || same($user, $role->creator)
                );
    }

    /**
     * Determine whether the user can attach any Permission to the model.
     *
     * @param  \App\User $user       The User to verify the Policy for.
     * @param  \App\User $model      The other User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function attachPermission(User $user)
    {
        return 
                $user->isAdmin() 
                || $user->hasPermissionFor(Role::class, "attach permission");
    }

    /**
     * Determine whether the user can detach Permissions from the model.
     *
     * @param  \App\User $user       The User to verify the Policy for.
     * @param  \App\User $model      The other User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function detachPermission(User $user)
    {
        return 
                $user->isAdmin() 
                || $user->hasPermissionFor(Role::class, "detach permission");
    }
}
