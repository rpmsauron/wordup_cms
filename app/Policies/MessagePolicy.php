<?php

namespace App\Policies;

use App\Message;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class MessagePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User    $user    The User to verify the Policy for.
     * @param  \App\Message $message The Message to verify the Policy for.
     *
     * @return bool         Returns true if Policy applies, false otherwise.
     */
    public function view(User $user, Message $message)
    {
        return 
                same($user, $message->to) 
                || same($user, $message->from);
    }

    /**
     * Determine whether the user can archive the model.
     *
     * @param  \App\User    $user    The User to verify the Policy for.
     * @param  \App\Message $message The Message to verify the Policy for.
     *
     * @return bool         Returns true if Policy applies, false otherwise.
     */
    public function archive(User $user, Message $message)
    {
        return 
                (
                    same($user, $message->to)
                    && $message->archived_for_to == 0
                )
                || (
                    same($user, $message->from)
                    && $message->archived_for_from == 0
                );
    }

    /**
     * Determine whether the user can create models for a second different 
     * specific user.
     * Second user (recipient) cannot be soft-deleted.
     *
     * @param  \App\User $user      The User to verify the Policy for.
     * @param  \App\User $recipient The recipient User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function message(User $user, User $recipient)
    {
        return 
                $user->is($user)
                && !same($user, $recipient)
                && !$recipient->deleted_at;
    }

    /**
     * Determine whether the user can reply to the model.
     *
     * @param  \App\User    $user    The User to verify the Policy for.
     * @param  \App\Message $message The Message to verify the Policy for.
     *
     * @return bool         Returns true if Policy applies, false otherwise.
     */
    public function reply(User $user, Message $message)
    {
        return 
                same($user, $message->to) 
                && $message->from->deleted_at == null;
    }
}
