<?php

namespace App\Policies;

use App\Comment;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CommentPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User    $user    The User to verify the Policy for.
     * @param  \App\Comment $comment The Comment to verify the Policy for.
     *
     * @return bool         Returns true if Policy applies, false otherwise.
     */
    public function view(User $user, Comment $comment)
    {
        return true;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user The User to verify the Policy for.
     *
     * @return bool       Returns true if Policy applies, false otherwise.
     */
    public function create(User $user)
    {
        return $user->is($user);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User    $user The User to verify the Policy for.
     * @param  \App\Comment $comment The Comment to verify the Policy for.
     *
     * @return bool         Returns true if Policy applies, false otherwise.
     */
    public function update(User $user, Comment $comment)
    {
        return 
                same($user, $comment->user) 
                || $user->isAdmin()
                || $user->hasPermissionFor(Comment::class, "update");
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User    $user The User to verify the Policy for.
     * @param  \App\Comment $comment The Comment to verify the Policy for.
     *
     * @return bool         Returns true if Policy applies, false otherwise.
     */
    public function delete(User $user, Comment $comment)
    {
        return 
                same($user, $comment->user) 
                || $user->isAdmin()
                || $user->hasPermissionFor(Comment::class, "delete");
    }

    /**
     * Determine whether the user can view a list of all Users' instances of the 
     * model (index).
     * This Policy is used for Dashboard listing, for instance, on a search feature, 
     * not for viewing a model instance on a /view on a CRUD logic.
     *
     * @param  \App\User $user The User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function manageEveryones(User $user)
    {
        return 
                $user->isAdmin() 
                || $user->isModerator()
                || $user->hasPermissionFor(Comment::class, "review");
    }

    /**
     * Determine whether the user can review the model.
     *
     * @param  \App\User    $user The User to verify the Policy for.
     * @param  \App\Comment $comment The Comment to verify the Policy for.
     *
     * @return bool         Returns true if Policy applies, false otherwise.
     */
    public function review(User $user, Comment $comment)
    {
        return 
                // Comment needs to have been reported and not yet reviewed.
                $comment->reported == 1
                && $comment->reviewed == 0
                
                // Cannot own the Comment being moderated.
                && !same($user, $comment->user)

                // Cannot be the reporter for the Comment.
                && !same($user, $comment->reporter)

                && (
                    $user->isAdmin() 
                    || $user->isModerator()
                    || $user->hasPermissionFor(Comment::class, "review")
                );
    }

    /**
     * Determine whether the user can moderate the model via the accept action.
     *
     * @param  \App\User    $user The User to verify the Policy for.
     * @param  \App\Comment $comment The Comment to verify the Policy for.
     *
     * @return bool         Returns true if Policy applies, false otherwise.
     */
    public function accept(User $user, Comment $comment)
    {
        return 
                // Cannot own the Comment being moderated.
                !same($user, $comment->user)

                // Cannot be the reporter for the Comment.
                && !same($user, $comment->reporter)

                && (
                    $user->isAdmin() 
                    || $user->isModerator()
                    || $user->hasPermissionFor(Comment::class, "review")
                );
    }

    /**
     * Determine whether the user can moderate the model via the reject action.
     *
     * @param  \App\User    $user The User to verify the Policy for.
     * @param  \App\Comment $comment The Comment to verify the Policy for.
     *
     * @return bool         Returns true if Policy applies, false otherwise.
     */
    public function reject(User $user, Comment $comment)
    {
        return 
                // Cannot own the Comment being moderated.
                !same($user, $comment->user)

                // Cannot be the reporter for the Comment.
                && !same($user, $comment->reporter)

                && (
                    $user->isAdmin() 
                    || $user->isModerator()
                    || $user->hasPermissionFor(Comment::class, "review")
                );
    }

    /**
     * Determine whether the user can report the model.
     *
     * @param  \App\User    $user The User to verify the Policy for.
     * @param  \App\Comment $comment The Comment to verify the Policy for.
     *
     * @return bool         Returns true if Policy applies, false otherwise.
     */
    public function report(User $user, Comment $comment)
    {
        return 
                $user->is($user)

                // Cannot own the Comment being reported.
                && !same($user, $comment->user);
    }
}
