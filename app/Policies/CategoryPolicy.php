<?php

namespace App\Policies;

use App\Category;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class CategoryPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the model.
     *
     * @param  \App\User     $user The User to verify the Policy for.
     * @param  \App\Category $category The Category to verify the Policy for.
     *
     * @return bool          Returns true if Policy applies, false otherwise.
     */
    public function view(User $user, Category $category)
    {
        return true;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User  $user The User to verify the Policy for.
     *
     * @return bool Returns true if Policy applies, false otherwise.
     */
    public function create(User $user)
    {
        return $user->is($user);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User     $user The User to verify the Policy for.
     * @param  \App\Category $category The Category to verify the Policy for.
     *
     * @return bool          Returns true if Policy applies, false otherwise.
     */
    public function update(User $user, Category $category)
    {
        return 
                same($user, $category->user)
                || $user->isAdmin()
                || $user->hasPermissionFor(Category::class, "update");
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User     $user The User to verify the Policy for.
     * @param  \App\Category $category The Category to verify the Policy for.
     *
     * @return bool          Returns true if Policy applies, false otherwise.
     */
    public function delete(User $user, Category $category)
    {
        return 
                same($user, $category->user) 
                || $user->isAdmin()
                || $user->hasPermissionFor(Category::class, "delete");
    }

    /**
     * Determine whether the user can toggle the model.
     *
     * @param  \App\User     $user The User to verify the Policy for.
     * @param  \App\Category $category The Category to verify the Policy for.
     *
     * @return bool          Returns true if Policy applies, false otherwise.
     */
    public function toggle(User $user, Category $category)
    {
        return 
                same($user, $category->user) 
                || $user->isAdmin()
                || $user->hasPermissionFor(Category::class, "toggle");
    }

    /**
     * Determine whether the user can view the model if candidate for merging.
     *
     * @param  \App\User $user The User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function showMergeable(User $user)
    {
        return 
                $user->isAdmin()
                || $user->hasPermissionFor(Category::class, "merge");
    }

    /**
     * Determine whether the user can merge instances of the model.
     *
     * @param  \App\User $user The User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function merge(User $user)
    {
        return 
                $user->isAdmin()
                || $user->hasPermissionFor(Category::class, "merge");
    }

    /**
     * Determine whether the user can perform management actions on the model, 
     * such as updating, deleting or toggling 'active' status.
     * If Category not specified, Policy applies for it in particular, otherwise
     * it applies for any Category in general.
     *
     * @param  \App\User          $user     The User to verify the Policy for.
     * @param  null|\App\Category $category The Category to verify the Policy for.
     *
     * @return bool               Returns true if Policy applies, false otherwise.
     */
    public function manage(User $user, Category $category = null)
    {
        return
                $user->isAdmin()
                || $user->hasPermissionFor(Category::class, ["update", "delete", "toggle"])
                
                // If Policy is for a specified Category.
                || (
                    $category && same($user, $category->created_by)
                );
    }
}
