<?php

namespace App\Policies;

use App\Post;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class PostPolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view a list of all Users' instances of the model (index).
     * This Policy is used for Dashboard listing, for instance, on a search feature, 
     * not for viewing a model instance on a /view on a CRUD logic.
     *
     * @param  \App\User $user The User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function manageEveryones(User $user)
    {
        return 
                $user->isAdmin() 
                || $user->isModerator()
                || $user->hasPermissionFor(Post::class, ["update", "delete"]);
    }

    /**
     * Determine whether the user can create models.
     *
     * @param  \App\User $user The User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function create(User $user)
    {
        return $user->is($user);
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param  \App\User $user The User to verify the Policy for.
     * @param  \App\Post $post The Post to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function update(User $user, Post $post)
    {
        return 
                same($user, $post->user) 
                || $user->isAdmin()
                || $user->hasPermissionFor(Post::class, "update");
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param  \App\User      $user The User to verify the Policy for.
     * @param  \App\Post|null $post The Post to verify the Policy for. If null, the
     *                              Policy is verified generically for no specific 
     *                              Post.
     *
     * @return bool           Returns true if Policy applies, false otherwise.
     */
    public function delete(User $user, Post $post = null)
    {
        return 
                // Owns the Post.
                (
                    $post
                    && same($user, $post->user)
                )
                
                || $user->isAdmin()
                || $user->hasPermissionFor(Post::class, "delete");
    }

    /**
     * Determine whether the user can restore the model.
     *
     * @param  \App\User $user The User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function restore(User $user)
    {
        return 
                $user->isAdmin()
                || $user->hasPermissionFor(Post::class, "restore");
    }

    /**
     * Determine whether the user can search the model.
     *
     * @param  \App\User $user The User to verify the Policy for.
     * @param  \App\Post $post The Post to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function search(User $user, Post $post)
    {
        return true;
    }

    /**
     * Determine whether the user can toggle-bookmark the model.
     *
     * @param  \App\User $user The User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function bookmark(User $user)
    {
        return $user->is($user);
    }

    /**
     * Determine whether the user can manually assign the model to some Category.
     *
     * @param  \App\User $user The User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function assign(User $user, Post $post)
    {
        return 
                same($user, $post->user) 
                || $user->isAdmin()
                || $user->hasPermissionFor(Post::class, "assign category")
                
                // Soft-deleted Posts cannot be assigned any Category.
                && !$post->deleted_at;
    }

    /**
     * Determine whether the user can moderate the model via the accept or reject 
     * actions.
     * Policy does not account for 'reviewed' column value as 0, because already 
     * reviewed Posts can still be reviewed again.
     * 
     * @param  \App\User $user The User to verify the Policy for.
     *
     * @return bool      Returns true if Policy applies, false otherwise.
     */
    public function review(User $user, Post $post)
    {
        return
                (
                    $user->isAdmin() 
                    || $user->isModerator()
                    || $user->hasPermissionFor(Post::class, "review") 
                )

                /**
                 * Even with the Policy to review Posts, one cannot review their 
                 * own Posts.
                 * This condition applies to all previous disjunction conditions 
                 * because the following scenario can occur:
                 * a User is an Admin that has Posts still pending from a time 
                 * when it wasn't Admin or had not the Permissions yet, and so one 
                 * could now approve their own pending Posts.
                 */
                && !same($user, $post->user)

                // Soft-deleted Posts cannot be reviewed.
                && !$post->deleted_at;
    }

    public function manageDeleted(User $user)
    {
        return
                $user->isAdmin()
                || $user->hasPermissionFor(Post::class, ["delete", "restore"]);
    }
}
