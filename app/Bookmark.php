<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Bookmark extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'user_id', 
        'post_id'
    ];

    /**
     * Returns the User which created this Bookmark.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class)
                ->withDefault(["name" => "<Deleted User>"]);
    }

    /**
     * Returns the Post which this Bookmark is for.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(Post::class)
                ->withDefault(["title" => "<Deleted Post>"]);
    }
}
