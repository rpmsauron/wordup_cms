<?php

namespace App\Exceptions;

use Exception;

/**
 * Description of InvalidConfigurationException
 *
 * @author rpsilva
 */
class InvalidConfigurationException extends Exception
{
}
