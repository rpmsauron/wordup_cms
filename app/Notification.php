<?php

namespace App;

use App\Comment;
use App\Post;
use App\User;
use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $fillable = [
        'action', 
        'type', 
        'notified_id',
        'notifiable_id', 
        'notifier_id',
        'new',
        'target_id'
    ];

    /**
     * Returns the notified User of this Notification.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notified()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Returns the notifier User of this Notification.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function notifier()
    {
        return $this->belongsTo(User::class)->withTrashed();
    }

    /**
     * Verifies dependently on the Notification data itself if the Notification's
     * notifier User should be hidden or not.
     *
     * @return bool Returns true if Notification should hide notifier User, false 
     * otherwise.
     */
    public function hideNotifier()
    {
        return 
                // Is a Post or Comment Notification by Moderation.
                (
                    (
                        $this->attributes['type'] == Post::class 
                        || $this->attributes['type'] == Comment::class
                    )

                    && (
                        $this->attributes['action'] == 'accept' 
                        || $this->attributes['action'] == 'reject'
                    )
                )

                // Has no notifier field value, simply.
                || !$this->attributes["notifier_id"]

                /**
                 * Is a Comment deletion Notification by Moderation, for the 
                 * Comment's User.
                 */
                || (
                    $this->attributes['type'] == Comment::class
                    && $this->attributes['action'] == 'delete'
                    && iAm($this->attributes['notified_id'])
                );
    }

    /**
     * Generates a textual presentation of this Notification based on its data 
     * fields, and depending on its 'action' and 'type' field values.
     *
     * @return string       The textual representation of the Notification.
     */
    public function toText()
    {
        $type   = $this->attributes['type'];
        $action = $this->attributes['action'];
  
        $verb = resolveVerb($type . "_" . $action);

        if($type == Comment::class)
        {
            // 'Create' notification for Post owner.
            if($action == "create")
            {
                return $this->notifier->name . " " . $verb . " your Post.";
            }

            // 'Reply' notification for Parent Comment owner.
            elseif($action == "reply")
            {
                return $this->notifier->name . " " . $verb . " your Comment.";
            }

            // 'Reply' notification for Parent Comment's Post owner.
            elseif($action == "reply_on")
            {
                return $this->notifier->name . " " . $verb . " your Post.";
            }

            elseif($action == "report")
            {
                $comment = Comment::find($this->notifiable_id);

                if($comment)
                {
                    return "'" . $this->notifier->name . "' " . $verb
                       . " a Comment by '" . $comment->user->name . "'.";
                }
            }

            elseif($action == "accept" || $action == "reject")
            {
                if(iAm($this->notifier))
                {
                    return "Your Comment was " . $verb . ".";
                }
                else
                {
                    return $this->notifier->name . "'s Comment you reported was "
                            . $verb
                            . ".";
                }
            }

            // 'Delete' Notification for Comment owner.
            if($action == "delete")
            {
                $comment = Comment::withTrashed()->find($this->notifiable_id);
                $post    = Post::find($comment->post_id);

                if($post)
                {
                    return "Your Comment on Post '" . $post->title . "' was "
                            . $verb . " by Moderation.";
                }
            }
        }

        elseif($type == Role::class)
        {
            /**
             * 'Attach Permission to Role' uses Notification's target_id field to 
             * store Permission reference.
             */
            if($action == "attach")
            {
                $permission = Permission::find($this->target_id);
                $role       = Role::find($this->notifiable_id);

                if($role && $permission)
                {
                    return $this->notifier->name . " " . $verb . " Permission '"
                            . $permission->slug . "' to Role '" . $role->slug
                            . "'.";
                }
            } 

            /**
             * 'Detach Permission from Role' uses Notification's target_id field 
             * to store Permission reference.
             */
            elseif($action == "detach")
            {
                $permission = Permission::find($this->target_id);
                $role       = Role::find($this->notifiable_id);

                if($role && $permission)
                {
                    return $this->notifier->name . " " . $verb . " Permission '"
                            . $permission->slug . "' from Role '" . $role->slug
                            . "'.";
                }
            } 
        }

        elseif($type == Post::class)
        {
            $post = Post::withTrashed()->find($this->notifiable_id);

            if($post)
            {
                if($action == "assign")
                {
                    $category = Category::find($this->target_id);

                    if($category)
                    {
                        return $this->notifier->name . " " . $verb . " your Post '"
                                . $post->title . "' to Category '" . $category->label
                                . "'.";
                    }
                }

                elseif($action == "accept" || $action == "reject")
                {
                    return "Your Post was " . $verb . ".";
                }

                elseif($action == "create")
                {
                    return $this->notifier->name . " " . $verb . " Post '"
                            . $post->title . "'"
                            . (
                                $post->category == null ? 
                                    ", uncategorised."
                                    : ", on Category '" . $post->category->label
                                        . "'."
                            );
                }
            }
        }

        elseif($type == Suggestion::class)
        {
            if($action == "create")
            {
                $category = Category::find($this->target_id);
                $post     = Post::withTrashed()->find($this->notifiable_id);

                if($category && $post)
                {
                    return $this->notifier->name . " " . $verb . " your Post '"
                            . $post->title . "' to Category '" . $category->label
                            . "'.";
                }
            }

            /**
             * 'Accept Suggestion' uses Notification's target_id field to store 
             * related Suggestion's Category reference.
             */
            elseif($action == "accept")
            {
                $category = Category::find($this->target_id);
                $post     = Post::withTrashed()->find($this->notifiable_id);

                if($category && $post)
                {
                    return $this->notifier->name . " " . $verb .
                            " your suggestion for Post '" . $post->title
                            . "' into Category '" . $category->label . "'.";
                }
            }
        }

        elseif($type == User::class)
        {
            $notifiedIsSelf = same($this->notified_id, me());

            /**
             * 'Attach Role to User' uses Notification's target_id field to store 
             * Role reference.
             */
            if($action == "attach_role")
            {
                $role          = Role::find($this->target_id);
                $user          = User::withTrashed()->find($this->notifiable_id);
                $messageTarget = $notifiedIsSelf ? "to you." : "to User '"
                                   . $user->name;

                if($role && $user)
                {
                    return $this->notifier->name . " " . $verb . " Role '"
                            . $role->name . "' " . $messageTarget;
                }
            } 

            /**
             * 'Detach Role from User' uses Notification's target_id field to 
             * store Role reference.
             */
            elseif($action == "detach_role")
            {
                $role          = Role::find($this->target_id);
                $user          = User::withTrashed()->find($this->notifiable_id);
                $messageTarget = $notifiedIsSelf ? "from you." : "from User '"
                                   . $user->name;

                if($role && $user)
                {
                    return $this->notifier->name . " " . $verb . " Role '"
                            . $role->name . "' " . $messageTarget;
                }
            }

            /**
             * 'Attach Permission to User' uses Notification's target_id field to 
             * store Permission reference.
             */
            elseif($action == "attach_permission")
            {
                $permission    = Permission::find($this->target_id);
                $user          = User::withTrashed()->find($this->notifiable_id);
                $messageTarget = $notifiedIsSelf ? "to you." : "to User '"
                                   . $user->name;

                if($permission && $user)
                {
                    return $this->notifier->name . " " . $verb . " Permission '"
                            . $permission->name . "' " . $messageTarget;
                }
            } 

            /**
             * 'Detach Permission from User' uses Notification's target_id field 
             * to store Permission reference.
             */
            elseif($action == "detach_permission")
            {
                $permission    = Permission::find($this->target_id);
                $user          = User::withTrashed()->find($this->notifiable_id);
                $messageTarget = $notifiedIsSelf ? "from you." : "from User '"
                                   . $user->name;

                if($permission && $user)
                {
                    return $this->notifier->name . " " . $verb . " Permission '"
                            . $permission->name . "' " . $messageTarget;
                }
            }

            /**
             * Replace word User with Account for all other cases which are 
             * User-reflexive.
             */
            else
            {
                return $this->notifier->name . " " . $verb . " your Account.";
            }
        }

        elseif($type == Contact::class)
        {
            if($action == "create")
            {
                $contact = Contact::find($this->notifiable_id);

                if($contact)
                {
                    return "Contact message from '" . $contact->email
                            . "' received.";
                }
            }
        }

        return $this->notifier->name . " " . $verb . " your " . resolveType($type)
                . ".";
    }

    /**
     * Gets a route for handling this Notification based on its 'action' and 
     * 'type' fields.
     *
     * @return string The route for this Notification.
     */
    public function getLinkRoute()
    {
        if($this->attributes['action'] == "delete")
        {
            switch($this->attributes['type'])
            {
                case "App\Category":
                    return route("categories.index");

                case "App\Comment":
                    $comment = Comment::withTrashed()->find($this->notifiable_id);

                    return $comment ? 
                        route("post.show", $comment->post->slug) 
                        : null;

                case "App\Post":
                    return route("post.index");

                case "App\User":
                    return route("users.index");
            }
        }

        if(
                $this->attributes['action'] == "report"
                && $this->attributes['type'] == Comment::class
        )
        {
            $comment = Comment::find($this->notifiable_id);

            return $comment ?
                route("comment.review", $comment)
                : null;
        }

        switch($this->attributes['type'])
        {
            case "App\Bookmark":
                return route("admin.index");

            case "App\Category":
                return route("categories.index");

            case "App\Comment":
                $comment  = Comment::withTrashed()->find($this->notifiable_id);

                if($comment)
                {
                    $postSlug = $comment->post()->withTrashed()->first()->slug;

                    return route("post.show", $postSlug);
                }
                break;

            case "App\Permission":
                return route("permissions.index");

            case "App\Role":
                return route("roles.index");

            case "App\Post":
                if($this->attributes['action'] == "create")
                {
                    return route('post.review', $this->notifiable_id);
                }

                $post = Post::withTrashed()->find($this->notifiable_id);

                return $post ? 
                    route("post.show", $post->slug) 
                    : null;

            case "App\Suggestion":
                return route("suggestions.received");

            case "App\User":
                $actions = [
                    "attach_role", 
                    "detach_role", 
                    "attach_permission", 
                    "detach_permission"
                ];

                if(in_array($this->attributes['action'], $actions))
                {
                    return route('user.profile.edit', $this->notifiable_id);
                }

                return route("user.profile.show", $this->notifiable_id);

            case "App\Contact":
                return route("contact.showreply", $this->notifiable_id);
        }

        return null;
    }
}
