<?php

namespace App;

use App\SearchUtils;
use Cviebrock\EloquentSluggable\Sluggable;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use Sluggable;

    /**
     * Return the sluggable configuration array for this model.
     *
     * @return array The sluggable configuration for model 'Category'.
     */
    public function sluggable()
    {
        return [
            'slug'         => [
                'source'   => 'label',
                'onUpdate' => true
            ]
        ];
    }

    protected $fillable = [
        'label', 
        'created_by', 
        'is_active', 
        'slug',
        'icon',
        'updated_by'
    ];

    /**
     * Returns the Posts of this Category.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * Returns the User which created this Category, even if soft-deleted.
     * Relation needs to be kept to show name of Category creator even if the User 
     * account itself no longer exists.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class, "created_by")
                ->withTrashed();
    }

    /**
     * Returns the User which created this Category.
     * An alias function to user().
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->user();
    }

    /**
     * Returns the User which last updated this Category.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updater()
    {
        return $this->belongsTo(User::class, "updated_by");
    }

    /**
     * Returns the Suggestions for this Category.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function suggestions()
    {
        return $this->hasMany(Suggestion::class);
    }

    /**
     * Verifies is this Category has a value for 'icon' attribute.
     *
     * @return bool Returns true is it has a value, false otherwise.
     */
    public function hasIcon()
    {
        return $this->attributes['icon'] != null;
    }

    /**
     * Accessor method that returns the 'icon' attribute of this Category with 
     * relative path.
     *
     * @param  string $value The attribute value to access.
     *
     * @return string The modified 'icon' attribute of this Category.
     */
    public function getIconAttribute($value)
    {
        return asset($value);
    }

    /**
     * Returns the 'icon' filename from server, of this Category.
     *
     * @return string The server path of the 'icon' filename.
     */
    public function getCategoryIconServerFile()
    {
        return public_path() . $this->attributes['icon'];
    }

    /**
     * Returns the relative, actual icon path stored on the database for this 
     * Category.
     *
     * @return string The relative path for this Category's icon field.
     */
    public function getIconRelativePath()
    {
        return $this->attributes['icon'];
    }

    /**
     * Searches Categories matching the received keywords as parameters.
     *
     * @param  array $keywords The keywords to search based on.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function search(array $keywords)
    {
        $mandatoryKeywords = SearchUtils::getMandatoryKeywords($keywords);
        $optionalKeywords  = SearchUtils::getOptionalKeywords($keywords);
        $excludedKeywords  = SearchUtils::getExcludedKeywords($keywords);
        $fields            = ["label"];

        $queryBuilder = Category::where(

        // Append the optional keywords (not using the 'include' operator).
            function($query) use ($optionalKeywords, $fields)
            {
                foreach($optionalKeywords as $keyword)
                {
                    SearchUtils::match($query, "categories", $keyword, $fields);
                }
            }
        )

        // Append the mandatory keywords from using the 'include' operator.
        ->where(
            function($query) use ($mandatoryKeywords, $fields)
            {
                foreach($mandatoryKeywords as $keyword)
                {
                    $query->where(
                        function($query) use ($keyword, $fields)
                        {
                            SearchUtils::match($query, "categories", $keyword, $fields);
                        }
                    );
                }
            }
        )

        // Append the excluded keywords from using the 'exclude' operator.
        ->where(
            function($query) use ($excludedKeywords, $fields)
            {
                foreach($excludedKeywords as $keyword)
                {
                    SearchUtils::notMatch($query, "categories", $keyword, $fields);
                }
            }
        )

        // Filter on active Categories or permissive Role.
        ->where(
            function($query)
            {
                if(loggedIn() && !iHaveRole('admin'))
                {
                    $query->where('is_active', '1');
                }
            }
        );

        return $queryBuilder;
    }
}
