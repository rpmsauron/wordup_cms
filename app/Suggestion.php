<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Suggestion extends Model
{
    protected $fillable = [
        'user_id', 
        'post_id',
        'category_id',
        'open',
        'count'
    ];

    /**
     * Returns the User which created this Suggestion.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Returns the Post which this Suggestion is for.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function post()
    {
        return $this->belongsTo(Post::class);
    }

    /**
     * Returns the Category this Suggestion is for.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    /**
     * Returns the Query Builder for retrieving the Suggestions received, for a User.
     * Is User is omitted, returns them for the logged in session User.
     *
     * @param  User                                 $user The User for whom to 
     *                                              retrieve the received Suggestions.
     *
     * @return Illuminate\Database\Eloquent\Builder The query builder for
     *                                              retrieving received Suggestions 
     *                                              for a User.
     */
    public static function getReceived(User $user = null)
    {
        $user ??= me();

        return $suggestions = Suggestion::join('posts', 'post_id', '=', 'posts.id')
            ->join("categories", "suggestions.category_id", "=", "categories.id")
            ->where("categories.is_active", 1)
            ->where("open", 1)
            ->where("posts.user_id", $user->id)
            ->select(
                    "suggestions.id", 
                    "suggestions.post_id", 
                    "suggestions.category_id", 
                    "suggestions.user_id", 
                    "open", 
                    "count"
            );
    }
}
