<?php

namespace App;

use App\Exceptions\InvalidConfigurationException;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Facades\Log;

class SearchUtils
{
    /**
     * Generates an array of unique (removes duplicates) keywords based on a textual-
     * based input, that may contain operators to modify it.
     *
     * Supported operators (defined by default on config):
     *
     * " symbol: delimits a literal string as a keyword, so that a search criteria 
     *           may contain spaces.
     *           I.e.: search "find me" will return literal occurrences of "find me", 
     *           and not the ones containing either just "find" nor "me".
     * + symbol: force includes term on search results.
     * - symbol: force excludes term from search results.
     * % symbol: delimits a keyword start or end, meaning the search term must end 
     *           with the adjoined character.
     * * symbol: is a wildcard, meaning anything in its place within the search term.
     *
     * @param  string $input The input to generate the keywords for.
     *
     * @return array  The keywords from the input.
     */
    public static function getKeywords(string $input)
    {
        $delimiter = '"'; // Literal delimiter symbol.
        $search    = trim($input);

        /**
         * If odd occurrences of literal delimiter symbol, ignore (remove) the last 
         * one in the search input string.
         */
        if(substr_count($search, $delimiter) % 2 > 0)
        {
            $search = preg_replace(
                    '/(' . $delimiter . '(?!.*' .$delimiter . '))/',
                    '',
                    $search
            );
        }

        $keywords     = [];
        $keyword      = "";
        $ignoreSpaces = false;

        /**
         * Counts occurrences of literal delimiter symbol for parity tracking 
         * (opening and closing).
         */
        $literalDelimiter = 0;

        // Iterates input search character by character.
        for($i = 0; $i < strlen($search); $i++)
        {
            // Found opening literal delimiter symbol.
            if(substr($search, $i, 1) == $delimiter && $literalDelimiter % 2 == 0)
            {
                $ignoreSpaces = true;
                $literalDelimiter++;
            } 
            // Found closing literal delimiter symbol.
            elseif(substr($search, $i, 1) == $delimiter && $literalDelimiter % 2 > 0)
            {
                $ignoreSpaces = false;
                $literalDelimiter++;
            } 
            // Found a regular symbol.
            else
            {
                if(substr($search, $i, 1) == " " && !$ignoreSpaces)
                {
                    array_push($keywords, trim($keyword));
                    $keyword = substr($search, $i, 1);
                } else
                {
                    $keyword .= substr($search, $i, 1);
                }
            }
        }

        // Store last one.
        array_push($keywords, trim($keyword));

        return array_unique($keywords);
    }

    /**
     * Returns an array of what keywords are mandatory based on them starting with 
     * the 'include' operator (fetched from config).
     *
     * @param  array $keywords The array of keywords from which to get the ones 
     *                         starting with the 'include' operator.
     *         
     * @return array The array of keywords that start with the 'include' operator.
     */
    public static function getMandatoryKeywords(array $keywords)
    {
        return self::getKeywordsForOperator($keywords, config("constants.search.operators.include"));
    }

    /**
     * Returns an array of what keywords are mandatory based on them starting with 
     * the 'exclude' operator (fetched from config).
     *
     * @param  array $keywords The array of keywords from which to get the ones 
     *                         starting with the 'exclude' operator.
     *         
     * @return array The array of keywords that start with the 'exclude' operator.
     */
    public static function getExcludedKeywords(array $keywords)
    {
        return self::getKeywordsForOperator($keywords, config("constants.search.operators.exclude"));
    }

    /**
     * Returns array of what keywords are optional based on them not starting with 
     * the 'include' or 'exclude' operators (fetched from config).
     *
     * @param  array $keywords         The array of keywords from which to get 
     *                                 the ones not starting with the 'include' 
     *                                 or 'exclude' operators.
     *
     * @return array $optionalKeywords The array of keywords that do not start 
     *                                 with the 'include' or 'exclude' operators.
     */
    public static function getOptionalKeywords(array $keywords)
    {
        $includeOperator  = config("constants.search.operators.include");
        $excludeOperator  = config("constants.search.operators.exclude");
        $optionalKeywords = [];

        foreach($keywords as $keyword)
        {
            // If keyword string does not start with the operator string.
            if(
                substr($keyword, 0, strlen($includeOperator)) != $includeOperator 
                && substr($keyword, 0, strlen($excludeOperator)) != $excludeOperator
            )
            {
                array_push($optionalKeywords, $keyword);
            } 
        }

        return $optionalKeywords;
    }

    /**
     * Returns an array of keywords stripped off from an attached given operator. 
     * Operator string is fetched from config.
     * Used for keyword delimiting operators only, not keyword inline operators 
     * (such asSQL's wildcard '%' functionality operator).
     *
     * @param  array  $keywords The array of keywords from which to get the ones 
     *                          starting with the given operator.
     * @param  string $operator The operator fetched from config, to check keywords 
     *                          for.
     *
     * @return array  The array of keywords that start with the given operator.
     */
    private static function getKeywordsForOperator(array $keywords, string $operator)
    {
        // Invalid Config.
        try
        {
            checkInvalidConfigDuplicateOperators();
        } catch(InvalidConfigurationException $exception)
        {
            Log::error($exception);

            // Continue and return empty set.
            return [];
        }

        // Actual algorithm.
        $matchingKeywords = [];

        foreach($keywords as $keyword)
        {
            // If keyword string starts with the operator string.
            if(substr($keyword, 0, strlen($operator)) == $operator)
            {
                array_push(
                        $matchingKeywords, 
                        substr($keyword, strlen($operator), strlen($keyword))
                );
            }
        }

        return $matchingKeywords;
    }

    /**
     * Returns a keyword with application-specific search wildcard operator 
     * replaced with SQL's wildcard operator to function with SQL's LIKE clause.
     *
     * @param  string $keyword The keyword to replace search operator.
     *
     * @return string The keyword with wildcard operator replaced.
     */
    private static function applyWildcard(string $keyword)
    {
        $wildcard = config("constants.search.operators.wildcard");

        /**
         * Usage of preg_quote() will quote regex-specific characters in case one 
         * of those is used, from the config. 
         * Take reference at the beginning of:
         * https://www.php.net/manual/en/function.preg-quote.php
         */
        $regex = '/' . preg_quote($wildcard) . '+/';

        return preg_replace($regex, "%", $keyword);
    }

    /**
     * Adds clause to Query Builder to add verification for keyword non-matching 
     * on given fields.
     *
     * @param Builder $query     The Query Builder to add clause to.
     * @param string  $tablename The name of the table to use, to prevent column 
     *                           ambiguity reference.
     * @param string  $keyword   The keyword as value for the clause to add.
     * @param array   $columns   An array of column names to match keyword value 
     *                           against, each as a clause.
     */
    public static function match(
            Builder &$query, 
            string $tablename, 
            string $keyword, 
            array $columns
    )
    {
        $keywordBuilder = self::buildDelimitedKeyword($keyword);

        foreach($columns as $column)
        {
            $query->orWhere("$tablename.$column", "LIKE", $keywordBuilder);
        }
    }

    /**
     * Adds clause to Query Builder to add verification for keyword matching on 
     * given fields.
     *
     * @param Builder $query     The Query Builder to add clause to.
     * @param string  $tablename The name of the table to use for column ambiguity 
     *                           prevention.
     * @param string  $keyword   The keyword as value for the clause to add.
     * @param array   $columns   An array of column names to match keyword value 
     *                           against, each as a clause.
     */
    public static function notMatch(
            Builder &$query, 
            string $tablename, 
            string $keyword, 
            array $columns
    )
    {
        $keywordBuilder = self::buildDelimitedKeyword($keyword);

        foreach($columns as $column)
        {
            $query->where("$tablename.$column", "NOT LIKE", $keywordBuilder);
        }
    }

    /**
     * Builds a keyword syntactically prepared for a SQL Like clause to fit in 
     * between wildcard symbols "%".
     * The app logic uses an operator to apply a logic reverse to SQL's: 
     * - it delimits the matching pattern instead of allowing arbitrary characters 
     *   in place of the wildcard symbol:
     *   -- On SQL: 
     *      % means arbitrary-length characters on a SQL Like clause, while having 
     *      nothing means delimitation;
     *   -- On the app search: 
     *      the operator means delimitation, while having nothing means arbitrary-
     *      length characters to match against a SQL Like clause.
     *
     * @param  string $keyword        The keyword given as a search term with 
     *                                delimiting operators.
     *
     * @return string $keywordBuilder The keyword prepared for wildcard-matching 
     *                                for a SQL's Like clause.
     */
    private static function buildDelimitedKeyword(string $keyword)
    {
        $operator           = config("constants.search.operators.delimiter");
        $keywordBuilder     = $keyword;
        $startingCharacters = substr($keyword, 0, strlen($operator));
        $endingCharacters   = substr($keyword, strlen($keyword) - strlen($operator), strlen($operator));

        // Keyword starts with operator string.
        if($startingCharacters == $operator)
        {
            // Trim search operator from keyword start.
            $keywordBuilder = substr($keywordBuilder, strlen($startingCharacters));
        } else
        {
            // Add SQL's wildcard operator to keyword start.
            $keywordBuilder = "%$keywordBuilder";
        }

        // Keyword ends in operator string.
        if($endingCharacters == $operator)
        {
            // Trim search operator from keyword ending.
            $keywordBuilder = substr($keywordBuilder, 0, 0 - strlen($endingCharacters));
        } else
        {
            // Add SQL's wildcard operator to keyword ending.
            $keywordBuilder = "$keywordBuilder%";
        }

        /**
         * Applying the application-specific wildcard operator needs to happen 
         * here, in order to prevent coincidental collision of application-specific 
         * delimiter operator being the same as SQL's native wildcard operator (%).
         * If applied elsewhere previous to here, the following would occur, from 
         * the following search example:
         * - a search for:
         * Le*cy rickydia*
         * - would be translated onto SQL as:
         * Le%cy rickydia%
         * - and only after (here), the delimiter logic would apply, thus removing 
         * the previously resultant replacement % characters, to run the delimitation 
         * operator logic.
         */
        return self::applyWildcard($keywordBuilder);
    }
}