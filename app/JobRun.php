<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class JobRun extends Model
{
    protected $fillable = [
        'job_id',
        'user_id'
    ];

    /**
     * Returns the User of this JobRun.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    /**
     * Returns the Job of this JobRun.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function job()
    {
        return $this->belongsTo(Job::class);
    }
}
