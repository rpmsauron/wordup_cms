<?php

namespace App;

use Askedio\SoftCascade\Traits\SoftCascadeTrait;
use App\SearchUtils;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;
    use SoftDeletes;
    use SoftCascadeTrait;

    protected $softCascade = [
        'posts'
    ];

    protected $dates = [
        'deleted_at'
    ];
    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username', 
        'name', 
        'email', 
        'password', 
        'avatar', 
        'is_public_email'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * Mutator method that encrypts this User's 'password' attribute.
     *
     * @param string $value The attribute value to modify.
     */
    public function setPasswordAttribute($value)
    {
        $this->attributes['password'] = bcrypt($value);
    }
    
    /**
     * Returns the Posts of this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function posts()
    {
        return $this->hasMany(Post::class);
    }

    /**
     * Returns the Posts reviewed by this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reviewedPosts()
    {
        return $this->hasMany(Post::class, 'reviewed_by');
    }

    /**
     * Returns the Comments reviewed by this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reviewedComments()
    {
        return $this->hasMany(Comment::class, 'reviewed_by');
    }

    /**
     * Returns the Comments reported by this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function reportedComments()
    {
        return $this->hasMany(Comment::class, 'reported_by');
    }

    /**
     * Returns the Categories created by this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categories()
    {
        return $this->hasMany(Category::class, 'created_by');
    }

    /**
     * Returns the Categories assigned by this User to any Post.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categoriesAssigned()
    {
        return $this->hasMany(Category::class, 'assigned_category_by');
    }

    /**
     * Returns the Categories updated by this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function categoriesUpdated()
    {
        return $this->hasMany(Category::class, 'updated_by');
    }

    /**
     * Returns the Posts last updated by this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsUpdated()
    {
        return $this->hasMany(Post::class, 'updated_by');
    }

    /**
     * Returns the Posts last deleted by this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsDeleted()
    {
        return $this->hasMany(Post::class, 'deleted_by');
    }

    /**
     * Returns the Posts last deleted by this User.
     * An alias function to commentsDeleted()
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsRemoved()
    {
        return $this->commentsDeleted();
    }

    /**
     * Returns the Posts last restored by this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsRestored()
    {
        return $this->hasMany(Post::class, 'restored_by');
    }

    /**
     * Returns the Posts last restored by this User.
     * An alias function to commentsRestored.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function postsRecovered()
    {
        return $this->commentsRestored();
    }

    /**
     * Returns the Comments last updated by this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commentsUpdated()
    {
        return $this->hasMany(Comment::class, 'updated_by');
    }

    /**
     * Returns the Comments last deleted by this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commentsDeleted()
    {
        return $this->hasMany(Comment::class, 'deleted_by');
    }

    /**
     * Returns the Comments last deleted by this User.
     * An alias function to commentsDeleted()
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commentsRemoved()
    {
        return $this->commentsDeleted();
    }

    /**
     * Returns the Comments last restored by this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commentsRestored()
    {
        return $this->hasMany(Comment::class, 'restored_by');
    }

    /**
     * Returns the Comments last restored by this User.
     * An alias function to commentsRestored.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function commentsRecovered()
    {
        return $this->commentsRestored();
    }

    /**
     * Returns the Comments by this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function comments()
    {
        return $this->hasMany(Comment::class);
    }

    /**
     * Returns the Permissions of this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class)
                ->withTimestamps()
                ->withPivot('granted_by');
    }

    /**
     * Returns the Roles of this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class)
                ->withTimestamps()
                ->withPivot('granted_by');
    }

    /**
     * Returns the Bookmarks to this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function bookmarks()
    {
        return $this->hasMany(Bookmark::class);
    }

    /**
     * Returns the Suggestions by this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function suggestions()
    {
        return $this->hasMany(Suggestion::class);
    }

    /**
     * Returns the Notifications of this User as notified User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notifications()
    {
        return $this->hasMany(Notification::class, 'notified_id');
    }

    /**
     * Returns the Notifications of this User as notifier Used.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function notificationsTriggered()
    {
        return $this->hasMany(Notification::class, 'notifier_id');
    }

    /**
     * Returns the Messages sent by this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messagesSent()
    {
        return $this->hasMany(Message::class, 'from_id');
    }

    /**
     * Returns the Messages received by this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messagesReceived()
    {
        return $this->hasMany(Message::class, 'to_id');
    }

    /**
     * Returns both Messages received and sent by this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->messagesSent()
                ->union($this->messagesReceived()->toBase());
    }

    /**
     * Returns the Contacts this User has replied to.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function repliedContacts()
    {
        return $this->hasMany(Contact::class, 'replied_by');
    }

    /**
     * Returns the Contacts this User has sent.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function sentContacts()
    {
        return $this->hasMany(Contact::class, 'from_id');
    }

    /**
     * Returns the Views of this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function views()
    {
        return $this->hasMany(View::class);
    }

    /**
     * Returns the Permissions created by this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function permissionsCreated()
    {
        return $this->hasMany(Permission::class, "created_by");
    }

    /**
     * Returns the Permissions updated by this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function permissionsUpdated()
    {
        return $this->hasMany(Permission::class, "updated_by");
    }

    /**
     * Returns the Roles created by this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rolesCreated()
    {
        return $this->hasMany(Role::class, "created_by");
    }

    /**
     * Returns the Roles updated by this User.
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function rolesUpdated()
    {
        return $this->hasMany(Role::class, "updated_by");
    }

    /**
     * Verifies is this User has a specific Role given by the 'slug' attribute of 
     * the Role.
     *
     * @param  string $slug The slug of the Role to verify.
     *
     * @return bool   Returns true if this User has the Role, false otherwise.
     */
    public function hasRole(string $slug)
    {
        foreach($this->roles as $role)
        {
            if($slug == $role->slug)
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Verifies is this User has a specific Permission given by the 'slug' 
     * attribute of the Permission.
     *
     * @param  string $permissionName The slug of the Permission to verify.
     *
     * @return bool   Returns true if this User has the Permission, false otherwise.
     */
    public function userHasPermission(string $permissionName)
    {
        foreach($this->permissions as $permission)
        {
            if($permissionName == $permission->name)
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Verifies is this User has a value for 'avatar' attribute.
     *
     * @return bool Returns true is it has a value, false otherwise.
     */
    public function hasAvatar()
    {
        return $this->attributes['avatar'] != null;
    }

    /**
     * Accessor method that returns the 'avatar' attribute of this User or a 
     * gravatar.
     *
     * @param  string $value The attribute value to access.
     *
     * @return string The url of the 'avatar' attribute or of the gravatar of this 
     *                User.
     */
    public function getAvatarAttribute($value)
    {
        return $this->hasAvatar() ? 
                $this->getAvatar($value) 
                : $this->getGravatar();
    }

    /**
     * Returns the 'avatar' attribute of this User modified to be a relative path.
     *
     * @param  string $value The value to access.
     *
     * @return string The modified path of the 'avatar' attribute.
     */
    private function getAvatar($value)
    {
        return asset($value);
    }

    /**
     * Returns the 'avatar' filename from server, of this User.
     *
     * @return string The server path of the 'avatar' filename.
     */
    public function getAvatarServerFile()
    {
        return public_path() . $this->attributes['avatar'];
    }

    /**
     * Returns a gravatar url for this User.
     *
     * @return string The gravatar url.
     */
    private function getGravatar()
    {
        $hash = md5(Str::lower(trim($this->attributes['email']))) . "?d=mm";

        return "http://gravatar.com/avatar/" . $hash;
    }

    /**
     * Verifies if this User has Categories they can manage. 
     * A User can manage a Category if they are its creator or has the Policy to 
     * do so, by means of Roles and Permissions and others (check CategoryPolicy 
     * class).
     *
     * @return bool Returns true if there are Categories created by this User, 
     *              false otherwise.
     */
    public function hasManageableCategories()
    {
        return 
                count($this->categories) > 0 
                || $this->can("manage", Category::class);
    }

    /**
     * Gets the Users that this User has granted a specific Role, or any Roles to.
     *
     * @param  string|null $roleSlug The Role slug to verify to have been granted 
     *                               to any User. If null, it checks for any Role.
     *
     * @return \Illuminate\Database\Eloquent\Builder The QueryBuilder for 
     *                                               retrieving the Users.
     */
    public function getUsersGranted(string $roleSlug = null)
    {
        $result = User::join('role_user', 'role_user.user_id', '=', 'users.id');

        if($roleSlug)
        {
            $result->join("roles", "roles.id", "=", "role_user.role_id")
                ->where('role_user.granted_by', $this->attributes['id'])
                ->where('roles.slug', $roleSlug);
        } else
        {
            $result->where('role_user.granted_by', $this->attributes['id']);
        }

        /**
         * Not to have overwritten equally-named fields from different joined 
         * tables, like 'id'.
         */
        $result->select('users.*'); 

        return $result;
    }

    /**
     * Verifies if this User has Users it has granted a specific Role, or any 
     * Roles to.
     *
     * @param  string $roleSlug The Role To verify if this User has granted to 
     *                           any other User. If null, it checks for any Role.
     *
     * @return bool   Returns true if this User has granted to any other Users a 
     *                specific Role or any Role (depending on the $roleSlug param), 
     *                false otherwise.
     */
    public function hasUsersGranted(string $roleSlug = null)
    {
        return $this->getUsersGranted($roleSlug)->count() > 0;
    }

    /**
     * Verifies if this User has granted another User a specific Role or any Role.
     *
     * @param  User        $user     The User to check for having granted Role to.
     * @param  string|null $roleSlug The Role slug to verify to have been granted. 
     *                               If null, it checks for any Role.
     *
     * @return bool        Returns true if this User has granted a Role to another 
     *                     User, false otherwise.
     */
    public function gaveRoleTo(User $user, string $roleSlug = null)
    {
        if($roleSlug)
        {
            $hasRoleUser = DB::table("role_user")
                ->join("roles", "roles.id", "=", "role_user.role_id")
                ->where("role_user.user_id", $user->id)
                ->where('role_user.granted_by', $this->attributes['id'])
                ->where('roles.slug', $roleSlug)
                ->first();
        } else
        {
            $hasRoleUser = DB::table("role_user")->where("user_id", $user->id)
                ->where('granted_by', $this->attributes['id'])
                ->first();
        }

        return $hasRoleUser ? true : false;
    }

    /**
     * Checks if this User has granted another User the 'Admin' Role. 
     *
     * @param  App\User $user The User to check if this User has given 'Admin' 
     *                        Role to.
     *
     * @return bool     Returns true if this has granted the other User the 
     *                  'Admin' Role, false otherwise.
     */
    public function gaveAdminTo(User $user)
    {
        return $this->gaveRoleTo($user, config('constants.roles.role.admin.slug'));
    }

    /**
     * Searches Users matching the received keywords as parameters.
     *
     * @param  array $keywords The keywords to search based on.
     *
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public static function search(array $keywords)
    {
        $mandatoryKeywords = SearchUtils::getMandatoryKeywords($keywords);
        $optionalKeywords  = SearchUtils::getOptionalKeywords($keywords);
        $excludedKeywords  = SearchUtils::getExcludedKeywords($keywords);
        $fields            = ["name", "username"];

        $queryBuilder = User::where(

        // Append the optional keywords (not using the 'include' operator).
            function($query) use ($optionalKeywords, $fields)
            {
                foreach($optionalKeywords as $keyword)
                {
                    SearchUtils::match($query, "users", $keyword, $fields);
                }
            }
        )

        // Append the mandatory keywords from using the 'include' operator.
        ->where(
            function($query) use ($mandatoryKeywords, $fields)
            {
                foreach($mandatoryKeywords as $keyword)
                {
                    $query->where(
                        function($query) use ($keyword, $fields)
                        {
                            SearchUtils::match($query, "users", $keyword, $fields);
                        }
                    );
                }
            }
        )

        // Append the excluded keywords from using the 'exclude' operator.
        ->where(
            function($query) use ($excludedKeywords, $fields)
            {
                foreach($excludedKeywords as $keyword)
                {
                    SearchUtils::notMatch($query, "users", $keyword, $fields);
                }
            }
        );

        return $queryBuilder;
    }

    /**
     * Gets all Notifications shown to this User, in paginated or regular set, 
     * with a limit that can be specified or otherwise set with a default value 
     * of 50. New Notifications (not cleared) show before, ordered by date, then 
     * the cleared old Notifications are shown, also ordered by date.
     *
     * @param  bool       $paginate Changes the return collection as a paginated 
     *                              result set if true, or a regular Eloquent 
     *                              Collection if false.
     * @param  int        $limit    Limit of results to fetch if value is given, 
     *                              otherwise it limits the results by a default 
     *                              value of 50.
     *
     * @return Collection The Eloquent Collection of all Notifications shown to 
     *                    this User.
     */
    public function getAllShownNotifications(bool $paginate, int $limit = 50)
    {
        $queryBuilder = Notification::where("notified_id", $this->attributes['id'])
            ->where(
                function($query)
                {
                    $this->excludeExceptions($query);
                }
            )
            ->orderBy("new", "DESC")
            ->orderBy("created_at", "DESC");

        return $paginate ? 
            $queryBuilder->paginate(config('constants.pagination.admin_notifications'))    
            : $queryBuilder->offset(0)->take($limit)->get();
    }

    /**
     * Gets Notifications limited to a specific default amount, from config, shown 
     * to this User.
     * New Notifications (not cleared) show before, ordered by date, then the 
     * cleared old Notifications are shown, also ordered by date.
     *
     * @param  bool       $paginate Changes the return collection as a paginated 
     *                              result set if true, or a regular Eloquent 
     *                              Collection if false.
     *
     * @return Collection The Eloquent Collection of limited amount Notifications 
     *                    shown to this User.
     */
    public function getNotificationsShownDefault(bool $paginate = false)
    {
        $queryBuilder = Notification::where("notified_id", $this->attributes['id'])
            ->where(
                function($query)
                {
                    $this->excludeExceptions($query);
                }
            )
            ->offset(0)->take(config('constants.notifications.max'))
            ->orderBy("new", "DESC")
            ->orderBy("created_at", "DESC");

        return $paginate ? 
            $queryBuilder->paginate(config("constants.notifications.max")) 
            : $queryBuilder->get();
    }

    /**
     * Gets the new Notifications (not cleared) shown to this User.
     *
     * @return Collection The Eloquent Collection of new Notifications shown to 
     *                    this User.
     */
    public function getNewNotifications()
    {
        return Notification::where("notified_id", $this->attributes['id'])
            ->where(
                function($query)
                {
                    $this->excludeExceptions($query);
                }
            )
            ->where("new", 1)
            ->get();
    }

    /**
     * Gets the amount of new Notifications (not cleared) shown to this User.
     *
     * @return int The amount of new Notifications shown to this User.
     */
    public function getCountNewNotifications()
    {
        return Notification::where("notified_id", $this->attributes['id'])
            ->where(
                function($query)
                {
                    $this->excludeExceptions($query);
                }
            )
            ->where("new", 1)
            ->count();
    }

    /**
     * Adds 'Where' clauses to Query Builder to leave out Notifications that are 
     * exceptional, and not to be shown to this User.
     *
     * @param  Builder $query The Query Builder to add 'Where' clauses to.
     *
     * @return Builder The Query Builder with the 'Where' clauses added to it.
     */
    private function excludeExceptions(Builder &$query)
    {
        // Exclude Notifications for a User's own Account deletion, from showing.
        $query
        ->where(
            function($query)
            {
                $query
                    ->where("action", "delete")
                    ->where("type", User::class)
                    ->where("notifiable_id", $this->attributes['id']);
            }
        , null, null, "and not")

        /**
         * Exclude Notifications for a User's new Post creation from showing to 
         * themselves.
         */
        ->where(
            function($query)
            {
                $query
                    ->where("action", "create")
                    ->where("type", Post::class)
                    // notifier_id is the Post author.
                    ->where("notifier_id", $this->attributes['id']);
            }
        , null, null, "and not")

        /**
         * Exclude Notifications for a Moderator or Admin User's new Post 
         * acceptance from showing to themselves.
         */
        ->where(
            function($query)
            {
                if(iAmAdmin() || iAmModerator())
                {
                    $query
                        ->where("action", "accept")
                        ->where("type", Post::class)
                        ->where("notified_id", $this->attributes['id']);
                }
            }
        , null, null, "and not")

        /**
         * Exclude Notifications for the User for their reporting of Comments.
         */
        ->where(
            function($query)
            {
                $query
                    ->where("action", "report")
                    ->where("type", Comment::class)
                    ->where("notifier_id", me()->id);
            }
        , null, null, "and not");
    }

    /**
     * Gets the amount of 'new' Messages shown to this User.
     *
     * @return int The amount of new Notifications shown to this User.
     */
    public function getCountNewMessages()
    {
        return Message::where("to_id", $this->attributes['id'])
            ->where("new", 1)
            ->count();
    }

    /**
     * Gets Messages limited to a specific default amount, from config, shown 
     * to this User.
     * New Messages show before, ordered by date, then the not 'new' Messages are 
     * shown, also ordered by date.
     *
     * @param  bool       $paginate Defines whether the returned collection is a 
     *                              paginated result set, or a regular Eloquent 
     *                              Collection.
     *
     * @return Illuminate\Database\Eloquent\Collection The Eloquent Collection of 
     *                                                 limited amount Messages 
     *                                                 shown to this User.
     */
    public function getMessagesShownDefault(bool $paginate = false)
    {
        $queryBuilder = Message::where("to_id", $this->attributes['id'])
            ->offset(0)->take(config('constants.messages.max'))
            ->orderBy("new", "DESC")
            ->orderBy("created_at", "DESC");

        return $paginate ? 
                $queryBuilder->paginate(config("constants.messages.max")) 
                : $queryBuilder->get();
    }

    /**
     * Gets all Users with specific Roles given by their names.
     *
     * @param  $rolenames The names of the Roles.
     *
     * @return Collection The Eloquent Collection of Users with a specific Role.
     */
    public static function getAllUsersWithRoleNames($rolenames)
    {
        $rolenames = is_array($rolenames) ? $rolenames : (array) $rolenames;

        return User::join("role_user", "users.id", "=", "user_id")
                ->join("roles", "roles.id", "=", "role_id")
                ->whereIn("roles.name", $rolenames)
                ->select("users.*")
                ->groupBy("username")
                ->get();
    }

    public static function getAllUsersWithRoleName(string $rolename)
    {
        return Role::where("name", $rolename)->first()->users;
    }
    
    /**
     * Gets the User that has deleted this User, if it has been deleted, based on 
     * the existence of a Notification for it.
     * It does not depend on whether the User still is deleted, but rather on 
     * whether there is Notification still open for it.
     *
     * @param  User $user The User that was deleted.
     *
     * @return User The User that deleted the first User, if it is deleted, 
     *              otherwise, returns null.
     */
    public function getRemover()
    {
        return User::join("notifications", "notifications.notifier_id", "=", "users.id")
            ->select("users.*")
            ->where("notified_id", $this->attributes['id'])
            ->where("action", "delete")
            ->where("type", User::class)
            ->where("new", 1)
            ->first();
    }

    /**
     * Checks if this User has any 'active' Permissions for one or more 'actions' 
     * over one or more 'models'.
     *
     * @param  mixed $models  The 'model' values which to verify there is any 
     *                        Permission for the given User.
     *                        Can be a string formatted 'model' value or as an 
     *                        array of values.
     * @param  mixed $actions The 'action' values which to verify there is any 
     *                        Permission for the given User. 
     *                        Can be a string formatted 'action' value or as an 
     *                        array of values.
     *
     * @return bool  Returns true if User has any Permission passed with 'action' 
     *               values and 'model' values, returns false otherwise.
     */
    public function hasPermissionFor($models, $actions = null)
    {
        $models                          = is_array($models) ? $models : [$models];
        $existingPermissionsQueryBuilder = Permission::where("is_active", 1)
            ->when($actions != null, 
            function($existingPermissionsQueryBuilder) use (&$actions)
            {
                $actions = is_array($actions) ? $actions : [$actions];
                $existingPermissionsQueryBuilder->whereIn("action", $actions);

            }
        );
        $existingPermissionsQueryBuilder->whereIn("model", $models);
        $existingPermissions = $existingPermissionsQueryBuilder->get();

        // Permissions attached to User directly.
        foreach($this->permissions as $userPermission)
        {
            foreach($existingPermissions as $existingPermission)
            {
                if($userPermission->id == $existingPermission->id)
                {
                    return true;
                }
            }
        }

        // Permissions attached to User's Roles.
        foreach($this->roles as $role)
        {
            foreach($role->permissions as $rolePermission)
            {
                foreach($existingPermissions as $existingPermission)
                {
                    if($rolePermission->id == $existingPermission->id)
                    {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    /**
     * Checks authorization for multiple Policies at once for the User, over a 
     * model classname or specific instance, passed as the 'target' parameter.
     *
     * @param  array $policies The Policies to check if this User is authorized for.
     * @param  mixed $target   The model class name or instance to check Policy 
     *                         over.
     *
     * @return bool  Returns true if this User is authorized any Policy, false 
     *               otherwise.
     */
    public function canAny(array $policies, $target)
    {
        foreach($policies as $policy)
        {
            if($this->can($policy, $target))
            {
                return true;
            }
        }

        return false;
    }

    /**
     * Checks whether this User has the Role 'Admin'.
     *
     * @return bool Returns true if this User has Role 'Admin', false otherwise.
     */
    public function isAdmin()
    {
       return $this->hasRole(config('constants.roles.role.admin.slug'));
    }

    /**
     * Checks whether this User has the Role 'Moderator'.
     *
     * @return bool Returns true if this User has Role 'Moderator', false otherwise.
     */
    public function isModerator()
    {
       return $this->hasRole(config('constants.roles.role.moderator.slug'));
    }
}
