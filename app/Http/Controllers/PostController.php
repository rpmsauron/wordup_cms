<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\StorePost;
use App\Http\Requests\UpdatePost;
use App\Post;
use App\SearchUtils;
use App\View;
use Illuminate\Support\Str;
use \Conner\Tagging\Model\Tag;

class PostController extends Controller
{
    /**
     * Controller action for route 'post.index'.
     * This action does not censor the non-reviewed nor disallowed Posts.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $page   = request('page') ? request('page') : "1";
        $filter = request('filter') ? request('filter') : "active";

        $itemsPerPage = config('constants.pagination.admin_listing_posts');

        // View everyone's posts, either as Moderator or Admin or with the Permission to do so.
        if(me()->can('manageEveryones', Post::class))
        {
            /**
             * Admins and those with the Permissions to do so, can see everyone's Posts, 
             * including deleted ones, for the ability of untrashing.
             */
            if(me()->canAny(['restore', 'delete' ], Post::class))
            {
                // If --originally-- had the filter parameter on the request.
                if(request('filter'))
                {
                    switch ($filter)
                    {
                        // Show (soft-)deleted Posts only.
                        case 'deleted':
                            $posts = Post::onlyTrashed()
                                ->paginate($itemsPerPage);
                            break;

                        // Show all Posts.
                        case 'all':
                            $posts = Post::withTrashed()
                                ->paginate($itemsPerPage);
                            break;

                        // Show one's own Posts.
                        case 'own':
                            $posts = me()->posts()->withTrashed()
                                ->paginate($itemsPerPage); // view own posts, only.
                            break;

                        // Show only active (non-soft-deleted) Posts, by default, if any other request param (including the default one, 'active').
                        default:
                            $posts = Post::paginate($itemsPerPage);
                    }
                } else
                {
                    // Show only active (non-soft-deleted) Posts, by default, if absent request param.
                    $posts = Post::paginate($itemsPerPage); // 'Active' Users by default, if absent request param.
                }
            }
            // Everyone else other than 'Admin' Users are shown everyone's 'Active' Posts (such as 'Moderators', for instance).
            else
            {
                $posts = Post::paginate($itemsPerPage);
            }
        } 
        // Show one's own Posts, only as fallback.
        else
        {
            $posts = me()->posts()->paginate($itemsPerPage);
        }

        return view('admin.posts.index', compact('posts', 'page', 'filter'));
    }

    /**
     * Controller action for route 'post.show'.
     *
     * @param  string $slug The slug of the Post to show.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($slug)
    {
        $post = Post::where('slug', $slug)->first();

        if($post && ($post->allowed == 1 || (iAmAdmin() || iAmModerator())))
        {
            $rootCommentsPaginated = $post->rootCommentsPaginated();
            // All Categories to list on side navigation.
            $categories            = Category::where('is_active', 1)->get();
            // All related Posts to list on side navigation.
            $relatedPosts          = $post->getRelated();

            // View count.
            $this->handleViewCount($post);

            return view('blog-post', compact('post', 'categories', 'rootCommentsPaginated', 'relatedPosts'));
        }

        abort(404);
    }

    /**
     * Controller action for route 'post.create'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $categories = Category::where('is_active', 1)->get();

        // If loading view from home
        if(request("from") == "home")
        {
            return view('home.posts.create', ['categories' => $categories]);
        }

        // If loading view from admin dashboard
        return view('admin.posts.create', ['categories' => $categories]);
    }

    /**
     * Controller action for route 'post.store'.
     *
     * @param  StorePost $request  The request.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(StorePost $request)
    {
        $inputs = $request->validated();

        // Get Tags from request without duplicates.
        $tags = array_unique(explode(",", $request["tags"]));

        // Enforce limit of Tags per Post.
        if(count($tags) > config('constants.tags.post_limit'))
        {
            session()->flash("post-tags-limit-message", config('constants.messages.error.post_tags_limit'));

            return redirect()->route("post.create");
        }

        if($request['post_image'])
        {
            $inputs['post_image'] = $request['post_image']->store('posts');
        }

        $inputs['category_id'] = $request['category_id'];

        $post = me()->posts()->create($inputs);
        $post->tag($tags);

        $seePost = false;

        if(iAmAdmin() || iAmModerator())
        {
            // Admins and Moderators do not need reviewing / moderation of their own Posts.
            $post->allowed     = 1;
            $post->reviewed    = 1;
            $post->reviewed_by = me()->id;
            $post->save();
            $seePost           = true;
            $message           = "Post '" . $inputs['title'] . "' was created.";
        } else
        {
            $message           = "Post '" . $inputs['title'] . "' was created and awaits approval.";
        }

        // Notify Moderators and Admins (even self, so that moderation link is created) 
        // of new Post created by this User.
        $roles = [config("constants.roles.role.admin.name"), config("constants.roles.role.moderator.name")];
        notifyAll($roles, true, null, $post->id, Post::class, "create");

        session()->flash("post-created-message", $message);

        if($seePost)
        {
            return redirect()->route("post.show", [$post->slug]);
        }

        return redirect()->route("home");
    }

    /**
     * Controller action for route 'post.edit'.
     *
     * @param  Post $post The post to edit.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit(Post $post)
    {
        $this->authorize('update', $post);

        // Request url get parameters needed to go back to, after update.
        $page   = request('page') ? request('page') : "1";
        $filter = request('filter') ? request('filter') : "active";

        // Save referer into session to redirect (two urls back), to the right place after update() is called.
        // Needs to verify first in case refer becomes the same page by having clicked Cancel once or more times, 
        // which redirects to the same page.
        if(strtok(session('referer'), '?') == request()->url())
        {
            session(['referer' => request()->headers->get('referer')]);
        }

        $categories = Category::where('is_active', 1)->get();

        return view('admin.posts.edit', compact('post', 'categories', 'page', 'filter'));
    }

    /**
     * Controller action for route 'post.update'.
     *
     * @param  UpdatePost $request The request.
     * @param  Post       $post    The post to update.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(UpdatePost $request, Post $post)
    {
        $oldImage = $post->getPostImageServerFile();

        $inputs = $request->validated();

        // Get Tags from request without duplicates.
        $tags = array_unique(explode(",", $request["tags"]));

        // Enforce limit of Tags per Post.
        if(count($tags) > config('constants.tags.post_limit'))
        {
            session()->flash("post-tags-limit-message", config('constants.messages.error.post_tags_limit'));

            return redirect()->route("post.create");
        }

        // Get current Tags for this Post, in order to check for changes in them.
        $currentTagValues = $post->tagNames();

        processFileField('post_image', $post);

        $post->category_id = $request['category_id'] == "" ? null : $request['category_id'];
        $post->title       = $inputs['title'];
        $post->body        = $inputs['body'];

        // Update related Tags.
        $post->retag($tags);
        sort($currentTagValues);
        sort($tags);

        // If changes occurred.
        if($post->isDirty() || $currentTagValues != $tags)
        {
            // Checks before model saving otherwise its 'dirty' status changes again.
            $imageChanged = $post->isDirty("post_image");

            $post->updated_by = me()->id;
            if($post->isDirty("category_id"))
            {
                $post->assigned_category_by = me()->id;
            }
            $post->save();

            // Handles deletion of old file (only if there were changes).
            handleOldFile($oldImage, $imageChanged);

            /**
             * Check for unused Tags for all Taggable models (should only be Posts, but no 
             * other modals are Taggable), and if any, remove them.
             */
            Tag::where('tagging_tags.count', 0)
                ->whereIn('tagging_tags.slug', $currentTagValues)
                ->delete();

            // Notify Post's User on update by some other User.
            if(!iAm($post->user))
            {
                notify($post->user, $post->id, Post::class, "update");
            }

            session()->flash("post-updated-message", "Post '" . $inputs['title'] . "' was updated.");

            return redirect()->route("post.index", $request->query());
        }

        session()->flash("post-updated-message", "Nothing to update.");

        return back()->with($request->query());
    }

    /**
     * Controller action for route 'post.destroy'.
     *
     * @param  Post $post The post to delete.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function destroy(Post $post)
    {
        $this->authorize('delete', $post);

        $bookmarkReferences = $post->bookmarks;

        $post->deleted_by = me()->id;
        $post->save();
        $post->delete();

        // Notify Post's User on deletion by some other User.
        if(!iAm($post->user))
        {
            notify($post->user, $post->id, Post::class, "delete");
        }
        /**
         * Notify any User that had bookmarked this Post except current session 
         * User (may also have bookmarked it but requires no Notification).
         */
        if($bookmarkReferences->count() > 0)
        {
            foreach($bookmarkReferences as $bookmark)
            {
                if(!iAm($bookmark->user))
                {
                    notify($bookmark->user, $post->id, Post::class, "delete");
                }
            }
        }
        // Automatically clears pending Notifications for this Post's update for everyone.
        unnotify(null, null, $post->id, Post::class, "update");

        session()->flash("post-deleted-message", "Post '" . $post->title . "' was deleted");

        return back();
    }

    /**
     * Controller action for route 'post.filter'.
     *
     * @param  string $slug The slug of the Category to filter Posts by.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function filter($slug)
    {
        // The Category to filter by.
        $category_filter = Category::where('slug', $slug)
            ->where('is_active', 1)
            ->first();

        if(!$category_filter)
        {
            abort(404);
        }

        // All Categories to list on side navigation.
        $categories = Category::where('is_active', 1)->get();

        // Posts filtered by Category.
        $posts = Post::getPublicPosts()
            ->where('category_id', $category_filter->id)
            ->orderBy('created_at', 'desc')
            ->paginate(config('constants.pagination.admin_listing_posts'));

        return view('home', compact('posts', 'category_filter', 'categories'));
    }

    /**
     * Controller action for route 'post.restore'.
     * Note: Route-Model binding returns 404 if model is soft-deleted: 
     * the id needs to be passed, then manually retrieve the model for it. 
     *
     * @param  int $post_id The id of the post to restore.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function restore(int $post_id)
    {
        $post = Post::withTrashed()->findOrFail($post_id);

        $this->authorize('restore', $post);

        /**
         * Not using restore(), changing field manually deliberately because the 
         * restore should not soft-cascade, only the deletion:
         * A User may have un-bookmarked a Post deliberately, and the recovery of 
         * that Post should never trigger an auto-restore of all soft-deleted 
         * related Bookmarks otherwise the manually deleted Bookmarks would be 
         * also untrashed by untrashing a Post.
         */
        $post->deleted_at = null;
        $post->restored_by = me()->id;
        $post->save();

        // Notify Post's User on untrashing by some other User.
        if(!iAm($post->user))
        {
            notify($post->user, $post->id, Post::class, "undelete");
        }

        session()->flash("post-restored-message", "Post '" . $post->title . "' was restored.");

        return back();
    }

    /**
     * Controller action for route 'post.filter.tag'.
     *
     * @param  string $slug The slug of the Tag to filter Posts by.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function filterByTag($slug)
    {
        // The Tag to filter by.
        $tag_filter = Tag::where('slug', $slug)->first();

        if(!$tag_filter)
        {
            abort(404);
        }

        // All Categories to list on side navigation.
        $categories = Category::where('is_active', 1)->get();

        // The Tag to filter by.
        $posts = Post::getPublicPosts()
            ->withAnyTag([$slug])
            ->paginate(config('constants.pagination.admin_listing_posts'));

        return view('home', compact('posts', 'tag_filter', 'categories'));
    }

    /**
     * Controller action for route 'post.search'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function search()
    {
        if(request("keywords") == null)
        {
            return back();
        }

        // All Categories to list on side navigation.
        $categories = Category::where('is_active', 1)->get();

        $search     = request("keywords");
        $keywords   = SearchUtils::getKeywords($search);

        $posts      = Post::search($keywords)
            ->where("reviewed", 1)
            ->where("allowed", 1)
            ->whereNotNull("reviewed_by")
            ->select('posts.id', 'user_id', 'title', 'body', 'post_image', 'posts.slug', 'category_id')
            /**
             * paginate() does not work with "distinct" clause, as it is a known 
             * Laravel5.2+ bug:
             * https://stackoverflow.com/questions/41283083/distinct-with-pagination-in-laravel-5-2-not-working
             * Using "group by" instead, and with MariaDB, all fields in "select" 
             * clause are required to be in "group by" clause.
             */
            ->groupBy(['posts.id', 'user_id', 'title', 'body', 'post_image', 'posts.slug', 'category_id'])
            ->paginate(config('constants.pagination.admin_listing_posts'));

        return view('home', compact('posts', 'categories', 'search'));
    }

    /**
     * Controller action for route 'post.bookmark'.
     *
     * @param  Post $post The post to bookmark.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function bookmark(Post $post)
    {
        $this->authorize('bookmark', $post);
        
        // Cannot bookmark own Posts.
        if(iAm($post->user))
        {
            abort(403);
        }

        $bookmark = $post->getBookmark();

        // Remove Bookmark.
        if($bookmark)
        {
            $bookmark->delete();
            $message     = "Post '" . $post->title . "' was removed from bookmarks.";
            $messageName = "post-unbookmarked-message";
        } else
        {
            // Add Bookmark if already exists in Trash.
            $bookmarkInTrash = $post->getBookmarkTrashed();
            if($bookmarkInTrash)
            {
                $bookmarkInTrash->restore();
            }
            // Add Bookmark if is new.
            else
            {
                $bookmark = me()->bookmarks()->create([
                    'user_id' => me()->id,
                    'post_id' => $post->id
                ]);
            }

            // Notify Post's User on bookmark by some other User.
            if(!iAm($post->user))
            {
                notify($post->user, $post->id, Post::class, "bookmark");
            }

            $message     = "Post '" . $post->title . "' was bookmarked.";
            $messageName = "post-bookmarked-message";
        }

        session()->flash($messageName, $message);

        $previousUrl = session()->previousUrl();
        $pattern     = request()->getHttpHost() . "/admin/post/*";

        if(Str::is("http://" . $pattern, $previousUrl) || Str::is("https://" . $pattern, $previousUrl))
        {
            return back();
        }

        return redirect(url()->previous() . "#post_" . $post->id);
    }

    /**
     * Controller action for route 'post.assign'.
     *
     * @param  Post $post The post to assign.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function assign(Post $post)
    {
        if(!request('category'))
        {
            abort(403);
        }

        $categoryId = request('category')[0];
        // Only 'Active' Categories can be assigned.
        $category   = Category::find($categoryId);

        if(!$category->is_active)
        {
            abort(403);
        }

        $post->category_id          = $categoryId;
        $post->assigned_category_by = me()->id;
        $post->updated_by           = me()->id;
        $post->save();

        // Notify Post's User on assigning by some other User.
        if(!iAm($post->user))
        {
            notify($post->user, $post->id, Post::class, "assign", $category->id);
        }

        session()->flash("post-assigned-message", "Post '" . $post->title
                . "' was assigned to Category '" . $category->label . "'.");

        return redirect()->route("home");
    }

    /**
     * Controller action for route 'posts.listreview'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function listreview()
    {
        // Request url get parameters needed to go back to, after update.
        $page = request('page') ? request('page') : "1";

        $posts = Post::where("reviewed", 0)
            ->paginate(config('constants.pagination.admin_listing_posts'));

        $customHeader = "Moderation for Posts";

        return view('admin.posts.index', compact('posts','customHeader', 'page'));
    }

    /**
     * Controller action for route 'post.review'.
     *
     * @param  Post $post The post to review.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function review(Post $post)
    {
        if($post->reviewed)
        {
            abort(403);
        }

        return view('admin.posts.review', compact('post'));
    }

    /**
     * Controller action for route 'post.accept'.
     *
     * @param  Post $post The post to accept.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function accept(Post $post)
    {
        $post->allowed     = 1;
        $post->reviewed    = 1;
        $post->reviewed_by = me()->id;
        $post->save();

        // Notify Post's User their Post was accepted.
        notify($post->user, $post->id, Post::class, "accept");

        session()->flash("post-accepted-message", "Post '" . $post->title . "' was accepted.");

        return redirect()->route("posts.listreview");
    }

    /**
     * Controller action for route 'post.reject'.
     *
     * @param  Post $post The post to reject.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function reject(Post $post)
    {
        $post->allowed     = 0;
        $post->reviewed    = 1;
        $post->reviewed_by = me()->id;
        $post->save();

        // Notify Post's User their Post was rejected.
        notify($post->user, $post->id, Post::class, "reject");

        session()->flash("post-rejected-message", "Post '" . $post->title . "' was rejected.");

        return redirect()->route("posts.listreview");
    }

    private function handleViewCount(Post $post)
    {
        $ipAddress  = request()->ip();

        if(loggedIn())
        {
            $userId = me()->id;
            $viewed = View::where("post_id", $post->id)
                ->where("user_id", $userId)
                ->first();
        } else
        {
            $userId = null;
            $viewed = View::where("post_id", $post->id)
                ->where("ip_address", $ipAddress)
                ->first();
        }

        // Update count for existing View.
        if(isset($viewed))
        {
            $viewed->count++;
            $viewed->save();
        } 
        // Create a new View.
        else
        {
            View::create(
                [
                    "post_id"    => $post->id,
                    "user_id"    => $userId,
                    "is_guest"   => !loggedIn(),
                    "ip_address" => $ipAddress
                ]
            );
        }
    }
}
