<?php

namespace App\Http\Controllers;

use App\Category;
use App\Contact;
use App\Http\Requests\StoreContact;
use App\Mail\ContactReply;
use App\User;
use Illuminate\Support\Facades\Mail;

class ContactController extends Controller
{
    /**
     * Controller action for route 'contact.create'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        // Only active Categories display on home page.
        $categories = Category::where('is_active', 1)->get();

        return view('contact', compact('categories'));
    }

    /**
     * Controller action for route 'contact.store'.
     *
     * @param  StoreContact $request The request.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(StoreContact $request)
    {
        $inputs     = $request->validated();

        if(loggedIn())
        {
            $fromId = me()->id;
            $email  = me()->email;
        } else
        {
            $from   = User::where("email", $inputs["email"])->first();
            $fromId = $from ? $from->id : null;
            $email  = $inputs["email"];
        }

        $contact = Contact::create(
            [
                "email"      => $email,
                "subject"    => $inputs["subject"],
                "body"       => $inputs["body"],
                "from_id"    => $fromId,
                "with_login" => loggedIn()
            ]
        );

        notifyAdmins($contact->id, Contact::class, "create");

        session()->flash("contact-sent-message", "Your message has been sent.");

        return redirect()->route("home");
    }

    /**
     * Controller action for route 'contact.index.pending'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function indexPending()
    {
        $contacts = Contact::where("replied", 0)
            ->whereNull("replied_by")
            ->where("is_reply", 0)
            ->orderBy("replied", "DESC")
            ->orderBy("created_at", "DESC")
            ->paginate(config('constants.pagination.admin_listing_entries'));

        return view("admin.contacts.index", compact('contacts'));
    }

    /**
     * Controller action for route 'contact.index.archived'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function indexArchived()
    {
        $contacts = Contact::where("replied", 1)
            ->whereNotNull("replied_by")
            ->where("is_reply", 0)
            ->orderBy("replied", "DESC")
            ->orderBy("created_at", "DESC")
            ->paginate(config('constants.pagination.admin_listing_entries'));

        return view("admin.contacts.index", compact('contacts'));
    }

    /**
     * Controller action for route 'contact.showreply'.
     *
     * @param  Contact $contact The Contact to show reply to.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showReply(Contact $contact)
    {
        return view("admin.contacts.reply", compact("contact"));
    }

    /**
     * Controller action for route 'contact.reply'.
     *
     * @param  Contact $contact The Contact to reply to.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function reply(Contact $contact)
    {
        $replyBody = request("body");
        Mail::to($contact->email)->send(new ContactReply($contact, $replyBody));

        // Update existing Contact being replied to.
        $contact->new        = 0;
        $contact->replied    = 1;
        $contact->replied_by = me()->id;
        $contact->save();

        // Create Reply Contact.
        Contact::create(
            [
                "email"           => me()->email,
                "subject"         => "Re #" . $contact->id . ": " . $contact["subject"],
                "body"            => $replyBody,
                "from_id"         => me()->id,
                "with_login"      => true,
                "is_reply"        => 1,
                "replied_contact" => $contact->id
            ]
        );

        session()->flash("contact-replied-message", "Reply was sent to '" . $contact->email . "'.");

        return redirect()->route("contact.index.pending");
    }
}

