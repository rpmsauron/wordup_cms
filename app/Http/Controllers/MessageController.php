<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMessage;
use App\Message;
use App\User;

class MessageController extends Controller
{
    /**
     * Controller action for route 'message.index'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $page = request('page') ? request('page') : "1";

        $itemsPerPage = config('constants.pagination.messages');

        if(request('filter'))
        {
            $filter = request('filter');
            switch ($filter)
            {
                // Show unarchived received Messages only.
                case 'received':
                    $messages = me()->messagesReceived()
                        ->where("archived_for_to", 0)
                        ->paginate($itemsPerPage);
                    break;

                // Show unarchived sent Messages only.
                case 'sent':
                    $messages = me()->messagesSent()
                        ->where("archived_for_from", 0)
                        ->paginate($itemsPerPage);
                    break;

                // Show archived Messages only, both sent and received.
                case 'archived':
                    $messages = me()->messages()

                        // Archiving filter for received messages.
                        ->where(
                            function($query)
                            {
                                $query->where("archived_for_to", 1)
                                    ->where("to_id", me()->id);
                            }
                        )

                        // Archiving filter for sent messages.
                        ->orWhere(
                            function($query)
                            {
                                $query->where("archived_for_from", 1)
                                    ->where("from_id", me()->id);
                            }
                        )

                        ->paginate($itemsPerPage);
                    break;

                /**
                 * Show received Messages only, by default, if any other request 
                 * param (including the default one, 'active').
                 */
                default:
                    $messages = me()->messagesReceived()
                        ->paginate($itemsPerPage);
            }
        } else
        {
            // Show received Messages only, by default, if absent request param.
            $messages = me()->messagesReceived()
                ->where("archived_for_to", 0)
                ->paginate($itemsPerPage);
        }

        return view('admin.messages.index', compact('messages', 'page'));
    }

    /**
     * Controller action for route 'message.show'.
     *
     * @param  Message $message The Message to show.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show(Message $message)
    {
        $message->new = 0;
        $message->save();

        return view('admin.messages.show', compact('message'));
    }

    /**
     * Controller action for route 'message.create'.
     *
     * @param  User $user The User to send message to.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create(User $user)
    {
        return view('admin.messages.create', ['user' => $user]);
    }

    /**
     * Controller action for route 'message.reply'.
     *
     * @param  Message $message The Message to reply to.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function reply(Message $message)
    {
        return view('admin.messages.create', ['user' => $message->from, 'message' => $message]);
    }

    /**
     * Controller action for route 'message.store'.
     *
     * @param  StoreMessage $request The request.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(StoreMessage $request, Message $parent = null)
    {
        if($parent)
        {
            $to                    = $parent->from->id;
            $parentId              = $parent->id;
            $feedbackMessage       = "You replied to ";
            $destinationRouteName  = "message.index";
            $destinationRouteParam = null;
        } else
        {
            $to                    = $request["to"];
            $parentId              = null;
            $feedbackMessage       = "Message was sent to ";
            $destinationRouteName  = "user.profile.show";
            $destinationRouteParam = $to;
        }

        $inputs  = $request->validated();

        $message = Message::create(
            [
                "subject"  => $inputs["subject"],
                "body"     => strip_tags($inputs["body"]),
                "from_id"  => me()->id,
                "to_id"    => $to,
                "reply_to" => $parentId
            ]
        );

        session()->flash("message-sent-message", $feedbackMessage . "'" . $message->to->name . "'.");

        return redirect()->route($destinationRouteName, $destinationRouteParam);
    }

    /**
     * Controller action for route 'message.archive'.
     *
     * @param  Message $message The Message to archive.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function archive(Message $message)
    {
        // Archiving a sent message.
        if(iAm($message->from))
        {
            $message->archived_for_from = 1;
            $provenance = " to '" . $message->to->name . "'";
        }

        // Archiving a received message.
        elseif(iAm($message->to))
        {
            $message->archived_for_to = 1;
            $provenance = " from '" . $message->from->name . "'";
        }
        // Fallback 'else' statement unneeded: such case is covered on the Policy access authorization.

        $message->save();

        session()->flash("message-archived-message", "Message '" . $message->subject . "' " . $provenance . " was archived.");

        return back();
    }
}
