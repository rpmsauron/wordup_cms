<?php

namespace App\Http\Controllers;

use App\Permission;
use App\Role;
use App\Http\Requests\StoreRole;
use App\Http\Requests\UpdateRole;
use Illuminate\Support\Str;


class RoleController extends Controller
{
    /**
     * Controller action for route 'roles.index'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $roles = Role::paginate(config('constants.pagination.admin_listing_entries'));

        return view("admin.roles.index", ["roles" => $roles]);
    }

    /**
     * Controller action for route 'roles.store'.
     *
     * @param  StoreRole $request The request.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(StoreRole $request)
    {
        $request->validated();

        $existing = Role::where("name", $request["name"])->first();
        if($existing)
        {
            return back()->withErrors(["name" => "A Role named '" . $request["name"] . "' already exists."]);
        }

        $role = Role::create([
            "name"           => Str::ucfirst($request["name"]),
            "slug"           => Str::kebab(Str::lower($request["name"])),
            "description"    => $request['description'],
            "is_undeletable" => 0,
            "created_by"     => me()->id
        ]);

        notifyAdmins($role->id, Role::class, "create");

        session()->flash("role-created-message", "Role '" . Str::ucfirst($request["name"]) . "' was created.");

        return back();
    }

    /**
     * Controller action for route 'roles.edit'.
     *
     * @param  Role $role The role to edit.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit(Role $role)
    {
        return view("admin.roles.edit", [
            "role"        => $role, 
            "permissions" => Permission::where("is_active", 1)
                ->orderBy("model")
                ->get()
        ]);
    }

    /**
     * Controller action for route 'roles.update'.
     *
     * @param  StoreRole $request The request.
     * @param  Role      $role    The role to update.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(UpdateRole $request, Role $role)
    {
        $oldName = $role->name;

        if($role->is_undeletable != 1 && $request['name'])
        {
            $request->validated();

            $role->name      = Str::ucfirst($request["name"]);
        }
        $role->slug          = Str::kebab(Str::lower($role->name));
        $role->description   = $request["description"];

        $isDuplicateFromName = Role::where("name", $role->name)
                                   ->where("id", "<>", $role->id)
                                   ->count() > 0;
        if($isDuplicateFromName)
        {
            return back()->withErrors(["name" => "A Role named '" . $request["name"] . "' already exists."]);
        }

        // If changes occurred.
        if($role->isDirty('name') || $role->isDirty('description'))
        {
            $role->updated_by = me()->id;
            $role->save();

            notifyAdmins($role->id, Role::class, "update");

            session()->flash("role-updated-message", "Role '" . $oldName . "' was updated.");

            return redirect()->route("roles.index");
        }

        session()->flash("role-updated-message", "Nothing to update.");

        return back();
    }

    /**
     * Controller action for route 'roles.destroy'.
     *
     * @param  Role $role The role to delete.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function destroy(Role $role)
    {
        $role->delete();

        notifyAdmins($role->id, Role::class, "delete");

        session()->flash("role-deleted-message", "Role '" . $role->name . "' was deleted.");

        return back();
    }

    /**
     * Controller action for route 'roles.attach'.
     *
     * @param Role $role The role to attach a Permission to.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function attachPermission(Role $role)
    {
        $permissionId = request('permission');
        $permission   = Permission::findOrFail($permissionId);
        $role->permissions()->attach($permissionId);

        notifyAdmins($role->id, Role::class, "attach", $permissionId);

        session()->flash("role-attached-message", "Permission '" . $permission->name
            . "' was attached to Role '" . $role->name . "'.");

        return back();
    }

    /**
     * Controller action for route 'roles.detach'.
     *
     * @param Role $role The role to detach a Permission from.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function detachPermission(Role $role)
    {
        $permissionId = request('permission');
        $permission   = Permission::findOrFail($permissionId);
        $role->permissions()->detach($permissionId);

        notifyAdmins($role->id, Role::class, "detach", $permissionId);

        session()->flash("role-detached-message", "Permission '" . $permission->name
            . "' was detached from Role '" . $role->name . "'.");

        return back();
    }

    /**
     * Controller action for route 'role.permission.detachAll'.
     *
     * @param  Role $role The Role to detach a Permission from.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function detachAllPermissions(Role $role)
    {
        $permissions = $role->permissions;

        if($permissions->count() == 0)
        {
            /**
             * Using 'role-attached-message' instead of 'role-detached-message' to 
             * make message show as a success message instead of a 'danger' message.
             */
            session()->flash("role-attached-message", "No permissions to detach.");

            return back();
        }

        $role->permissions()->detach();

        /**
         * Notify Admin Users of Permissions detached from a Role, by some other 
         * User, one Notification for each Permissions detached.
         */
        foreach($permissions as $permission)
        {
            notifyAdmins($role->id, Role::class, "detach_permission", $permission->id);
        }

        session()->flash("role-detached-message", "All permissions were detached "
                . "from Role '" . $role->name . "'.");

        return back();
    }
}
