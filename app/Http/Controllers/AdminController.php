<?php

namespace App\Http\Controllers;

use App\Bookmark;
use App\Category;
use App\Comment;
use App\JobRun;
use App\Post;
use App\SearchUtils;
use App\Suggestion;
use App\User;
use \Conner\Tagging\Model\Tag;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    /**
     * Controller action for route 'admin.index'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $bookmarks = Bookmark::where('user_id', me()->id)
            ->paginate(config('constants.pagination.admin_listing_posts'));

        $notifications = me()->getNotificationsShownDefault();

        $suggestions = Suggestion::getReceived()
            ->paginate(config('constants.pagination.admin_listing_entries'));

        return view("admin.dashboard", compact("bookmarks", "notifications", "suggestions"));
    }

    /**
     * Controller action for route 'admin.cleanup'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function cleanup()
    {
        $jobruns = JobRun::orderBy("created_at", "desc")
            ->paginate(config("constants.pagination.admin_jobs_run"));

        // Check for orphan Tags.
        $numberOrphanTags = Tag::where('tagging_tags.count', 0)->count();

        // Check for orphan User avatar files.
        $numberOrphanUserAvatars = 0;
        $orphanUserAvatars = [];
        foreach(Storage::files("images") as $filename)
        {
            if(User::where('avatar', $filename)->count() == 0 && !ignoreFile($filename))
            {
                $numberOrphanUserAvatars++;
                array_push($orphanUserAvatars, $filename);
            }
        }

        // Check for orphan Post image files.
        $numberOrphanPostImages = 0;
        $orphanPostImages = [];
        foreach(Storage::files("posts") as $filename)
        {
            if(Post::where('post_image', $filename)->count() == 0 && !ignoreFile($filename))
            {
                $numberOrphanPostImages++;
                array_push($orphanPostImages, $filename);
            }
        }

        // Check for orphan Category icon files.
        $numberOrphanCategoryIcons = 0;
        $orphanCategoryIcons = [];
        foreach(Storage::files("categories") as $filename)
        {
            if(Category::where('icon', $filename)->count() == 0 && !ignoreFile($filename))
            {
                $numberOrphanCategoryIcons++;
                array_push($orphanCategoryIcons, $filename);
            }
        }

        return view('admin.cleanup.index', compact('jobruns', 'numberOrphanTags',
            'numberOrphanUserAvatars', 'orphanUserAvatars',
            'numberOrphanPostImages', 'orphanPostImages',
            'numberOrphanCategoryIcons', 'orphanCategoryIcons'
        ));
    }

    /**
     * Controller action for route 'user.avatar.cleanup'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function cleanupOrphanUserAvatars()
    {
        $filenames = request()->input('avatars');
        if(!$filenames)
        {
            session()->flash("cleanup-cleaned-message", "Nothing was selected to delete.");

            return back();
        }

        $deleted = $this->deleteOrphanAvatars($filenames);

        registerJob(config("constants.jobs.clear_user_avatars.name"));

        session()->flash("cleanup-cleaned-message", $deleted
            . " orphaned User avatars were deleted from disk.");

        return back();
    }

    /**
     * Controller action for route 'user.avatar.cleanup.all'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function cleanupAllOrphanUserAvatars()
    {
        $deleted = $this->deleteOrphanAvatars(Storage::files("images"));

        registerJob(config("constants.jobs.clear_all_user_avatars.name"));

        session()->flash("cleanup-cleaned-message", "All " . $deleted
            . " orphaned User avatars were deleted from disk.");

        return back();
    }

    /**
     * Controller action for route 'post.image.cleanup'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function cleanupOrphanPostImages()
    {
        $filenames = request()->input('post_images');
        if(!$filenames)
        {
            session()->flash("cleanup-cleaned-message", "Nothing was selected to delete.");

            return back();
        }

        $deleted = $this->deleteOrphanPostImages($filenames);

        registerJob(config("constants.jobs.clear_post_images.name"));

        session()->flash("cleanup-cleaned-message", $deleted
            . " orphaned Post images were deleted from disk.");

        return back();
    }

    /**
     * Controller action for route 'post.image.cleanup.all'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function cleanupAllOrphanPostImages()
    {
        $deleted = $this->deleteOrphanPostImages(Storage::files("posts"));

        registerJob(config("constants.jobs.clear_all_post_images.name"));

        session()->flash("cleanup-cleaned-message", "All " . $deleted
            . " orphaned Post images were deleted from disk.");

        return back();
    }

    /**
     * Controller action for route 'category.icon.cleanup'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function cleanupOrphanCategoryIcons()
    {
        $filenames = request()->input('category_icons');
        if(!$filenames)
        {
            session()->flash("cleanup-cleaned-message", "Nothing was selected to delete.");

            return back();
        }

        $deleted = $this->deleteOrphanCategoryIcons($filenames);

        registerJob(config("constants.jobs.clear_category_icons.name"));

        session()->flash("cleanup-cleaned-message", $deleted
            . " orphaned Category icons were deleted from disk.");

        return back();
    }

    /**
     * Controller action for route 'category.icon.cleanup.all'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function cleanupAllOrphanCategoryIcons()
    {
        $deleted = $this->deleteOrphanCategoryIcons(Storage::files("categories"));

        registerJob(config("constants.jobs.clear_all_category_icons.name"));

        session()->flash("cleanup-cleaned-message", "All " . $deleted
            . " orphaned Category icons were deleted from disk.");

        return back();
    }

    /**
     * Controller action for route 'admin.search'.
     * 
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function search()
    {
        if(request("keywords") == null)
        {
            return back();
        }

        $page        = request('page') ? request('page') : "1";

        $search      = request("keywords");
        $keywords    = SearchUtils::getKeywords($search);

        $posts       = Post::search($keywords, false) // Already has left join with 'tagging_tagged'.
            ->select(
                    'username', 
                    'user_id',
                    'slug', 
                    'title', 
                    'posts.id AS id', 
                    'posts.deleted_at AS deleted_at', 
                    'reviewed', 
                    'allowed', 
                    'reviewed_by',
                    'posts.created_at'
            )
            ->join('users', 'users.id', '=', 'posts.user_id')
            ->distinct()
            ->orderBy("posts.created_at")
            ->get();

        $users       = User::search($keywords)
            ->select(
                    'username', 
                    'name', 
                    'id', 
                    'deleted_at',
                    'users.created_at'
            )
            ->orderBy("users.created_at")
            ->get();

        $categories  = Category::search($keywords)
            ->select(
                    'username', 
                    'created_by',
                    'categories.id', 
                    'label', 
                    'is_active',
                    'categories.created_at'
            )
            ->join('users', 'users.id', '=', 'categories.created_by')
            ->orderBy("categories.created_at")
            ->get();

        $comments    = Comment::search($keywords) // Already has join with 'posts'.
            ->select(
                    'comments.id',
                    'username', 
                    'comments.user_id',
                    'comments.id AS comment_id', 
                    'comments.body AS body', 
                    'slug', 
                    'comments.deleted_at AS deleted_at',
                    'comments.allowed AS allowed',
                    'comments.reported AS reported',
                    'comments.reviewed AS reviewed',
                    'comments.created_at'
            )
            ->join('users', 'users.id', '=', 'comments.user_id')
            ->orderBy("comments.created_at")
            ->get();

        /**
         * toBase() allows duplicated ids on Collection, since it may happen on 
         * having instances of different models.
         */
        $merged = $posts->toBase()
                ->merge($users)
                ->merge($categories)
                ->merge($comments);

        $searchables = $merged->paginate(config('constants.pagination.admin_search_results'));
        /**
         * Keep commented code as reference:
         *
         * Refer to AppServiceProvider for borrowed code, for enabling pagination 
         * for merged Collections, instead of Laravel's Eloquent Collections.
         *
         * $searchables = collect($merged->toArray())
         *     ->paginate(config('constants.pagination.admin_search_results'));
         */       

        return view('admin.search.index', compact('searchables', 'search', 'page'));
    }

    /**
     * Removes the files given by name if they are not referenced as any User's 
     * avatar file in the system.
     *
     * @param  array $filenames The array of file names to delete from.
     *
     * @return int   The number of deleted files.
     */
    private function deleteOrphanAvatars(array $filenames)
    {
        $deleted = 0;
        foreach($filenames as $filename)
        {
            if(User::where('avatar', $filename)->count() == 0 && !ignoreFile($filename))
            {
                Storage::delete($filename);
                $deleted++;
            }
        }

        return $deleted;
    }

    /**
     * Removes the files given by name if they are not referenced as any Post's 
     * image file in the system.
     *
     * @param  array $filenames The array of file names to delete from.
     *
     * @return int   The number of deleted files.
     */
    private function deleteOrphanPostImages(array $filenames)
    {
        $deleted = 0;
        foreach($filenames as $filename)
        {
            if(Post::where('post_image', $filename)->count() == 0 && !ignoreFile($filename))
            {
                Storage::delete($filename);
                $deleted++;
            }
        }

        return $deleted;
    }

    /**
     * Removes the files given by name if they are not referenced as any Category's 
     * icon file in the system.
     *
     * @param  array $filenames The array of file names to delete from.
     *
     * @return int   The number of deleted files.
     */
    private function deleteOrphanCategoryIcons(array $filenames)
    {
        $deleted = 0;
        foreach($filenames as $filename)
        {
            if(Category::where('icon', $filename)->count() == 0 && !ignoreFile($filename))
            {
                Storage::delete($filename);
                $deleted++;
            }
        }

        return $deleted;
    }
}
