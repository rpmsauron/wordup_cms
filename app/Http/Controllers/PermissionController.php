<?php

namespace App\Http\Controllers;

use App\Permission;
use Illuminate\Support\Str;
use App\Http\Requests\StorePermission;
use App\Http\Requests\UpdatePermission;

class PermissionController extends Controller
{
    /**
     * Controller action for route 'permissions.index'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        $permissions = Permission::orderBy("model")
                ->paginate(config('constants.pagination.admin_listing_entries'));

        return view("admin.permissions.index", ["permissions" => $permissions]);
    }

    /**
     * Controller action for route 'permissions.store'.
     *
     * @param  StorePermission $request The request.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(StorePermission $request)
    {
        $request->validated();

        $name           = $request["name"];
        $model          = $request["model"];
        $action         = isset($request["action"]) ? $request["action"] : null;

        // Handling duplication.
        $errors                = [];
        $isDuplicateFromName   = Permission::where("name", $name)
                                   ->count() > 0;
        $isDuplicateFromValues = Permission::where("action", $action)
                                   ->where("model", $model)
                                   ->whereNotNull("action") // Allow 'model' entries without 'action' values;
                                   ->count() > 0;
        if($isDuplicateFromName)
        {
            $errors["name"]   = "A Permission named '" . $name . "' already exists.";
        }
        if($isDuplicateFromValues)
        {
            $errors["values"] = "A Permission with that action and model exists.";
        }
        if(!empty($errors))
        {
            return back()->withErrors($errors);
        }

        $permission = Permission::create([
            "name"           => Str::ucfirst($name),
            "slug"           => Str::kebab(Str::lower($name)),
            "description"    => $request['description'],
            "model"          => $model,
            "action"         => $action,
            "created_by"     => me()->id
        ]);

        notifyAdmins($permission->id, Permission::class, "create");

        session()->flash("permission-created-message", "Permission '" . Str::ucfirst($name) . "' was created.");

        return back();
    }

    /**
     * Controller action for route 'permissions.edit'.
     *
     * @param  Permission $permission The permission to edit.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit(Permission $permission)
    {
        $this->authorize('update', $permission);

        return view("admin.permissions.edit", ["permission" => $permission]);
    }

    /**
     * Controller action for route 'permissions.update'.
     *
     * @param  StorePermission $request    The request.
     * @param  Permission      $permission The permission to update.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(UpdatePermission $request, Permission $permission)
    {
        $oldName = $permission->name;

        if($permission->is_undeletable != 1 && $request['name'])
        {
            $request->validated();
            $permission->name    = Str::ucfirst($request["name"]);
        }
        $permission->slug        = Str::kebab(Str::lower($permission->name));
        $permission->description = $request["description"];
        $permission->model       = $request["model"];
        $permission->action      = isset($request["action"]) ? $request["action"] : null;

        // Handling duplication.
        $errors                = [];
        $isDuplicateFromName   = Permission::where("name", $permission->name)
                                   ->where("id", "<>", $permission->id)
                                   ->count() > 0;
        $isDuplicateFromValues = Permission::where("action", $permission->action)
                                   ->where("model", $permission->model)
                                   ->whereNotNull("action") // Allow 'model' entries without 'action' values;
                                   ->where("id", "<>", $permission->id)
                                   ->count() > 0;
        if($isDuplicateFromName)
        {
            $errors["name"]   = "A Permission named '" . $permission->name . "' already exists.";
        }
        if($isDuplicateFromValues)
        {
            $errors["values"] = "A Permission with that action and model exists.";
        }
        if(!empty($errors))
        {
            return back()->withErrors($errors);
        }

        // If changes occurred.
        if($permission->isDirty(['name', 'description', 'model', 'action']))
        {
            $permission->updated_by = me()->id;
            $permission->save();

            notifyAdmins($permission->id, Permission::class, "update");

            session()->flash("permission-updated-message", "Permission '" . $oldName . "' was updated.");

            return redirect()->route("permissions.index");
        }

        session()->flash("permission-updated-message", "Nothing to update.");

        return back();
    }

    /**
     * Controller action for route 'permissions.destroy'.
     *
     * @param  Permission $permission The permission to delete.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function destroy(Permission $permission)
    {
        $this->authorize('delete', $permission);

        $permission->delete();

        notifyAdmins($permission->id, Permission::class, "delete");

        session()->flash("permission-deleted-message", "Permission '" . $permission->name . "' was deleted.");

        return back();
    }

    /**
     * Controller action for route 'permissions.toggle'.
     *
     * @param  Permission $permission The permission to toggle the 'is_active' status for.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function toggle(Permission $permission)
    {
        if($permission->is_active == 1)
        {
            $permission->is_active = 0;
            $action                = "disable";
        } else
        {
            $permission->is_active = 1;
            $action                = "enable";
        }
        $verb = resolveVerb(Permission::class . "_" . $action);

        // If changes occurred.
        if($permission->isDirty('is_active'))
        {
            $permission->save();

            notifyAdmins($permission->id, Permission::class, $action);

            session()->flash("permission-" . $verb . "-message", "Permission '" . $permission->name . "' was " . $verb . ".");
        }

        return back();
    }

    /**
     * Controller action for route 'permissions.action', called via AJAX directly to URL defined on route.
     *
     * @param  string $model The model name to get actions for.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function actions(string $model)
    {
        foreach(config("constants.permissions.models") as $key => $value)
        {
            if($key == $model && isset($value["actions"]))
            {
                return response()->json($value["actions"]);
            }
        }

        return [];
    }
}
