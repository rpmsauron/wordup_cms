<?php

namespace App\Http\Controllers;

use App\Category;
use App\Http\Requests\MergeCategory;
use App\Http\Requests\StoreCategory;
use App\Http\Requests\UpdateCategory;

class CategoryController extends Controller
{
    /**
     * Controller action for route 'categories.index'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $categories = Category::paginate(config('constants.pagination.admin_listing_entries'));

        return view('admin.categories.index', ['categories' => $categories]);
    }

    /**
     * Controller action for route 'categories.store'.
     *
     * @param  StoreCategory $request The request.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(StoreCategory $request)
    {
        $inputs = $request->validated();

        if(request("icon"))
        {
            $inputs["icon"] = request("icon")->store("categories");
        }

        $inputs["created_by"] = me()->id;
        $inputs["is_active"]  = 1;

        Category::create($inputs);

        session()->flash("category-created-message", "Category '" . request("label") . "' was created.");

        return back();
    }

    /**
     * Controller action for route 'categories.edit'.
     *
     * @param  Category $category The category to edit.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit(Category $category)
    {
        $this->authorize('update', $category);

        return view("admin.categories.edit", ["category" => $category]);
    }

    /**
     * Controller action for route 'categories.update'.
     *
     * @param  StoreCategory $request  The request.
     * @param  Category      $category The category to update.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(UpdateCategory $request, Category $category)
    {
        $oldLabel = $category->label;
        $oldIcon  = $category->getCategoryIconServerFile();

        $request->validated();

        processFileField('icon', $category);

        $isActiveValue       = $request["is_active"] == "on" ? 1 : 0;
        $category->label     = $request["label"];
        $category->is_active = $isActiveValue;

        // If changes occurred.
        if($category->isDirty('label') || $category->isDirty('is_active') || $category->isDirty('icon'))
        {
            // Checks before model saving otherwise its 'dirty' status changes again.
            $imageChanged = $category->isDirty("icon");

            $category->updated_by = me()->id;
            $category->save();

            // Handles deletion of old file (only if there were changes).
            handleOldFile($oldIcon, $imageChanged);

            // Notify Category's User on update by some other User.
            if(!iAm($category->user))
            {
                notify($category->user, $category->id, Category::class, "update");
            }

            session()->flash("category-updated-message", "Category '" . $oldLabel . "' was updated.");

            return redirect()->route("categories.index");
        }

        session()->flash("category-updated-message", "Nothing to update.");

        return back();
    }

    /**
     * Controller action for route 'categories.destroy'.
     *
     * @param Category $category the category to delete.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function destroy(Category $category)
    {
        $category->delete();

        // Remove resources files from disk.
        $icon = $category->getCategoryIconServerFile();
        handleOldFile($icon);

        // Notify Category's User on deletion by some other User.
        if(!iAm($category->user))
        {
            notify($category->user, $category->id, Category::class, "delete");
        }

        session()->flash("category-deleted-message", "Category '" . $category->label . "' was deleted.");

        return back();
    }

    /**
     * Controller action for route 'categories.toggle'.
     *
     * @param Category $category The category to toggle the 'is_active' status for.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function toggle(Category $category)
    {
        $this->authorize('toggle', $category);

        if($category->is_active == 1)
        {
            $category->is_active = 0;
            $action              = "disable";
        } else
        {
            $category->is_active = 1;
            $action              = "enable";
        }
        $verb = resolveVerb(Category::class . "_" . $action);

        // If changes occurred.
        if($category->isDirty('is_active'))
        {
            $category->save();

            // Notify Category's User on toggling by some other User.
            if(!iAm($category->user))
            {
                notify($category->user, $category->id, Category::class, $action);
            }

            session()->flash("category-toggled-message", "Category '" . $category->label . "' was " . $verb . ".");
        }

        return back();
    }

    /**
     * Controller action for route 'categories.show.mergeable'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function showMergeable()
    {
        $this->authorize('showMergeable', Category::class);

        $categories = Category::where('is_active', 1)->whereHas('posts')->get();

        return view('admin.cleanup.categories.mergeable', ['categories' => $categories]);
    }

    /**
     * Controller action for route 'categories.merge'.
     *
     * @param  MergeCategory $request The request.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function merge(MergeCategory $request)
    {
        //------------------------------
        // Create new Category (the merged one).
        $categoryLabels       = explode(",", $request->input('categories')[0]);

        $inputs               = $request->validated();

        /**
         * Store new label for later assignment, and generate a temporary label, in case
         * of unique-field collision label with that of any Category to be merged.
         */
        $newLabel             = $inputs["label"];
        $inputs["label"]      .= "_" . uniqid();

        if(null != $request['ids'])
        {
            $category = Category::find($request['ids'][0]);
            //@todo: prevent tampered value from html view to go; check if existing already is an existing file for any of the given categories
            $inputs["icon"]   = $category->getIconRelativePath();
        }

        // Authorship of merge result changes to the merging User.
        $inputs["created_by"] = me()->id;
        $inputs["is_active"]  = 1;

        $targetCategory = Category::create($inputs); 


        //----------------------------------
        // Change reference in related Posts.
        $sourceCategories = Category::whereIn("label", $categoryLabels)->get();
        foreach($sourceCategories as $sourceCategory)
        {
            foreach($sourceCategory->posts()->get() as $post)
            {
                $post->category_id = $targetCategory->id;
                $post->save();
            }
        }


        //-----------------------------------------------------------
        // Delete old Categories that were merged, and leftover icons.
        foreach($sourceCategories as $sourceCategory)
        {
            $icon = $sourceCategory->getCategoryIconServerFile();
            
            $sourceCategory->delete();

            // Remove old icons from merged Categories except the one to keep.
            if($sourceCategory->icon != $targetCategory->icon)
            {
                // Remove resources files from disk.
                handleOldFile($icon);
            } 
        }


        //---------------------------------------
        // Assign new name to new merged Category.
        $targetCategory->label = $newLabel;
        $targetCategory->save();


        //-------------------------------------------------------------------
        // Notify All merged Categories's Users on merging by some other User.
        foreach($sourceCategories as $category)
        {
            if(!iAm($category->user))
            {
                notify($category->user, $category->id, Category::class, "merge");
            }
        }

        registerJob(config("constants.jobs.merge_categories.name"));

        session()->flash("category-merged-message", "Categories '" . implode(",", $categoryLabels) . "' were merged into '" . $targetCategory->label . "'.");

        return back();
    }
}
