<?php

namespace App\Http\Controllers;

use \Conner\Tagging\Model\Tag;

class TagController extends Controller
{
    /**
     * Controller action for route 'tag.cleanup'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function cleanupOrphanTags()
    {
        $deleted = Tag::where('tagging_tags.count', 0)->delete();

        registerJob(config("constants.jobs.clear_tags.name"));

        session()->flash("cleanup-cleaned-message", $deleted . " orphaned Tags were removed.");

        return back();
    }
}