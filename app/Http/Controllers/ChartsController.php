<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Post;
use App\User;
use App\View;
use \Conner\Tagging\Model\Tagged;

class ChartsController extends Controller
{
    /**
     * Controller action for route 'admin.charts'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function charts()
    {
        $postsCount      = Post::count();
        $usersCount      = User::count();
        $categoriesCount = Category::where('is_active', 1)->count();
        $commentsCount   = Comment::count();
        /**
         * Only Tags in use are accounted for, since soft-deleted Posts do not 
         * delete related Tags, as configured on tagging config file.
         */
        $tagsCount       = Tagged::join('posts', 'posts.id', '=', 'tagging_tagged.taggable_id')
            ->whereNull('posts.deleted_at')
            ->count();

        return view('admin.charts.index', compact('postsCount', 'usersCount', 'categoriesCount', 'commentsCount', 'tagsCount'));
    }

    /**
     * Controller action for route 'admin.user.charts'.
     *
     * @param  User $user The User to get stats charts for. If omitted, it is for the session User by default.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function userCharts(User $user = null)
    {
        $user ??= me();

        $postsCount      = $user->posts()
            ->where("reviewed", 1)
            ->where("allowed", 1)
            ->whereNotNull("reviewed_by")
            ->count();

        $commentsCount   = $user->comments()
            ->where("allowed", 1)
            ->count();

        $usersCount      = null;
        $categoriesCount = null;
        $tagsCount       = null;

        return view('admin.charts.index', compact('user', 'postsCount', 'usersCount', 'categoriesCount', 'commentsCount', 'tagsCount'));
    }

    /**
     * Controller action for route 'admin.post.charts'.
     *
     * @param  Post $post The Post to get stats charts for.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function postCharts(Post $post)
    {
        $commentsCount = $post->commentsAllowed->count();
        $views         = View::where("post_id", $post->id)->distinct()->count();

        return view('home.charts.post', compact('post', 'commentsCount', 'views'));
    }
}