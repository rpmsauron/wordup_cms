<?php

namespace App\Http\Controllers;

class CaptchaServiceController extends Controller
{
    /**
     * Controller action for route 'captcha.reload', called via AJAX directly to URL defined on route.
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function reloadCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }
}