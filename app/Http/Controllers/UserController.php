<?php

namespace App\Http\Controllers;

use App\Http\Requests\UpdateUser;
use App\Permission;
use App\Role;
use App\Suggestion;
use App\User;

class UserController extends Controller
{
    /**
     * Controller action for route 'users.index'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $itemsPerPage = config('constants.pagination.admin_listing_entries');

        if(iAmAdmin() || iHavePermissionFor(User::class))
        {
            if(request('filter'))
            {
                $filter = request('filter');
                switch ($filter)
                {
                    case 'deleted':
                        // Show (soft-)deleted Users only.
                        $users = User::onlyTrashed()
                            ->paginate($itemsPerPage);
                        break;

                    case 'all':
                        // Show all Users.
                        $users = User::withTrashed()
                            ->paginate($itemsPerPage);
                        break;

                    default:
                        /**
                         * Show only active (non-soft-deleted) Users, by default, if any 
                         * other request param (including the default one, 'active'.
                         */
                        $users = User::paginate($itemsPerPage); 
                }
            } else
            {
                // Show only active (non-soft-deleted) Users, by default, if absent request param.
                $users = User::paginate($itemsPerPage);
            }

            return view('admin.users.index', ['users' => $users]);
        }

        /**
         * If no-longer-admin User has granted 'Admin' Role before to any User 
         * (a Role-tracker User), those Users are still shown to logged in User.
         */
        elseif(me()->hasUsersGranted(config('constants.roles.role.admin.slug')))
        {
            $users = me()->getUsersGranted(config('constants.roles.role.admin.slug'))
                ->paginate($itemsPerPage);

            return view('admin.users.index', ['users' => $users]);
        }

        abort(403);
    }

    /**
     * Controller action for route 'user.profile.show'.
     * Note: Route-Model binding returns 404 if model is soft-deleted: 
     * the id needs to be passed, then manually retrieve the model for it. 
     *
     * @param  user_id $user_id The id of the User to show.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show($user_id)
    {
        $user = iAmAdmin() ? 
            User::withTrashed()->findOrFail($user_id)
            : User::findOrFail($user_id);

        $userPosts = $user->posts()->paginate(config('constants.pagination.admin_listing_posts'));

        return view('admin.users.profile', compact('user', 'userPosts'));
    }

    /**
     * Controller action for route 'user.edit'.
     *
     * @param  User $user The user to edit.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit(User $user)
    {
        return view('admin.users.edit', [
            'user'        => $user,
            'roles'       => Role::all(),
            'permissions' => Permission::where("is_active", 1)
                ->orderBy("model")
                ->get()
        ]);
    }

    /**
     * Controller action for route 'user.update'.
     *
     * @param  UpdateUser $request The request.
     * @param  User       $user    The user to update.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(UpdateUser $request, User $user)
    {
        $oldUsername = $user->username;
        $oldAvatar   = $user->getAvatarServerFile();

        $inputs = $request->validated();

        processFileField('avatar', $user);

        $user->username        = $inputs['username'];
        $user->name            = $inputs['name'];
        $user->email           = $inputs['email'];
        $user->is_public_email = $request['is_public_email'] == 'on' ? 1 : 0;
        $passwordTouched       = false;
        if($request['password'] != null)
        {
            $user->password    = $request['password'];
            $passwordTouched   = true;
        }

        // If changes occurred.
        // Password is only verified to have been input, not changed or kept the old one, for security reasons on UI feedback.
        if($user->isDirty(['username', 'name', 'email', 'is_public_email', 'avatar']) || $passwordTouched)
        {
            // Checks before model saving otherwise its 'dirty' status changes again.
            $imageChanged = $user->isDirty("avatar");

            $user->save();

            // Handles deletion of old file (only if there were changes).
            handleOldFile($oldAvatar, $imageChanged);

            // Notify User on update to it, by some other User.
            if(!iAm($user))
            {
                notify($user, $user->id, User::class, "update");
            }

            session()->flash("user-updated-message", "User '" . $oldUsername . "' was updated.");

            return back();
        } 

        session()->flash("user-updated-message", "Nothing to update.");

        return back();
    }

    /**
     * Controller action for route 'user.destroy'.
     *
     * @param  User $user The user to delete.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function destroy(User $user)
    {
        $destroySelf = iAm($user);
        if($destroySelf)
        {
            auth()->logout();
            $message = "Your account was deleted.";
        } else
        {
            $message = "User '" . $user->username . "' was deleted.";
        }

        $user->delete();

        // User's Suggestions are closed.
        Suggestion::where("user_id", $user->id)
            ->where("open", 1)
            ->update(["open" => 0]);

        // Notify User on their deletion, by some other User.
        if(!iAm($user))
        {
            notify($user, $user->id, User::class, "delete");
        }

        if($destroySelf)
        {
            return redirect()->route("home")->with(["user-deleted-message" => $message]);
        }

        session()->flash("user-deleted-message", $message);

        return back();
    }

    /**
     * Controller action for route 'user.destroy.self.prompt'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function destroySelfPrompt()
    {
        return view("admin.users.delete_prompt");
    }

    /**
     * Controller action for route 'user.role.attach'.
     *
     * @param User $user The User to attach a Role to.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function attachRole(User $user)
    {
        $roleId = request('role');
        $role   = Role::findOrFail($roleId);
        $user->roles()->attach($roleId, ['granted_by' => me()->id]);

        // Notify User of Role attachment by some other User.
        if(!iAm($user))
        {
            notify($user, $user->id, User::class, "attach_role", $roleId);
        }

        session()->flash("user-attached-message", "Role '" . $role->name
                . "' was attached to User '" . $user->username . "'.");

        return back();
    }

    /**
     * Controller action for route 'user.role.detach'.
     *
     * @param  User $user The User to detach a Role from.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function detachRole(User $user)
    {
        $adminRole = config('constants.roles.role.admin.slug');

        $roleId    = request('role');
        $role      = Role::findOrFail($roleId);
        $user->roles()->detach($roleId);

        // Notify User of Role detachment by some other User.
        if(!iAm($user))
        {
            notify($user, $user->id, User::class, "detach_role", $roleId);
        }

        session()->flash("user-detached-message", "Role '" . $role->name
                . "' was detached from User '" . $user->username . "'.");

        /**
         * As a non-Admin User (Role-tracker User), you can go back to edit view 
         * as long as you do not remove the tracked User's 'Admin' Role.
         */
        if($role->slug == $adminRole && !iHaveRole($adminRole))
        {
            return redirect()->route('user.profile.show', $user->id);
        }

        /**
         * If as a non-Admin, the Role detached was 'Admin', the User can no longer 
         * go back to the providee's edit view, since, as a non-Admin, the User has 
         * lost the possibility to edit the other User, due to loss of pivot entry 
         * to track Role granting.
         */
        return back();
    }

    /**
     * Controller action for route 'user.permission.attach'.
     *
     * @param User $user The User to attach a Permission to.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function attachPermission(User $user)
    {
        $permissionId = request('permission');
        $permission   = Permission::findOrFail($permissionId);
        $user->permissions()->attach($permissionId, ['granted_by' => me()->id]);

        // Notify User of Permission attachment by some other User.
        if(!iAm($user))
        {
            notify($user, $user->id, User::class, "attach_permission", $permissionId);
        }

        session()->flash("user-attached-message", "Permission '" . $permission->name
                . "' was attached to User '" . $user->username . "'.");

        return back();
    }

    /**
     * Controller action for route 'user.permission.detach'.
     *
     * @param  User $user The User to detach a Permission from.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function detachPermission(User $user)
    {
        $permissionId    = request('permission');
        $permission      = Permission::findOrFail($permissionId);
        $user->permissions()->detach($permissionId);

        // Notify User that has their Permission detached by some other User.
        if(!iAm($user))
        {
            notify($user, $user->id, User::class, "detach_permission", $permissionId);
        }

        session()->flash("user-detached-message", "Permission '" . $permission->name
                . "' was detached from User '" . $user->username . "'.");

        return back();
    }

    /**
     * Controller action for route 'user.permission.detachAll'.
     *
     * @param  User $user The User to detach a Permission from.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function detachAllPermissions(User $user)
    {
        $permissions = $user->permissions;

        if($permissions->count() == 0)
        {
            /**
             * Using 'user-attached-message' instead of 'user-detached-message' to 
             * make message show as a success message instead of a 'danger' message.
             */
            session()->flash("user-attached-message", "No permissions to detach.");

            return back();
        }

        $user->permissions()->detach();

        /**
         * Notify User that has their Permissions detached by some other User, 
         * one Notification for each Permissions detached.
         */
        if(!iAm($user))
        {
            foreach($permissions as $permission)
            {
                notify($user, $user->id, User::class, "detach_permission", $permission->id);
            }
        }

        session()->flash("user-detached-message", "All permissions were detached "
                . "from User '" . $user->username . "'.");

        return back();
    }


    /**
     * Controller action for route 'user.restore'.
     * Note: Route-Model binding returns 404 if model is soft-deleted: 
     * the id needs to be passed, then manually retrieve the model for it. 
     *
     * @param  int $user_id The id of the user to restore.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function restore(int $user_id)
    {
        $user = User::withTrashed()->findOrFail($user_id);

        $this->authorize('restore', $user);

        // Automatically clears pending Notification for this User's deletion.
        $remover = $user->getRemover();

        $user->restore();

        // Notify User on their untrashing, by some other User.
        if(!iAm($user))
        {
            notify($user, $user->id, User::class, "undelete");
        }
        // Clear previous Notification of User deletion.
        if($remover)
        {
            unnotify($remover, $user, $user->id, User::class, "delete");
        }

        session()->flash("user-restored-message", "User '" . $user->username . "' was restored.");

        return back();
    }
}
