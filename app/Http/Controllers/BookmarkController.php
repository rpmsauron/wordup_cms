<?php

namespace App\Http\Controllers;

use App\Bookmark;

class BookmarkController extends Controller
{
    /**
     * Controller action for route 'admin.bookmarks.index'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $bookmarks     = Bookmark::where('user_id', me()->id)
            ->paginate(config('constants.pagination.admin_listing_posts'));

        return view("admin.bookmarks.index", compact("bookmarks"));
    }
}
