<?php

namespace App\Http\Controllers;

use App\Notification;

class NotificationController extends Controller
{
    /**
     * Controller action for route 'notifications.clear'.
     *
     * @param  Notification $notification The Notification to clear.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function clear(Notification $notification)
    {
        $notification->new = 0;
        $notification->save();

        return back();
    }

    /**
     * Controller action for route 'notifications.clearall'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function clearAll()
    {
        Notification::where("notified_id", me()->id)
            ->update(["new" => 0]);

        return back();
    }

    /**
     * Controller action for route 'notifications.index'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $notifications = me()->getAllShownNotifications(true, config("constants.notifications.all_max"));

        return view('admin.notifications.index', ['notifications' => $notifications]);
    }
}
