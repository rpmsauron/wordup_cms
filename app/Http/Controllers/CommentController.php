<?php

namespace App\Http\Controllers;

use App\Category;
use App\Comment;
use App\Post;
use App\Http\Requests\StoreComment;
use App\Http\Requests\UpdateComment;

class CommentController extends Controller
{
    /**
     * Controller action for route 'comment.store'.
     *
     * @param  StoreComment $request The request.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(StoreComment $request, Comment $parent = null)
    {
        $post = Post::find($request['post_id']);

        if(!$post)
        {
            abort(404);
        }

        $inputs = $request->validated();

        $inputs['user_id']   = me()->id;
        $inputs['parent_id'] = $parent ? $parent->id : null;
        $inputs['post_id']   = $post->id;
        $inputs['body']      = cleanText($inputs['body']);

        $comment = me()->comments()->create($inputs);

        if(!$parent)
        {
            // Notify Post's User on a new Comment by some other User.
            if(!iAm($post->user))
            {
                notify($post->user, $comment->id, Comment::class, "create");
            }
        } else
        {
            // Notify Parent Comment's User on a new Reply by some other User.
            if(!iAm($parent->user))
            {
                notify($parent->user, $parent->id, Comment::class, "reply");
            }
            // Notify Parent Comment's Post User on a new Reply by some other User.
            if(!iAm($parent->user))
            {
                notify($post->user, $comment->id, Comment::class, "reply_on");
            }
        }

        $message = $parent ? "Your reply was sent." : "Your comment was sent.";
        session()->flash("comment-created-message", $message);

        return redirect()->route("post.show", ['slug' => $post->slug]);
    }

    /**
     * Controller action for route 'comment.edit'.
     *
     * @param  Comment $comment The comment to edit.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function edit(Comment $comment)
    {
        $this->authorize('update', $comment);

        $post = $comment->post;

        if(!$post)
        {
            abort(404);
        }

        $rootCommentsPaginated = $post->rootCommentsPaginated();
        // All Categories to list on side navigation
        $categories            = Category::where('is_active', 1)->get();
        // All related Posts to list on side navigation
        $relatedPosts          = $post->getRelated();

        return view('blog-post', [
            'post'                  => $post,
            'categories'            => $categories,
            'commentToEdit'         => $comment,
            'rootCommentsPaginated' => $rootCommentsPaginated,
            'relatedPosts'          => $relatedPosts
        ]);
    }

    /**
     * Controller action for route 'comment.update'.
     *
     * @param  UpdateComment $request The request.
     * @param  Comment       $comment The comment to update.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function update(UpdateComment $request, Comment $comment)
    {
        $post = $comment->post;

        if(!$post)
        {
            abort(404);
        }

        $inputs = $request->validated();

        $comment->body = cleanText($inputs['body']);

        $post->updated_by = me()->id;
        $comment->save();

        // Notify Comment's User on update by some other User.
        if(!iAm($comment->user))
        {
            notify($comment->user, $comment->id, Comment::class, "update");
        }

        session()->flash("comment-updated-message", "Your comment was edited.");

        return redirect()->route("post.show", ['slug' => $post->slug]);
    }

    /**
     * Controller action for route 'comment.destroy'.
     * Also clears its related Notifications for "create" and "reply" actions.
     *
     * @param  Comment $comment The comment to delete alongside all recursive replies.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function destroy(Comment $comment)
    {
        $this->authorize('delete', $comment);

        $commentUser = $comment->user;

        $this->delete($comment);

        /**
         * Notify Comment owner of deletion by some other User, and only the 
         * original recursion chain Comment owner's.
         */
        if(!iAm($commentUser))
        {
            notify($commentUser, $comment->id, Comment::class, "delete");
        }

        session()->flash("comment-deleted-message", "The comment was deleted.");

        return back();
    }

    /**
     * Recursive iterative deletion of all replies to a Comment, and clears its 
     * related Notifications for "create", "reply" and "reply_on" actions.
     *
     * @param Comment $comment The root comment to delete from.
     */
    private function delete(Comment $comment)
    {
        $children    = $comment->children;
        $parent      = $comment->parent; 
        $commentUser = $comment->user;
        $postUser    = $comment->post->user;

        $comment->deleted_by = me()->id;
        $comment->save();
        $comment->delete();

        /**
         * Clear Notifications for chained Reply Comments in thread tree for 
         * 'create', 'reply_on' and 'reply' actions.
         */
        if($parent)
        {
            unnotify($commentUser, $parent->user, $parent->id, Comment::class, "reply");
            unnotify($commentUser, $postUser, $comment->id, Comment::class, "reply_on");
        } else
        {
            unnotify($commentUser, $postUser, $comment->id, Comment::class, "create");
        }

        if($children->count() > 0)
        {
            foreach($children as $child)
            {
                $commentUser = $child->user;
                $postUser    = $child->post->user;

                $this->delete($child);
            }
        }
    }

    /**
     * Controller action for route 'comment.report'.
     * Comment authors are not notified that their Comment has been reported, only 
     * that it was later rejected, if so.
     * Middleware Policy note: User cannot report their own Comments.
     *
     * @param  Comment $comment The comment to report.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function report(Comment $comment)
    {
        $comment->reported    = 1;
        $comment->reported_by = me()->id;
        $comment->save();

        /**
         * Notify Moderators and Admins (even the reporter themselves, so that 
         * moderation link is created) that a Comment has been reported and needs 
         * moderation. However, a Moderator or Admin User is not notified though 
         * if they own the Comment that is reported.
         */
        $roles = [config("constants.roles.role.admin.name"), config("constants.roles.role.moderator.name")];
        notifyAll($roles, true, $comment->user, $comment->id, Comment::class, "report");

        session()->flash("comment-reported-message", "Comment was reported and will be reviewed upon availability.");

        return redirect()->route("post.show", [$comment->post->slug]);
    }

    /**
     * Controller action for route 'comment.listreview'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function listreview()
    {
        // Request url get parameters needed to go back to, after update.
        $page = request('page') ? request('page') : "1";

        $comments = Comment::where("reported", 1)
            ->where("reviewed", 0)
            // Can't see one's own Comments that are reported by someone else.
            ->where("user_id", "<>", me()->id)
            // Can't see one's own reports on Comments by other Users.
            ->where("reported_by", "<>", me()->id)
            ->paginate(config('constants.pagination.admin_listing_comments'));

        $customHeader = "Moderation for Comments";

        return view('admin.comments.index', compact('comments','customHeader', 'page'));
    }

    /**
     * Controller action for route 'comment.review'.
     *
     * @param  Comment $comment The comment to show 'review' view for.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function review(Comment $comment)
    {
        if($comment->reviewed)
        {
            abort(403);
        }

        return view('admin.comments.review', compact('comment'));
    }

    /**
     * Controller action for route 'comment.accept'.
     * Middleware Policy note: Moderator User cannot accept their own Comments, 
     * but can moderate the Comments they themselves report:
     * That's how they moderate Comments that have not yet been reported but 
     * required moderation.
     *
     * @param  Comment $comment The comment to accept.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function accept(Comment $comment)
    {
        $comment->allowed     = 1;
        $comment->reviewed    = 1;
        $comment->reviewed_by = me()->id;
        $comment->save();

        // Notify Comment's reporting User the Comment they reported was accepted.
        notify($comment->reporter, $comment->id, Comment::class, "accept");
        /**
         * Clear all pending Notifications of this Comment's 'report', for all 
         * Moderators and Admin Users.
         */
        unnotify(null, null, $comment->id, Comment::class, "report");

        session()->flash("comment-accepted-message", "Comment on Post '" . $comment->post->title . "' was accepted.");

        return redirect()->route("comments.listreview");
    }

    /**
     * Controller action for route 'comment.reject'.
     * Middleware Policy note: Moderator User cannot reject their own Comments, 
     * but can moderate the Comments they themselves report:
     * That's how they moderate Comments that have not yet been reported but 
     * required moderation.
     *
     * @param  Comment $comment The comment to reject.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function reject(Comment $comment)
    {
        $comment->allowed     = 0;
        $comment->reviewed    = 1;
        $comment->reviewed_by = me()->id;
        $comment->save();

        // Notify Comment's reporting User the Comment they reported was rejected.
        notify($comment->reporter, $comment->id, Comment::class, "reject");
        // Notify Comment's User their Comment was rejected.
        notify($comment->user, $comment->id, Comment::class, "reject");
        /**
         * Clear all pending Notifications of this Comment's 'report', for all 
         * Moderators and Admin Users.
         */
        unnotify(null, null, $comment->id, Comment::class, "report");

        session()->flash("comment-rejected-message", "Comment on Post '" . $comment->post->title . "' was rejected.");

        return redirect()->route("comments.listreview");
    }
}
