<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;

class HomeController extends Controller
{
    /**
     * Controller action for route 'home' (shows the application home).
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $posts = Post::getPublicPosts()
                ->paginate(config('constants.pagination.home_posts'));

        // Only active Categories display on home page.
        $categories = Category::where('is_active', 1)->get();

        return view('home', compact('posts', 'categories'));
    }
}
