<?php

namespace App\Http\Controllers;

use App\Category;
use App\Post;
use App\Suggestion;
use App\Http\Requests\StoreSuggestion;

class SuggestionController extends Controller
{
    /**
     * Controller action for route 'suggestions.create'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function create()
    {
        $categories = Category::paginate(config('constants.pagination.admin_listing_entries'));
        $post       = Post::find(request('post'));

        return view('home.categories.list', ['categories' => $categories, 'post' => $post]);
    }

    /**
     * Controller action for route 'suggestions.store'.
     *
     * @param  StoreSuggestion $request The request.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function store(StoreSuggestion $request)
    {
        if(!$request['category'])
        {
            abort(403);
        }

        $postId     = $request['post'];
        $categoryId = $request['category'][0];
        // Only active Categories can be suggested.
        $category   = Category::find($categoryId);
        if(!$category->is_active)
        {
            abort(403);
        }

        $existing   = Suggestion::where("user_id", me()->id)
            ->where("post_id", $postId)
            ->first();

        // User has exceeded the amount of Suggestions for a Post.
        $post       = Post::find($postId);
        if($post->suggestedTimesMaxedOut())
        {
            abort(403);
        }

        // Post has no Suggestion by User.
        if($existing == null)
        {
            $suggestion = Suggestion::create([
                "category_id" => $categoryId,
                "post_id"     => $postId,
                "user_id"     => me()->id
            ]);
        }

        // Post has the same Suggestion by User.
        elseif($existing->category_id == $categoryId)
        {
            abort(403);
        } 

        // Post has a previous different Suggestion by User.
        else
        {
            $existing->category_id = $categoryId;
            $existing->open = 1;
            $existing->count++;
            $existing->save();
            $suggestion = $existing;
        }

        // Notify Post's User of Suggestion by some other User.
        if(!iAm($post->user))
        {
            notify($post->user, $post->id, Suggestion::class, "create", $category->id);
        }

        session()->flash("suggestion-created-message", "Category '" . $suggestion->category->label
                . "' was suggested for Post '" . $suggestion->post->title . "'.");

        return redirect()->route("home");
    }

    /**
     * Controller action for route 'suggestions.accept'.
     *
     * @param  Suggestion $suggestion The Suggestion to accept.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function accept(Suggestion $suggestion)
    {
        $suggestion->open = 0;
        $suggestion->save();

        $category = Category::find($suggestion->category_id);

        // Suggestions can only be accepted if related Category is active and Post still exists.
        if($category->is_active && $suggestion->post)
        {
            $suggestion->post->category_id = $suggestion->category_id;
            $suggestion->post->save();

            // Notify Suggestion's suggesting User that Suggestion was accepted.
            notify($suggestion->user, $suggestion->post->id, Suggestion::class, "accept", $category->id);
            // Automatically clears pending Notification for this Suggestion's creation.
            unnotify($suggestion->user, me(), $suggestion->post->id, Suggestion::class, "create");

            session()->flash("suggestion-accepted-message", $suggestion->user->name
                    . "'s suggestion for Post '" . $suggestion->post->title
                    . "' as of Category '" . $suggestion->category->label . "' was accepted.");

            return back();
        } 

        session()->flash("suggestion-rejected-message", $suggestion->user->name
                . "'s suggestion for Post '" . $suggestion->post->title
                . "' as of Category '" . $suggestion->category->label . "' cannot be accepted.");

        return back();
    }

    /**
     * Controller action for route 'suggestions.reject'.
     *
     * @param  Suggestion $suggestion The Suggestion to reject.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function reject(Suggestion $suggestion)
    {
        $suggestion->open = 0;
        $suggestion->save();

        // Automatically clears pending Notification for this Suggestion's creation.
        unnotify($suggestion->user, me(), $suggestion->post->id, Suggestion::class, "create");

        session()->flash("suggestion-rejected-message", $suggestion->user->name
                . "'s suggestion for Post '" . $suggestion->post->title
                . "' as of Category '" . $suggestion->category->label . "' was rejected.");

        return back();
    }

    /**
     * Controller action for route 'suggestions.given'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function given()
    {
        $suggestions = Suggestion::leftjoin("categories", "category_id", "=", "categories.id")
            ->where("categories.is_active", 1)
            ->where("open", 1)
            ->where("user_id", me()->id)
            ->select("suggestions.id", "suggestions.post_id", "suggestions.category_id", "suggestions.user_id", "open", "count")
            ->paginate(config('constants.pagination.admin_listing_entries'));

        return view('admin.suggestions.given', compact('suggestions'));
    }

    /**
     * Controller action for route 'suggestions.received'.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function received()
    {
        $suggestions = Suggestion::getReceived()
            ->paginate(config('constants.pagination.admin_listing_entries'));

        return view('admin.suggestions.received', compact('suggestions'));
    }

    /**
     * Controller action for route 'suggestions.remove'.
     *
     * @param  Suggestion $suggestion The Suggestion to remove.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function remove(Suggestion $suggestion)
    {
        $suggestion->open = 0;
        $suggestion->save();

        // Automatically clears pending Notification for this Suggestion's creation.
        unnotify($suggestion->user, $suggestion->post->user, $suggestion->post->id, Suggestion::class, "create");

        session()->flash("suggestion-remove-message", "Suggestion for Post '"
                . $suggestion->post->title . "' was removed.");

        return back();
    }
}
