<?php

namespace App\Http\Requests;

use App\Role;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRole extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can("update", Role::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * Allowed:
     * \w => Alphanumeric characters and underscores;
     * \s => spaces;
     * \- => dashes;
     * + => no empty string (one or more occurrences);
     * / => regex delimiter;
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name" => "required|min:3|max:255|regex:/^[\w\s\-']+$/",
        ];
    }
}
