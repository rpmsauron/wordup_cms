<?php

namespace App\Http\Requests;

use App\Post;
use Illuminate\Foundation\Http\FormRequest;

class StorePost extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can("create", Post::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "title"      => "required|min:3|max:255",
            "post_image" => "file|mimes:jpg,png,bmp,jpeg",
            "body"       => "required|min:1",
            'captcha'    => 'required|captcha'
        ];
    }
}
