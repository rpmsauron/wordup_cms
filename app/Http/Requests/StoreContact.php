<?php

namespace App\Http\Requests;


use Illuminate\Foundation\Http\FormRequest;

class StoreContact extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "subject" => "required|min:3|max:255",
            "body"    => "required|min:1",
            "email"   => "email|max:255",
            "captcha" => "required|captcha"
        ];
    }
}
