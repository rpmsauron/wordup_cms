<?php

namespace App\Http\Requests;

use App\Permission;
use Illuminate\Foundation\Http\FormRequest;

class StorePermission extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can("create", Permission::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * Allowed:
     * \w => Alphanumeric characters and underscores;
     * \s => spaces;
     * \- => dashes;
     * + => no empty string (one or more occurrences);
     * / => regex delimiter;
     *
     * @return array
     */
    public function rules()
    {
        return [
            "name"  => "required|min:3|max:255|regex:/^[\w\s\-']+$/",
            "model" => "required|min:3|max:255"
        ];
    }

    /**
     * Configure the validator instance.
     *
     * @param  \Illuminate\Validation\Validator  $validator
     *
     * @return void
     */
    public function withValidator($validator)
    {
        // If 'action' input is absent, it needs to be verified if it is allowed for given 'model' input.
        $validator->sometimes('action', 'required', 
            function($input)
            {
                return $input->model && !modelAllowsEmptyAction($input->model);
            }
        )
        ->sometimes('action', 'nullable|max:0', 
            function($input)
            {
                return $input->model && modelAllowsEmptyAction($input->model);
            }
        );
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'action.required' => 'An Action is required to handle this Model.',
            'action.max'      => 'No Action supported for this Model.',
        ];
    }
}
