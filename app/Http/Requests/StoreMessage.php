<?php

namespace App\Http\Requests;

use App\Message;
use App\User;
use Illuminate\Foundation\Http\FormRequest;

class StoreMessage extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        // Is root message.
        if($this->to)
        {
            $recipientUser = User::findOrFail($this->to); // fetched from the Request.
        } 
        // Is reply message.
        elseif($this->parent->from->id)
        {
            $recipientUser = User::findOrFail($this->parent->from->id); // fetched from the Request.
        }

        return $this->user()->can('message', [Message::class, $recipientUser]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            "subject" => "required|min:3|max:255",
            "body"    => "required|min:1"
        ];
    }
}
