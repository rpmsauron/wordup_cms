<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUser extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        /**
         * Note:
         * $this->user() is session auth User (source User: the one that edits the 
         * other), and $this->user is the User in the Request (target User: the one 
         * to be edited), which can be a different User instance.
         */
        return $this->user()->can("update", $this->user);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'username' => ['required', 'string', 'max:255', 'regex:/^[a-z. -]+$/'],
            'name'     => ['required', 'string', 'max:255'],
            'email'    => ['required', 'email', 'max:255'],
            'avatar'   => ['file'],
//            'password' => ['required', 'string', 'min:8', 'confirmed'],
        ];
    }
}
