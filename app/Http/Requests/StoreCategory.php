<?php

namespace App\Http\Requests;

use App\Category;
use Illuminate\Foundation\Http\FormRequest;

class StoreCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can("create", Category::class);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * Allowed:
     * \w => Alphanumeric characters and underscores;
     * \s => spaces;
     * \- => dashes;
     * + => no empty string (one or more occurrences);
     * / => regex delimiter;
     *
     * Strictly forbidden symbols: 
     * , => used for form field delimiter on array input;
     *
     * @return array
     */
    public function rules()
    {
        return [
            "label"   => "required|min:3|max:255|regex:/^[\w\s\-']+$/",
            "icon"    => "file|mimes:jpg,png,bmp,jpeg",
            "captcha" => "required|captcha"
        ];
    }
}
