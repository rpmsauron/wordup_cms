<?php

namespace App\Http\Requests;

use App\Category;
use Illuminate\Foundation\Http\FormRequest;

class MergeCategory extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return $this->user()->can("merge", Category::class);
    }

    /**
     * Get the validation rules that apply to the request.
     * Strictly forbidden symbols: 
     * , => used for form field delimiter on array input;
     *
     * @return array
     */
    public function rules()
    {
        return [
            "label" => "required|alpha_dash|min:3|max:255"
        ];
    }
}
