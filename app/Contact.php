<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    protected $fillable = [
        'email', 
        'subject', 
        'body',
        'new',
        'replied',
        'replied_by',
        'from_id',
        'with_login',
        'is_reply',
        'replied_contact'
    ];

    /**
     * Returns the User which sent this Message.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function from()
    {
        return $this->belongsTo(User::class, "from_id");
    }

    /**
     * Returns the User which sent this Message.
     * An alias function for from().
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function sender()
    {
        return $this->from();
    }

    /**
     * Returns the User which replied to this Message.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function repliedBy()
    {
        return $this->belongsTo(User::class, "replied_by");
    }

    /**
     * Returns the User which replied to this Message.
     * An alias function for repliedBy().
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function replier()
    {
        return $this->replied_by();
    }

    /**
     * Returns the parent Contact of this Contact (being this one thus a Reply).
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function parent()
    {
        return $this->belongsTo(Contact::class, "replied_contact");
    }

    /**
     * Returns the children Contacts to this Contact (Replies).
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function contacts()
    {
        return $this->hasMany(Contact::class, 'replied_contact');
    }

    /**
     * Returns the children Contacts to this Contact (Replies).
     * An alias function to contacts().
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->contacts();
    }

    /**
     * Returns the children Contacts to this Contact (Replies).
     * An alias function to contacts().
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function replies()
    {
        return $this->contacts();
    }
}
