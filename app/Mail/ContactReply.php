<?php

namespace App\Mail;

use App\Contact;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ContactReply extends Mailable
{
    use Queueable, SerializesModels;

    public $contact;

    public $reply;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Contact $contact, string $reply)
    {
        $this->contact = $contact;
        $this->reply   = $reply;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('emails.contact_reply');
    }
}
