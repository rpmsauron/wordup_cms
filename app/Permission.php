<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;

class Permission extends Model
{
    protected $fillable = [
        'name', 
        'slug', 
        'is_active',
        'description', 
        'is_undeletable',
        'action',
        'model',
        'created_by',
        'updated_by'
    ];

    /**
     * Returns the Roles with this Permission.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function roles()
    {
        return $this->belongsToMany(Role::class)->withTimestamps();
    }

    /**
     * Returns the Users with this Permission.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function users()
    {
        return $this->belongsToMany(User::class);
    }

    
    /**
     * Returns the User which created this Permission.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function creator()
    {
        return $this->belongsTo(User::class, "created_by");
    }

    /**
     * Returns the User which last updated this Permission.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function updater()
    {
        return $this->belongsTo(User::class, "updated_by");
    }

    /**
     * Returns this Permission's 'model' attribute on a human-readable format, 
     * read from the config's associative array.
     *
     * @return string The human-readable format of the 'model' attribute of this 
     *                Permission.
     */
    public function modelHumanReadable()
    {
        foreach(config("constants.permissions.models") as $key => $value)
        {
            if(
                $value["classname"]
                && $value["classname"] == $this->attributes["model"]
            )
            {
                return Str::ucfirst($key);
            }
        }

        return null;
    }

    /**
     * Checks if this Permission handling is a specific 'action' over a specific 
     * 'model'.
     *
     * @param  string $action The action value to check against this Permission's.
     * @param  string $model  The model value to check against this Permission's.
     *
     * @return bool   Returns true if this Permission's field 'action' is the given 
     *                'action' value and field 'model' is the given 'model' value.
     */
    public function handles(string $action, string $model)
    {
        return 
                $this->attributes["action"] == $action
                && $this->attributes["model"] == $model;
    }
}
