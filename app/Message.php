<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends Model
{
    use SoftDeletes;

    protected $dates = [
        'deleted_at'
    ];

    protected $fillable = [
        'reply_to', 
        'from_id', 
        'to_id',
        'archived_for_from',
        'archived_for_to',
        'subject',
        'body',
        'new'
    ];

    /**
     * Returns the User which sent this Message.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function from()
    {
        return $this->belongsTo(User::class, "from_id");
    }

    /**
     * Returns the User which received this Message.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function to()
    {
        return $this->belongsTo(User::class, "to_id");
    }

    /**
     * Returns the Message this Message is a reply to.
     *
     * @return Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function replyTo()
    {
        return $this->belongsTo(Message::class, "reply_to");
    }

    /**
     * Returns the children Messages to this Message (Replies).
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function messages()
    {
        return $this->hasMany(Message::class, 'reply_to');
    }

    /**
     * Returns the children Messages to this Message (Replies).
     * An alias function to messages().
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function children()
    {
        return $this->messages();
    }

    /**
     * Returns the children Messages to this Message (Replies).
     * An alias function to messages().
     *
     * @return Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function replies()
    {
        return $this->messages();
    }

    /**
     * Verifies if this Message has children Messages (replies).
     *
     * @return bool Returns true if it has replies, or false otherwise.
     */
    public function hasChildren()
    {
        return $this->messages()->count() > 0;
    }

    /**
     * Verifies if this Message has a parent Message (if this is not a Reply).
     *
     * @return bool Returns true if it has a parent, or false otherwise.
     */
    public function hasParent()
    {
        return $this->attributes['reply_to'] != null;
    }
}
