
# word↑

## Intro
This project consists on a Blogging platform.

It was developed on PHP version 7.4.5, with the Laravel framework version 7.14.1, over Mysql version 8.0.19.

The frontend uses PHP’s Blade templating engine on HTML, with Bootstrap 4 for responsiveness.

The project uses jQuery, Bootstrap, ChartJS, MCE TinyEditor, as frontend JS libraries;
The project uses askedio/laravel-soft-cascade, rtconner/laravel-tagging, mews/captcha, unisharp/laravel-filemanager, as Laravel packages.


## Background ##
This project started as a low complexity one, part of an [Udemy](https://www.udemy.com/course/php-with-laravel-for-beginners-become-a-master-in-laravel/learn/lecture/9550466#questions) intermediate course on [Laravel](https://laravel.com) instructed by [Edwin Diaz](https://edwindiaz.com/), which I took further onto a far more complete and finished project itself, with more features that time to time, came into mind, and bug fixes and application enhancements.

  
## Architecture
The following entities exist in the application:

### On an end-user level: 

___
#### Users

The users of the applications, with respective login, register, recover password functionalities, and a dashboard to manage their account info and their content, such as blog posts.

___
#### Posts

The posts blogged by users, containing rich-text content varying from text to images, with formatting. 
A post also has a field that may contain a main image for displaying and previewing purposes, in order to make it visually more eye-catchy.
Posts by regular Users need approval by a Moderator or Admin User first before becoming publically accessible. If the Post belongs to an Admin or Moderator, it does not require approval.

___
#### Tags

Key words related to posts to allow relating between them in terms of topic, and also used for search purposes.

___
#### Categories

Topics to group posts under, based on a common theme. 
Every user can create their own, for when a fitting one does not exist yet for a new post, for instance. Admins can then condense/merge them, and manage them into a cleaner set. Categories may contain an icon image to make them more contextually perceptible (a category “PHP” could display the PHP logo, for instance). They can be deactivated and reactivated.

___
#### Suggestions

Regarding uncategorised posts, other users can suggest a category for them, which then the post author can either accept or reject.

___
#### Bookmarks

Posts can be bookmarked for further reference or easier finding, by other users (never one’s own posts, though).

___
#### Comments

Posts can be commented on, and comments replied to, removed and edited. 
Basic *markup* / text formatting is allowed such as `<b>`, `<i>` and `<u>` tags for bold, italic or underline formatting. Just use it like HTML tags:
`<i>`this is italic`</i>` where `<b>`this is bold`</b>`.
will display:
<i>this is italic</i> where <b>this is bold</b>.
Comments can be reported by Users if for some reason it's thought they are inappropriate. 
They will remain public and visible until and if rejection is decided from an Administrator or Moderator User.
Administrators' and Moderators' Comments can also be reported, and will hence be moderated by others with the same application Role.

___
#### Views

Views are visits to Posts. A User's visit to a Post, regardless of how many times, accounts for a a single visit. 
It is a measurement reflective of the Post's popularity. Anonymous guest visits also account for it, based on unique IP addresses.

___
#### Messages

Users can contact each other from within the application using private messages which can be replied to, in a nested logic, and can be archived on either User's end, on their respective inbox.
Messages are identified either as 'new' or 'seen'.

___
#### Contacts

Guests and application Users may contact the application administration using Contact messages from the main home page, through the Contact link at the top.
Administrators are notified about those and can reply back through the application that will hence send an email using a configured email address
(the application uses third-party service Mailgun).
If a given email address under which a Contact message is sent, matches a User's email address within the application, it is identified as possibly being that User.

___
#### Notifications

Some actions throughout the application will trigger notifications for users, such as commenting on a user’s post, which will notify that post author that someone has commented or replied on their post, or a user that has commented before being notified that someone has replied to them.

### On an administrator level:

___
#### Roles

The application uses a system of roles to manage allowing and disallowing of user access to certain functionalities or areas in the application. 
The application is packed by default with some basic programmatically-required non-editable and unremovable roles.
A role can be seen as a passcard to certain areas or as a group of specific permissions, that is customisable, by editing which permissions are attached to it, thus. being changeable. 
One example is the Admin role that allows application administration on things like user management. Another example is the Moderator role, which does not have access to the whole application administration area, but will have power over moderating content by other users. (Not implemented yet.)

___
#### Permissions

Individual accesses to specific actions over specific data in the system, that can be grouped onto roles, by attaching them. (Not implemented yet.)

___
#### Jobs

On an administration level, there are a few cleanup jobs available that allow to do data cleanup, and executions of these jobs are registered with which Administrator ran them and when.
 
## Some features

#### Image fields:

Image fields on forms for users, posts, categories, etc.:
the image files submitted can be previewed, removed and changed, reflecting the respective preview, before submitting.

---
#### Security:

Role and Policy-based protection throughout the application views to validate showing or hiding forms and accesses, but also at a Controller action level, to prevent injection.

---
#### Action confirmation dialogues:

All major and more sensible actions have confirmation modals, such as deleting users and posts.

---
#### Post tags filling:

The application allows to fill in the form field, with pressing Enter key for separating tag from tag, and show them as label boxes with a remove button for each. Pressing Delete key while on form field without ongoing input removes the last one completely and can sequentially remove all others one by one pressing Delete key in sequence. 
Posts are limited to having ten tags, each.

---
#### Data deletion:

The application employs the usage of the soft-delete mechanism for data dismissal, for possibility of later retrieval.
Softdelete-based cascading is also implemented, since relational SQL cascading cannot work with soft-deletion implementation.
For example, deleting a User also cascades deletion to the user's posts, but none is actually ever deleted from the database.

---
#### Captcha:

The application uses a local Captcha package to generate captcha codes to validate certain actions such as creating Posts, Categories, and mostly the Contact form, on home which does not required login.


## Some business logic:

#### Home
The home of the application will display on top a regular navigation bar, with basic *Home* link, login and register, and a contact link.
The page will show a list of paginated posts shown ordered from the most recent, will allow to create a new post directly from home (which, if logged out or unregistered, will redirect to the authentication screen), will show a search bar below, for posts, and will show a list of categories by the right side, which if you click on any, will show you posts only about that topic.  
Each listed post is previewed summarised, with the main image of the post shown on focus, with an instant bookmark flag, a “see more” link to see the post entirely, and some metadata about it, such as the author, publication timestamp, and the category it is in.  
Pagination links are at the bottom.

---
#### Home: post
Viewing a post, all its public information will show also with the related tags beneath its title so that you are aware of its most relevant terms, which are used for search feature and to relate it to other similar topic posts, and suggest so.  
Right next to it there is an stats icon that, if clicked, will show statistics for that Post, such as Comments in it and unique visits from both Users and guests.  
The commenting area is shown lower beneath the post data itself, in a single-level thread format:
root comments shown aligned and all replies and replies to replies and so on, aligned in one single indentation line (imagine Facebook’s commenting view) so to prevent horizontal expansion for long threads of chained replies.  
Clicking a tag will list you all posts that share that topic.  
If the post does not have a category, you can suggest one to the author.  
Clicking the author’s name will show you the author’s profile in the application, if you are logged in. Same for comment author references.
Categories are shown to the right side, still, and also a listing of related posts suggestion, underneath it.

---
#### Home: search
The post search feature on home allows you to search for **everyone's** posts based on its title and tags, and supports the following operators:

* \+   results must include the term;
* \-  results must exclude the term;
* “”  group terms separated by spaces as a literal string, as if a one-word search term;
* %%  delimits word as literal full term (by default search terms <php> will return result “elephpant” too, using <%php%> will not match this result since the term is included within a word);
* \*  wildcard for search term, can mean anything: search for "jo*oe" should find both "john doe" and "joanna joe", for instance.

---
#### Dashboard: search
The search feature on the Dashboard allows a User to find their own content, like one's own Posts, Categories, Comments, and Admin Users can find other content they can manage like Users, too.
It supports the same operators as Home search features.

---
#### Dashboard
Upon logging in with a registered user account, you are immediately redirected to your user dashboard.  
The dashboard shows you bookmarked posts by default.  
On top navigation bar you have an expandable clickable avatar for your user, clicking on which gets you to your profile and logout features. 
To its left you have an expandable notification area where you can access you latest notifications:
New ones are highlighted in green with a clear icon to the right to clear them out, also with a “Clear All Notifications” and a “Show All Notifications” features show at the bottom. Clicking on “Show All Notifications” directs to a view with notifications listed down with the same functionalities but shows more far behind.  
A content search field is also at the top, same as home’s, but also finds Users, Categories, Comments, besides Posts.  
On the left side menu, you have list of content management areas, for your posts (or all posts, if you happen to be an Admin), categories, bookmarks and suggestions.  
If you happen to be an admin, you will also have access to management for users, permissions and roles, to charts that show statistical info on the application content, and to tools for cleanup.

---
#### Dashboard: posts
Here you can create posts, and view posts. 
Viewing posts will show posts contextually: if you are a regular user it shows yours, if you are an Admin user, there is a filter radio button on top that switches from showing you live posts, deleted posts only (for retrieval), all posts, or just your own. 
Management actions for posts are shown to the right.

---
#### Dashboard: categories
Creating a category and showing categories show all together in the same view. 
Management actions appear for your categories if you are a regular user, if you are an admin, they show for all. Categories may be enabled and disabled through these actions.

---
#### Dashboard: bookmarks
The view that shows by default, once you access the user dashboard. 
You can remove your bookmarks from this view.

---
#### Dashboard: suggestions
Given suggestions: shows suggestions of categories for posts given to other users. You can withdraw them with the delete button.  
Received suggestions: shows suggestions given by other users to your posts, for categories. You can accept or reject them. Suggesting users will be notified if you accept their suggestions, but not if you reject them.

---
#### Dashboard: users
As an Admin user, this area will show you the users in the application.
A filter radio buttons exists on top to either show you all active users (with account not deleted), deleted users only (for retrieval), or all users. 
Management actions for users are shown to the right.

---
#### Dashboard: charts
As an Admin user, this will show some statistics on the application content, such as the number of posts, comments, etc.

---
#### Dashboard: authorizations
As an Admin user, for Roles and Permissions, in each subarea you can respectively create a new instance of each and manage the existing ones beneath.  
Some Roles are application-default and cannot have their name changed nor can they be deleted, since they are used programatically in the application. 
They can however have their description changed and attached to or detached from Permissions.  
Permissions can be deactivated or reactivated.

---
#### Dashboard: Tools
As an Admin user, this will give you access to some jobs, such as *cleanup* tasks, for unused tags remnant in the system, leftover image files for no longer existing references in fields for posts or users (by means of replacement), for instance. 
It will also allow to merge several categories into one, thus assigning all related posts to the merged one, and choose which category icon remains.  
Latest job executions will be shown below the icons for the cleanup tasks.

---
#### Dashboard: Contact messages
As an Admin user, this will give you access to Contact messages to the application administration, either from logged in Users or guests of the application.
It shows both archived and pending Contacts (not the same as replied to: previously replied to Contact messages can be replied to again).

---
#### Dashboard: Moderation reviews
As an Admin or Moderator user, this will show the Posts to review and approve, and Comments reported by other Users that need moderation.
They can be either approved or rejected. Acting on a Post will notify its author on either decision. Acting on a reported Comment will notify the reporting User on either decision and if the Comment is rejected, the Comment author will also be notified of such.
Administrators' and Moderator's Comments that are reported are not notified or such as can only be moderated by other Administrators or Moderator Users, to prevent power abuse and bias.

---
#### Edit User profile
On dashboard, clicking “Profile” from top navigation user avatar, will show the user's profile, with their data and an Edit button beneath the avatar.  
Right on top, next to the section title, there is a stats icon that, if clicked, will show stats for the User, such as how many Comments and Posts it has submitted.  
The user’s posts will be listed underneath with a direct delete button.  
A delete button at the bottom allows for own account removal. Password confirmation and modal confirmation are subsequently required for this.  
On editing the user, a list of roles is shown indicating which ones the user has.