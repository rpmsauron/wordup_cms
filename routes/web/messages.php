<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['auth'])->group(
    function()
    {
        Route::get('/message/{message}', 'MessageController@show')
            ->name('message.show')
            ->middleware(['can:view,message']);

        Route::get('/messages/', 'MessageController@index')
            ->name('message.index');

        Route::get('/messages/create/{user}', 'MessageController@create')
            ->name('message.create')
            ->middleware("can:message,App\Message,user");

        Route::get('/messages/{message}/reply', 'MessageController@reply')
            ->name('message.reply')
            ->middleware(['can:reply,message']);

        Route::post('/messages/{parent?}', 'MessageController@store')
            ->name('message.store');

        Route::patch('/messages/{message}/archive', 'MessageController@archive')
            ->name('message.archive')
            ->middleware(['can:archive,message']);
    }
);
