<?php

Route::middleware('auth')->group(
    function()
    {
        Route::get('/categories', 'CategoryController@index')
            ->name('categories.index');

        Route::post('/categories', 'CategoryController@store')
            ->name('categories.store');

        Route::delete('/categories/{category}/destroy', 'CategoryController@destroy')
            ->name('categories.destroy');

        Route::get('/categories/{category}/edit', 'CategoryController@edit')
            ->name('categories.edit');

        Route::patch('/categories/{category}', 'CategoryController@update')
            ->name('categories.update');

        Route::patch('/categories/{category}/activate', 'CategoryController@activate')
            ->name('categories.activate');

        Route::patch('/categories/{category}/toggle', 'CategoryController@toggle')
            ->name('categories.toggle');
    }
);
