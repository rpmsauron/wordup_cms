<?php

use Illuminate\Support\Facades\Route;

Route::get('/post/{slug}', 'PostController@show')
    ->name('post.show');

Route::get('/posts/cat/{slug}', 'PostController@filter')
    ->name('post.filter');

Route::get('/posts/tag/{slug}', 'PostController@filterByTag')
    ->name('post.filter.tag');

Route::middleware(['auth'])->group(
    function()
    {
        Route::get('/posts/', 'PostController@index')
            ->name('post.index');

        Route::get('/posts/create', 'PostController@create')
            ->name('post.create');

        Route::post('/posts', 'PostController@store')
            ->name('post.store');

        Route::delete('/posts/{post}/destroy', 'PostController@destroy')
            ->name('post.destroy');

        Route::get('/posts/{post}/edit', 'PostController@edit')
            ->name('post.edit');

        Route::patch('/posts/{post}', 'PostController@update')
            ->name('post.update');

        Route::patch('/posts/{post}/bookmark', 'PostController@bookmark')
            ->name('post.bookmark');

        Route::patch('/posts/{post}/assign', 'PostController@assign')
            ->name('post.assign');

        Route::patch('/posts/{post_id}/restore', 'PostController@restore')
            ->name('post.restore')
            ->middleware(['can:restore,App\Post']);
    }
);

Route::get('/posts/listreview', 'PostController@listreview')
    ->name('posts.listreview')
    ->middleware(['can:manageEveryones,App\Post']);

Route::middleware(['can:review,post'])->group(
    function()
    {
        Route::get('/posts/{post}/review', 'PostController@review')
            ->name('post.review');

        Route::patch('/posts/{post}/accept', 'PostController@accept')
            ->name('post.accept');

        Route::patch('/posts/{post}/reject', 'PostController@reject')
            ->name('post.reject');
    }
);
