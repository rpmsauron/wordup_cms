<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')
    ->name('home');

Route::get('/post/search', 'PostController@search')
    ->name('post.search');

Route::middleware('auth')->group(
    function()
    {
        Route::get('/admin', 'AdminController@index')
            ->name('admin.index');

        Route::get('/admin/search', 'AdminController@search')
            ->name('admin.search');
    }
);

Route::get('/reload-captcha', 'CaptchaServiceController@reloadCaptcha')
    ->name('captcha.reload');