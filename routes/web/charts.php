<?php

use Illuminate\Support\Facades\Route;

Route::get('/post/charts/{post}', 'ChartsController@postCharts')
    ->name('post.charts');

Route::middleware('auth')->group(
    function()
    {
        Route::get('/admin/charts', 'ChartsController@charts')
            ->name('admin.charts')
            ->middleware(['role:admin']);

        Route::get('/admin/user/charts/{user?}', 'ChartsController@userCharts')
            ->name('admin.user.charts');
    }
);