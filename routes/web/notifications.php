<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['auth'])->group(
    function()
    {
        Route::patch('/notifications/{notification}', 'NotificationController@clear')
            ->name('notifications.clear');

        Route::patch('/notifications/', 'NotificationController@clearAll')
            ->name('notifications.clearall');

        Route::get('/notifications', 'NotificationController@index')
            ->name('notifications.index');
    }
);