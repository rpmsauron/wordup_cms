<?php

Route::middleware(['can:merge,App\Category'])->group(
    function()
    {
        // show cleanup view
        Route::get('/cleanup', 'AdminController@cleanup')
            ->name('admin.cleanup');

        // show mergeable categories
        Route::get('/cleanup/category/show', 'CategoryController@showMergeable')
            ->name('categories.show.mergeable');

        // merge categories
        Route::post('/cleanup/category/merge', 'CategoryController@merge')
            ->name('categories.merge');
    }
);

Route::middleware(['role:admin', 'auth'])->group(
    function()
    {
        // clean up tags
        Route::delete('/cleanup/tag', 'TagController@cleanupOrphanTags')
            ->name('tag.cleanup');

        // clean up user avatars
        Route::delete('/cleanup/user/avatars', 'AdminController@cleanupOrphanUserAvatars')
            ->name('user.avatar.cleanup');

        // clean up all user avatars
        Route::delete('/cleanup/user/avatars/all', 'AdminController@cleanupAllOrphanUserAvatars')
            ->name('user.avatar.cleanup.all');

        // clean up post images
        Route::delete('/cleanup/post/images', 'AdminController@cleanupOrphanPostImages')
            ->name('post.image.cleanup');

        // clean up all post images
        Route::delete('/cleanup/post/images/all', 'AdminController@cleanupAllOrphanPostImages')
            ->name('post.image.cleanup.all');

        // clean up category icons
        Route::delete('/cleanup/category/icons', 'AdminController@cleanupOrphanCategoryIcons')
            ->name('category.icon.cleanup');

        // clean up all category icons
        Route::delete('/cleanup/category/icons/all', 'AdminController@cleanupAllOrphanCategoryIcons')
            ->name('category.icon.cleanup.all');
    }
);