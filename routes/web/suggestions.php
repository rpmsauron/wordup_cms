<?php

Route::middleware('auth')->group(
    function()
    {
        Route::get('/suggestions/{post}/create', 'SuggestionController@create')
            ->name('suggestions.create');

        Route::post('/suggestions/{post}', 'SuggestionController@store')
            ->name('suggestions.store');

        Route::patch('/suggestions/{suggestion}/accept', 'SuggestionController@accept')
            ->name('suggestions.accept');

        Route::patch('/suggestions/{suggestion}/reject', 'SuggestionController@reject')
            ->name('suggestions.reject');

        Route::get('/suggestions/given', 'SuggestionController@given')
            ->name('suggestions.given');

        Route::get('/suggestions/received', 'SuggestionController@received')
            ->name('suggestions.received');

        Route::patch('/suggestions/{suggestion}/remove', 'SuggestionController@remove')
            ->name('suggestions.remove');
    }
);