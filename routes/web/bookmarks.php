<?php

Route::middleware('auth')->group(
    function()
    {
        Route::get('/admin/bookmarks', 'BookmarkController@index')
            ->name('admin.bookmarks.index');   
        }
);