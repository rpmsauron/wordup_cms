<?php

Route::get('/roles', 'RoleController@index')
    ->name('roles.index')
    ->middleware(['can:index,App\Role']);

Route::post('/roles', 'RoleController@store')
    ->name('roles.store')
    ->middleware(['can:store,App\Role']);

Route::delete('/roles/{role}/destroy', 'RoleController@destroy')
    ->name('roles.destroy')
    ->middleware(['can:delete,role']);

Route::get('/roles/{role}/edit', 'RoleController@edit')
    ->name('roles.edit')
    ->middleware(['can:edit,App\Role']);

Route::put('/roles/{role}', 'RoleController@update')
    ->name('roles.update')
    ->middleware(['can:update,App\Role']);

Route::put('/roles/{role}/attach', 'RoleController@attachPermission')
    ->name('role.permission.attach')
    ->middleware(['can:attachPermission,App\Role']);

Route::put('/roles/{role}/detach', 'RoleController@detachPermission')
    ->name('role.permission.detach')
    ->middleware(['can:detachPermission,App\Role']);

Route::put('/roles/{role}/detachpermission/all', 'RoleController@detachAllPermissions')
    ->name('role.permission.detachAll')
    ->middleware(['can:detachPermission,role']);