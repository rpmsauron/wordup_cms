<?php

use Illuminate\Support\Facades\Route;

Route::get('/contact/index/pending', 'ContactController@indexPending')
    ->name('contact.index.pending');

Route::get('/contact/index/archived', 'ContactController@indexArchived')
    ->name('contact.index.archived');

Route::get('/contact/create', 'ContactController@create')
    ->name('contact.create');

Route::post('contact/', 'ContactController@store')
    ->name('contact.store');

Route::middleware(["can:reply,contact"])->group(
    function()
    {
        Route::get('/contact/{contact}/showReply', 'ContactController@showReply')
            ->name('contact.showreply');

        Route::patch('contact/{contact}/reply', 'ContactController@reply')
            ->name('contact.reply');
    }
);