<?php

use Illuminate\Support\Facades\Route;

Route::middleware(['auth'])->group(
    function()
    {
        Route::post('/comments/{parent?}', 'CommentController@store')
            ->name('comment.store');

        Route::delete('/comments/{comment}/destroy', 'CommentController@destroy')
            ->name('comment.destroy');

        Route::get('/comments/{comment}/edit', 'CommentController@edit')
            ->name('comment.edit');

        Route::patch('/comments/{comment}', 'CommentController@update')
            ->name('comment.update');
    }
);

Route::get('/comment/listreview', 'CommentController@listreview')
    ->name('comments.listreview')
    ->middleware(['can:manageEveryones,App\Comment']);

Route::get('/comment/{comment}/review', 'CommentController@review')
    ->name('comment.review')
    ->middleware(['can:review,comment']);

Route::patch('/comment/{comment}/report', 'CommentController@report')
    ->name('comment.report')
    ->middleware(['can:report,comment']);

Route::patch('/comment/{comment}/accept', 'CommentController@accept')
    ->name('comment.accept')
    ->middleware(['can:accept,comment']);

Route::patch('/comment/{comment}/reject', 'CommentController@reject')
    ->name('comment.reject')
    ->middleware(['can:reject,comment']);