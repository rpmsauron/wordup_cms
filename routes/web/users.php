<?php

use Illuminate\Support\Facades\Route;

Auth::routes(['verify' => true]);

Route::middleware('auth')->group(
    function()
    {
        Route::get('/users', 'UserController@index')
            ->name('users.index');

        Route::get('/users/{user_id}/profile', 'UserController@show')
            ->name('user.profile.show');

        Route::delete('/users/{user}/destroy', 'UserController@destroy')
            ->name('user.destroy')
            ->middleware(['can:delete,user']);

        Route::get('/users/destroy', 'UserController@destroySelfPrompt')
            ->name('user.destroy.self.prompt')
            ->middleware(['verified', 'password.confirm']);

        Route::patch('/users/{user_id}/restore', 'UserController@restore')
            ->name('user.restore')
            ->middleware(['can:restore,App\User']);

        Route::get('/users/{user}/profile/edit', 'UserController@edit')
            ->name('user.profile.edit')
            ->middleware(['can:edit,user']);

        Route::patch('/users/{user}/update', 'UserController@update')
            ->name('user.profile.update')
            ->middleware(['can:update,user']);

        Route::put('/users/{user}/attachrole', 'UserController@attachRole')
            ->name('user.role.attach')
            ->middleware(['can:attachRole,user']);

        Route::put('/users/{user}/detachrole', 'UserController@detachRole')
            ->name('user.role.detach')
            ->middleware(['can:detachRole,user']);

        Route::put('/users/{user}/attachpermission', 'UserController@attachPermission')
            ->name('user.permission.attach')
            ->middleware(['can:attachPermission,user']);

        Route::put('/users/{user}/detachpermission', 'UserController@detachPermission')
            ->name('user.permission.detach')
            ->middleware(['can:detachPermission,user']);

        Route::put('/users/{user}/detachpermission/all', 'UserController@detachAllPermissions')
            ->name('user.permission.detachAll')
            ->middleware(['can:detachPermission,user']);
    }
);
