<?php

Route::get('/permissions', 'PermissionController@index')
    ->name('permissions.index')
    ->middleware(['can:index,App\Permission']);

Route::post('/permissions', 'PermissionController@store')
    ->name('permissions.store')
    ->middleware(['can:store,App\Permission']);

Route::delete('/permissions/{permission}/destroy', 'PermissionController@destroy')
    ->name('permissions.destroy')
    ->middleware(['can:delete,permission']);

Route::get('/permissions/{permission}/edit', 'PermissionController@edit')
    ->name('permissions.edit')
    ->middleware(['can:edit,App\Permission']);

Route::put('/permissions/{permission}', 'PermissionController@update')
    ->name('permissions.update')
    ->middleware(['can:update,App\Permission']);

Route::patch('/permissions/{permission}/toggle', 'PermissionController@toggle')
    ->name('permissions.toggle')
    ->middleware(['can:toggle,App\Permission']);

// AJAX repopulating of Permission 'action' dropdown.
Route::get('/permissions/{model}/actions', 'PermissionController@actions')
    ->name('permissions.actions');
