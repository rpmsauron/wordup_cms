<?php

return [
    'image' => [
        'user' => [
            'image_size'               => '300', // size of image on admin as uploaded, larger
            'avatar_size'              => '100', // size of image as shown on avatar placeholders through the application
            'avatar_comment_size'      => '30', // size of image as shown on avatar placeholders for root comments
            'avatar_notification_size' => '30', // size of image on dashboard Notifications preview
            'avatar_reply_size'        => '20' //size of image as shown on avatar placeholders for reply comments
        ],
        'post' => [
            'thumb_max_width'          => '200',
            'thumb_max_height'         => '100',
            'image_max_width'          => '730'
        ],
        'category' => [
            'icon_size'                => '40'
        ]
    ],

    'pagination' => [
        'admin_listing_entries'        => '10',
        'home_posts'                   => '20',
        'admin_listing_posts'          => '5',
        'comments_entries'             => '2',
        'admin_search_results'         => '10',
        'admin_notifications'          => '20',
        'admin_jobs_run'               => '50',
        'admin_listing_comments'       => '5',
        'messages'                     => '10'
    ],

    'fallback_messages' => [
        'post_without_author'          => '<Author not available>'
    ],

    // Do not change existing values: These values are used by migration '2020_06_19_141356_create_default_roles'.
    'roles' => [
        'role'                         => [
            'admin'                    => [
                'slug'                 => 'admin',
                'name'                 => 'Admin'
            ],
            'moderator'                => [
                'slug'                 => 'moderator',
                'name'                 => 'Moderator'
            ],
            'author'                   => [
                'slug'                 => 'author',
                'name'                 => 'Author'
            ],
        ],
        'default'                      => [
            'description'              => 'System default role. Cannot be deleted.'
        ]
    ],
    'permissions' => [
        'models'                       => [
            'system'                   => [
                'classname'            => 'system'
            ],
            'categories'               => [
                'classname'            => 'App\Category',
                'actions'              => [
                    'update', 'merge', 'delete', 'toggle'
                ]
            ],
            'comments'                 => [
                'classname'            => 'App\Comment',
                'actions'              => [
                    'update', 'delete', 'review'
                ]
            ],
            'contacts'                 => [
                'classname'            => 'App\Contact',
                'actions'              => [
                    'reply'
                ]
            ],
            'permissions'              => [
                'classname'            => 'App\Permission',
                'actions'              => [
                    'create', 'update', 'delete', 'toggle'
                ]
            ],
            'posts'                    => [
                'classname'            => 'App\Post',
                'actions'              => [
                    'update', 'delete', 'restore', 'assign category', 'review'
                ]
            ],
            'roles'                    => [
                'classname'            => 'App\Role',
                'actions'              => [
                    'create', 'update', 'delete', 'attach permission', 'detach permission'
                ]
            ],
            'users'                    => [
                'classname'            => 'App\User',
                'actions'              => [
                    'update', 'delete', 'restore', 'attach role', 'detach role', 'attach permission', 'detach permission'
                ]
            ]
        ],
    ],
    'messages' => [
        'error'                        => [
            'post_tags_limit'          => 'You can tag up to ten keywords per post.'
        ]
    ],
    'tags' => [
        'post_limit'                   => 10
    ],
    'limits' => [
        'home'                         => [
            'related'                  => 5
        ]
    ],
    'folder' => [
        'user'                         => [
            'avatar'                   => 'images',
        ],
        'post'                         => [
            'image_post'               => 'posts',
        ],
        'category'                     => [
            'icon'                     => 'categories',
        ],
    ],
    'suggestions' => [
        'post_user_max'                => 5
    ],
    'search' => [
        'operators'                    => [
            'include'                  => '+',
            'exclude'                  => '-',
            'delimiter'                => '%',
            'wildcard'                 => '*'
        ]
    ],
    'notifications' => [
        'max'                          => 8,
        'all_max'                      => 200,
        'verbs'                        => [
            'default'                  => 'on', // Default verb for unparseable Notification action

            'App\Post_create'          => 'created', // All admins and moderators except creator if neither of those, are notified
            'App\Post_update'          => 'updated',
            'App\Post_delete'          => 'deleted',
            'App\Post_undelete'        => 'restored',
            'App\Post_assign'          => 'assigned',
            'App\Post_bookmark'        => 'bookmarked',
            'App\Post_accept'          => 'accepted',
            'App\Post_reject'          => 'rejected',

            'App\Bookmark_delete'      => 'deleted', // unneeded

            'App\Category_update'      => 'updated',
            'App\Category_delete'      => 'deleted',
            'App\Category_assign'      => 'assigned', // Post is assigned to your Category
            'App\Category_enable'      => 'enabled',
            'App\Category_disable'     => 'disabled',
            'App\Category_merge'       => 'merged',

            /**
             * Comment's Post owner gets notified on a new Comment to their Post;
             * 'notifiable_id' is the related Post's id, while 'type' is 'Comment':
             */
            'App\Comment_create'       => 'commented on', // Comment's Post User is notified
            'App\Comment_update'       => 'updated',
            'App\Comment_delete'       => 'deleted',
            'App\Comment_undelete'     => 'restored', // Controller action / logic not implemented
            /**
             * 'notifiable_id' is the related Parent Comment's id:
             */
            'App\Comment_reply'        => 'replied to', // Parent Comment's User is notified
            'App\Comment_reply_on'     => 'replied on', // Parent Comment's Post User is notified
            'App\Comment_report'       => 'reported',
            'App\Comment_accept'       => 'accepted',
            'App\Comment_reject'       => 'rejected',
            

            'App\Permission_create'    => 'created', // All admins except creator are notified
            'App\Permission_update'    => 'updated', // All admins except editor are notified
            'App\Permission_delete'    => 'deleted', // All admins except remover are notified
            'App\Permission_enable'    => 'enabled', // All admins except toggler are notified
            'App\Permission_disable'   => 'disabled', // All admins except toggler are notified

            'App\Role_create'          => 'created', // All admins except creator are notified
            'App\Role_update'          => 'updated', // All admins except editor are notified
            'App\Role_delete'          => 'deleted', // All admins except remover are notified
            'App\Role_attach'          => 'attached', // All admins except editor are notified
            'App\Role_detach'          => 'detached', // All admins except editor are notified

            'App\Suggestion_create'    => 'suggested', 
            'App\Suggestion_remove'    => 'removed', // unneeded
            'App\Suggestion_accept'    => 'accepted', 
            'App\Suggestion_reject'    => 'rejected', // unneeded

            'App\Contact_create'       => 'contacted', // unneeded

            'Cleanup_run'              => 'cleaned up', // All admins are notified

            'App\User_update'          => 'updated',
            /**
             * Needed only to retrieve the removing User of another User, because User is not 
             * notified of its own Account deletion via a Notification, but rather on login attempt.
             */
            'App\User_delete'          => 'deleted',
            'App\User_undelete'        => 'restored',
            'App\User_attach_role'     => 'granted',
            'App\User_detach_role'     => 'revoked',
            'App\User_attach_permission' => 'granted',
            'App\User_detach_permission' => 'revoked'
         ]
    ],
    'messages' => [
        'max'                          => 8,
        'reply_to_prefix'              => 'Re: '
    ],
    'jobs' => [                         
        'clear_tags'                   => [
            'name'                     => 'clear_tags',
            'display_text'             => 'cleared orphaned tags'
        ],
        'clear_user_avatars'           => [
            'name'                     => 'clear_user_avatars',
            'display_text'             => 'cleared orphaned User avatars'
        ],
        'clear_all_user_avatars'       => [
            'name'                     => 'clear_all_user_avatars',
            'display_text'             => 'cleared all orphaned User avatars'
        ],
        'clear_post_images'            => [
            'name'                     => 'clear_post_images',
            'display_text'             => 'cleared orphaned Post images'
        ],
        'clear_all_post_images'        => [
            'name'                     => 'clear_all_post_images',
            'display_text'             => 'cleared all orphaned Post images'
        ],
        'clear_category_icons'         => [
            'name'                     => 'clear_category_icons',
            'display_text'             => 'cleared orphaned Category icons'
        ],
        'clear_all_category_icons'     => [
            'name'                     => 'clear_all_category_icons',
            'display_text'             => 'cleared all orphaned Category icons'
        ],
        'merge_categories'             => [
            'name'                     => 'merge_categories',
            'display_text'             => 'merged Categories'
        ]
    ],
    'captcha' => [
        'label'                        => 'Show you are human'
    ]
];
